$(document).ready(function () {

  $('.count').each(function () {
    
  })





  //tien-ich tabs
  $('#tabs-nav li:first-child').addClass('active');
  $('.tab-content').hide();
  $('.tab-content:first').show();
  $('#tabs-nav li').click(function () {
    $('#tabs-nav li').removeClass('active');
    $(this).addClass('active');
    $('.tab-content').hide();
    const activeTab = $(this).find('a').attr('href');
    $(activeTab).fadeIn(1100);
    $('.slide-tienich,.thumb-tienich').slick("setPosition", 0);
    return false;
  });

  //gallery tabs 
  $('#control-tabs-gallery li:first-child').addClass('active');
  $('.tab-gallery-content').hide();
  $('.tab-gallery-content:first').show();
  $('#control-tabs-gallery li').click(function () {
    $('#control-tabs-gallery li').removeClass('active');
    $(this).addClass('active');
    $('.tab-gallery-content').hide();
    const activeTabGallery = $(this).find('a').attr('href');
    $(activeTabGallery).fadeIn(1100);
    $('.slide-gallery,.thumb-gallery').slick("setPosition", 0);
    return false;
  });

  $('#control-tabs-mb li:first-child').addClass('active');
  $('.tab-mb-content').hide();
  $('.tab-mb-content:first').show();
  $('#control-tabs-mb li').click(function () {
    $('#control-tabs-mb li').removeClass('active');
    $(this).addClass('active');
    $('.tab-mb-content').hide();
    const activeTabGallery = $(this).find('a').attr('href');
    $(activeTabGallery).fadeIn(1100);
    $('.slide-mb,.thumb-mb').slick("setPosition", 0);
    return false;
  });

  //mat bang tong the tabs
  $('#overall-ground-tabs-nav li:first-child').addClass('active');
  $('.overall-ground-tab-content').hide();
  $('.overall-ground-tab-content:first').show();
  $('#overall-ground-tabs-nav li img').css("visibility", "collapse");
  $('#overall-ground-tabs-nav li img:first').css("visibility", "visible");
  $('#overall-ground-tabs-nav li').click(function () {
    $('#overall-ground-tabs-nav li .overall-ground-tabs-nav-image-dot').css("visibility", "collapse");
    $('#overall-ground-tabs-nav li').removeClass('active');
    $(this).addClass('active');
    $(this).find('img').css("visibility", "visible");
    $('.overall-ground-tab-content').hide();
    const activeTab = $(this).find('a').attr('href');
    $(activeTab).fadeIn(1100);
    return false;
  });



  var numSlick = 0;
  $('.slide-tienich').each(function () {
    numSlick++;
    $(this).addClass('slider-' + numSlick).not('.slick-initialized').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      prevArrow: '<a href="" title="" class="prev-big"><img src="./assets/clients/imgs/tienich/pre1.png"></a>',
      nextArrow: '<a href="" title="" class="next-big"><img src="./assets/clients/imgs/tienich/next1.png"></a>',
      speed: 1000,
      
      dots: false,
      arrows: true,
      asNavFor: '.thumb-tienich.slider-' + numSlick
    });
  });

  numSlick = 0;
  $('.thumb-tienich').each(function () {
    numSlick++;
    $(this).addClass('slider-' + numSlick).not('.slick-initialized').slick({
      prevArrow: '<a href="" title="" class="prev-thumb"><img src="./assets/clients/imgs/tienich/pre2.png"></a>',
      nextArrow: '<a href="" title="" class="next-thumb"><img src="./assets/clients/imgs/tienich/next2.png"></a>',
      speed: 1000,
      vertical: false,
      slidesToShow: 6,
      slidesToScroll: 1,
      asNavFor: '.slide-tienich.slider-' + numSlick,
      arrows: true,
      dots: false,
      focusOnSelect: true,
      responsive: [{
        breakpoint: 480,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
          arrows: false,
        }
      }]
    });
  });



  var numSlickGallery = 0;
  $('.slide-gallery').each(function () {
    numSlickGallery++;
    $(this).addClass('slider-' + numSlickGallery).not('.slick-initialized').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      prevArrow: '<a href="" title="" class="prev-big"><img src="./assets/clients/imgs/tienich/pre1.png"></a>',
      nextArrow: '<a href="" title="" class="next-big"><img src="./assets/clients/imgs/tienich/next1.png"></a>',
      speed: 1000,
      dots: false,
      arrows: true,
      asNavFor: '.thumb-gallery.slider-' + numSlickGallery
    });
  });

  numSlickGallery = 0;
  $('.thumb-gallery').each(function () {
    numSlickGallery++;
    $(this).addClass('slider-' + numSlickGallery).not('.slick-initialized').slick({
      prevArrow: '<a href="" title="" class="prev-thumb"><img src="./assets/clients/imgs/tienich/pre2.png"></a>',
      nextArrow: '<a href="" title="" class="next-thumb"><img src="./assets/clients/imgs/tienich/next2.png"></a>',
      speed: 1000,
      vertical: false,
      slidesToShow: 5,
      slidesToScroll: 1,
      asNavFor: '.slide-gallery.slider-' + numSlickGallery,
      arrows: true,
      dots: false,
      focusOnSelect: true,
      responsive: [{
        breakpoint: 480,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
          arrows: false,
        }
      }]
    });
  });


  var numSlickMB = 0;
  $('.slide-mb').each(function () {
    numSlickMB++;
    $(this).addClass('slider-' + numSlickMB).not('.slick-initialized').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      prevArrow: '<a href="" title="" class="prev-big"><img src="./assets/clients/imgs/tienich/pre1.png"></a>',
      nextArrow: '<a href="" title="" class="next-big"><img src="./assets/clients/imgs/tienich/next1.png"></a>',
      speed: 1000,
      dots: false,
      arrows: true,
      asNavFor: '.thumb-mb.slider-' + numSlickMB
    });
  });

  numSlickMB = 0;
  $('.thumb-mb').each(function () {
    numSlickMB++;
    $(this).addClass('slider-' + numSlickMB).not('.slick-initialized').slick({
      prevArrow: '<a href="" title="" class="prev-thumb"><img src="./assets/clients/imgs/tienich/pre2.png"></a>',
      nextArrow: '<a href="" title="" class="next-thumb"><img src="./assets/clients/imgs/tienich/next2.png"></a>',
      speed: 1000,
      vertical: false,
      slidesToShow: 5,
      slidesToScroll: 1,
      asNavFor: '.slide-mb.slider-' + numSlickMB,
      arrows: true,
      dots: false,
      focusOnSelect: true,
      responsive: [{
        breakpoint: 480,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
          arrows: false,
        }
      }]
    });
  });








  //slide service
  $('.slide-service').not('.slick-initialized').slick({
    prevArrow: '<a href="" title="" class="prev-service"><img src="./assets/clients/imgs/tienich/pre1.png"></a>',
    nextArrow: '<a href="" title="" class="next-service"><img src="./assets/clients/imgs/tienich/next1.png"></a>',
    speed: 1000,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
  });







  //tien-ich tabs
  $('.carousel').not('.slick-initialized').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    mobileFirst: true,
    arrows: false,
    dots: true,
    responsive: [{
      breakpoint: 480,
      settings: 'unslick',
    }, ]
  });



  $('.slide-project').not('.slick-initialized').slick({
    prevArrow: '<a href="" title="" class="prev-service"><img src="./assets/clients/imgs/tiendoduan/pre.png"></a>',
    nextArrow: '<a href="" title="" class="next-service"><img src="./assets/clients/imgs/tiendoduan/next.png"></a>',
    speed: 1000,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    responsive: [{
      breakpoint: 480,
      settings: {
        arrows: false,
      }
    }]
  });



  $(window).on('resize', function () {
    $('.carousel').slick('resize');
  });
  //end tien do du an

  var tl = new TimelineMax();
  tl.staggerFromTo('.wave4', 2, {
    scale: 1
  }, {
    scaleY: 1.4,
    repeat: -1,
    yoyo: true,
  }, 0.5)
  tl.staggerFromTo('.wave1', 1.5, {
    scale: 2
  }, {
    scaleY: 1.4,
    repeat: -1,
    yoyo: true,
  }, 0.5)
  tl.staggerFromTo('.wave3', 1.8, {
    scale: 1
  }, {
    scaleY: 1.4,
    repeat: -1,
    yoyo: true,
  }, 0.5)
  tl.staggerFromTo('.wave2', 1, {
    scale: 1
  }, {
    scaleY: 1.4,
    repeat: -1,
    yoyo: true,
  }, 0.4)


  function MoveBackground() {
    function e() {
      gsap.to(".moving", 1, {
        x: 0,
        y: 0,
        ease: Power2.easeOut
      }), gsap.to(".moving  img", 1, {
        x: 0,
        y: 0,
        ease: Power2.easeOut
      })
    }

    function t() {
      DX = o.X - l, DY = o.Y - i, MoveX = DY / i, MoveY = -(DX / l), Radius = Math.sqrt(Math.pow(MoveX, 2) + Math.pow(MoveY, 2)), Degree = 2 * Radius, TweenLite.to(".moving", 1, {
        x: 30 * MoveX,
        y: 30 * MoveY,
        ease: Power2.easeOut
      }), TweenLite.to(".moving img", 1, {
        x: 60 * MoveX,
        y: 60 * MoveY,
        ease: Power2.easeOut
      })
    }

    var a = null,
      o = {
        X: 0,
        Y: 0
      },
      l = $(window).width() / 2,
      i = $(window).height() / 2;
    $(".right-tropical-paradise").addClass("moving"), $('.header, .nav-right').on("mouseenter", function () {
      cancelAnimationFrame(a), e()
    }), $(window).width() > 1100 ? $(".tropical-paradise").on("mousemove", function (e) {
      o.X = e.pageX, o.Y = e.pageY, cancelAnimationFrame(a), a = requestAnimationFrame(t)
    }) : $(".tropical-paradise").on("mousemove", function () {
      cancelAnimationFrame(a), e()
    }), $(window).resize(function () {
      $(window).width() > 1100 ? (l = $(window).width() / 2, i = $(window).height() / 2) : e()
    })
  }
  MoveBackground();


  $('.map').maphilight({
    fill: !0,
    fillColor: "000000",
    fillOpacity: .3,
    stroke: !0,
    strokeColor: "ffffff",
    strokeOpacity: 1,
    strokeWidth: 4,
    fade: !0,
    alwaysOn: !1,
    neverOn: !1,
    groupBy: !1,
    wrapClass: !1,
    shadow: !0,
    shadowX: 5,
    shadowY: 5,
    shadowRadius: 10,
    shadowColor: "000000",
    shadowOpacity: .8,
    shadowPosition: "outside",
    shadowFrom: !1
  });

  /// map hover
  $('.text-map-matbang .group-text-map:first').show();
  $('.fancybox-map').hover(function () {
    var tab_id = $(this).attr('data-tab');
    $('.text-map-matbang .group-text-map').removeClass('current');
    $("#" + tab_id).addClass('current');
  })
  $(".fancybox-map").fancybox({
    animationEffect: 'zoom-in-out',
    transitionEffect: 'slide'
  });

  $(".fancybox-map-ipad-pro").fancybox({
    animationEffect: 'zoom-in-out',
    transitionEffect: 'slide'
  });

  $(".fancybox-map-ipad-mini").fancybox({
    animationEffect: 'zoom-in-out',
  });

  $('.fancybox-map-ipad-mini').hover(function () {
    var tab_id = $(this).attr('data-tab');
    $('.text-map-matbang .group-text-map').removeClass('current');
    $("#" + tab_id).addClass('current');
  })

  $('.fancybox-map-ipad-pro').hover(function () {
    var tab_id = $(this).attr('data-tab');
    $('.text-map-matbang .group-text-map').removeClass('current');
    $("#" + tab_id).addClass('current');
  })
  


  //tintuc mobile
  $('.flex-mobile-row').not('.slick-initialized').slick({
    prevArrow: '<a href="" title="" class="prev-service"><img src="./assets/clients/imgs/tiendoduan/pre.png"></a>',
    nextArrow: '<a href="" title="" class="next-service"><img src="./assets/clients/imgs/tiendoduan/next.png"></a>',
    speed: 1000,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    focusOnSelect: true,
    responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          arrows: false,
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          dots: true,
          arrows: false,
        }
      }
    ]
  });

  $('.thumb-project-outline').not('.slick-initialized').slick({
    prevArrow: '<a href="" title="" id="thum-project-lick-pre" class="prev-thumb"><img src="./assets/clients/imgs/tiendoduan/pre.png"></a>',
    nextArrow: '<a href="" title="" id="thum-project-lick-next" class="next-thumb"><img src="./assets/clients/imgs/tiendoduan/next.png"></a>',
    speed: 500,
    dots: false,
    focusOnSelect: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    rows: 1,
    fade: true,
    cssEase: 'linear',
    swipe: true,
    swipeToSlide: true,
    infinite: true,
    centerPadding: '10px',
    arrows: true,
    responsive: [{
        breakpoint: 768,
        settings: {
          arrows: true,
          centerMode: true,
          centerPadding: '10px',
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 700,
        settings: {
          arrows: true,
          centerMode: true,
          centerPadding: '10px',
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 540,
        settings: {
          arrows: true,
          centerMode: true,
          centerPadding: '10px',
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: true,
          centerMode: true,
          centerPadding: '10px',
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 440,
        settings: {
          arrows: true,
          centerMode: true,
          centerPadding: '10px',
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 380,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '10px',
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 320,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '10px',
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      }
    ]
  });

  $(".fancybox-thumb").fancybox({
    animationEffect: 'zoom-in-out',
    transitionEffect: 'slide'
  });
  $('.fancybox-thumb-canho').fancybox({
    animationEffect: 'zoom-in-out',
    transitionEffect: 'slide',
  });

  $('.fancybox-thumb-tien-ich').fancybox({
    animationEffect: 'zoom-in-out',
    transitionEffect: 'slide',
  });

  $('.fancybox-thumb-tien-ich-hot').fancybox({
    animationEffect: 'zoom-in-out',
    transitionEffect: 'slide',
  });
  
  $('.fancybox-thumb-tien-ich-hot').fancybox({
    beforeShow : function(){
     this.title =  this.title + " - " + $(this.element).data("caption");
    }
   });

  $.fancybox.defaults.hash = false;

  

  $('.num1').click(function () {
    $('.number-1').addClass('active-map');
    $('.number-2').removeClass('active-map');
    $('.number-3').removeClass('active-map');
  })
  $('.num2').click(function () {
    $('.number-1').removeClass('active-map');
    $('.number-2').addClass('active-map');
    $('.number-3').removeClass('active-map');
  })
  $('.num3').click(function () {
    $('.number-1').removeClass('active-map');
    $('.number-2').removeClass('active-map');
    $('.number-3').addClass('active-map');
  })

  // var position = $(window).scrollTop(); 
  // console.log(position);
  // var $width = $(window).innerWidth();
  // if($width <= 1024){
  //   $(window).scroll(function() {
  //     var scroll = $(window).scrollTop();
  //     console.log(scroll);
  //     if(scroll > position) {
  //         $('.header, .button-nav, .text-slogan-page').addClass('active-header');
  //     }else{
  //         $('.header,.button-nav ,.text-slogan-page').removeClass('active-header');
  //     }
  //     position = scroll;
  //   });
  // }

  // Hide Header on on scroll down
  var didScroll;
  var lastScrollTop = 0;
  var delta = 5;
  var navbarHeight = $('header').outerHeight();
  var $width = $(window).innerWidth();
  if ($width <= 1024) {
    $(window).scroll(function (event) {
      didScroll = true;
    });

    setInterval(function () {
      if (didScroll) {
        hasScrolled();
        didScroll = false;
      }
    }, 250);

    function hasScrolled() {
      var st = $(this).scrollTop();
  
      // Make sure they scroll more than delta
      if (Math.abs(lastScrollTop - st) <= delta)
        return;
  
      // If they scrolled down and are past the navbar, add class .nav-up.
      // This is necessary so you never see what is "behind" the navbar.
      if (st > lastScrollTop && st > navbarHeight) {
        // Scroll Down
        $('.header, .button-nav, .text-slogan-page').addClass('active-header');
      } else {
        // Scroll Up
        if (st + $(window).height() < $(document).height()) {
          $('.header,.button-nav ,.text-slogan-page').removeClass('active-header');
        }
      }
  
      lastScrollTop = st;
    }
  }



  var numSlickTI = 0;
  $('.slide-ti').each(function () {
    numSlickTI++;
    $(this).addClass('slider-' + numSlickTI).not('.slick-initialized').slick({
      prevArrow: '<a href="" title="" class="prev-big"><img src="./assets/clients/imgs/tienich/pre1.png"></a>',
      nextArrow: '<a href="" title="" class="next-big"><img src="./assets/clients/imgs/tienich/next1.png"></a>',
      speed: 1000,
      dots: true,
      arrows: true,
      
      asNavFor: '.thumb-ti.slider-' + numSlickTI,
      centerMode: true,
      slidesToShow: 3,
      infinite: true,
      responsive: [
        {
          breakpoint: 480,
          settings: {
            centerMode: false,
            infinite: true,
            speed: 300,
            initialSlide: 0,
            slidesToShow: 1
          }
        }
      ]
    });
  });

  numSlickTI = 0;
  $('.thumb-ti').each(function () {
    numSlickTI++;
    $(this).addClass('slider-' + numSlickTI).not('.slick-initialized').slick({
      infinite: true,
      speed: 1000,
      slidesToShow: 1,
      arrows: false,
      adaptiveHeight: true
    });
  });


  $(window).on('resize', function () {
    $('.carousel').slick('resize');
  });

  $(".slick-ti").css("max-width", $(window).width());


  $(window).on('resize', function () {
    setScrollNews();
  });
  setScrollNews();

  function setScrollNews() {
    var $width = $(window).innerWidth();
    if ($width > 1024) {
      $("#scrollnews").niceScroll({
        cursorwidth: '20px',
        autohidemode: false ,
        cursorcolor: '#d0b179',
        cursorborderradius: '20px',
        cursorfixedheight: 88,
        enabletranslate3d: true,
      });
  
      var valueScroll = "";
      function scrollAnimate() {
          $("#scrollnews").animate({
              scrollLeft: valueScroll
          }, 100, "linear", function() {
              "" != valueScroll && scrollAnimate()
          })
      }
    
      $(".scroll-right").hover(function() {
        valueScroll = "+=30",
          scrollAnimate()
      }, function() {
        valueScroll = ""
      }),
      $(".scroll-left").hover(function() {
        valueScroll = "-=30",
          scrollAnimate()
      }, function() {
        valueScroll = ""
      });
    } else {
      $("#scrollnews").getNiceScroll().remove();
    }
  }
  
})