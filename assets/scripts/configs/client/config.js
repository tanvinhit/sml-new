angular.module('vpApp').config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider){
    $routeProvider.when('/', {
        templateUrl: "Clientcontroller/loadView/home"
    })
    .when('/', {
        templateUrl: 'Clientcontroller/loadView/home'
    })
    .when('/gioi-thieu', {
        templateUrl: 'Clientcontroller/loadView/gioi-thieu'
    })
    .when('/vi-tri', {
        templateUrl: 'Clientcontroller/loadView/vi-tri'
    })
    .when('/tien-ich', {
        templateUrl: 'Clientcontroller/loadView/tien-ich'
    })
    .when('/mat-bang', {
        templateUrl: 'Clientcontroller/loadView/mat-bang/matbang'
    })
    .when('/mat-bang/:url', {
        templateUrl: 'Clientcontroller/loadView/mat-bang/matbang'
    })
    .when('/thu-vien', {
        templateUrl: 'Clientcontroller/loadView/thu-vien'
    })
    .when('/tin-tuc', {
        templateUrl: 'Clientcontroller/loadView/tin-tuc/tintuc'
    })
    .when('/tin-tuc/:url', {
        templateUrl: 'Clientcontroller/loadView/tin-tuc/tintuc'
    })
    .when('/lien-he', {
        templateUrl: 'Clientcontroller/loadView/lien-he'
    })
    .otherwise({
        redirectTo: '/'
    });    
    $locationProvider.html5Mode(true);
}]);   