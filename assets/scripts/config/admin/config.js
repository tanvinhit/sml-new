angular.module('vpApp').config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider){
    $routeProvider.when('/smlhlp', {
        templateUrl: 'Admincontroller/loadView/dashboard'
    })
    .when('/smlhlp/dashboard', {
        templateUrl: 'Admincontroller/loadView/dashboard'
    })
    .when('/smlhlp/languages', {
        templateUrl: 'Admincontroller/loadView/languages'
    })
    .when('/smlhlp/users', {
        templateUrl: 'Admincontroller/loadView/users'
    })
    .when('/smlhlp/about', {
        templateUrl: 'Admincontroller/loadView/about'
    })
    .when('/smlhlp/contact', {
        templateUrl: 'Admincontroller/loadView/contact'
    })
    .when('/smlhlp/vi-tri', {
        templateUrl: 'Admincontroller/loadView/position/position'
    })
    .when('/smlhlp/tong-quan-vi-tri', {
        templateUrl: 'Admincontroller/loadView/tong-quan/position'
    })
    .when('/smlhlp/chi-tiet-vi-tri', {
        templateUrl: 'Admincontroller/loadView/chi-tiet/position'
    })
    .when('/smlhlp/partner', {
        templateUrl: 'Admincontroller/loadView/partner/contact'
    })
    .when('/smlhlp/tai-lieu', {
        templateUrl: 'Admincontroller/loadView/tailieu'
    })
    .when('/smlhlp/content', {
        templateUrl: 'Admincontroller/loadView/content'
    })
    .when('/smlhlp/connect', {
        templateUrl: 'Admincontroller/loadView/connect'
    })
    .when('/smlhlp/templates', {
        templateUrl: 'Admincontroller/loadView/templates'
    })
    .when('/smlhlp/categories', {
        templateUrl: 'Admincontroller/loadView/categories/category'
    })
    .when('/smlhlp/productCategories', {
        templateUrl: 'Admincontroller/loadView/categories/category'
    })
    .when('/smlhlp/article', {
        templateUrl: 'Admincontroller/loadView/articles/article'
    })
    .when('/smlhlp/widgets', {
        templateUrl: 'Admincontroller/loadView/widgets'
    })
    .when('/smlhlp/menu', {
        templateUrl: 'Admincontroller/loadView/navigates'
    })  
    .when('/smlhlp/slide', {
        templateUrl: 'Admincontroller/loadView/cateSlides'
    }) 
    .when('/smlhlp/image', {
        templateUrl: 'Admincontroller/loadView/image'
    }) 
    .when('/smlhlp/slide/:cateId', {
        templateUrl: 'Admincontroller/loadView/slide'
    }) 
    .when('/smlhlp/product', {
        templateUrl: 'Admincontroller/loadView/products/product'
    })  
    .when('/smlhlp/orders', {
        templateUrl: 'Admincontroller/loadView/orders'
    })        
    .when('/smlhlp/customer', {
        templateUrl: 'Admincontroller/loadView/customers'
    })
    .when('/smlhlp/postman', {
        templateUrl: 'Admincontroller/loadView/postmans'
    })     
    .when('/smlhlp/service', {
        templateUrl: 'Admincontroller/loadView/services'
    })
    .when('/smlhlp/address', {
        templateUrl: 'Admincontroller/loadView/map'
    })
    .when('/smlhlp/resources', {
        templateUrl: 'Admincontroller/loadView/resourceManage'
    })
    .when('/smlhlp/contactmail', {
        templateUrl: 'Admincontroller/loadView/mailcontact'
    })    
    .when('/smlhlp/article/add', {
        templateUrl: 'Admincontroller/loadView/editArticle/article'
    })
    .when('/smlhlp/product/add', {
        templateUrl: 'Admincontroller/loadView/editProduct/product'
    })    
    .when('/smlhlp/category/add', {
        templateUrl: 'Admincontroller/loadView/editCategory/category'
    })
    .when('/smlhlp/productCategory/add', {
        templateUrl: 'Admincontroller/loadView/editCategory/category'
    })
    .when('/smlhlp/article/edit/:id', {
        templateUrl: 'Admincontroller/loadView/editArticle/article'
    })
    .when('/smlhlp/product/edit/:id', {
        templateUrl: 'Admincontroller/loadView/editProduct/product'
    })
    .when('/smlhlp/category/edit/:id', {
        templateUrl: 'Admincontroller/loadView/editCategory/category'
    })
    .when('/smlhlp/productCategory/edit/:id', {
        templateUrl: 'Admincontroller/loadView/editCategory/category'
    })
    .when('/smlhlp/bills', {
        templateUrl: 'Admincontroller/loadView/bills/bill'
    })
    .when('/smlhlp/bills/add', {
        templateUrl: 'Admincontroller/loadView/billDetail/bill'
    }) 
    .when('/smlhlp/billDetail/:id', {
        templateUrl: 'Admincontroller/loadView/billDetail/bill'
    }) 
    .when('/smlhlp/plus-service', {
        templateUrl: 'Admincontroller/loadView/plusService'
    })
    .when('/smlhlp/prices', {
        templateUrl: 'Admincontroller/loadView/postalCharges/price'
    }) 
    .when('/smlhlp/packages', {
        templateUrl: 'Admincontroller/loadView/packages/package'
    }) 
    .when('/smlhlp/tapping-to/packages', {
        templateUrl: 'Admincontroller/loadView/packageTappingTo/package',
        controller: 'packageTappingToCtrl'
    })
    .when('/smlhlp/package/add', {
        templateUrl: 'Admincontroller/loadView/packageDetail/package'
    }) 
    .when('/smlhlp/package/detail/:id', {
        templateUrl: 'Admincontroller/loadView/packageDetail/package'
    }) 
    .when('/smlhlp/tapping-to/package/detail/:id', {
        templateUrl: 'Admincontroller/loadView/packageTappingToDetail/package',
        controller: 'packageTappingToDetailCtrl'
    }) 
    .when('/smlhlp/shipment', {
        templateUrl: 'Admincontroller/loadView/shipments/shipment',
        controller: 'shipmentCtrl'
    }) 
    .when('/smlhlp/tapping-to/shipment', {
        templateUrl: 'Admincontroller/loadView/shipmentTappingTo/shipment',
        controller: 'shipmentTappingToCtrl'
    }) 
    .when('/smlhlp/shipment/add', {
        templateUrl: 'Admincontroller/loadView/shipmentDetail/shipment',
        controller: 'shipmentDetailCtrl'
    }) 
    .when('/smlhlp/shipment/detail/:id', {
        templateUrl: 'Admincontroller/loadView/shipmentDetail/shipment',
        controller: 'shipmentDetailCtrl'
    }) 
    .when('/smlhlp/tapping-to/shipment/detail/:id', {
        templateUrl: 'Admincontroller/loadView/shipmentDetailTappingTo/shipment',
        controller: 'shipmentTappingToDetailCtrl'
    }) 
    .when('/smlhlp/assignments', {
        templateUrl: 'Admincontroller/loadView/assignments/assign'
    })
    .when('/smlhlp/approve-assign', {
        templateUrl: 'Admincontroller/loadView/approve/assign'
    })
    .when('/smlhlp/delivery-assign', {
        templateUrl: 'Admincontroller/loadView/delivery/assign'
    })
    .when('/smlhlp/list-assign', {
        templateUrl: 'Admincontroller/loadView/list/assign'
    })
    .when('/smlhlp/mat-bang', {
        templateUrl: 'Admincontroller/loadView/mat-bang/matbang'
    })
    .when('/smlhlp/loai-mat-bang', {
        templateUrl: 'Admincontroller/loadView/loai-mat-bang/matbang'
    })
    .when('/smlhlp/chi-tiet-mat-bang', {
        templateUrl: 'Admincontroller/loadView/chi-tiet-mat-bang/matbang'
    })
    .when('/smlhlp/chi-tiet-mat-bang/add', {
        templateUrl: 'Admincontroller/loadView/edit-chi-tiet-mat-bang/matbang'
    })
    .when('/smlhlp/chi-tiet-mat-bang/edit/:id', {
        templateUrl: 'Admincontroller/loadView/edit-chi-tiet-mat-bang/matbang'
    })
    .when('/smlhlp/add-script', {
        templateUrl: 'Admincontroller/loadView/script'
    })
    .when('/smlhlp/file-du-an', {
        templateUrl: 'Admincontroller/loadView/file'
    })
    .when('/smlhlp/tien-do', {
        templateUrl: 'Admincontroller/loadView/td-cateSlides/library'
    })
    .when('/smlhlp/tien-do/:cateId', {
        templateUrl: 'Admincontroller/loadView/td-slide/library'
    })
    .when('/smlhlp/process-tq', {
        templateUrl: 'Admincontroller/loadView/processtq/process'
    })
    .when('/smlhlp/contact-tq', {
        templateUrl: 'Admincontroller/loadView/contacttq/contact'
    })
    .when('/smlhlp/article-tq', {
        templateUrl: 'Admincontroller/loadView/articletq/article'
    })
    .when('/smlhlp/noi-dung-loi-ich', {
        templateUrl: 'Admincontroller/loadView/tong-quan/benefit'
    })
    .when('/smlhlp/danh-sach-loi-ich', {
        templateUrl: 'Admincontroller/loadView/list/benefit'
    })
    .when('/smlhlp/utilities-tq', {
        templateUrl: 'Admincontroller/loadView/utilities_tq/utilities'
    })
    .when('/smlhlp/tien-ich', {
        templateUrl: 'Admincontroller/loadView/tienich-cateSlides/library'
    })
    .when('/smlhlp/tien-ich/:cateId', {
        templateUrl: 'Admincontroller/loadView/tienich-slide/library'
    })
    .when('/smlhlp/home', {
        templateUrl: 'Admincontroller/loadView/home'
    })
    .otherwise({
        redirectTo: '/smlhlp'
    });    
    $locationProvider.html5Mode(true);
}]);   