'use strict';

angular.module('vpApp', ['ngRoute', 'ckeditor', 'ngSanitize', 
'angular-md5', 'ngFlash', 'ui.bootstrap.datetimepicker', 'angularUtils.directives.dirPagination', 'checklist-model', 'ngMap', 'angular-js-xlsx']);
app.value('version', '0.0.1');
angular.element(document).ready(function (){   
    angular.bootstrap(document.documentElement, ['vpApp']);
});