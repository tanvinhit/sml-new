angular.module('vpApp').controller('editCategoryCtrl', ['$scope', '$rootScope', '$window', '$location', 'baseService', 'requestApiHelper', '$routeParams', function($scope, $rootScope, $window, $location, baseService, requestApiHelper, $routeParams){
    $scope.meta = $location.path();
    var CatID = $routeParams.id;
    $scope.selectedCate = {}; 
    $scope.filterLang = 'vi';    
    $scope.options = baseService.ckeditorOptions;
    $scope.options.height = 300;
    var typeLink = $location.path();
    $rootScope.currentPage = {
        parent: (typeLink.indexOf('productCategory') !== -1)? 'Product Category' : 'Category',
        child: ''        
    }; 
    $rootScope.dataType = (typeLink.indexOf('productCategory') !== -1)? 2 : 1;
    $('.collapse-link:not(.binded)').addClass("binded").click(function () {
        var ibox = $(this).closest('div.ibox');
        var button = $(this).find('i');
        var content = ibox.find('div.ibox-content');
        content.slideToggle(200);
        button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        ibox.toggleClass('border-bottom');
        setTimeout(function () {
            ibox.resize();
            ibox.find('[id^=map-]').resize();
        }, 50);
    });
    function init(){  
        if($scope.meta.indexOf('add') !== -1){ 
            $scope.selectedCate = {
                CatID: 0,
                CatName: '',
                CatMeta: '',                
                CatDescribes: '',                 
                CatLang: $scope.filterLang,
                CatParent: '0',
                Image: '',
                SeoTitle: '',
                SeoKeyword: '',
                SeoDescribes: '',
                SeoCanonica: '',
                MetaRobot: '',
                Status: '1'
            }; 
            $('#img').attr('src', '');           
        }  
        else {
            var controller = requestApiHelper.CATEGORY.EDIT.concat('/', CatID);
            baseService.GET(controller).then(function(response){
                $scope.selectedCate = response[0];                 
            }, function(err){
                console.log(err);
            });
        }      
    }; 
    $rootScope.getLanguages();
    $rootScope.getTemplates();
    $rootScope.getCategories($scope.filterLang, $rootScope.dataType);
    init();
    $scope.event = {
        submitForm: function() {               
            if($scope.Form.$valid){
                if($scope.meta.indexOf('edit') !== -1){
                    var controller = requestApiHelper.CATEGORY.UPDATE;                      
                    if($scope.selectedCate.$$hashKey !== undefined)
                        delete $scope.selectedCate.$$hashKey;
                    baseService.POST(controller, $scope.selectedCate).then(function(response){
                        if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }
                        else if(response === 'true'){       
                            $rootScope.getCategories($scope.filterLang, $rootScope.dataType);                     
                            baseService.showToast('Update successful!', 'success');
                        }
                        else{
                            baseService.showToast('Update fail!', 'danger');                            
                        }
                    }, function(err){
                        baseService.showToast('Update fail!', 'danger');
                        console.log(err.message);
                    });
                }
                else{
                    var controller = requestApiHelper.CATEGORY.ADD;
                    $scope.selectedCate.Type = $rootScope.dataType;                    
                    baseService.POST(controller, $scope.selectedCate).then(function(response){    
                        if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }
                        else if(response === 'true'){
                            init();     
                            $rootScope.getCategories($scope.filterLang, $rootScope.dataType);                       
                            baseService.showToast('Add successful!', 'success');
                        }  
                        else{
                            baseService.showToast('Add fail!', 'danger');
                        }
                    }, function(err){
                        baseService.showToast('Add fail!', 'danger');
                        console.log(err.message);
                    });
                }
            }                      
        },
        catName_changed: function(){            
            var cateAlias = '';
            if($scope.selectedCate.CatName !== undefined || $scope.selectedCate.CatName !== ''){                         
                cateAlias = baseService.getAlias($scope.selectedCate.CatName); 
                $scope.selectedCate.CatMeta = cateAlias;
                $scope.selectedCate.SeoTitle = $scope.selectedCate.CatName;
                $scope.selectedCate.SeoKeyword = $scope.selectedCate.CatName;         
            }
        },
        browserImg: function(){
            CKFinder.popup({
                chooseFiles: true,  
                selectActionFunction: function(url){
                    var newUrl = baseService.getImageUrl(url);
                    $('#img').attr('src',url);                    
                    $scope.selectedCate.Image = newUrl;
                }
            });
        }  
    };
    $scope.$watch('selectedCate.CatMeta', function(){
        $scope.selectedCate.SeoCanonica = baseService.ROOT_URL.concat($scope.selectedCate.CatMeta);
    });
}]);