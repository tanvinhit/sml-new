angular.module('vpApp').controller('mailcontactCtrl',['$scope', '$rootScope', 'baseService', 'requestApiHelper', function($scope, $rootScope, baseService, requestApiHelper){
    $rootScope.currentPage = {
        parent: 'Hộp thư',
        child: ''
    };
    $scope.numberPage = '10';
    $scope.listToDelete = [];
    var selectedAll = 0;
    $rootScope.getMailList();    
    $scope.event = {
        view: function(item){
            $rootScope.selectedMail = {};
            $rootScope.selectedMail = item;
            $('#myModal').modal('show');
        },  
        del: function(){                        
            var controller = requestApiHelper.CONTACT.DELETE;
            var param = {ID: $rootScope.selectedMail.ID};
            baseService.POST(controller, param).then(function(response){                
                if(response === 'true'){
                    $rootScope.getMailList();
                    $('#delAlert').modal('hide');  
                    baseService.showToast('Xóa thành công!', 'success');
                }                        
            }, function(err){
                baseService.showToast('Xóa thất bại!', 'danger');
                console.log(err);
            })
        },
        getItem: function(item){
            $rootScope.selectedMail = {};                   
            $rootScope.selectedMail = item;                     
        },
        checkAll: function(){
            if(selectedAll === 0){
                selectedAll = 1;
                for(var i = 0; i < $rootScope.mailList.length; i++){                    
                    $scope.listToDelete.push($rootScope.mailList[i].ID);                
                }            
            }
            else{
                selectedAll = 0;
                for(var i = 0; i < $scope.listToDelete.length; i++){                    
                    $scope.listToDelete.splice(i, 1);                
                } 
            }
        },
        delAllItem: function(){
            var controller = requestApiHelper.CONTACT.DEL_MULTI;
            baseService.POST(controller, $scope.listToDelete).then(function(response){
                if(response === 'true'){
                    baseService.showToast('Xóa nhiều mục thành công!', 'success');
                    $('#delAllAlert').modal('hide');
                    $rootScope.getMailList(); 
                }
                else if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else{
                    baseService.showToast('Xóa nhiều mục thất bại!', 'danger');
                }
            }, function(err){
                baseService.showToast(err.data.message, 'danger');
            })
        }
    };
}]);