angular.module('vpApp').controller('widgetCtrl',['$scope', '$rootScope', '$window', '$route', 'baseService', 'requestApiHelper', function($scope, $rootScope, $window, $route, baseService, requestApiHelper){
    $rootScope.currentPage = {
        parent: 'Widgets',
        child: '',
        class: 'fa fa-flask'
    };
    $scope.header = {};
    $scope.options = baseService.ckeditorOptions2;
    $scope.listWidgets = {
        noArea: [],
        header: [],
        body: [],
        sidebar: [],
        footer: []
    };  
    $scope.filterLang = 'vi';    
    $scope.newWidget = {};   
    $scope.areas = [
        {value: 'noArea', text: 'None'},
        {value: 'header', text: 'Header'},
        {value: 'body', text: 'Body'},
        {value: 'sidebar', text: 'Sidebar'},
        {value: 'footer', text: 'Footer'},
    ];
    $scope.label = {
        title: ''  
    };
    var listNestableHeader = [];
    var listNestableBody = [];
    var listNestableSidebar = [];
    var listNestableFooter = [];
    var listNestableNoArea = [];
    var noAreaIsChanged = false;
    var headerIsChanged = false;
    var bodyIsChanged = false;
    var sidebarIsChanged = false;
    var footerIsChanged = false;
    $('.collapse-link:not(.binded)').addClass("binded").click(function () { 
        var ibox = $(this).closest('div.ibox');
        var button = $(this).find('i');
        var content = ibox.find('div.ibox-content');
        content.slideToggle(200);
        button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        ibox.toggleClass('').toggleClass('border-bottom');
        setTimeout(function () {
            ibox.resize();
            ibox.find('[id^=map-]').resize();
        }, 50);
    });
    function getWidgets(area){
        var controller = requestApiHelper.WIDGET.ALL + '/' + $scope.filterLang;
        var param = {type: 'admin', area: area};
        baseService.POST(controller, param).then(function(response){
            console.log(response);
            if(area === 'header'){                
                $scope.listWidgets.header = [];
                $scope.listWidgets.header = response;
                for(var i = 0;i < $scope.listWidgets.header.length; i++){
                    if($scope.listWidgets.header[i].Method === 'wd_visitorStatistic'){
                        var temp = $scope.listWidgets.header[i].Content.split(',');
                        $scope.listWidgets.header[i].Content = temp;
                    }
                }
            }
            else if(area === 'body'){                
                $scope.listWidgets.body = [];
                $scope.listWidgets.body = response;
                for(var i = 0;i < $scope.listWidgets.body.length; i++){
                    if($scope.listWidgets.body[i].Method === 'wd_visitorStatistic'){
                        var temp = $scope.listWidgets.body[i].Content.split(',');
                        $scope.listWidgets.body[i].Content = temp;
                    }
                }
            }
            else if(area === 'sidebar'){                
                $scope.listWidgets.sidebar = [];
                $scope.listWidgets.sidebar = response;
                for(var i = 0;i < $scope.listWidgets.sidebar.length; i++){
                    if($scope.listWidgets.sidebar[i].Method === 'wd_visitorStatistic'){
                        var temp = $scope.listWidgets.sidebar[i].Content.split(',');
                        $scope.listWidgets.sidebar[i].Content = temp;
                    }
                }
            }
            else if(area === 'footer'){                
                $scope.listWidgets.footer = [];
                $scope.listWidgets.footer = response;
                for(var i = 0;i < $scope.listWidgets.footer.length; i++){
                    if($scope.listWidgets.footer[i].Method === 'wd_visitorStatistic'){
                        var temp = $scope.listWidgets.footer[i].Content.split(',');
                        $scope.listWidgets.footer[i].Content = temp;
                    }
                }
            }
            else{                
                $scope.listWidgets.noArea = [];
                $scope.listWidgets.noArea = response;
            }
        });
    };      
    function init(){        
        getWidgets('noArea');
        getWidgets('header');
        getWidgets('body');
        getWidgets('sidebar');
        getWidgets('footer');
    };    
    function getCateSlide() {
        var controller = requestApiHelper.CATEGORY.GET + '/' + $scope.filterLang;
        var param = {
            type: 'admin',
            dataType: 3
        };
        baseService.POST(controller, param).then(function (response) {
            $rootScope.cateSlideList = response;
            console.log($rootScope.cateSlideList);
        }, function (err) {
            console.log(err);
        });
    }  
    function saveChange(area, list){
        var controller = requestApiHelper.WIDGET.SORT; 
        for(var i = 0; i < list.length; i++){
            list[i].area = area;
            if(list[i].$scope !== undefined)
                delete list[i].$scope;
        }
        baseService.POST(controller, list).then(function(response){
            if(response.redirect !== undefined){
                $window.location.href = response.redirect;   
            }
            else if(response === 'true'){
                $route.reload();                                
                //init();                
                baseService.showToast('Cập nhật thành công!', 'success');
            }
            else{
                baseService.showToast('Cập nhật thất bại!', 'danger');
            }
        }, function(err){
            baseService.showToast('Cập nhật thất bại!', 'danger');
            console.log(err);
        });
    };
    function updateOutputNoArea(e) {
        var list = e.length ? e : $(e.target);        
        console.log(list);
        noAreaIsChanged = true;
    };
    function updateOutputHeader(e) {
        var list = e.length ? e : $(e.target);        
        console.log(list);
        headerIsChanged = true;
    };
    function updateOutputBody(e) {
        var list = e.length ? e : $(e.target);        
        console.log(list);
        bodyIsChanged = true;
    };
    function updateOutputSidebar(e) {
        var list = e.length ? e : $(e.target);        
        console.log(list);
        sidebarIsChanged = true;
    };
    function updateOutputFooter(e) {
        var list = e.length ? e : $(e.target);        
        console.log(list);
        footerIsChanged = true;
    };
    $('#nestableWidget').nestable({
        maxDepth: 1,
        group: 1
    }).on('change', updateOutputNoArea);
    $('#nestableHeader').nestable({
        maxDepth: 1,
        group: 1
    }).on('change', updateOutputHeader);
    $('#nestableBody').nestable({
        maxDepth: 1,
        group: 1
    }).on('change', updateOutputBody);
    $('#nestableSidebar').nestable({
        maxDepth: 1,
        group: 1
    }).on('change', updateOutputSidebar);
    $('#nestableFooter').nestable({
        maxDepth: 1,
        group: 1
    }).on('change', updateOutputFooter);        
    function getHeader(){
        var controller = requestApiHelper.HEADER.GET;          
        baseService.GET(controller).then(function(response){
            $scope.header = response;
        });
    };       
    getHeader();
    init();
    $rootScope.getAllCategories($scope.filterLang);
    $rootScope.getLanguages();
    getCateSlide();
    $scope.event = {         
        add: function(){
            $scope.newWidget = {};
            $scope.newWidget = {
                Area: 'noArea',
                Language: $scope.filterLang,
                Status: '1',
                Type: 1
            };
            $scope.label.title = 'Add Widget'
        },
        delete: function(item){            
            $scope.newWidget = {};
            $scope.newWidget = item;
            $('#delAlert').modal('show');
        },
        save: function(){
            if(noAreaIsChanged){
                listNestableNoArea = $('#nestableWidget').nestable('serialize');                
                saveChange('noArea', listNestableNoArea);
                noAreaIsChanged = false;
            }
            if(headerIsChanged){
                listNestableHeader = $('#nestableHeader').nestable('serialize');                
                saveChange('header', listNestableHeader);
                headerIsChanged = false;
            }
            if(bodyIsChanged){
                listNestableBody = $('#nestableBody').nestable('serialize');                
                saveChange('body', listNestableBody);
                bodyIsChanged = false;
            }
            if(sidebarIsChanged){
                listNestableSidebar = $('#nestableSidebar').nestable('serialize');                
                saveChange('sidebar', listNestableSidebar);
                sidebarIsChanged = false;
            }
            if(footerIsChanged){
                listNestableFooter = $('#nestableFooter').nestable('serialize');                
                saveChange('footer', listNestableFooter);
                footerIsChanged = false;
            }
        },
        updateFormVisitCounter: function(obj){
            var temp = '';            
            for(var i = 0; i < obj.Content.length; i++)
                temp += obj.Content[i] + ',';
            obj.Content = temp; 
            var controller = requestApiHelper.WIDGET.UPDATE;                
            baseService.POST(controller, obj).then(function(response){
                if(response.redirect !== undefined){
                    $window.location.href = response.redirect;   
                }
                else if(response === 'true'){
                    //init();
                    baseService.showToast('Cập nhật thành công!', 'success');
                }
                else{
                    baseService.showToast('Cập nhật thất bại!', 'danger');
                }
            }, function(err){
                baseService.showToast('Cập nhật thất bại!', 'danger');
                console.log(err);
            });
        },
        updateForm: function(obj, isUpdate){
            if(isUpdate){
                var controller = requestApiHelper.WIDGET.UPDATE;
                baseService.POST(controller, obj).then(function(response){
                    if(response.redirect !== undefined){
                        $window.location.href = response.redirect;   
                    }
                    else if(response === 'true'){
                        //init();
                        baseService.showToast('Update successful!', 'success');
                    }
                    else{
                        baseService.showToast('Update fail!', 'danger');
                    }
                }, function(err){
                    baseService.showToast('Update fail!', 'danger');
                    console.log(err);
                });
            }
            else{
                var controller = requestApiHelper.WIDGET.ADD;
                baseService.POST(controller, $scope.newWidget).then(function(response){
                    if(response.redirect !== undefined){
                        $window.location.href = response.redirect;   
                    }
                    else if(response === 'true'){
                        init();
                        $('#widgetModal').modal('hide');
                        baseService.showToast('Add successful!', 'success');
                    }
                    else{
                        baseService.showToast('Add fail!', 'danger');
                    }
                }, function(err){
                    baseService.showToast('Add fail!', 'danger');
                    console.log(err);
                });
            }
        },  
        delWidget: function(){
            var controller = requestApiHelper.WIDGET.DELETE;
            var param = {ID: $scope.newWidget.ID};
            baseService.POST(controller, param).then(function(response){
                if(response.redirect !== undefined){
                    $window.location.href = response.redirect;   
                }
                else if(response === 'true'){
                    init();
                    $('#delAlert').modal('hide');
                    baseService.showToast('Delete successful!', 'success');
                }
                else{
                    baseService.showToast('Delete fail!', 'danger');
                }
            }, function(err){
                baseService.showToast('Delete fail!', 'danger');
                console.log(err);
            });  
        },
        updateHeader: function(){            
            var controller = requestApiHelper.HEADER.UPDATE;
            baseService.POST(controller, $scope.header).then(function(response){
                if(response.redirect !== undefined){
                    $window.location.href = response.redirect;   
                }
                else if(response === 'true'){
                    //getHeader(); 
                    baseService.showToast('Update successful!', 'success');
                }
                else{
                    baseService.showToast('Update fail!', 'danger');
                }
            }, function(err){
                baseService.showToast('Update fail!', 'danger');
                console.log(err);
            });
        },        
        selectLang: function (code, id) {
            $scope.filterLang = code;                        
            init();
            $('#tab-' + code).addClass('active');                                    
            for (var i = 0; i < $rootScope.languages.length; i++) {
                var checkClass = $('#tab-' + $rootScope.languages[i].Code).hasClass('active');
                var idLang = $rootScope.languages[i].ID;
                if (checkClass && id !== idLang) {
                    $('#tab-' + $rootScope.languages[i].Code).removeClass('active');
                    break;
                }
            }            
        },
        browserImg: function(){            
            CKFinder.popup({
                chooseFiles: true,  
                selectActionFunction: function(url){
                    var newUrl = baseService.getImageUrl(url);
                    $('#img').attr('src',url);                    
                    $scope.header.Logo = newUrl;
                }                
            });
        },
        browserImgBanner: function(){            
            CKFinder.popup({
                chooseFiles: true,  
                selectActionFunction: function(url){
                    var newUrl = baseService.getImageUrl(url);
                    $('#img-banner').attr('src',url);                    
                    $scope.header.Banner = newUrl;
                }                
            });
        },
        collapse: function (id) {               
            $('#bt-' + id).find('i').toggleClass('fa-caret-down').toggleClass('fa-caret-up');
            $('#form-' + id).slideToggle(200);
        }
    };
}]);