angular.module('vpApp').controller('homeCtrl',['$scope', '$rootScope', '$window', 'baseService', 'requestApiHelper', function($scope, $rootScope, $window, baseService, requestApiHelper){
    $rootScope.currentPage = {
        parent: 'Trang giới thiệu',
        child: ''
    };
    $scope.filterLang = 'vi';
    $scope.isUpdate = false;
    $scope.numberPage = '10';
    $scope.numOfPage = 5;
    $scope.label = {
        title: ''        
    };
    $scope.notice = {
        class: '',
        message: ''
    };
    $scope.homepageList = [];
    $scope.selectedHomepage = {};
    function init(){
        var controller = requestApiHelper.HOME_PAGE.ALL;
        baseService.GET(controller).then(function(response){
            $scope.homepageList = response; 
        }, function(err){
            console.log(err);
        });
    };
    $rootScope.getLanguages();
    init();    
    $scope.event = {
        edit: function(item){
            $scope.label.title = 'Sửa thông tin';
            $scope.isUpdate = true;
            $scope.selectedHomepage = {};            
            $scope.selectedHomepage = item;             
            $scope.notice.class = '';
            $scope.notice.message = '';  
        },
        submitForm: function(){
            //var noimage = 'assets/includes/upload/images/users/noimage.jpg';<span class="char1">G</span>
            if($scope.Form.$valid){
                if($scope.isUpdate){
                    var controller = requestApiHelper.HOME_PAGE.UPDATE_HOME_PAGE;
                    baseService.POST(controller, $scope.selectedHomepage).then(function(response){
                        if(response){
                            init();
                            $('#myModal').modal('hide');  
                            baseService.showToast('Cập nhật thành công!', 'success');
                        }  
                        else{
                            $scope.notice.class = 'text-danger';
                            $scope.notice.message = 'Cập nhật thất bại!';
                        }
                    }, function(err){                        
                        console.log(err);
                    });
                }
            }          
        },
        getItem: function(item){
            $scope.selectedHomepage = {};                   
            $scope.selectedHomepage = item;                     
        },
        browserBack: function(){
            CKFinder.popup({
                chooseFiles: true,  
                selectActionFunction: function(url){
                    var newUrl = baseService.getImageUrl(url);
                    $('#img').attr('src',url);
                    $('#txt_url').val(newUrl);
                    $scope.selectedHomepage.background = newUrl;
                }
            });
        },
    };
}]);