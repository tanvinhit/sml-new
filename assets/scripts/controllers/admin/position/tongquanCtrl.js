angular.module('vpApp').controller('tongquanCtrl',['$scope', '$rootScope', '$window', 'baseService', 'requestApiHelper', 
    function($scope, $rootScope, $window, baseService, requestApiHelper){
    $rootScope.currentPage = {
        parent: 'Tổng quan vị trí',
        child: ''
    };
    $scope.filterLang = 'vi';
    $scope.isUpdate = false;
    $scope.numberPage = '10';
    $scope.numOfPage = 5;
    $scope.label = {
        title: ''        
    };
    $scope.notice = {
        class: '',
        message: ''
    };
    $scope.positionTQList = [];
    $scope.selectedTQVT = {};
    function init(){
        var controller = requestApiHelper.TQVT.GETALL;
        baseService.GET(controller).then(function(response){
            console.log(response);
            $scope.positionTQList = response; 
        }, function(err){
            console.log(err);
        });
    };
    $rootScope.getLanguages();
    init();
    $scope.event = {
        edit: function(item){
            $scope.label.title = 'Sửa thông tin';
            $scope.isUpdate = true;
            $scope.selectedTQVT = {};            
            $scope.selectedTQVT = item;             
            $scope.notice.class = '';
            $scope.notice.message = '';  
        },
        add: function(){
            $scope.label.title = 'Thêm thông tin';
            $scope.isUpdate = false;
            $scope.selectedTQVT = {
                fromPlace: "",
                toPlace: "",
                detailPlace: "",
                time: 0
            };
            $scope.notice.class = '';
            $scope.notice.message = '';  
        },
        submitForm: function(){
            if($scope.Form.$valid){
                if($scope.isUpdate){
                    var controller = requestApiHelper.TQVT.UPDATE;
                    baseService.POST(controller, $scope.selectedTQVT).then(function(response){
                        if(response){
                            init();
                            $('#myModal').modal('hide');  
                            baseService.showToast('Cập nhật thành công!', 'success');
                        }  
                        else{
                            $scope.notice.class = 'text-danger';
                            $scope.notice.message = 'Cập nhật thất bại!';
                        }
                    }, function(err){                        
                        console.log(err);
                    });
                }
                else{
                    var controller = requestApiHelper.TQVT.ADD;
                   // $scope.selectedTQVT.image = ($scope.selectedTQVT.Avatar === '')? noimage : $scope.selectedTQVT.image;
                    baseService.POST(controller, $scope.selectedTQVT).then(function(response){
                        if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }
                        else if(response){
                            init();
                            $('#myModal').modal('hide');  
                            baseService.showToast('Thêm thành công!', 'success');
                        } 
                        else{
                            $scope.notice.class = 'text-danger';
                            $scope.notice.message = 'Thêm thất bại!';
                        }
                    }, function(err){                        
                        console.log(err);
                    });
                }
            }          
        },
        del: function(){                        
            var controller = requestApiHelper.TQVT.DELETE;
            var param = {id: $scope.selectedTQVT.id};
            baseService.POST(controller, param).then(function(response){
                if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else if(response){
                    init();
                    $('#delAlert').modal('hide');  
                    baseService.showToast('Xóa thành công!', 'success');
                } 
                else{
                    baseService.showToast('Xóa thất bại!', 'danger');
                }
            }, function(err){                
                console.log(err);
            })
        },
        getItem: function(item){
            $scope.selectedTQVT = {};                   
            $scope.selectedTQVT = item;                     
        },
    };
}]);