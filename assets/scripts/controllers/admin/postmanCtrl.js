angular.module('vpApp').controller('postmanCtrl',['$scope', '$rootScope', '$window', 'baseService', 'encodingExtensions', 'requestApiHelper', function($scope, $rootScope, $window, baseService, encodingExtensions, requestApiHelper){
    $scope.isUpdate = false;
    $scope.numberPage = '10';
    $scope.usersList = [];
    $scope.numOfPage = 5;
    $scope.label = {
        title: ''        
    };    
    $scope.notice = {};
    $rootScope.currentPage = {
        parent: 'Member',
        child: '',
        class: 'fa fa-users'
    };
    var email = '';
    $scope.selectedPostman = {};          
    $scope.checkExistsEmail = false;  
    $scope.checkExistsAccount = false;
    var Account = '';
    $scope.movies = [];
    // gives another movie array on change
	$scope.updateMovies = function(typed){
        console.log($scope.movies);
		var controller = requestApiHelper.POSTMAN.SEARCH_PROVINCE;
        var param = {name: typed};
        console.log('param: ',param);
        baseService.POST(controller, param).then(function(response){
            $scope.movies = response;
            console.log($scope.movies);
        }, function(err){
            console.log(err.message);
        });
    }

    function init(){     
        $scope.notice = {
            class: '',
            message: ''
        };
        var controller = requestApiHelper.POSTMAN.ALL;
        var param = {type: 'Bưu tá'};
        baseService.POST(controller, param).then(function(response){                         
            $scope.usersList = response;              
        }, function(error){
            console.log(error);
        });
    };            
    init();      
    function checkEmail(){
        var controller = requestApiHelper.USER.CHECK_MAIL;
        var param = {email: $scope.selectedPostman.Email};
        baseService.POST(controller, param).then(function(response){
            if(response === '1'){
                $scope.checkExistsEmail = true;                        
            }
            else{
                $scope.checkExistsEmail = false;                        
            }
        }, function(err){
            console.log(err.message);
        });
    };

    function checkAccount(){
        var controller = requestApiHelper.USER.CHECK_ACCOUNT;
        var param = {Account: $scope.selectedPostman.Account};
        baseService.POST(controller, param).then(function(response){
            if(response === '1'){
                $scope.checkExistsAccount = true;                        
            }
            else{
                $scope.checkExistsAccount = false;                        
            }
        }, function(err){
            console.log(err.message);
        });
    };
    
    $scope.event = {
        edit: function(item){
            $scope.label.title = 'Chỉnh sửa bưu tá';
            $scope.isUpdate = true;
            $scope.selectedPostman = {};            
            $scope.selectedPostman = item; 
            email = item.Email; 
            Account = item.Account;            
        },
        add: function(){
            $scope.label.title = 'Thêm mới bưu tá';
            $scope.isUpdate = false;
            $scope.selectedPostman = {                
                Avatar: '',
                Phone: '',
                Status: '1',
                IsContact: '0',
                Permission: 'Bưu tá'
            };       
            $('#img').attr('src', '');
            email = '';    
            Account= '';         
        },
        submitForm: function(){
            var noimage = 'assets/includes/upload/images/users/noimage.jpg';
            if($scope.selectedPostman.prov_name !== '' && $scope.selectedPostman.prov_name !== undefined){
                var controller = requestApiHelper.POSTMAN.CHECK_PROVINCE;
                    var param = {prov_name: $scope.selectedPostman.prov_name};
                    baseService.POST(controller, param).then(function(response){
                        $scope.check_province = response;   
                        if($scope.check_province !== 'null' ){
                            $scope.selectedPostman.prov_id = $scope.check_province[0].prov_id;
                            if(!$scope.checkExistsEmail && !$scope.checkExistsAccount){
                                if($scope.Form.$valid){
                                    if($scope.isUpdate){                                        
                                        var controller = requestApiHelper.USER.UPDATE;                        
                                        $scope.selectedPostman.Avatar = ($scope.selectedPostman.Avatar === '')? noimage : $scope.selectedPostman.Avatar;
                                        if($scope.selectedPostman.Password !== undefined && $scope.selectedPostman.Password !== '')
                                            $scope.selectedPostman.Password = encodingExtensions.encrypt($scope.selectedPostman.Password);
                                        if($scope.selectedPostman.$$hashKey !== undefined)
                                            delete $scope.selectedPostman.$$hashKey;
                                        baseService.POST(controller, $scope.selectedPostman).then(function(response){
                                            if(response.redirect !== undefined){
                                                $window.location.href = response.redirect;
                                            }
                                            else if(response[0] !== 'null'){
                                                init(); 
                                                $('#myModal').modal('hide');
                                                if(response[0].IdUser === $rootScope.user){
                                                    $rootScope.user = response[0];
                                                }
                                                baseService.showToast('Update successful!', 'success');
                                            }
                                            else{
                                                $scope.notice.class = 'left text-danger';
                                                $scope.notice.message = 'Update fail!';
                                            }
                                        }, function(err){
                                            $scope.notice.class = 'left text-danger';
                                            $scope.notice.message = 'Update fail!';
                                            console.log(err);
                                        });
                                    }
                                    else{
                                        if($scope.selectedPostman.Password === $scope.selectedPostman.ConfirmPassword){
                                            var controller = requestApiHelper.USER.ADD;                            
                                            $scope.selectedPostman.Avatar = ($scope.selectedPostman.Avatar === '')? noimage : $scope.selectedPostman.Avatar;
                                            $scope.selectedPostman.Password = encodingExtensions.encrypt($scope.selectedPostman.Password);                            
                                            if($scope.selectedPostman.$$hashKey !== undefined)
                                                delete $scope.selectedPostman.$$hashKey;
                                            delete $scope.selectedPostman.ConfirmPassword;
                                            baseService.POST(controller, $scope.selectedPostman).then(function(response){  
                                                if(response.redirect !== undefined){
                                                    $window.location.href = response.redirect;
                                                }
                                                else if(response === 'true'){
                                                    init(); 
                                                    $('#myModal').modal('hide');
                                                    baseService.showToast('Add successful!', 'success');
                                                }  
                                                else{
                                                    $scope.notice.class = 'left text-danger';
                                                    $scope.notice.message = 'Add fail!';
                                                }
                                            }, function(err){
                                                $scope.notice.class = 'left text-danger';
                                                $scope.notice.message = 'Add fail!';
                                                console.log(err);
                                            });
                                        }
                                        else{                                    
                                            $scope.notice.class = 'left text-danger';
                                            $scope.notice.message = 'Password is not match!';
                                        }
                                    }
                                }
                            }
                        }else{
                            $scope.notice.class = 'left text-danger';
                            $scope.notice.message = 'Vui lòng chọn đúng tên tỉnh!';
                        }
                    },function(err){
                    console.log(err.message);
                });
                
            }else{
                $scope.notice.class = 'left text-danger';
                $scope.notice.message = 'Vui lòng chọn tỉnh!';
            }
                        
        },
        del: function(){                        
            var controller = requestApiHelper.POSTMAN.DELETE;            
            var param = {'userId' : $scope.selectedPostman.UserID};
            baseService.POST(controller, param).then(function(response){
                if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else if(response === 'true'){
                    init();                    
                    $('#delAlert').modal('hide');
                    baseService.showToast('Delete successful!', 'success');
                }
                else{
                    $('#delAlert').modal('hide');
                    baseService.showToast('Delete fail!', 'danger');
                }
            }, function(err){
                baseService.showToast('Delete fail!', 'danger');
                console.log(err);
            });
        },
        getItem: function(item){
            $scope.selectedPostman = {};                   
            $scope.selectedPostman = item;                     
        },        
        browserImg: function(){
            CKFinder.popup({
                chooseFiles: true,  
                selectActionFunction: function(url){
                    var newUrl = baseService.getImageUrl(url);
                    $('#img').attr('src',url);
                    $('#txt_url').val(newUrl);
                    $scope.selectedPostman.Avatar = newUrl;
                }
            });
        },
        checkExists: function(){
            if($scope.selectedPostman.Email !== undefined && $scope.selectedPostman.Email !== ''){
                if($scope.isUpdate){
                    if($scope.selectedPostman.Email !== email){
                        checkEmail();
                    }
                }
                else{
                    checkEmail();
                }
            }
        },
        checkAccountExists: function(){
            if($scope.selectedPostman.Account !== undefined && $scope.selectedPostman.Account !== ''){
                if($scope.isUpdate){
                    if($scope.selectedPostman.Account !== Account){
                        checkAccount();
                    }
                }
                else{
                    checkAccount();
                }
            }
        }
    };
}]);
