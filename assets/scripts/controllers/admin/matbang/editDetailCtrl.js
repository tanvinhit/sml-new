angular.module('vpApp').controller('editDetailCtrl',['$scope', '$rootScope', '$window', '$location', '$routeParams', 'baseService', 'requestApiHelper', function($scope, $rootScope, $window, $location, $routeParams, baseService, requestApiHelper){
    $scope.meta = $location.path();
    var ArtID = $routeParams.id;
    $scope.options = baseService.ckeditorOptions;
    $scope.selectedMasterDetail = {}; 
    var title = 'Chi tiết mặt bằng';    
    $rootScope.currentPage = {
        parent:  title,
        child: ($scope.meta.indexOf('edit') !== -1)? 'Chỉnh sửa ' + title : 'Thêm mới ' + title,
        class: 'fa fa-edit'
    };   
    $scope.type_display = [
        {Name: 'Rect'},
        {Name: 'Poly'},
        {Name: 'Circle'},
    ];
    $rootScope.dataType = 1;  
    $scope.cateList = [];  
    function init(){        
        if($scope.meta.indexOf('add') !== -1){
            $('#img').attr('src', 'assets/admin/img/no_image.png');                    
            $scope.selectedMasterDetail = { 
                sort: 0,
                Status: '1',
                Image: 'assets/admin/img/no_image.png'                
            };      
        }
        else{
            var controller = requestApiHelper.DETAIL_MASTER_PLAN.EDIT_MASTER_DETAIL + '/' + ArtID;
            baseService.GET(controller).then(function(response){
                $scope.selectedMasterDetail = response[0]; 
                // var meta = $scope.selectedMasterDetail.ArtMeta;
                // $scope.selectedMasterDetail.ArtMeta = meta.substring(0, meta.lastIndexOf('.'));                                             
                // if($scope.selectedMasterDetail.CatId !== null && $scope.selectedMasterDetail.CatId !== '' && $scope.selectedMasterDetail.CatId !== undefined){
                //     $scope.cateList = $scope.selectedMasterDetail.CatId.split(',');
                //     $scope.cateList.splice(0,1); $scope.cateList.splice($scope.cateList.length-1,1);                    
                // }
                // console.log($scope.selectedMasterDetail.CatId);
            }, function(err){
                console.log(err);
            });
        }
    };
    init();
    $('.collapse-link:not(.binded)').addClass("binded").click(function () {
        var ibox = $(this).closest('div.ibox');
        var button = $(this).find('i');
        var content = ibox.find('div.ibox-content');
        content.slideToggle(200);
        button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        ibox.toggleClass('border-bottom');
        setTimeout(function () {
            ibox.resize();
            ibox.find('[id^=map-]').resize();
        }, 50);
    });
    getMasterPlan();
    $scope.event = {
        submitForm: function(){            
            if($scope.Form.$valid){                                               
                if($scope.cateList.length > 0){
                    $scope.selectedMasterDetail.CatId = ',' + $scope.cateList.join(',') + ',';
                }
                if ($scope.selectedMasterDetail.Username !== undefined)
                    delete $scope.selectedMasterDetail.Username;                  
                if($scope.meta.indexOf('edit') !== -1){
                    var controller = requestApiHelper.DETAIL_MASTER_PLAN.UPDATE_MASTER_DETAIL;                                        
                    baseService.POST(controller, $scope.selectedMasterDetail).then(function(response){
                        if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }
                        else if(response){
                            baseService.showToast('Update successful!', 'success');                       
                        }
                        else{
                            baseService.showToast('Update fail!', 'danger');
                        }
                    }, function(err){                        
                        console.log(err.message);
                        baseService.showToast('Update fail!', 'danger');
                    });
                }
                else{
                    var controller = requestApiHelper.DETAIL_MASTER_PLAN.ADD_MASTER_DETAIL;                                 
                    baseService.POST(controller, $scope.selectedMasterDetail).then(function(response){    
                        if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }
                        else if(response){                           
                            //init();
                            $location.path('admin/article/edit/'.concat(response.last));
                            baseService.showToast('Post successful!', 'success');                              
                        }
                        else{
                            baseService.showToast('Post fail!', 'danger');
                        }
                    }, function(err){                        
                        console.log(err.message);
                        baseService.showToast('Post fail!', 'danger');
                    }); 
                }
            }
        },        
        selectedChange: function(){            
            var Alias = '';              
            if($scope.selectedMasterDetail.SeoTitle !== undefined || $scope.selectedMasterDetail.SeoTitle !== ''){
                Alias = baseService.getAlias($scope.selectedMasterDetail.SeoTitle);
                $scope.selectedMasterDetail.SeoCanonica = baseService.ROOT_URL.concat(Alias);
                $scope.selectedMasterDetail.SeoKeyword = $scope.selectedMasterDetail.ArtName;
            }            
        },
        browserImg: function(){
            CKFinder.popup({
                chooseFiles: true,
                selectActionFunction: function(url){
                    var newUrl = baseService.getImageUrl(url);
                    $('#img').attr('src',url);                    
                    $scope.selectedMasterDetail.Image = newUrl;
                }                
            });
        }        
    };    
    $scope.$watch('selectedMasterDetail.ArtMeta', function(){
        $scope.selectedMasterDetail.SeoCanonica = baseService.ROOT_URL.concat($scope.selectedMasterDetail.ArtMeta);
    });
    function getMasterPlan() {
        var controller = requestApiHelper.MASTER_PLAN.ALL;
        baseService.GET(controller).then(function(response){
            $scope.masterplan = response; 
            console.log($scope.masterplan);
        }, function(err){
            console.log(err);
        });
    };
}]);