angular.module('vpApp').controller('MaterplantypeCtrl',['$scope', '$rootScope', '$window', 'baseService', 'requestApiHelper', function($scope, $rootScope, $window, baseService, requestApiHelper){
    $rootScope.currentPage = {
        parent: 'Loại mặt bằng',
        child: ''
    };
    $scope.filterLang = 'vi';
    $scope.isUpdate = false;
    $scope.numberPage = '10';
    $scope.numOfPage = 5;
    $scope.label = {
        title: ''        
    };
    $scope.notice = {
        class: '',
        message: ''
    };
    $scope.plusServiceList = [];
    $scope.selectedType = {};
    function init(){
        var controller = requestApiHelper.MASTER_PLAN.ALL;
        baseService.GET(controller).then(function(response){
            $scope.plusServiceList = response; 
        }, function(err){
            console.log(err);
        });
    };
    $rootScope.getLanguages();
    init();    
    $scope.event = {
        edit: function(item){
            $scope.label.title = 'Sửa loại mặt bằng';
            $scope.isUpdate = true;
            $scope.selectedType = {};            
            $scope.selectedType = item;             
            $scope.notice.class = '';
            $scope.notice.message = '';  
        },
        add: function(){
            $scope.label.title = 'Thêm loại mặt bằng';
            $scope.isUpdate = false;
            $scope.selectedType = {
                Status: 1
            };
            $('#img').attr('src', '');
            $scope.notice.class = '';
            $scope.notice.message = '';  
        },
        submitForm: function(){
            if($scope.Form.$valid){
                if($scope.isUpdate){
                    var controller = requestApiHelper.MASTER_PLAN.UPDATE_DB;
                    baseService.POST(controller, $scope.selectedType).then(function(response){
                        if(response){
                            init();
                            $('#myModal').modal('hide');  
                            baseService.showToast('Cập nhật thành công!', 'success');
                        }  
                        else{
                            $scope.notice.class = 'text-danger';
                            $scope.notice.message = 'Cập nhật thất bại!';
                        }
                    }, function(err){                        
                        console.log(err);
                    });
                }
                else{
                    var controller = requestApiHelper.MASTER_PLAN.ADD_MASTER_PLAN;
                    baseService.POST(controller, $scope.selectedType).then(function(response){
                        if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }
                        else if(response === 'true'){
                            init();
                            $('#myModal').modal('hide');  
                            baseService.showToast('Thêm thành công!', 'success');
                        } 
                        else{
                            $scope.notice.class = 'text-danger';
                            $scope.notice.message = 'Thêm thất bại!';
                        }
                    }, function(err){                        
                        console.log(err);
                    });
                }
            }          
        },
        del: function(){                        
            var controller = requestApiHelper.MASTER_PLAN.DELETE_MASTER_PLAN;
            var param = {id: $scope.selectedType.id};
            baseService.POST(controller, param).then(function(response){
                if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else if(response === 'true'){
                    init();
                    $('#delAlert').modal('hide');  
                    baseService.showToast('Xóa thành công!', 'success');
                } 
                else{
                    baseService.showToast('Xóa thất bại!', 'danger');
                }
            }, function(err){                
                console.log(err);
            })
        },
        getItem: function(item){
            $scope.selectedType = {};                   
            $scope.selectedType = item;                     
        }
    };
}]);