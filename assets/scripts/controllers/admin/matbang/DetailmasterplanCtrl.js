angular.module('vpApp').controller('DetailmasterplanCtrl',['$scope', '$rootScope', '$window', 'baseService', 'requestApiHelper', '$location', function($scope, $rootScope, $window, baseService, requestApiHelper, $location){
    $rootScope.currentPage = {
        parent: 'Mặt bằng',
        child: ''
    };
    $scope.filterLang = 'vi';
    $scope.isUpdate = false;
    $scope.numberPage = '10';
    $scope.numOfPage = 5;
    $scope.label = {
        title: ''        
    };
    $scope.notice = {
        class: '',
        message: ''
    };
    $scope.plusServiceList = [];
    $scope.selectedMasterPlanType = {};
    function init(){
        var controller = requestApiHelper.DETAIL_MASTER_PLAN.ALL;
        baseService.GET(controller).then(function(response){
            $scope.plusServiceList = response; 
        }, function(err){
            console.log(err);
        });
    };
    $rootScope.getLanguages();
    init(); 
    
    $scope.event = {
        edit: function(item){                        
            $scope.selectedArt = {};
            $scope.selectedArt = item;   
            $rootScope.selectLang = $scope.filterLang;
            $location.path('admin/chi-tiet-mat-bang/edit/' + item.id);            
        },
        add: function(){                                      
            $rootScope.selectLang = $scope.filterLang;
            $location.path('admin/chi-tiet-mat-bang/add');            
        },
        del: function(){
            var idPro = $scope.selectedArt.ArtID;
            var controller = requestApiHelper.ARTICLE.DELETE;
            var param = {
                id: idPro,
                user: $rootScope.user.IdUser
            };
            baseService.POST(controller, param).then(function(response){
                if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else if(response === 'true'){
                    init();
                    $('#delAlert').modal('hide');
                    baseService.showToast('Delete successful!', 'success');
                }
                else{
                    baseService.showToast('Delete fail!', 'danger');
                }
            }, function(err){
                console.log(err.message);
                baseService.showToast('Delete fail!', 'danger');
            });
        },
        getItem: function(item){
            $scope.selectedArt = {};
            $scope.selectedArt = item;            
        },             
        selectLang: function(code, id){    
            $scope.filterLang = code;
            if(id === 0){                
                init();
                $('#tab0').addClass('active');             
            }
            else{               
                init();
                $('#tab'+id).addClass('active');     
            }
            var checkTabAll = $('#tab0').hasClass('active');
            if(checkTabAll && id !== 0){
                $('#tab0').removeClass('active');
            }
            else{
                for(var i=0;i<$rootScope.languages.length;i++){
                    var checkClass = $('#tab'+$rootScope.languages[i].ID).hasClass('active');
                    var idLang = $rootScope.languages[i].ID;
                    if(checkClass && id !== idLang){
                        $('#tab'+$rootScope.languages[i].ID).removeClass('active');
                        break;
                    }
                }
            }
        },
        checkAll: function(){
            if(selectedAll === 0){
                selectedAll = 1;
                for(var i = 0; i < $scope.articles.length; i++){                    
                    $scope.listToDelete.push($scope.articles[i].ArtID);                
                }            
            }
            else{
                selectedAll = 0;
                for(var i = 0; i < $scope.listToDelete.length; i++){                    
                    $scope.listToDelete.splice(i, 1);                
                } 
            }
        },
        delAllItem: function(){
            var controller = requestApiHelper.ARTICLE.DEL_MULTI;
            baseService.POST(controller, $scope.listToDelete).then(function(response){
                if(response === 'true'){
                    baseService.showToast('Xóa nhiều mục thành công!', 'success');
                    $('#delAllAlert').modal('hide');
                    init();
                }
                else if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else{
                    baseService.showToast('Xóa nhiều mục thất bại!', 'danger');
                }
            }, function(err){
                baseService.showToast(err.data.message, 'danger');
            })
        }
    };
    
}]);