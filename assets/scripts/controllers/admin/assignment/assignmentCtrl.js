angular.module('vpApp')
    .controller('assignmentCtrl',['$scope', '$rootScope', '$location', '$routeParams', '$timeout', 'baseService', 'requestApiHelper', '$templateCache', function ($scope, $rootScope, $location, $routeParams, $timeout, baseService, requestApiHelper, $templateCache) {
        $scope.numberPage_postman = '5';
        $scope.choosePostman = '';
        $scope.numOfPage_postman = 5;
        var selectedAll = 0;
        $scope.numberPage_assign = '10';
        $scope.numOfPage_assign = 5;
        $scope.listToChoose = {
            listBillIds: [],
            listUserIds: [],
            BillIds: [],
        };
        $scope.item_purchase = {};
        $subscription = {};
        $scope.filterLang = "vi";
        $scope.postman = [];
        $scope.assign = [];
        $scope.assign_list = [];
        $scope.selectedProvince = {};
        $scope.selectedPostman = {};
        $rootScope.currentPage = {
            parent: 'Phân công',
            child: 'Phân công cho bưu tá'
        };
        $scope.notice = {};
        $scope.movies = [];
        $scope.checkExistsList = false;
        $scope.checkExistsListUser = false;
        $scope.billCustomId = '';
        $scope.postmanIsChoosed = '';
        $scope.dateAssign = baseService.getDateNow();
        // gives another movie array on change
        $scope.updateMovies = function(typed){
            console.log($scope.movies);
            var controller = requestApiHelper.POSTMAN.SEARCH_PROVINCE;
            var param = {name: typed};
            console.log('param: ',param);
            baseService.POST(controller, param).then(function(response){
                $scope.movies = response;
                console.log($scope.movies);
            }, function(err){
                console.log(err.message);
            });
        }

        $scope.updatePostman = function(typed){
            var controller = requestApiHelper.POSTMAN.SEARCH_POSTMAN;
            var param = {name: typed, prov_name: $scope.selectedProvince.prov_name};
            console.log('param: ',param);
            baseService.POST(controller, param).then(function(response){
                $scope.postman = response;
                console.log($scope.postman);
            }, function(err){
                console.log(err.message);
            });
        }

        function init(){
            
        }
        init();

        $scope.onRefresh = () => {
            
        };

        $scope.assignPost = (item) => {
           console.log(item); 
           console.log($scope.listToChoose.listBillIds.length); 

           var controller = requestApiHelper.ASSIGN.UPDATE;
            var param = {UserID: item, LIST_ID: $scope.listToChoose.listBillIds, COUNT_LENGTH: $scope.listToChoose.listBillIds.length, prov_name: $scope.selectedProvince.prov_name};
            baseService.POST(controller, param).then(function(response){
                $scope.movies = response;
                baseService.showToast('Phân công thành công!', 'success');
                $scope.assign = [];  
                var controller_2 = requestApiHelper.BILL.GET_BY_PROVINCE;
                var param_2 = {prov_id: $scope.check_province[0].prov_id};
                baseService.POST(controller_2, param_2).then(function(response){  
                    $scope.assign = [];                       
                    $scope.assign = response;              
                }, function(error){
                    $scope.assign = [];  
                    console.log(error);
                });
                var controller3 = requestApiHelper.ASSIGN.GET_BY_USER_ID;
                var param3 = {user_id: parseInt($scope.listToChoose.listUserIds[0]),assignDate: $scope.dateAssign};
                baseService.POST(controller3, param3).then(function(response){                         
                    $scope.assign_list = response;              
                }, function(error){
                    console.log(error);
                });
            }, function(err){
                console.log(err.message);
            });
        };

        $scope.loadData = () => {
            var controller = requestApiHelper.POSTMAN.CHECK_PROVINCE;
                    var param = {prov_name: $scope.selectedProvince.prov_name};
                    baseService.POST(controller, param).then(function(response){
                        $scope.check_province = response;   
                        if($scope.check_province !== 'null' ){
                            $scope.selectedProvince.prov_id = $scope.check_province[0].prov_id;
                            var controller_1 = requestApiHelper.USER.GET_BY_PROVINCE;
                            var param_1 = {prov_id: $scope.check_province[0].prov_id};
                            baseService.POST(controller_1, param_1).then(function(response){                         
                                $scope.postman = response;              
                            }, function(error){
                                console.log(error);
                            });

                            var controller_2 = requestApiHelper.BILL.GET_BY_PROVINCE;
                            var param_2 = {prov_id: $scope.check_province[0].prov_id};
                            baseService.POST(controller_2, param_2).then(function(response){                         
                                $scope.assign = response;              
                            }, function(error){
                                console.log(error);
                            });
                        }else{
                            $scope.notice.class = 'right text-danger';
                            $scope.notice.message = 'Vui lòng chọn đúng tên tỉnh!';
                        }
                    },function(err){
                    console.log(err.message);
                });
        };

        $scope.checkAll = function(){
            if(selectedAll === 0){
                selectedAll = 1;
                for(var i = 0; i < $scope.assign.length; i++){   
                    var checked_exists =  $scope.listToChoose.listBillIds.includes($scope.assign[i].billId);  
                    if(checked_exists){
                        var index = $scope.listToChoose.listBillIds.indexOf($scope.assign[i].billId);
                        $scope.listToChoose.listBillIds.splice(index,1);
                    }               
                    $scope.listToChoose.listBillIds.push($scope.assign[i].billId);                
                }            
            }
            else{
                selectedAll = 0;
                $scope.listToChoose.listBillIds = [];
            }

            if($scope.listToChoose.listBillIds.length > 0){
                $scope.checkExistsList = true;
            }else{
                $scope.checkExistsList = false;
            }
        };

        $scope.toggleAll = (item) => {
            console.log($scope.listToChoose.listBillIds);
            if($scope.listToChoose.listBillIds.length > 0){
                $scope.checkExistsList = true;
            }else{
                $scope.checkExistsList = false;
            }
        };

        $scope.toggleOne = (item) => {
            if($scope.listToChoose.listUserIds.length > 1){
                var index = $scope.listToChoose.listUserIds.indexOf($scope.listToChoose.listUserIds[0]);
                $scope.listToChoose.listUserIds.splice(index, 1);
            }
            if($scope.listToChoose.listUserIds.length > 0){
                $scope.checkExistsListUser = true;
            }else{
                $scope.checkExistsListUser = false;
            }
            console.log($scope.listToChoose.listUserIds[0]);
            var controller = requestApiHelper.ASSIGN.GET_BY_USER_ID;
            var param = {user_id: parseInt($scope.listToChoose.listUserIds[0]),assignDate: $scope.dateAssign};
            baseService.POST(controller, param).then(function(response){                         
                $scope.assign_list = response;              
            }, function(error){
                console.log(error);
            });
        };

        $scope.onEnterCode = () => {
            $scope.code = baseService.TO_UNSIGN_NO_LOWERCASE($scope.billCustomId);
            
            if($scope.listToChoose.listUserIds.length > 0){
                let ctrl = requestApiHelper.BILL.GET_BY_CODE;
                baseService.GET(ctrl.concat('/', $scope.billCustomId)).then((rsp) => {
                    if (rsp) {
                        // let bill = rsp;
                        $scope.listToChoose.BillIds.push(rsp.billId); 
                        $scope.selectBill();
                    }
                    else {
                        baseService.showToast("Không tìm thấy vận đơn", "warning");
                    }
                }, (err) => {
                    console.log(err);
                });
                $timeout(function () {
                    $scope.billCustomId = '';
                }, 1000);
            }else{
                baseService.showToast("Chọn bưu tá!", "danger");
            }
        };

        $scope.selectBill = (x) => {
            var controller = requestApiHelper.ASSIGN.UPDATE;
            var param = {UserID: $scope.listToChoose.listUserIds[0], LIST_ID: $scope.listToChoose.BillIds, COUNT_LENGTH: $scope.listToChoose.BillIds.length, prov_name: $scope.selectedProvince.prov_name};
            baseService.POST(controller, param).then(function(response){
                $scope.movies = response;
                baseService.showToast('Phân công thành công!', 'success');
                $scope.assign = [];  
                var controller_2 = requestApiHelper.BILL.GET_BY_PROVINCE;
                var param_2 = {prov_id: $scope.check_province[0].prov_id};
                baseService.POST(controller_2, param_2).then(function(response){  
                    $scope.assign = [];                       
                    $scope.assign = response;              
                }, function(error){
                    $scope.assign = [];  
                    console.log(error);
                });
                var controller3 = requestApiHelper.ASSIGN.GET_BY_USER_ID;
                var param3 = {user_id: parseInt($scope.listToChoose.listUserIds[0]),assignDate: $scope.dateAssign};
                baseService.POST(controller3, param3).then(function(response){                         
                    $scope.assign_list = response;              
                }, function(error){
                    console.log(error);
                });
            }, function(err){
                console.log(err.message);
            });
        };

        $scope.selectDate = function(dt) {
            var controller3 = requestApiHelper.ASSIGN.GET_BY_USER_ID;
                var param3 = {user_id: parseInt($scope.listToChoose.listUserIds[0]),assignDate: $scope.dateAssign};
                baseService.POST(controller3, param3).then(function(response){                         
                    $scope.assign_list = response;              
                }, function(error){
                    console.log(error);
                });
        }

        $scope.onPrint = () => {
            window.open(
                `Printcontroller/printPostman/30`,
                "_blank"
            );   
        }

        $scope.onSendApprove = function() {
            if($scope.assign_list.length > 0){
                for (var i=0; i<$scope.assign_list.length; i++) {
                    var controller = requestApiHelper.ASSIGN.SEND_APPROVE;
                    var param = {user_id: $scope.assign_list[i].user_id,assignDate: $scope.assign_list[i].assignDate,assignment_Id: $scope.assign_list[i].id, billCustomId: $scope.assign_list[i].billCustomId};
                    baseService.POST(controller, param).then(function(response){   
                        baseService.showToast('Phân công thành công', 'success');                                  
                    }, function(error){
                        console.log(error);
                    });
                }
            }
            var controller2 = requestApiHelper.ASSIGN.GET_BY_USER_ID;
            var param2 = {user_id: parseInt($scope.listToChoose.listUserIds[0]),assignDate: $scope.dateAssign};
            baseService.POST(controller2, param2).then(function(response){                         
                $scope.assign_list = response;              
            }, function(error){
                console.log(error);
            });
        }

        $scope.detail = function (item) {
            $scope.item_purchase = item;
        };

        $scope.del = function () {
            baseService.GET(requestApiHelper.ASSIGN.DELETE + '/' + $scope.item_purchase.billCustomId).then(function (respone) {
                if (respone) {
                    var message = 'Xóa đơn hàng thành công!';
                    baseService.showToast(message, 'success');
                    $('.modal').modal('hide');
                    var controller3 = requestApiHelper.ASSIGN.GET_BY_USER_ID;
                    var param3 = {user_id: parseInt($scope.listToChoose.listUserIds[0]),assignDate: $scope.dateAssign};
                    baseService.POST(controller3, param3).then(function(response){                         
                        $scope.assign_list = response;              
                    }, function(error){
                        console.log(error);
                    });
                    var controller_2 = requestApiHelper.BILL.GET_BY_PROVINCE;
                            var param_2 = {prov_id: $scope.check_province[0].prov_id};
                            baseService.POST(controller_2, param_2).then(function(response){                         
                                $scope.assign = response;              
                            }, function(error){
                                console.log(error);
                            });

                } else {

                    var message = 'Xóa đơn hàng không thành công!';
                    baseService.showToast(message, 'danger');
                    $('.modal').modal('hide');
                }
            });
        };
}]);