angular.module('vpApp')
    .controller('approveCtrl',['$scope', '$rootScope', '$location', '$window', 'baseService', 'requestApiHelper', function ($scope, $rootScope, $location, $window, baseService, requestApiHelper) {
        $rootScope.user = {};
        $scope.LIST_PURCHASE_APPROVE = [];
        $scope.item_purchase = {};
        $scope.listToDelete = {
            listBillIds: []
        };
        $scope.numberPage = '10';
        $scope.numOfPage = 5; 
        $rootScope.currentPage = {
            parent: 'Xác nhận phân công',
            child: ''
        };
        function init(){
            var controller_user = requestApiHelper.USER.CURRENT;
            baseService.GET(controller_user).then(function(response){
                if(response.redirect === undefined){
                    $rootScope.user = response;  
                    var controller = requestApiHelper.BILL.ALL
                    var param = {type: 'APPROVE', user_id: $rootScope.user.IdUser,fromDate: null, toDate: null };
                    baseService.POST(controller, param).then(function(respone){                         
                        if (respone){
                            $scope.LIST_PURCHASE_APPROVE = [];
                            $scope.LIST_PURCHASE_APPROVE = respone;
                        }
                        else console.log('Không có dữ liệu đơn hàng');            
                    }, function(error){
                         console.log(error);
                    });
                }
                else{
                    $window.location.url = response.redirect;
                }
            }, function(err){
                console.log(err);
            }); 
        }
        init();

        $scope.detail = function (item) {
            $scope.item_purchase = item;
        };

        $scope.del = function () {
            baseService.GET(requestApiHelper.BILL.DELETE + '/' + $scope.item_purchase.billId).then(function (respone) {
                if (respone) {
                    $scope.LIST_PURCHASE_APPROVE = respone;
                    var message = 'Xóa đơn hàng thành công!';
                    baseService.showToast(message, 'success');
                    $('.modal').modal('hide');
                    init();
                } else {
                    $scope.LIST_PURCHASE_APPROVE = [];
                    var message = 'Xóa đơn hàng không thành công!';
                    baseService.showToast(message, 'danger');
                    $('.modal').modal('hide');
                }
            });
        };

        $scope.checkAll = function(){
            if(selectedAll === 0){
                selectedAll = 1;
                for(var i = 0; i < $scope.LIST_PURCHASE_APPROVE.length; i++){                    
                    $scope.listToDelete.listBillIds.push($scope.LIST_PURCHASE_APPROVE[i].orderId);                
                }            
            }
            else{
                selectedAll = 0;
                for(var i = 0; i < $scope.listToDelete.listBillIds.length; i++){                    
                    $scope.listToDelete.listBillIds.splice(i, 1);                
                } 
            }
        };

        $scope.delAllItem = function(){
            var controller = requestApiHelper.BILL.DEL_MULTI;
            baseService.POST(controller, $scope.listToDelete.listBillIds).then(function(response){
                if(response === 'true'){
                    baseService.showToast('Xóa nhiều mục thành công!', 'success');
                    $('#delAllAlert').modal('hide');
                    init();
                }
                else if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else{
                    baseService.showToast('Xóa nhiều mục thất bại!', 'danger');
                }
            }, function(err){
                baseService.showToast(err.data.message, 'danger');
            })
        };

        $scope.appendSelectStatus = function (id) {
            $('#status-' + id).children().remove();
            var html = '<select class="form-group" onchange="angular.element(this).scope().updateStatus(' + id + ', this.value)" onblur="angular.element(this).scope().cancelUpdate(' + id + ')"><option value="">== Chọn tình trạng ==</option><option value="2">Phát thành công</option><option value="1">Đang vận chuyển</option><option value="0">Đang đóng gói</option><option value="-1">Hủy đơn hàng</option><option value="3">Tồn - Thông báo chuyển hoàn</option><option value="4">Hoàn thành công - Chuyển trả người gửi</option></select>';
            $('#status-' + id).append(html);
        };

        $scope.updateStatus = function (id,value) {
            var data = {
                billId: id,
                status: value
            }
            baseService.POST(requestApiHelper.BILL.UPDATE_STATUS, data).then(function (response){
                if (response) {
                    var msg = 'Cập nhật tình trạng thành công!';
                    baseService.showToast(msg, 'success');
                    $window.location.reload();
                }
                else{
                    var msg = 'Cập nhật tình trạng không thành công!';
                    baseService.showToast(msg, 'danger');
                }
            })
        };

        $scope.cancelUpdate = function(id){
            $('#status-' + id).children().remove();
            var item = $scope.LIST_PURCHASE_APPROVE.find(function(d){return d.orderId === id});
            if(item !== undefined){
                switch(item.status){
                    case '4': $('#status-' + id).append('<span class="label label-warning">Hoàn thành công - Chuyển trả người gửi</span>');
                        break;
                    case '3': $('#status-' + id).append('<span class="label label-danger">Tồn - Thông báo chuyển hoàn</span>');
                        break;
                    case '2': $('#status-' + id).append('<span class="label label-success">Phát thành công</span>');
                        break;
                    case '1': $('#status-' + id).append('<span class="label label-primary">Đang vận chuyển</span>');
                        break;
                    case '0': $('#status-' + id).append('<span class="label label-warning">Đang đóng gói</span>');
                        break;
                    case '-1': $('#status-' + id).append('<span class="label label-danger">Đã hủy đơn hàng</span>');
                        break;
                }
            }
        };

        $scope.add = () => {
            $location.path("admin/bills/add");
        };

        $scope.edit = (x) => {
            $location.path(`admin/billDetail/${x.billId}`);
        };

        $scope.approve = (x) => {
            var controller_user = requestApiHelper.USER.CURRENT;
            baseService.GET(controller_user).then(function(response){
                if(response.redirect === undefined){
                    $rootScope.user = response;  
                    var controller = requestApiHelper.ASSIGN.UPDATE_APPROVE;
                    var param = {data: x,};
                    console.log(x.assignment_Id);
                    baseService.POST(controller, param).then(function(respone){                         
                        if (respone){
                            $scope.LIST_PURCHASE_APPROVE = [];
                            baseService.showToast('Phân công thành công!', 'success');
                            var controller_user = requestApiHelper.USER.CURRENT;
                            baseService.GET(controller_user).then(function(response){
                                if(response.redirect === undefined){
                                    $rootScope.user = response;  
                                    var controller = requestApiHelper.BILL.ALL
                                    var param = {type: 'APPROVE', user_id: $rootScope.user.IdUser,fromDate: null, toDate: null };
                                    baseService.POST(controller, param).then(function(respone){                         
                                        if (respone){
                                            $scope.LIST_PURCHASE_APPROVE = [];
                                            $scope.LIST_PURCHASE_APPROVE = respone;
                                        }
                                        else{
                                            console.log('Không có dữ liệu đơn hàng');
                                            $scope.LIST_PURCHASE_APPROVE = [];
                                        }             
                                    }, function(error){
                                        console.log(error);
                                    });
                                }
                                else{
                                    $window.location.url = response.redirect;
                                }
                            }, function(err){
                                console.log(err);
                            });
                        }
                        else console.log('Không cập nhật được đơn hàng');            
                    }, function(error){
                         console.log(error);
                    });
                }
                else{
                    $window.location.url = response.redirect;
                }
            }, function(err){
                console.log(err);
            }); 
        };

        /* Import Excel */
	$scope.read = (file) => {
		if (!!file.BANGKE && file.BANGKE.length > 0) {
			let data = [];
            let api = requestApiHelper.BILL.CREATE_MANY;
            let i = 0;
			file.BANGKE.forEach(item => {
                if(i > 0){
                    if(item.length !== 0){
                        let obj = {
                            customerSend: !!item[0] ? Number(item[0]) : 0,
                            billCustomId: !!item[1] ? item[1] : "",
                            createdDate: !!item[2] ? moment(item[2]).format("YYYY-MM-DD HH:mm") : new Date(),
                            weight: !!item[3] ? Number(item[3]) : 0,
                            valuesOfParcel: !!item[4] ? Number(item[4]) : 0,
                            serviceCOD: !!item[5] ? Number(item[5]) : 0,
                            additionalFee: !!item[6] ? Number(item[6]) : "",
                            payments: !!item[7] ? item[7] : "",
                            typeOfParcel: !!item[8] ? item[8] : "",
                            serviceId: !!item[9] ? item[9] : "",
                            plusServices: !!item[10] ? item[10] : "",
                            provinceTo: !!item[11] ? item[11] : "",
                            districtTo: !!item[12] ? item[12] : "",
                            receiveName: !!item[13] ? item[13] : "",
                            addressTo: !!item[14] ? item[14] : "",
                            receivePhone: !!item[15] ? item[15] : "",
                            serviceNote: !!item[16] ? item[16] : "",
                            userId: $rootScope.user.IdUser
                        };

                        data.push(obj);
                    }
                }
                i++;
			});
			baseService.POST(api, data).then(rsp => {
				if (rsp.success) {
                    baseService.showToast("Nhập file excel thành công!", "success");	
                    init();				
				}
				else {
					baseService.showToast("Nhập file excel không thành công!", "warning");					
                }
			}).catch(ex => {
				console.log(ex);
			});
		}

		$scope.$apply();
	};
}]);