angular.module('vpApp').controller('imageCtrl', ['$scope', '$rootScope', '$window', 'baseService', 'requestApiHelper', function ($scope, $rootScope, $window, baseService, requestApiHelper) {
    $scope.numberPage = '10';
    $scope.numOfPage = 5;
    $scope.selectedCustomer = {};
    $scope.filterLang = 'vi';
    $scope.notice = {};
    $scope.listCate = [];
    var selectedAll = 0;
    $scope.listToDelete = {
        listCatId: []
    }
    $scope.isUpdate = false;
    $rootScope.currentPage = {
        parent: 'Slide thư viện',
        child: ''
    }
    
    $scope.category = {
        CatID: 0,
        CatName: '',
        CatMeta: 'thu-vien',                
        CatDescribes: '',                 
        CatLang: $scope.filterLang,
        CatParent: '0',
        Image: '',
        SeoTitle: '',
        SeoKeyword: '',
        SeoDescribes: '',
        SeoCanonica: '',
        MetaRobot: '',
        Type: 4,
        Status: '1'
    }

    function init() {
        $scope.cate = {
            Language: $scope.filterLang
        };
        $scope.notice = {
            class: '',
            message: ''
        };
        var controller = requestApiHelper.CATEGORY.GET + '/' + $scope.filterLang;
        var data = {
            type: 'admin',
            dataType: 3
        };
        baseService.POST(controller, data).then(function (response) {
            $scope.listCate = response;
        }, function (err) {
            console.log(err);
        });
    };
    $rootScope.getLanguages();
    init();
    
    $scope.SubmitForm = function(){
        $scope.category.CatName = $scope.cateSlideName;
        if($scope.isUpdate){
            var controller = requestApiHelper.CATEGORY.UPDATE;
            baseService.POST(controller, $scope.category).then(function (response) {
                if(response === 'true'){
                    $scope.listCate = response;
                    baseService.showToast('Update successful!', 'success');
                    init();
                    $('.modal').hide();
                }
                else baseService.showToast('Update fail!', 'danger');
            }, function (err) {
                console.log(err);
            });
        }else{
            var controller = requestApiHelper.CATEGORY.ADD;
            baseService.POST(controller, $scope.category).then(function (response) {
                if(response === 'true'){
                    $scope.listCate = response;
                    baseService.showToast('Insert successful!', 'success');
                    init();
                    $('.modal').hide();
                }
                else baseService.showToast('Insert fail!', 'danger');
            }, function (err) {
                console.log(err);
            });
        }
    }
    $scope.Read = function(item){
        $scope.category = item;
        $scope.isUpdate = true;
        $scope.frmTitle = 'Cập nhật slideshow';
        $scope.cateSlideName = item.CatName;
        $('#addModal').modal('show');
    };
    $scope.getItem = function(item){
        $scope.category = item;
    };
    $scope.addModal = function(){
        $scope.isUpdate = false;
        $scope.frmTitle = 'Thêm mới slideshow';
        $scope.cateSlideName = '';
    };
    $scope.delete = function(){
        var controller = requestApiHelper.CATEGORY.DELETE;
        let param = {
            cateId: $scope.category.CatID
        }
        baseService.POST(controller, param).then(function (response) {
            if(response === 'true'){
                baseService.showToast('Delete successful!', 'success');
                init();
                $('.modal').hide();
            }
            else baseService.showToast('Delete fail!', 'danger');
        }, function (err) {
            console.log(err);
        });
    }
    $scope.checkAll = function(){
        if(selectedAll === 0){
            selectedAll = 1;
            for(var i = 0; i < $scope.listCate.length; i++){                    
                $scope.listToDelete.listCatId.push($scope.listCate[i].CatID);                
            }            
        }
        else{
            selectedAll = 0;
            for(var i = 0; i < $scope.listToDelete.listCatId.length; i++){                    
                $scope.listToDelete.listCatId.splice(i, 1);                
            } 
        }
    };
    $scope.deleteMulti = function(){
        var controller = requestApiHelper.CATEGORY.DEL_MULTI;
        baseService.POST(controller, $scope.listToDelete.listCatId).then(function (response) {
            if(response === 'true'){
                baseService.showToast('Delete successful!', 'success');
                init();
                $('.modal').hide();
            }
            else baseService.showToast('Delete fail!', 'danger');
        }, function (err) {
            console.log(err);
        });
    };
    $scope.selectLang = function(code, id){    
        $scope.filterLang = code;            
        if(id === 0){                
            init();
            $('#tab0').addClass('active');             
        }
        else{               
            init();
            $('#tab'+id).addClass('active');     
        }
        var checkTabAll = $('#tab0').hasClass('active');
        if(checkTabAll && id !== 0){
            $('#tab0').removeClass('active');
        }
        else{
            for(var i=0;i<$rootScope.languages.length;i++){
                var checkClass = $('#tab'+$rootScope.languages[i].ID).hasClass('active');
                var idLang = $rootScope.languages[i].ID;
                if(checkClass && id !== idLang){
                    $('#tab'+$rootScope.languages[i].ID).removeClass('active');
                    break;
                }
            }
        }
    }
}]);