angular.module('vpApp')
    .controller('billDetailCtrl',['$scope', '$rootScope', '$location', '$routeParams', 'baseService', 'requestApiHelper', function ($scope, $rootScope, $location, $routeParams, baseService, requestApiHelper) {
        $scope.meta = $location.path();
        var billId = $routeParams.id;
        $scope.filterLang = "vi";
        $scope.bill = {};
        $scope.assign = {};
        $scope.assigndetail = {};
        $scope.isUpdate = false;
        $scope.plusServices = [];
        $scope.plusServiceList = [];
        $rootScope.currentPage = {
            parent: 'Vận đơn',
            child: 'Chi tiết vận đơn'
        };
        $scope.user = {};
        $scope.fuelFee = 15;
        $scope.vat = 10;
        $scope.priceInValid = false;
        $scope.checkApprove = false;
        $scope.checkDelivery= false;
        $scope.checkStatus= false;
        $scope.notice = {};
        $scope.item_delivery = {};
        $scope.permissions = [
            {IdLevel: 2, LevelName: 'Phát thành công'},
            {IdLevel: 3, LevelName: 'Tồn - Thông báo chuyển hoàn'},
        ]; 

        function init(){
            if($scope.meta.indexOf('add') === -1){
                $scope.isUpdate = true;
                baseService.GET(requestApiHelper.BILL.GET_BY_ID.concat('/', billId)).then(function (respone) {
                    if (respone){
                        $scope.bill = respone;
                        console.log($scope.bill.assignment_Id);
                        if($scope.bill.status == 2){
                            $scope.checkStatus= true; 
                        }
                        var controller2 = requestApiHelper.ASSIGN.GET_BY_ID
                        var param2 = {billCustomId: $scope.bill.billCustomId};
                        baseService.POST(controller2, param2).then(function(respone2){                         
                            if (respone2){
                                $scope.assigndetail = respone2;
                                console.log(respone2[0].status_assign);
                                if(respone2[0].status_assign == 1){
                                    $scope.checkApprove = true;
                                }
                                if(respone2[0].status_assign == 2){
                                    $scope.checkDelivery = true;
                                }
                            }
                            else console.log('Không có dữ liệu đơn hàng');            
                        }, function(error){
                             console.log(error);
                        });
                        if(!!$scope.bill.plusServices){
                            $scope.plusServiceList = $scope.bill.plusServices.split(',');
                            $scope.plusServiceList.splice(0,1);
                            $scope.plusServiceList.splice($scope.plusServiceList.length - 1, 1);                    
                        }
                    }
                    else console.log('Không có dữ liệu đơn hàng');
                });
            }
            else{
                $scope.isUpdate = false;
                $scope.bill = {};
                $scope.bill = { 
                    userName: $scope.user.userName,
                    unitPrice: 0,
                    additionalFee: 0,
                    otherFee: 0,
                    fuelFee: 0,
                    totalDiscount: 0,
                    vat: 0,
                    totalPrice: 0,
                    valuesOfParcel: 0,
                    createdDate: baseService.getDatetimeNow(),
                    typeOfParcel: 'Hàng hoá',
                    payments: 'Theo hợp đồng',
                    status: 0,
                    serviceId: 'CPN'          
                };
                $scope.plusServiceList = [];
            }
        }

        getServices();
        getPlusServices();
        init();

        $scope.submitForm = function () {
            if($scope.priceInValid){
                alert("Bảng giá cước không hợp lệ. Không thể lưu phiếu gửi!");
                return;
            }

            if($scope.plusServiceList.length > 0){
                $scope.bill.plusServices = ',' + $scope.plusServiceList.join(',') + ',';
            }

            if($scope.meta.indexOf('add') === -1){
                baseService.POST(requestApiHelper.BILL.UPDATE, $scope.bill).then(function (respone) {
                    if (respone) {
                        console.log(respone);
                        var message = 'Cập nhật đơn hàng thành công!';
                        baseService.showToast(message, 'success');
                    } else {
                        var message = 'Cập nhật đơn hàng không thành công!';
                        baseService.showToast(message, 'danger');
                    }
                });
            }
            else{
                var controller_user = requestApiHelper.USER.CURRENT;
                baseService.GET(controller_user).then(function(response){
                    if(response.redirect === undefined){
                        $scope.user = response; 
                        if($scope.user.Level === 'Bưu tá'){
                            $scope.assign.billCustomId = $scope.bill.billCustomId;
                            $scope.assign.user_id = null;
                            $scope.assign.status_assign = 0;
                            $scope.assign.status = $scope.bill.status;
                            $scope.assign.assignDate = null;


                            $scope.bill.user_id = $scope.user.IdUser;
                            $scope.bill.userName = $scope.user.Username;
                            console.log($scope.bill);
                            baseService.POST(requestApiHelper.BILL.ADD, $scope.bill).then(function (respone) {
                                if (respone.success) {
                                    var message = 'Thêm đơn hàng thành công!';
                                    baseService.showToast(message, 'success');
                                    billId = respone.data;
                                } else {
                                    var message = respone.message;
                                    baseService.showToast(message, 'danger');
                                }
                            });
                            baseService.POST(requestApiHelper.ASSIGN.ADD, $scope.assign).then(function (respone) {
                                if (respone.success) {
                                    var message = 'Thêm đơn hàng thành công!';
                                    baseService.showToast(message, 'success');
                                    billId = respone.data;
                                } else {
                                    var message = respone.message;
                                    baseService.showToast(message, 'danger');
                                }
                            });
                        }else{
                            $scope.assign.billCustomId = $scope.bill.billCustomId;
                            $scope.assign.user_id = null;
                            $scope.assign.status_assign = 0;
                            $scope.assign.status = $scope.bill.status;
                            $scope.assign.assignDate = null;

                            $scope.bill.user_id = 0;
                            $scope.bill.userName = $scope.user.Username;
                            baseService.POST(requestApiHelper.BILL.ADD, $scope.bill).then(function (respone) {
                                if (respone.success) {
                                    var message = 'Thêm đơn hàng thành công!';
                                    baseService.showToast(message, 'success');
                                    billId = respone.data;
                                } else {
                                    var message = respone.message;
                                    baseService.showToast(message, 'danger');
                                }
                            });
                            baseService.POST(requestApiHelper.ASSIGN.ADD, $scope.assign).then(function (respone) {
                                if (respone.success) {
                                    var message = 'Thêm đơn hàng thành công!';
                                    baseService.showToast(message, 'success');
                                    billId = respone.data;
                                } else {
                                    var message = respone.message;
                                    baseService.showToast(message, 'danger');
                                }
                            });
                        }
                        
                    }
                    else{
                        $window.location.url = response.redirect;
                    }
                }, function(err){
                    console.log(err);
                });
            }
        };

        $scope.findCustomer = () => {
            if(!!$scope.bill.customerSend){
                baseService.GET(requestApiHelper.CUSTOMER.GET_BY_ID.concat('/', $scope.bill.customerSend)).then(function (response) {
                    if (response){
                        let customer = response;
                        $scope.bill.customerSendName = customer.Name;
                        $scope.bill.customerSendPhone = customer.Phone;
                        $scope.bill.companyName = customer.companyName;
                        $scope.bill.addressFrom = customer.Address;
                        $scope.bill.fromPlace = customer.district;
                        $scope.bill.fromPlaceDetail = customer.districtDetail;
                        $scope.bill.provinceFrom = customer.province;
                    }
                    else console.log('Không có dữ liệu khách hàng');
                });
            }
        };

        $scope.onGetPostalCharges = () => {
            if(!!$scope.bill.provinceFrom && !!$scope.bill.provinceTo && !!$scope.bill.weight && !!$scope.bill.serviceId){
                baseService.GET(requestApiHelper.POSTAL_CHARGE.GET_BY_ID.concat('/', $scope.bill.provinceFrom, '/', $scope.bill.provinceTo, '/', $scope.bill.weight, '/', $scope.bill.serviceId)).then(function (response) {
                    if (!!response){
                        $scope.bill.unitPrice = response.dcpPrice;
                        $scope.onCalculate();
                        $scope.priceInValid = false;
                    }
                    else{ 
                        $scope.priceInValid = true;
                        baseService.showToast('Không tìm thấy giá cước.', 'warning');
                    }
                });
            }
        };

        $scope.onGetAddressDetail = () => {
            $scope.bill.addressDetail = '';
            if(!!$scope.bill.districtToDetail)
                $scope.bill.addressDetail += $scope.bill.districtToDetail;
            if(!!$scope.bill.addressTo)
                $scope.bill.addressDetail = $scope.bill.addressTo + ', ' + $scope.bill.addressDetail + ', ' + $scope.bill.provinceTo;
        };

        $scope.getDistrictToById = (districtId) => {
            if(!!districtId){
                districtId = districtId.toUpperCase();
                var controller = requestApiHelper.ADDRESS.GET_DISTRICTS_BY_ID;
                baseService.GET(controller.concat('/', districtId)).then(function(response) {
                    if(!!response){
                        let district = response[0];
                        $scope.bill.districtToDetail = district.TEN_QUANHUYEN;
                        $scope.bill.provinceTo = district.MATINH;
                        $scope.bill.districtTo = district.MAQUANCHAR;
                        $scope.onGetAddressDetail();
                        $scope.onGetPostalCharges();
                    }
                }, function(err){
                    console.log(err);
                });
            }
        }

        $scope.getDistrictFromToById = (districtId) => {
            if(!!districtId){
                districtId = districtId.toUpperCase();
                var controller = requestApiHelper.ADDRESS.GET_DISTRICTS_BY_ID;
                baseService.GET(controller.concat('/', districtId)).then(function(response) {
                    if(!!response){
                        let district = response[0];
                        $scope.bill.fromPlaceDetail = district.TEN_QUANHUYEN;
                        $scope.bill.provinceFrom = district.MATINH;
                        $scope.bill.fromPlace = district.MAQUANCHAR;
                        // $scope.onGetAddressDetail();
                        // $scope.onGetPostalCharges();
                    }
                }, function(err){
                    console.log(err);
                });
            }
        }

        $scope.onGetPlusServiceFee = (x, e) => {
            let isChecked = e.target.checked;

            if(x.typePrice == 0){
                if(isChecked)
                    $scope.bill.additionalFee += parseFloat($scope.bill.unitPrice) / 100 * parseFloat(x.unitPrice);
                else
                    $scope.bill.additionalFee -= parseFloat($scope.bill.unitPrice) / 100 * parseFloat(x.unitPrice);                
            }
            else{
                if(isChecked)
                    $scope.bill.additionalFee += parseFloat(x.unitPrice);
                else
                    $scope.bill.additionalFee -= parseFloat(x.unitPrice);                
            }
            $scope.bill.additionalFee += $scope.bill.otherFee;
            $scope.onCalculate();
        };

        $scope.onChangeOtherFee = () => {
            let additionalFee = $scope.bill.additionalFee;
            additionalFee += Number($scope.bill.otherFee);
            $scope.bill.additionalFee = additionalFee;
            $scope.onCalculate();
        }

        $scope.onRefresh = () => {
            if($scope.isUpdate)
                $location.path('/admin/bills/add');
            else
                init();
        };

        $scope.onCalculate = () => {
            let charge = $scope.bill.unitPrice;
            $scope.bill.fuelFee = (charge * $scope.fuelFee / 100);
            let totalPrice = charge + $scope.bill.fuelFee + $scope.bill.additionalFee;
            $scope.bill.vat = (totalPrice * $scope.vat / 100);
            $scope.bill.totalPrice = totalPrice + $scope.bill.vat + $scope.bill.totalDiscount;
        };

        $scope.onPrint = () => {
            if (!!billId) {
                window.open(
                    `Printcontroller/printBill/${billId}`,
                    "_blank"
                );
            }    
        }

        $scope.onApprove = () => {
            $scope.bill.assignment_Id = parseInt($scope.assigndetail[0].id);
            var controller = requestApiHelper.ASSIGN.UPDATE_APPROVE;
                var param = {data: $scope.bill};
                baseService.POST(controller, param).then(function(respone){                         
                if (respone){
                    baseService.showToast('Phân công thành công!', 'success');
                    $location.path("admin/approve-assign");
                }
                else console.log('Phân công không thành công!');            
                }, function(error){
                         console.log(error);
            });
        }

        $scope.onDelivery = () => {
            $scope.bill.assignment_Id = parseInt($scope.assigndetail[0].id);
            $scope.bill.image = $scope.item_delivery.image;
            $scope.bill.Status = parseInt($scope.item_delivery.Status);
            $scope.bill.Comment = $scope.item_delivery.Comment;
            $scope.bill.receiveIsName = $scope.item_delivery.receiveIsName;

            if($scope.item_delivery.image !== ''){
                if($scope.item_delivery.receiveIsName !== ''){
                    if($scope.item_delivery.Status !== undefined){
                        var controller = requestApiHelper.ASSIGN.UPDATE_DELIVERY;
                        var param = {data: $scope.bill};
                        baseService.POST(controller, param).then(function(respone){                         
                            if (respone){
                                baseService.showToast('Hoàn thành phát hàng!', 'success');
                                $('.modal').modal('hide');
                                $location.path("admin/delivery-assign");       
                            }
                            else console.log('Không cập nhật được đơn hàng');            
                        }, function(error){
                            console.log(error);
                        });
                    }else{
                        $scope.notice.class = 'right text-danger';
                        $scope.notice.message = 'Vui lòng chọn trạng thái giao hàng!';
                    }
                }else{
                    $scope.notice.class = 'right text-danger';
                    $scope.notice.message = 'Vui lòng cập nhật tên người nhận hàng!';
                }
            }else{
                $scope.notice.class = 'right text-danger';
                $scope.notice.message = 'Vui lòng cập nhật hình ảnh giao hàng!';
            }
        }

        $scope.event = {       
            browserImg: function(){
                CKFinder.popup({
                    chooseFiles: true,  
                    selectActionFunction: function(url){
                        var newUrl = baseService.getImageUrl(url);
                        $('#img').attr('src',url);
                        $('#txt_url').val(newUrl);
                        $scope.item_delivery.image = newUrl;
                    }
                });
            },
        };

        function getServices() {
            var controller = requestApiHelper.SERVICE.GET + '/' + $scope.filterLang;
            var param = {type: 'admin'};
            baseService.POST(controller, param).then(function(response){
                $scope.services = response; 
            }, function(err){
                console.log(err);
            });
        };

        function getPlusServices() {
            var controller = requestApiHelper.PLUS_SERVICE.GET;            
            baseService.GET(controller).then(function(response){
                $scope.plusServices = response;
            }, function(err){
                console.log(err);
            });
        };
}]);
