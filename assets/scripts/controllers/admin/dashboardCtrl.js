angular.module('vpApp').controller('dashboardCtrl',[
    '$scope', '$rootScope', '$window', 'baseService', 'requestApiHelper', 
    function($scope, $rootScope, $window, baseService, requestApiHelper){
    $rootScope.currentPage = {
        parent: 'Bảng tin',
        child: '',
        class: 'fa fa-dashboard'
    };    
    if($rootScope.user.IdUser !== undefined){
        $rootScope.dashboardStatistic();  
        
    }
    $scope.filterLang = 'vi';
        $scope.isUpdate = false;
        $scope.numberPage = '10';
        $scope.numOfPage = 5;
        $scope.label = {
            title: ''        
        };
        $scope.notice = {
            class: '',
            message: ''
        };
        $scope.enableList = [];
        $scope.selectedEnable = {};
        function init(){
            var controller = requestApiHelper.ENABLE.ALL;
            baseService.GET(controller).then(function(response){
                console.log(response);
                $scope.enableList = response; 
            }, function(err){
                console.log(err);
            });
        };
        $rootScope.getLanguages();
        getMasterPlanType();
        init();    
        $rootScope.event = {
            edit: function(item){
                $scope.label.title = 'Sửa thông tin';
                $scope.isUpdate = true;
                $scope.selectedEnable = {};            
                $scope.selectedEnable = item;             
                $scope.notice.class = '';
                $scope.notice.message = '';  
            },
            add: function(){
                $scope.label.title = 'Thêm mặt bằng';
                $scope.isUpdate = false;
                $scope.selectedEnable = {
                    image: '',
                    floor_name: '',
                    name: '',
                    description: '',
                    path_name: '',
                    sort: 0,
                    Status: 1
                };
                $('#img').attr('src', '');
                $scope.notice.class = '';
                $scope.notice.message = '';  
            },
            submitForm: function(){
                //var noimage = 'assets/includes/upload/images/users/noimage.jpg';<span class="char1">G</span>
                if($scope.Form.$valid){
                    if($scope.isUpdate){
                        if($scope.selectedEnable.enable == "0"){
                            $scope.selectedEnable.enable = 0;
                        } else{
                            $scope.selectedEnable.enable = 1;
                        }
                        var controller = requestApiHelper.ENABLE.UPDATE_ENABLE;
                        baseService.POST(controller, $scope.selectedEnable).then(function(response){
                            if(response){
                                init();
                                $('#myModal').modal('hide');  
                                baseService.showToast('Cập nhật thành công!', 'success');
                            }  
                            else{
                                $scope.notice.class = 'text-danger';
                                $scope.notice.message = 'Cập nhật thất bại!';
                            }
                        }, function(err){                        
                            console.log(err);
                        });
                    }
                    else{
                        var controller = requestApiHelper.MASTER_PLAN.ADD_MASTER_PLAN;
                       // $scope.selectedEnable.image = ($scope.selectedEnable.Avatar === '')? noimage : $scope.selectedEnable.image;
                        baseService.POST(controller, $scope.selectedEnable).then(function(response){
                            if(response.redirect !== undefined){
                                $window.location.href = response.redirect;
                            }
                            else if(response === 'true'){
                                init();
                                $('#myModal').modal('hide');  
                                baseService.showToast('Thêm thành công!', 'success');
                            } 
                            else{
                                $scope.notice.class = 'text-danger';
                                $scope.notice.message = 'Thêm thất bại!';
                            }
                        }, function(err){                        
                            console.log(err);
                        });
                    }
                }          
            },
            del: function(){                        
                var controller = requestApiHelper.MASTER_PLAN.DELETE_MASTER_PLAN;
                var param = {id: $scope.selectedEnable.id};
                baseService.POST(controller, param).then(function(response){
                    if(response.redirect !== undefined){
                        $window.location.href = response.redirect;
                    }
                    else if(response === 'true'){
                        init();
                        $('#delAlert').modal('hide');  
                        baseService.showToast('Xóa thành công!', 'success');
                    } 
                    else{
                        baseService.showToast('Xóa thất bại!', 'danger');
                    }
                }, function(err){                
                    console.log(err);
                })
            },
            getItem: function(item){
                $scope.selectedEnable = {};                   
                $scope.selectedEnable = item;                     
            },
            browserImg: function(){
                CKFinder.popup({
                    chooseFiles: true,  
                    selectActionFunction: function(url){
                        var newUrl = baseService.getImageUrl(url);
                        $('#img').attr('src',url);
                        $('#txt_url').val(newUrl);
                        $scope.selectedEnable.image = newUrl;
                    }
                });
            },
            
        };
        function getMasterPlanType() {
            var controller = requestApiHelper.MASTER_PLAN_TYPE.ALL;
            baseService.GET(controller).then(function(response){
                $scope.masterplantype = response; 
            }, function(err){
                console.log(err);
            });
        };
}]);