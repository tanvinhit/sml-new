angular.module('vpApp').controller('resourceManageCtrl',['$scope', '$rootScope', '$window', '$sce', 'baseService', 'requestApiHelper', function ($scope, $rootScope, $window, $sce, baseService, requestApiHelper) {
    $rootScope.currentPage = {
        parent: 'Tài nguyên',
        child: '',
        class: 'fa fa-folder-o'
    };
    $scope.label = {
        title: ''
    }
    $scope.typeArticle = {
        product: 2,
        article: 1
    };
    $scope.resourceCate = [];
    $scope.resourceType = [];
    $scope.resources = [];
    $scope.notice = {};
    $scope.isUpdate = false;
    $scope.trustAsHtml = $sce.trustAsHtml;
    $scope.filterLang = 'vi';
    $scope.resType = 0;
    $scope.resCate = 0;
    $scope.selectedRes = {};
    $scope.selectedCat = {};
    function getResources(){
        $scope.notice = {
            class: '',
            message: ''
        }
        var controller = requestApiHelper.RESOURCE.GET + '/' + $scope.filterLang;
        var param = {type: 'admin', resourceCate: $scope.resCate};
        baseService.POST(controller, param).then(function(response){
            $scope.resources = response;
        }, function(err){
            console.log(err);
        });
    };
    function getResourceCate(){
        $scope.notice = {
            class: '',
            message: ''
        }
        var controller = requestApiHelper.RESOURCE.GET_CATE + '/' + $scope.filterLang;
        var param = {
            type: 'admin',
            resourceType: $scope.resType
        };
        baseService.POST(controller, param).then(function (response) {
            $scope.resourceCate = response;
            $scope.resCate = (response.length > 0)? response[0]['ID'] : 0;
            getResources();
        }, function(err){
            console.log(err);
        });
    };    
    function init() {
        var controller = requestApiHelper.RESOURCE.GET_TYPE;
        baseService.GET(controller).then(function (response) {
            $scope.resourceType = response;
            $scope.resType = response[0]['TypeID'];
            getResourceCate();
        }, function(err){
            console.log(err);
        });        
    };    
    init();
    $rootScope.getLanguages();
    $rootScope.getCategories($scope.filterLang, $scope.typeArticle.product);
    $rootScope.getArticles($scope.filterLang, $scope.typeArticle.product);
    $scope.event = {
        addCat: function(){
            $scope.label.title = 'Add Category';  
            $scope.selectedCat = {
                Type: $scope.resType,
                Language: $scope.filterLang
            };
            $scope.isUpdate = false;                        
        },
        editCat: function(item){
            $scope.label.title = 'Edit Category';
            $scope.selectedCat = {};
            $scope.selectedCat = item;
            $('#catModal').modal('show');
            $scope.isUpdate = true;            
            $('#catModal').modal('show');
        },
        getCatItem: function(item){
            $scope.selectedCat = {};
            $scope.selectedCat = item;  
            $('#delCatAlert').modal('show');
        },
        delCat: function(){
            var controller = requestApiHelper.RESOURCE.DELETE_CATE;    
            var param = {ID: $scope.selectedCat.ID};
            baseService.POST(controller, param).then(function(response){
                if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }                        
                else if(response === 'true'){
                    getResourceCate();       
                    $('#delCatAlert').modal('hide');
                    baseService.showToast('Delete successful!', 'success');
                }
                else{
                    baseService.showToast('Delete fail!', 'danger');
                }
            }, function(err){
                baseService.showToast('Delete fail!', 'danger');
                console.log(err);
            });
        },
        submitCatForm: function(){
            if($scope.catForm.$valid){
                if($scope.isUpdate){
                    var controller = requestApiHelper.RESOURCE.UPDATE_CATE;                    
                    baseService.POST(controller, $scope.selectedCat).then(function(response){
                        if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }                        
                        else if(response === 'true'){
                            getResourceCate();
                            $('#catModal').modal('hide');
                            baseService.showToast('Update successful!', 'success');
                        }
                        else{
                            $scope.notice.class = "text-danger";
                            $scope.notice.message = 'Update fail!';
                        }
                    }, function(err){
                        baseService.showToast('Update fail!', 'danger');
                        console.log(err);
                    });
                }
                else{
                    var controller = requestApiHelper.RESOURCE.ADD_CATE;                    
                    baseService.POST(controller, $scope.selectedCat).then(function(response){
                        if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }                        
                        else if(response === 'true'){
                            getResourceCate();
                            $('#catModal').modal('hide');
                            baseService.showToast('Add successful!', 'success');
                        }
                        else{
                            $scope.notice.class = "text-danger";
                            $scope.notice.message = 'Add fail!';
                        }
                    }, function(err){
                        baseService.showToast('Add fail!', 'danger');
                        console.log(err);
                    });
                }
            }  
        },
        add: function(){
            $scope.label.title = 'Add Resource';  
            $scope.selectedRes = {
                ResCate: $scope.resCate,
                ResLang: $scope.filterLang,
                CateLink: '0',
                ProLink: '0'
            };
            $scope.isUpdate = false;
            $('#resImg').attr('src', '');            
        },
        edit: function(item){
            $scope.label.title = 'Edit Resource';
            $scope.selectedRes = {};
            $scope.selectedRes = item;
            $('#resModal').modal('show');
            $scope.isUpdate = true;            
        },
        getItem: function(item){
            $scope.selectedRes = {};
            $scope.selectedRes = item;  
        },
        del: function(){
            var controller = requestApiHelper.RESOURCE.DELETE;    
            var param = {ResID: $scope.selectedRes.ResID};
            baseService.POST(controller, param).then(function(response){
                if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }                        
                else if(response === 'true'){
                    getResources();       
                    $('#delAlert').modal('hide');
                    baseService.showToast('Delete successful!', 'success');
                }
                else{
                    baseService.showToast('Delete fail!', 'danger');
                }
            }, function(err){
                baseService.showToast('Delete fail!', 'danger');
                console.log(err);
            });
        },
        submitForm: function(){
            if($scope.Form.$valid){
                if($scope.isUpdate){
                    var controller = requestApiHelper.RESOURCE.UPDATE;                    
                    baseService.POST(controller, $scope.selectedRes).then(function(response){
                        if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }                        
                        else if(response === 'true'){
                            getResources();
                            $('#resModal').modal('hide');
                            baseService.showToast('Update successful!', 'success');
                        }
                        else{
                            $scope.notice.class = "text-danger";
                            $scope.notice.message = 'Update fail!';
                        }
                    }, function(err){
                        baseService.showToast('Update fail!', 'danger');
                        console.log(err);
                    });
                }
                else{
                    var controller = requestApiHelper.RESOURCE.ADD;                    
                    baseService.POST(controller, $scope.selectedRes).then(function(response){
                        if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }                        
                        else if(response === 'true'){
                            getResources();
                            $('#resModal').modal('hide');
                            baseService.showToast('Add successful!', 'success');
                        }
                        else{
                            $scope.notice.class = "text-danger";
                            $scope.notice.message = 'Add fail!';
                        }
                    }, function(err){
                        baseService.showToast('Add fail!', 'danger');
                        console.log(err);
                    });
                }
            }  
        },
        selectLang: function (code, id) {
            $scope.filterLang = code;
            getResourceCate();
            $('#tab-' + code).addClass('active');
            for (var i = 0; i < $rootScope.languages.length; i++) {
                var checkClass = $('#tab-' + $rootScope.languages[i].Code).hasClass('active');
                var idLang = $rootScope.languages[i].ID;
                if (checkClass && id !== idLang) {
                    $('#tab-' + $rootScope.languages[i].Code).removeClass('active');
                    break;
                }
            }
        },
        selectResType: function(typeId){
            $scope.resType = typeId;
            getResourceCate();
            for(var i = 0; i < $scope.resourceType.length; i++){
                var li = $('#type-' + $scope.resourceType[i].TypeID).hasClass('active');
                if(li){
                    $('#type-' + $scope.resourceType[i].TypeID).removeClass('active');
                    break;
                }
            }
            $('#type-' + typeId).addClass('active');
        },
        selectResCate: function(cateId){
            $scope.resCate = cateId;
            getResources();    
            for(var i = 0; i < $scope.resourceType.length; i++){
                var li = $('#cat-' + $scope.resourceType[i].TypeID).hasClass('resCate');
                if(li){
                    $('#cat-' + $scope.resourceType[i].TypeID).removeClass('resCate');
                    break;
                }
            }
            $('#cat-' + cateId).addClass('resCate');
        },
        browserImg: function(){
            CKFinder.popup({
                chooseFiles: true,  
                selectActionFunction: function(url){
                    var newUrl = baseService.getImageUrl(url);
                    $('#resImg').attr('src',url);
                    $('#img_url').val(newUrl);
                    $scope.selectedRes.Image = newUrl;
                }
            });
        },
        browserFile: function(){
            CKFinder.popup({
                chooseFiles: true,  
                selectActionFunction: function(url){
                    var newUrl = baseService.getImageUrl(url);                    
                    $('#file_url').val(newUrl);
                    $scope.selectedRes.File = newUrl;
                }
            });
        }        
    };
}]);