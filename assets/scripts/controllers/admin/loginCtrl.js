angular.module('vpApp').controller('loginCtrl',['$scope', '$rootScope', '$window', 'baseService', 'encodingExtensions', function($scope, $rootScope, $window, baseService, encodingExtensions){
       
    $scope.form = {
        Account: '',
        password: '',
        isRemember: true
    };    
    
    $scope.event = {
        submitForm: function(){            
            if($scope.userForm.$valid){       
                var controller = 'Usercontroller/actionLogin';
                var pwd = $('#pwd').val();
                $scope.form.password = encodingExtensions.encrypt(pwd);
                baseService.POST(controller, $scope.form).then(function(response){
                    if(response.state === 'OK'){
                        $window.location.href = response.link;                               
                    }
                    else{                       
                        baseService.showToast(response.message, 'danger');                         
                    }
                }, function(err){
                    baseService.showToast(err.message, 'danger');
                    console.log(err);
                }); 
            }
                        
        }  
    };
}]);