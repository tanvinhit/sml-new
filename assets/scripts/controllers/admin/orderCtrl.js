angular.module('vpApp')
    .controller('orderCtrl',['$scope', '$rootScope', '$location', '$window', 'baseService', 'requestApiHelper', function ($scope, $rootScope, $location, $window, baseService, requestApiHelper) {
        $scope.LIST_PURCHASE = [];
        $scope.item_purchase = {};
        $scope.listToDelete = {
            listBillIds: []
        };
        $scope.numberPage = '10';
        $scope.numOfPage = 5; 
        $rootScope.currentPage = {
            parent: 'Orders',
            child: ''
        };
        function init(){
            baseService.GET(requestApiHelper.ORDER.ALL).then(function (respone) {
                if (respone)
                    $scope.LIST_PURCHASE = respone;
                else console.log('Không có dữ liệu đơn hàng');
            });
        }
        init();

        $scope.detail = function (item) {
            $scope.item_purchase = item;
            baseService.GET(requestApiHelper.ORDER.GET_BY_ID + '/' + item.orderId).then(function (response) {
                $scope.order_detail = [];
                if (response) {
                    $scope.order_detail = response;
                    sumMoney($scope.order_detail);
                } else console.log('Không có dữ liệu');
            });
        };

        $scope.update = function () {
            baseService.POST(requestApiHelper.ORDER.UPDATE, $scope.item_purchase).then(function (respone) {
                if (respone) {
                    $scope.LIST_PURCHASE = respone;
                    var message = 'Cập nhật đơn hàng thành công!';
                    baseService.showToast(message, 'success');
                    $('.modal').modal('hide');
                    init();
                } else {
                    var message = 'Cập nhật đơn hàng không thành công!';
                    baseService.showToast(message, 'danger');
                    $('.modal').modal('hide');
                }
            });
        }
        
        $scope.del = function () {
            baseService.GET(requestApiHelper.ORDER.DELETE + '/' + $scope.item_purchase.orderId).then(function (respone) {
                if (respone) {
                    $scope.LIST_PURCHASE = respone;
                    var message = 'Xóa đơn hàng thành công!';
                    baseService.showToast(message, 'success');
                    $('.modal').modal('hide');
                    init();
                } else {
                    $scope.LIST_PURCHASE = [];
                    var message = 'Xóa đơn hàng không thành công!';
                    baseService.showToast(message, 'danger');
                    $('.modal').modal('hide');
                }
            });
        }

        $scope.checkAll = function(){
            if(selectedAll === 0){
                selectedAll = 1;
                for(var i = 0; i < $scope.LIST_PURCHASE.length; i++){                    
                    $scope.listToDelete.listBillIds.push($scope.LIST_PURCHASE[i].orderId);                
                }            
            }
            else{
                selectedAll = 0;
                for(var i = 0; i < $scope.listToDelete.listBillIds.length; i++){                    
                    $scope.listToDelete.listBillIds.splice(i, 1);                
                } 
            }
        }

        $scope.delAllItem = function(){
            var controller = requestApiHelper.ORDER.DEL_MULTI;
            baseService.POST(controller, $scope.listToDelete.listBillIds).then(function(response){
                if(response === 'true'){
                    baseService.showToast('Xóa nhiều mục thành công!', 'success');
                    $('#delAllAlert').modal('hide');
                    init();
                }
                else if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else{
                    baseService.showToast('Xóa nhiều mục thất bại!', 'danger');
                }
            }, function(err){
                baseService.showToast(err.data.message, 'danger');
            })
        }

        function sumMoney(data) {
            $scope.sum = 0;
            for (var i = 0; i < data.length; i++) {
                $scope.sum += data[i].quantity * data[i].proPrice;
            }
        }

        $scope.appendSelectStatus = function (id) {
            $('#status-' + id).children().remove();
            var html = '<select class="form-group" onchange="angular.element(this).scope().updateStatus(' + id + ', this.value)" onblur="angular.element(this).scope().cancelUpdate(' + id + ')"><option value="">== Chọn tình trạng ==</option><option value="2">Phát thành công</option><option value="1">Đang vận chuyển</option><option value="0">Đang đóng gói</option><option value="-1">Hủy đơn hàng</option><option value="3">Tồn - Thông báo chuyển hoàn</option><option value="4">Hoàn thành công - Chuyển trả người gửi</option></select>';
            $('#status-' + id).append(html);
        }

        $scope.updateStatus = function (id,value) {
            var data = {
                orderId: id,
                status: value
            }
            baseService.POST(requestApiHelper.ORDER.UPDATE_STATUS, data).then(function (response){
                if (response) {
                    var msg = 'Cập nhật tình trạng thành công!';
                    baseService.showToast(msg, 'success');
                    $window.location.reload();
                }
                else{
                    var msg = 'Cập nhật tình trạng không thành công!';
                    baseService.showToast(msg, 'danger');
                }
            })
        }

        $scope.cancelUpdate = function(id){
            $('#status-' + id).children().remove();
            var item = $scope.LIST_PURCHASE.find(function(d){return d.orderId === id});
            if(item !== undefined){
                switch(item.status){
                    case '4': $('#status-' + id).append('<span class="label label-warning">Hoàn thành công - Chuyển trả người gửi</span>');
                        break;
                    case '3': $('#status-' + id).append('<span class="label label-danger">Tồn - Thông báo chuyển hoàn</span>');
                        break;
                    case '2': $('#status-' + id).append('<span class="label label-success">Phát thành công</span>');
                        break;
                    case '1': $('#status-' + id).append('<span class="label label-primary">Đang vận chuyển</span>');
                        break;
                    case '0': $('#status-' + id).append('<span class="label label-warning">Đang đóng gói</span>');
                        break;
                    case '-1': $('#status-' + id).append('<span class="label label-danger">Đã hủy đơn hàng</span>');
                        break;
                }
            }
        }
}]);