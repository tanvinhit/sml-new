angular.module('vpApp').controller('serviceCtrl',['$scope', '$rootScope', '$window', 'baseService', 'requestApiHelper', function($scope, $rootScope, $window, baseService, requestApiHelper){
    $rootScope.currentPage = {
        parent: 'Service',
        child: ''
    };
    $scope.filterLang = 'vi';
    $scope.isUpdate = false;
    $scope.numberPage = '10';
    $scope.numOfPage = 5;
    $scope.label = {
        title: ''        
    };
    $scope.notice = {
        class: '',
        message: ''
    };
    $scope.selectedService = {};
    function init(){
        var controller = requestApiHelper.SERVICE.GET + '/' + $scope.filterLang;
        var param = {type: 'admin'};
        baseService.POST(controller, param).then(function(response){
            $rootScope.serviceList = response; 
        }, function(err){
            console.log(err);
        });
    };
    $rootScope.getLanguages();
    init();    
    $scope.event = {
        edit: function(item){
            $scope.label.title = 'Sửa dịch vụ';
            $scope.isUpdate = true;
            $scope.selectedService = {};            
            $scope.selectedService = item;             
            $scope.notice.class = '';
            $scope.notice.message = '';  
        },
        add: function(){
            $scope.label.title = 'Thêm dịch vụ';
            $scope.isUpdate = false;
            $scope.selectedService = {
                Language: $scope.filterLang
            };
            $('#img').attr('src', '');
            $scope.notice.class = '';
            $scope.notice.message = '';  
        },
        submitForm: function(){
            if($scope.Form.$valid){
                if($scope.isUpdate){
                    var controller = requestApiHelper.SERVICE.UPDATE;
                    $scope.selectedService.Language = $scope.filterLang;
                    baseService.POST(controller, $scope.selectedService).then(function(response){
                        if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }
                        else if(response === 'true'){
                            init();
                            $('#myModal').modal('hide');  
                            baseService.showToast('Cập nhật thành công!', 'success');
                        }  
                        else{
                            $scope.notice.class = 'text-danger';
                            $scope.notice.message = 'Cập nhật thất bại!';
                        }
                    }, function(err){                        
                        console.log(err);
                    });
                }
                else{
                    var controller = requestApiHelper.SERVICE.ADD;
                    $scope.selectedService.Language = $scope.filterLang;
                    baseService.POST(controller, $scope.selectedService).then(function(response){
                        if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }
                        else if(response === 'true'){
                            init();
                            $('#myModal').modal('hide');  
                            baseService.showToast('Thêm thành công!', 'success');
                        } 
                        else{
                            $scope.notice.class = 'text-danger';
                            $scope.notice.message = 'Thêm thất bại!';
                        }
                    }, function(err){                        
                        console.log(err);
                    });
                }
            }          
        },
        del: function(){                        
            var controller = requestApiHelper.SERVICE.DELETE;
            var param = {ID: $scope.selectedService.ID};
            baseService.POST(controller, param).then(function(response){
                if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else if(response === 'true'){
                    init();
                    $('#delAlert').modal('hide');  
                    baseService.showToast('Xóa thành công!', 'success');
                } 
                else{
                    baseService.showToast('Xóa thất bại!', 'danger');
                }
            }, function(err){                
                console.log(err);
            })
        },
        getItem: function(item){
            $scope.selectedService = {};                   
            $scope.selectedService = item;                     
        },        
        browserImg: function(){
            CKFinder.popup({
                chooseFiles: true,  
                selectActionFunction: function(url){
                    var newUrl = baseService.getImageUrl(url);
                    $('#img').attr('src',url);
                    $('#txt_url').val(newUrl);
                    $scope.selectedService.Image = newUrl;
                }
            });
        },
        selectLang: function (code, id) {
            $scope.filterLang = code;            
            init();
            $('#tab-' + code).addClass('active');                                    
            for (var i = 0; i < $rootScope.languages.length; i++) {
                var checkClass = $('#tab-' + $rootScope.languages[i].Code).hasClass('active');
                var idLang = $rootScope.languages[i].ID;
                if (checkClass && id !== idLang) {
                    $('#tab-' + $rootScope.languages[i].Code).removeClass('active');
                    break;
                }
            }            
        }  
    };
}]);