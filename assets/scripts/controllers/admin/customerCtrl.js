angular.module('vpApp').controller('customerCtrl',['$scope', '$rootScope', '$window', 'baseService', 'requestApiHelper', function($scope, $rootScope, $window, baseService, requestApiHelper){
    $scope.numberPage = '10';
    $scope.numOfPage = 5;      
    $scope.selectedCustomer = {}; 
    $scope.filterLang = 'vi';
    $scope.newCustomer = {};
    $scope.notice = {};  
    $scope.listToDelete = [];
    var selectedAll = 0;
    $rootScope.currentPage = {
        parent: 'Customer',
        child: ''
    }; 
    $scope.customers = [];    
    function init(){   
        $scope.newCustomer = {                                    
            Language: $scope.filterLang            
        };
        $scope.notice = {
            class: '',
            message: ''
        };          
        var controller = requestApiHelper.CUSTOMER.GET + '/' + $scope.filterLang;
        var param = {type: 'admin'};
        baseService.POST(controller, param).then(function(response){
            $rootScope.customerList = response; 
        }, function(err){
            console.log(err);
        });        
    };      
    $rootScope.getLanguages();
    init();    
    $('.collapse-link:not(.binded)').addClass("binded").click( function() {
        var ibox = $(this).closest('div.ibox');
        var button = $(this).find('i');
        var content = ibox.find('div.ibox-content');
        content.slideToggle(200);
        button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        ibox.toggleClass('').toggleClass('border-bottom');
        setTimeout(function () {
            ibox.resize();
            ibox.find('[id^=map-]').resize();
        }, 50);
    });
    $scope.event = {
        edit: function(item){                        
            $scope.selectedCustomer = {};            
            $scope.selectedCustomer = item;  
            $('#img-update').attr('src', '');          
        },        
        submitForm: function(isUpdate){
            if(isUpdate){   
                if($scope.Form.$valid){
                    var controller = requestApiHelper.CUSTOMER.UPDATE;  
                    $scope.selectedCustomer.district = angular.uppercase($scope.selectedCustomer.district);                      
                    if($scope.selectedCustomer.$$hashKey !== undefined)
                        delete $scope.selectedCustomer.$$hashKey;
                    baseService.POST(controller, $scope.selectedCustomer).then(function(response){
                        if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }
                        else if(response === 'true'){
                            init(); 
                            $('#myModal').modal('hide');
                            baseService.showToast('Update successful!', 'success');
                        }
                        else{
                            $scope.notice.class = 'left text-danger';
                            $scope.notice.message = 'Update fail!';
                        }
                    }, function(err){
                        $scope.notice.class = 'left text-danger';
                        $scope.notice.message = 'Update fail!';
                        console.log(err.message);
                    });
                }
            }
            else{       
                if($scope.addForm.$valid){
                    var controller = requestApiHelper.CUSTOMER.ADD;
                    $scope.newCustomer.Type = $rootScope.dataType;     
                    $scope.newCustomer.district = angular.uppercase($scope.newCustomer.district);             
                    baseService.POST(controller, $scope.newCustomer).then(function(response){    
                        if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }
                        else if(response === 'true'){
                            init();                            
                            baseService.showToast('Add successful!', 'success');
                        }  
                        else{
                            baseService.showToast('Add fail!', 'danger');
                        }
                    }, function(err){
                        baseService.showToast('Add fail!', 'danger');
                        console.log(err.message);
                    });
                }
            }            
        },
        del: function(){                        
            var controller = requestApiHelper.CUSTOMER.DELETE;            
            var param = {ID : $scope.selectedCustomer.ID};
            baseService.POST(controller, param).then(function(response){
                if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else if(response === 'true'){
                    init();                 
                    $('#delAlert').modal('hide');
                    baseService.showToast('Delete successful!', 'success');
                }
                else{
                    baseService.showToast('Delete fail!', 'danger');
                }
            }, function(err){
                baseService.showToast('Delete fail!', 'danger');
                console.log(err.message);
            });
        },
        getItem: function(item){
            $scope.selectedCustomer = {};
            $scope.selectedCustomer = item;            
        },
        selectLang: function(code, id){    
            $scope.filterLang = code;
            $scope.newCustomer.CatLang = code;
            if(id === 0){                
                init();
                $('#tab0').addClass('active');             
            }
            else{               
                init();
                $('#tab'+id).addClass('active');     
            }
            var checkTabAll = $('#tab0').hasClass('active');
            if(checkTabAll && id !== 0){
                $('#tab0').removeClass('active');
            }
            else{
                for(var i=0;i<$rootScope.languages.length;i++){
                    var checkClass = $('#tab'+$rootScope.languages[i].ID).hasClass('active');
                    var idLang = $rootScope.languages[i].ID;
                    if(checkClass && id !== idLang){
                        $('#tab'+$rootScope.languages[i].ID).removeClass('active');
                        break;
                    }
                }
            }
        },
        checkAll: function(){
            if(selectedAll === 0){
                selectedAll = 1;
                for(var i = 0; i < $scope.customers.length; i++){                    
                    $scope.listToDelete.push($scope.customers[i].ID);                
                }            
            }
            else{
                selectedAll = 0;
                for(var i = 0; i < $scope.listToDelete.length; i++){                    
                    $scope.listToDelete.splice(i, 1);                
                } 
            }
        },
        delAllItem: function(){
            var controller = requestApiHelper.CUSTOMER.DEL_MULTI;
            baseService.POST(controller, $scope.listToDelete).then(function(response){
                if(response === 'true'){
                    baseService.showToast('Xóa nhiều mục thành công!', 'success');
                    $('#delAllAlert').modal('hide');
                    init();
                }
                else if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else{
                    baseService.showToast('Xóa nhiều mục thất bại!', 'danger');
                }
            }, function(err){
                baseService.showToast(err.data.message, 'danger');
            })
        },
        browserImg: function(isUpdate){
            CKFinder.popup({
                chooseFiles: true,  
                selectActionFunction: function(url){
                    var newUrl = baseService.getImageUrl(url);
                    if(!isUpdate) {
                        $('#img').attr('src',url);                    
                        $scope.newCustomer.Image = newUrl;
                    }
                    else {
                        $('#img-update').attr('src',url);                    
                        $scope.selectedCustomer.Image = newUrl;
                    }
                }                
            });
        }  
    };

    $scope.getDistrictNewCustomerById = (districtId) => {
        if(!!districtId){
            var controller = requestApiHelper.ADDRESS.GET_DISTRICTS_BY_ID;
            baseService.GET(controller.concat('/', angular.uppercase(districtId))).then(function(response) {
                if(!!response){
                    let district = response[0];
                    if(district === undefined){
                        baseService.showToast('Mã quận huyện chưa đúng!', 'danger');
                    }else{
                        $scope.newCustomer.province = district.MATINH;
                        $scope.newCustomer.districtDetail = (district.TEN_TINH + ', ' + district.TEN_QUANHUYEN);  
                    }
                                                                  
                }
            }, function(err){
                baseService.showToast('Mã quận huyện chưa đúng!', 'danger');
            });
        }
    };
    $scope.getDistrictCustomerById = (districtId) => {
        if(!!districtId){
            var controller = requestApiHelper.ADDRESS.GET_DISTRICTS_BY_ID;
            baseService.GET(controller.concat('/', angular.uppercase(districtId))).then(function(response) {
                if(!!response){
                    let district = response[0];
                    if(district === undefined){
                        baseService.showToast('Mã quận huyện chưa đúng!', 'danger');
                    }else{
                        $scope.selectedCustomer.province = district.MATINH;
                        $scope.selectedCustomer.districtDetail = (district.TEN_TINH + ', ' + district.TEN_QUANHUYEN);   
                    }                                             
                }
            }, function(err){
                console.log(err);
            });
        }
    }
}]);