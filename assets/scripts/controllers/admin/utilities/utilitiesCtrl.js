angular.module('vpApp').controller('utilitiesCtrl',['$scope', '$rootScope', '$window', 'baseService', 'requestApiHelper', function($scope, $rootScope, $window, baseService, requestApiHelper){
    $rootScope.currentPage = {
        parent: 'Trang Nội dung',
        child: ''
    };
    $scope.filterLang = 'vi';
    $scope.isUpdate = false;
    $scope.numberPage = '10';
    $scope.numOfPage = 5;
    $scope.label = {
        title: ''        
    };
    $scope.notice = {
        class: '',
        message: ''
    };
    $scope.utilList = [];
    $scope.selectedUtil = {};
    function init(){
        var controller = requestApiHelper.UTILITIES.ALL;
        baseService.GET(controller).then(function(response){
            $scope.utilList = response; 
        }, function(err){
            console.log(err);
        });
    };
    $rootScope.getLanguages();
    init();    
    $scope.event = {
        edit: function(item){
            $scope.label.title = 'Sửa thông tin';
            $scope.isUpdate = true;
            $scope.selectedUtil = {};            
            $scope.selectedUtil = item;             
            $scope.notice.class = '';
            $scope.notice.message = '';  
        },
        submitForm: function(){
            //var noimage = 'assets/includes/upload/images/users/noimage.jpg';<span class="char1">G</span>
            if($scope.Form.$valid){
                if($scope.isUpdate){
                    var controller = requestApiHelper.UTILITIES.UPDATE_UTILITIES;
                    baseService.POST(controller, $scope.selectedUtil).then(function(response){
                        if(response){
                            init();
                            $('#myModal').modal('hide');  
                            baseService.showToast('Cập nhật thành công!', 'success');
                        }  
                        else{
                            $scope.notice.class = 'text-danger';
                            $scope.notice.message = 'Cập nhật thất bại!';
                        }
                    }, function(err){                        
                        console.log(err);
                    });
                }
            }          
        },
        del: function(){                        
            var controller = requestApiHelper.MASTER_PLAN.DELETE_MASTER_PLAN;
            var param = {id: $scope.selectedUtil.id};
            baseService.POST(controller, param).then(function(response){
                if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else if(response === 'true'){
                    init();
                    $('#delAlert').modal('hide');  
                    baseService.showToast('Xóa thành công!', 'success');
                } 
                else{
                    baseService.showToast('Xóa thất bại!', 'danger');
                }
            }, function(err){                
                console.log(err);
            })
        },
        getItem: function(item){
            $scope.selectedUtil = {};                   
            $scope.selectedUtil = item;                     
        },
        browserBack: function(){
            CKFinder.popup({
                chooseFiles: true,  
                selectActionFunction: function(url){
                    var newUrl = baseService.getImageUrl(url);
                    $('#img').attr('src',url);
                    $('#txt_url').val(newUrl);
                    $scope.selectedUtil.background = newUrl;
                }
            });
        },
        
    };
}]);