angular.module('vpApp').controller('scriptCtrl',['$scope', '$rootScope', '$window', 'baseService', 'requestApiHelper', function($scope, $rootScope, $window, baseService, requestApiHelper){
    $rootScope.currentPage = {
        parent: 'Thêm script cho website',
        child: ''
    };
    $scope.filterLang = 'vi';
    $scope.isUpdate = false;
    $scope.numberPage = '10';
    $scope.numOfPage = 5;
    $scope.label = {
        title: ''        
    };
    $scope.notice = {
        class: '',
        message: ''
    };
    $scope.scriptList = [];
    $scope.selectedScript = {};
    function init(){
        var controller = requestApiHelper.SCRIPT.ALL;
        baseService.GET(controller).then(function(response){
            $scope.scriptList = response; 
        }, function(err){
            console.log(err);
        });
    };
    $rootScope.getLanguages();
    getMasterPlanType();
    init();    
    $scope.event = {
        edit: function(item){
            $scope.label.title = 'Sửa thông tin';
            $scope.isUpdate = true;
            $scope.selectedScript = {};            
            $scope.selectedScript = item;             
            $scope.notice.class = '';
            $scope.notice.message = '';  
        },
        submitForm: function(){
            if($scope.isUpdate){
                var controller = requestApiHelper.SCRIPT.UPDATE_SCRIPT;
                baseService.POST(controller, $scope.selectedScript).then(function(response){
                    if(response){
                        init();
                        $('#myModal').modal('hide');  
                        baseService.showToast('Cập nhật thành công!', 'success');
                    }  
                    else{
                        $scope.notice.class = 'text-danger';
                        $scope.notice.message = 'Cập nhật thất bại!';
                    }
                }, function(err){                        
                    console.log(err);
                });
            }         
        },
        getItem: function(item){
            $scope.selectedScript = {};                   
            $scope.selectedScript = item;                     
        },
    };
}]);