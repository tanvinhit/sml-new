angular.module('vpApp').controller('userCtrl',['$scope', '$rootScope', '$window', 'baseService', 'encodingExtensions', 'requestApiHelper', function($scope, $rootScope, $window, baseService, encodingExtensions, requestApiHelper){
    $scope.isUpdate = false;
    $scope.numberPage = '10';
    $scope.usersList = [];
    $scope.numOfPage = 5;
    $scope.label = {
        title: ''        
    }; 
    $scope.permissions = [
        {IdLevel: 0, LevelName: 'Admin'},
        {IdLevel: 1, LevelName: 'Tổng bưu cục'},
    ];    
    $scope.notice = {};
    $rootScope.currentPage = {
        parent: 'Member',
        child: '',
        class: 'fa fa-users'
    };
    var email = '';
    var Account = '';
    $scope.selectedUser = {};          
    $scope.checkExistsEmail = false;               
    function init(){     
        $scope.notice = {
            class: '',
            message: ''
        };
        var controller = requestApiHelper.USER.ALL;
        var param = {type: 'BQT'};
        baseService.POST(controller, param).then(function(response){                         
            $scope.usersList = response;              
        }, function(error){
            console.log(error);
        });
    };            
    init();      
    function checkEmail(){
        var controller = requestApiHelper.USER.CHECK_MAIL;
        var param = {email: $scope.selectedUser.Email};
        baseService.POST(controller, param).then(function(response){
            if(response === '1'){
                $scope.checkExistsEmail = true;                        
            }
            else{
                $scope.checkExistsEmail = false;                        
            }
        }, function(err){
            console.log(err.message);
        });
    };
    function checkAccount(){
        var controller = requestApiHelper.USER.CHECK_ACCOUNT;
        var param = {Account: $scope.selectedUser.Account};
        baseService.POST(controller, param).then(function(response){
            if(response === '1'){
                $scope.checkExistsAccount = true;                        
            }
            else{
                $scope.checkExistsAccount = false;                        
            }
        }, function(err){
            console.log(err.message);
        });
    };
    $scope.event = {
        edit: function(item){
            $scope.label.title = 'Edit Member';
            $scope.isUpdate = true;
            $scope.selectedUser = {};            
            $scope.selectedUser = item; 
            email = item.Email;      
            Account = item.Account;        
        },
        add: function(){
            $scope.label.title = 'Add Member';
            $scope.isUpdate = false;
            $scope.selectedUser = {                
                Avatar: '',
                Phone: '',
                Status: '1',
                IsContact: '0'
            };       
            $('#img').attr('src', '');
            email = ''; 
            Account= '';            
        },
        submitForm: function(){
            var noimage = 'assets/includes/upload/images/users/noimage.jpg';
            if(!$scope.checkExistsEmail  && !$scope.checkExistsAccount){
                if($scope.Form.$valid){
                    if($scope.isUpdate){                                        
                        var controller = requestApiHelper.USER.UPDATE;                        
                        $scope.selectedUser.Avatar = ($scope.selectedUser.Avatar === '')? noimage : $scope.selectedUser.Avatar;
                        if($scope.selectedUser.Password !== undefined && $scope.selectedUser.Password !== '')
                            $scope.selectedUser.Password = encodingExtensions.encrypt($scope.selectedUser.Password);
                        if($scope.selectedUser.$$hashKey !== undefined)
                            delete $scope.selectedUser.$$hashKey;
                        baseService.POST(controller, $scope.selectedUser).then(function(response){
                            if(response.redirect !== undefined){
                                $window.location.href = response.redirect;
                            }
                            else if(response[0] !== 'null'){
                                init(); 
                                $('#myModal').modal('hide');
                                if(response[0].IdUser === $rootScope.user){
                                    $rootScope.user = response[0];
                                }
                                baseService.showToast('Update successful!', 'success');
                            }
                            else{
                                $scope.notice.class = 'left text-danger';
                                $scope.notice.message = 'Update fail!';
                            }
                        }, function(err){
                            $scope.notice.class = 'left text-danger';
                            $scope.notice.message = 'Update fail!';
                            console.log(err);
                        });
                    }
                    else{
                        if($scope.selectedUser.Password === $scope.selectedUser.ConfirmPassword){
                            var controller = requestApiHelper.USER.ADD;                            
                            $scope.selectedUser.Avatar = ($scope.selectedUser.Avatar === '')? noimage : $scope.selectedUser.Avatar;
                            $scope.selectedUser.Password = encodingExtensions.encrypt($scope.selectedUser.Password);                            
                            if($scope.selectedUser.$$hashKey !== undefined)
                                delete $scope.selectedUser.$$hashKey;
                            delete $scope.selectedUser.ConfirmPassword;
                            baseService.POST(controller, $scope.selectedUser).then(function(response){  
                                if(response.redirect !== undefined){
                                    $window.location.href = response.redirect;
                                }
                                else if(response === 'true'){
                                    init(); 
                                    $('#myModal').modal('hide');
                                    baseService.showToast('Add successful!', 'success');
                                }  
                                else{
                                    $scope.notice.class = 'left text-danger';
                                    $scope.notice.message = 'Add fail!';
                                }
                            }, function(err){
                                $scope.notice.class = 'left text-danger';
                                $scope.notice.message = 'Add fail!';
                                console.log(err);
                            });
                        }
                        else{                                    
                            $scope.notice.class = 'left text-danger';
                            $scope.notice.message = 'Password is not match!';
                        }
                    }
                }
            }            
        },
        del: function(){                        
            var controller = requestApiHelper.USER.DELETE;            
            var param = {'userId' : $scope.selectedUser.UserID};
            baseService.POST(controller, param).then(function(response){
                if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else if(response === 'true'){
                    init();                    
                    $('#delAlert').modal('hide');
                    baseService.showToast('Delete successful!', 'success');
                }
                else{
                    $('#delAlert').modal('hide');
                    baseService.showToast('Delete fail!', 'danger');
                }
            }, function(err){
                baseService.showToast('Delete fail!', 'danger');
                console.log(err);
            });
        },
        getItem: function(item){
            $scope.selectedUser = {};                   
            $scope.selectedUser = item;                     
        },        
        browserImg: function(){
            CKFinder.popup({
                chooseFiles: true,  
                selectActionFunction: function(url){
                    var newUrl = baseService.getImageUrl(url);
                    $('#img').attr('src',url);
                    $('#txt_url').val(newUrl);
                    $scope.selectedUser.Avatar = newUrl;
                }
            });
        },
        checkExists: function(){
            if($scope.selectedUser.Email !== undefined && $scope.selectedUser.Email !== ''){
                if($scope.isUpdate){
                    if($scope.selectedUser.Email !== email){
                        checkEmail();
                    }
                }
                else{
                    checkEmail();
                }
            }
        },
        checkAccountExists: function(){
            if($scope.selectedUser.Account !== undefined && $scope.selectedUser.Account !== ''){
                if($scope.isUpdate){
                    if($scope.selectedUser.Account !== Account){
                        checkAccount();
                    }
                }
                else{
                    checkAccount();
                }
            }
        }
    };
}]);
