angular.module('vpApp').controller('navigateCtrl',['$scope', '$rootScope', 'baseService', '$window', 'requestApiHelper', function ($scope, $rootScope, baseService, $window, requestApiHelper) {
    $rootScope.currentPage = {
        parent: 'Menu',
        child: '',
        class: 'fa fa-sitemap'
    };
    $scope.filterLang = 'vi';
    $scope.navigates = '';
    $scope.dataType = {
        article: 1,
        product: 2
    };
    $scope.selected = {};
    var isChanged = false;
    $scope.artCategories = [];
    $scope.proCategories = [];
    $scope.listArticles = [];
    $scope.listProducts = [];
    $scope.listPages = [];
    $scope.selectedNav = {};
    $scope.newNavigate = {
        NavLang: $scope.filterLang
    };
    $scope.nestableNav = [];    
    function getData(){
        var controller = requestApiHelper.CATEGORY.GET + '/' + $scope.filterLang;
        var param = {type: 'admin', dataType: $scope.dataType.article};
        baseService.POST(controller,param).then(function(response){                           
            $scope.artCategories = response;                      
        }, function(error){
            console.log(error.message);
        });
        var controller = requestApiHelper.CATEGORY.GET + '/' + $scope.filterLang;
        var param = {type: 'admin', dataType: $scope.dataType.product};
        baseService.POST(controller,param).then(function(response){                           
            $scope.proCategories = response;                      
        }, function(error){
            console.log(error.message);
        });
        var controller = requestApiHelper.ARTICLE.GET + '/' + $scope.filterLang;
        var param = {type: 'admin'};
        baseService.POST(controller,param).then(function(response){                           
            $scope.listArticles = response;                      
        }, function(error){
            console.log(error.message);
        });
        var controller = requestApiHelper.PRODUCT.GET + '/' + $scope.filterLang;
        var param = {type: 'admin'};
        baseService.POST(controller,param).then(function(response){                           
            $scope.listProducts = response;                      
        }, function(error){
            console.log(error.message);
        });
        var controller = requestApiHelper.TEMPLATE.PAGES;
        var param = {type: 'page', lang: $scope.filterLang};
        baseService.POST(controller,param).then(function(response){                           
            $scope.listPages = response;                      
        }, function(error){
            console.log(error.message);
        });
    };
    function init() {        
        $scope.selected = {
            cates: [],
            articles: [],
            proCates: [],
            products: [],
            pages: []
        };
        var controller = requestApiHelper.NAVIGATE.GET + '/' + $scope.filterLang;
        var param = {
            type: 'admin'
        };
        baseService.POST(controller, param).then(function (response) {
            $scope.navigates = response;
        });        
    };    
    init();  
    getData();
    $rootScope.getLanguages();
    $rootScope.getTemplates();
    $('.collapse-link:not(.binded)').addClass("binded").click(function () {
        var ibox = $(this).closest('div.ibox');
        var button = $(this).find('i');
        var content = ibox.find('div.ibox-content');
        content.slideToggle(200);
        button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        ibox.toggleClass('').toggleClass('border-bottom');
        setTimeout(function () {
            ibox.resize();
            ibox.find('[id^=map-]').resize();
        }, 50);
    });
    function updateOutput(e) {
        var list = e.length ? e : $(e.target),
            output = list.data('output');
        if ($window.JSON) {            
            isChanged = true;
        } else {
            console.log('JSON browser support required for this demo.');
        }
    }; 
    $('#nestable').nestable({
        group: 1
    }).on('change', updateOutput);
    $scope.event = {
        selectLang: function (code, id) {
            $scope.filterLang = code;
            $scope.newNavigate = {
                NavLang: $scope.filterLang
            };
            if (id === 0) {
                init();
                $('#tab0').addClass('active');
            } else {
                init();
                $('#tab' + id).addClass('active');
            }
            var checkTabAll = $('#tab0').hasClass('active');
            if (checkTabAll && id !== 0) {
                $('#tab0').removeClass('active');
            } else {
                for (var i = 0; i < $rootScope.languages.length; i++) {
                    var checkClass = $('#tab' + $rootScope.languages[i].ID).hasClass('active');
                    var idLang = $rootScope.languages[i].ID;
                    if (checkClass && id !== idLang) {
                        $('#tab' + $rootScope.languages[i].ID).removeClass('active');
                        break;
                    }
                }
            }
        },
        slide: function (id) {
            $('#bt-' + id).find('i').toggleClass('fa-caret-down').toggleClass('fa-caret-up');
            $('#form-' + id).slideToggle(200);
        },
        saveChange: function () {
            console.log($window.JSON.stringify($scope.nestableNav));
            $scope.nestableNav = $('#nestable').nestable('serialize');
            var controller = requestApiHelper.NAVIGATE.CHANGE_POSITION;
            baseService.POST(controller, $scope.nestableNav).then(function (response) {
                isChanged = false;
                //init();       
                $scope.selected = {
                    cates: [],
                    articles: [],
                    proCates: [],
                    products: [],
                    pages: []
                };         
                baseService.showToast('Update position successful!', 'success');
            }, function(err){
                baseService.showToast('Update position fail!', 'danger');
                console.log(err);
            });
        },
        addNavigatesList: function (array, type) {
            var controller = requestApiHelper.NAVIGATE.ADD_LIST + '/' + type;
            if(isChanged)
                $scope.event.saveChange();
            baseService.POST(controller, array).then(function (response) {
                if (response.redirect !== undefined) {
                    $window.location.href = response.redirect;
                } else {
                    init();                    
                    baseService.showToast('Add menu successful!', 'success');
                }
            }, function(err){
                baseService.showToast('Add menu fail!', 'danger');
                console.log(err);
            });
        },
        submitForm: function () {
            if ($scope.addForm.$valid) {
                var controller = requestApiHelper.NAVIGATE.ADD_CUSTOM;
                $scope.newNavigate.Type = 1;                
                if(isChanged)
                    $scope.event.saveChange();
                baseService.POST(controller, $scope.newNavigate).then(function (response) {
                    if (response.redirect !== undefined) {
                        $window.location.href = response.redirect;
                    } else {
                        $scope.newNavigate = {};
                        init();
                        baseService.showToast('Add menu sussessful!', 'success');
                    }
                }, function(err){
                    baseService.showToast('Add menu fail!', 'danger');
                    console.log(err);
                });
            }
        },
        update: function (id, name, status, lang, meta = undefined) {
            var controller = requestApiHelper.NAVIGATE.UPDATE;
            if (meta === undefined) {
                var obj = {
                    NavID: id,
                    NavName: name,
                    Status: status,
                    NavLang: lang                    
                };
            } else {
                var obj = {
                    NavID: id,
                    NavName: name,
                    NavMeta: meta,
                    Status: status,
                    NavLang: lang                    
                };
            }
            if(isChanged)
                $scope.event.saveChange();
            baseService.POST(controller, obj).then(function (response) {
                if (response.redirect !== undefined) {
                    $window.location.href = response.redirect;
                } else {
                    $scope.newNavigate = {};
                    //init();
                    $scope.selected = {
                        cates: [],
                        articles: [],
                        proCates: [],
                        products: [],
                        pages: []
                    };
                    baseService.showToast('Update successful!', 'success');
                }
            }, function(err){
                baseService.showToast('Update fail!', 'danger');
                console.log(err);
            });
        },
        delete: function (id, parent, position) {
            $scope.newNavigate = {};
            $scope.newNavigate = {
                NavID: id,
                ParentId: parent,
                Position: position
            };            
            $('#delAlert').modal('show');
        },
        delMenu: function(){
            var controller = requestApiHelper.NAVIGATE.DELETE; 
            if(isChanged)
                $scope.event.saveChange();
            baseService.POST(controller, $scope.newNavigate).then(function (response) {
                if (response.redirect !== undefined) {
                    $window.location.href = response.redirect;
                } else {
                    $scope.newNavigate = {};                    
                    init();
                    $('#delAlert').modal('hide');
                    baseService.showToast('Delete successful!', 'success');
                }
            }, function(err){
                baseService.showToast('Delete fail!', 'danger');
                console.log(err);
            });
        },                
        expandAll: function(){
            $('.dd').nestable('expandAll');
        },
        collapseAll: function(){
            $('.dd').nestable('collapseAll');
        }
    };
    $('#accordion a .panel-heading .panel-title').click(function() {
        var $previous = $('#accordion').find('div.panel.active');     
        if($(this).parent().parent().parent().hasClass('active')) {
            $previous.removeClass('active');
        }else{
            $previous.removeClass('active');
            $(this).parent().parent().parent().stop().addClass('active');        
        }
    });
}]);