angular.module('vpApp').controller('fileCtrl',['$scope', '$rootScope', '$window', 'baseService', 'requestApiHelper', function($scope, $rootScope, $window, baseService, requestApiHelper){
    $rootScope.currentPage = {
        parent: 'Thêm script cho website',
        child: ''
    };
    $scope.filterLang = 'vi';
    $scope.isUpdate = false;
    $scope.numberPage = '10';
    $scope.numOfPage = 5;
    $scope.label = {
        title: ''        
    };
    $scope.notice = {
        class: '',
        message: ''
    };
    $scope.fileList = [];
    $scope.selectedTL = {};
    function init(){
        var controller = requestApiHelper.FILE.ALL;
        baseService.GET(controller).then(function(response){
            $scope.fileList = response; 
        }, function(err){
            console.log(err);
        });
    };
    $rootScope.getLanguages();
    init();    
    $scope.event = {
        edit: function(item){
            $scope.label.title = 'Sửa thông tin';
            $scope.isUpdate = true;
            $scope.selectedTL = {};            
            $scope.selectedTL = item;             
            $scope.notice.class = '';
            $scope.notice.message = '';  
        },
        submitForm: function(){
            if($scope.isUpdate){
                var controller = requestApiHelper.FILE.UPDATE_FILE;
                baseService.POST(controller, $scope.selectedTL).then(function(response){
                    if(response){
                        init();
                        $('#myModal').modal('hide');  
                        baseService.showToast('Cập nhật thành công!', 'success');
                    }  
                    else{
                        $scope.notice.class = 'text-danger';
                        $scope.notice.message = 'Cập nhật thất bại!';
                    }
                }, function(err){                        
                    console.log(err);
                });
            }         
        },
        getItem: function(item){
            $scope.selectedTL = {};                   
            $scope.selectedTL = item;                     
        },
        browserFile: function(){
            CKFinder.popup({
                chooseFiles: true,  
                selectActionFunction: function(url){
                    var newUrl = baseService.getImageUrl(url);
                    $('#txt_url3').val(newUrl);
                    $scope.selectedTL.File = newUrl;
                }
            });
        },
    };
}]);