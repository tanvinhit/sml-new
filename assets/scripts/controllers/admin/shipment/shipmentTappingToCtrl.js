angular.module('vpApp').controller('shipmentTappingToCtrl',['$scope', '$rootScope', '$window', '$location', 'baseService', 'requestApiHelper', function($scope, $rootScope, $window, $location, baseService, requestApiHelper){
    $scope.numberPage = '10';        
    $scope.numOfPage = 5;
    $scope.shipments = [];
    $scope.selectedShipment = {};
    $scope.listToDelete = {
        listPackIds: []
    };
    var selectedAll = 0;
    $scope.isUpdate = false;
    var typeLink = $location.path();
    $rootScope.currentPage = {
        parent: 'Chuyến thư đến',
        child: '',
        class: 'fa fa-edit'
    };         
    function init(){        
        var controller = requestApiHelper.SHIPMENT.GET_TAPPING_TO;
        baseService.GET(controller).then(function(response){                           
            $scope.shipments = response;                     
        }, function(error){
            console.log(error.message);
        });
    };            
    init();                 
    $scope.event = {
        edit: function(x){
            $scope.isUpdate = true;
            $scope.selectedShipment = {};
            $scope.selectedShipment = x;
            $location.path(`admin/tapping-to/shipment/detail/${x.shipmentId}`);
        }        
    };  
}]);