angular.module('vpApp').controller('shipmentCtrl',['$scope', '$rootScope', '$window', '$location', 'baseService', 'requestApiHelper', function($scope, $rootScope, $window, $location, baseService, requestApiHelper){
    $scope.numberPage = '10';        
    $scope.numOfPage = 5;
    $scope.shipments = [];
    $scope.selectedShipment = {};
    $scope.listToDelete = {
        listPackIds: []
    };
    var selectedAll = 0;
    $scope.isUpdate = false;
    var typeLink = $location.path();
    $rootScope.currentPage = {
        parent: 'Chuyến thư',
        child: '',
        class: 'fa fa-edit'
    };         
    function init(){        
        var controller = requestApiHelper.SHIPMENT.GET;
        baseService.GET(controller).then(function(response){                           
            $scope.shipments = response;                     
        }, function(error){
            console.log(error.message);
        });
    };            
    init();                 
    $scope.event = {
        add: function(){
            $scope.isUpdate = false;
            $scope.selectedShipment = {};
            $location.path('admin/shipment/add');
        },
        edit: function(x){
            $scope.isUpdate = true;
            $scope.selectedShipment = {};
            $scope.selectedShipment = x;
            $location.path(`admin/shipment/detail/${x.shipmentId}`);
        },
        del: function(){
            var controller = requestApiHelper.SHIPMENT.DELETE.concat("/", $scope.selectedShipment.shipmentId);
            baseService.GET(controller).then(function(response){
                if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else if(response){
                    init();
                    $('#delAlert').modal('hide');
                    baseService.showToast('Delete successful!', 'success');
                }
                else{
                    baseService.showToast('Delete fail!', 'danger');
                }
            }, function(err){
                console.log(err.message);
                baseService.showToast('Delete fail!', 'danger');
            });
        },
        getItem: function(item){
            $scope.selectedShipment = {};
            $scope.selectedShipment = item;            
        },  
        checkAll: function(){
            if(selectedAll === 0){
                selectedAll = 1;
                for(var i = 0; i < $scope.shipments.length; i++){                    
                    $scope.listToDelete.listPackIds.push($scope.shipments[i].shipmentId);                
                }            
            }
            else{
                selectedAll = 0;
                for(var i = 0; i < $scope.listToDelete.listPackIds.length; i++){                    
                    $scope.listToDelete.listPackIds.splice(i, 1);                
                } 
            }
        },
        delAllItem: function(){
            var controller = requestApiHelper.SHIPMENT.DEL_MULTI;
            baseService.POST(controller, $scope.listToDelete.listPackIds).then(function(response){
                if(response === 'true'){
                    baseService.showToast('Xóa nhiều mục thành công!', 'success');
                    $('#delAllAlert').modal('hide');
                    init();
                }
                else if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else{
                    baseService.showToast('Xóa nhiều mục thất bại!', 'danger');
                }
            }, function(err){
                baseService.showToast(err.data.message, 'danger');
            });
        }
    };  
}]);