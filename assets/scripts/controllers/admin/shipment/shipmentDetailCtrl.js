angular.module('vpApp')
    .controller('shipmentDetailCtrl',['$scope', '$rootScope', '$location', '$routeParams', '$timeout', 'baseService', 'requestApiHelper', function ($scope, $rootScope, $location, $routeParams, $timeout, baseService, requestApiHelper) {
        $scope.meta = $location.path();
        var shipmentId = $routeParams.id;
        $scope.filterLang = "vi";
        $scope.shipment = {};
        $scope.selectedShipment = {};
        $scope.isUpdate = false;
        $scope.fromPlace = '';
        $scope.toPlace = '';
        $scope.shipmentCustomId = '';
        $scope.selectedPackage = "-- Chọn 1 tải --";
        $rootScope.currentPage = {
            parent: 'Chuyến thư chi tiết',
            child: 'Tạo túi thư'
        };
        $scope.listPackages = [];
        $scope.shipmentDetails = [];
        $scope.listToDelete = {
            listShipmentDetails: []
        };
        function init(){
            if($scope.meta.indexOf('add') === -1){
                $scope.isUpdate = true;
                baseService.GET(requestApiHelper.SHIPMENT.GET_BY_ID.concat('/', shipmentId)).then(function (respone) {
                    if (respone){
                        $scope.shipment = respone;
                    }
                    else console.log('Không có dữ liệu chuyến thư');
                });

                getShipmentDetails();
            }
            else{
                $scope.isUpdate = false;
                $scope.shipment = {
                    typeShip: 'Chuyển nhanh'
                };
                $scope.fromPlace = '';
                $scope.toPlace = '';
                $scope.shipmentDetails = [];
            }
        }

        getPackages();
        init();

        $scope.submitForm = function () {
            if($scope.meta.indexOf('add') === -1){
                baseService.POST(requestApiHelper.SHIPMENT.UPDATE, $scope.shipment).then(function (respone) {
                    if (respone) {
                        var message = 'Cập nhật chuyến thư thành công!';
                        baseService.showToast(message, 'success');
                    } else {
                        var message = 'Cập nhật chuyến thư không thành công!';
                        baseService.showToast(message, 'danger');
                    }
                });
            }
            else{
                baseService.POST(requestApiHelper.SHIPMENT.INSERT, $scope.shipment).then(function (respone) {
                    if (respone.success) {
                        var message = 'Thêm chuyến thư thành công!';
                        baseService.showToast(message, 'success');
                        shipmentId = respone.data;
                        $scope.shipmentDetails.forEach(item => {
                            item.shipmentId = shipmentId;
                        });
                        baseService.POST(requestApiHelper.SHIPMENT.ADD_SHIPMENT_DETAIL, $scope.shipmentDetails).then(function (respone) {
                            if (respone.success) {
                                init();
                            } else {
                                var message = 'Cập nhật chuyến thư không thành công!';
                                baseService.showToast(message, 'danger');
                            }
                        });
                    } else {
                        var message = respone.message;
                        baseService.showToast(message, 'danger');
                    }
                });
            }
        };

        $scope.getDistrictFromById = (districtId) => {
            if(!!districtId){
                districtId = districtId.toUpperCase();
                var controller = requestApiHelper.ADDRESS.GET_HUB_BY_ID;
                baseService.GET(controller.concat('/', districtId)).then(function(response) {
                    if(!!response){
                        let district = response[0];
                        $scope.shipment.fromPlaceDetail = district.TNH;
                    }
                }, function(err){
                    console.log(err);
                });
            }
        }

        $scope.getDistrictToById = (districtId) => {
            if(!!districtId){
                districtId = districtId.toUpperCase();
                var controller = requestApiHelper.ADDRESS.GET_HUB_BY_ID;
                baseService.GET(controller.concat('/', districtId)).then(function(response) {
                    if(!!response){
                        let district = response[0];
                        $scope.shipment.toPlaceDetail = district.TNH;
                    }
                }, function(err){
                    console.log(err);
                });
            }
        };

        $scope.onEnterCode = () => {
            $scope.code = baseService.TO_UNSIGN_NO_LOWERCASE($scope.packageCustomId);
    
            let ctrl = requestApiHelper.PACKAGE.GET_BY_CODE;
            baseService.GET(ctrl.concat('/', $scope.packageCustomId)).then((rsp) => {
                if (rsp) {
                    let pack = rsp;
                    $scope.selectPackage(pack);
                }
                else {
                    baseService.showToast("Không tìm thấy tải", "warning");
                }
            }, (err) => {
                console.log(err);
            });
    
            $timeout(function () {
                $scope.packageCustomId = '';
            }, 1000);
        };

        $scope.selectPackage = (x) => {
            $scope.selectedPackage = x.packageCustomId + '-' + x.fromPlaceDetail;
            let details = {
                shipDetailId: 0,
                shipmentId: shipmentId,
                packageId: x.packageId,
                packageCustomId: x.packageCustomId,
                packageDatetime: x.packageDatetime,
                fromPlaceDetail: x.fromPlaceDetail,
                toPlaceDetail: x.toPlaceDetail,
                weight: x.weight
            };
            if($scope.isUpdate){
                let idx = $scope.shipmentDetails.find(function (d) { return d.packageId === details.packageId && d.shipmentId === details.shipmentId });
                if (idx === undefined){
                    let shipDetails = []
                    shipDetails.push(details);
                    baseService.POST(requestApiHelper.SHIPMENT.ADD_SHIPMENT_DETAIL, shipDetails).then(response => {
                        if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }                        
                        if(response.success){
                            getShipmentDetails();
                        }
                        else
                            baseService.showToast(response.message, 'danger');
                    }, err => {
                        baseService.showToast("Xảy ra lỗi", 'danger');
                    });
                }
            }
            else{
                let idx = $scope.shipmentDetails.find(function (d) { return d.packageId === details.packageId && d.shipmentId === details.shipmentId });
                if (idx === undefined)
                    $scope.shipmentDetails.push(details);
            }
        };

        $scope.event = {
            del: function(){
                if($scope.isUpdate){
                    var controller = requestApiHelper.SHIPMENT.DEL_SHIPMENT_DETAIL.concat("/", $scope.selectedShipment.packDetailId);
                    baseService.GET(controller).then(function(response){
                        if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }
                        else if(response){
                            getShipmentDetails();
                            $('#delAlert').modal('hide');
                            baseService.showToast('Delete successful!', 'success');
                        }
                        else{
                            baseService.showToast('Delete fail!', 'danger');
                        }
                    }, function(err){
                        console.log(err.message);
                        baseService.showToast('Delete fail!', 'danger');
                    });
                }
                else {
                    let idx = $scope.shipmentDetails.findIndex(function (d) { return d.packageId === $scope.selectedShipment.packageId });
                    $scope.shipmentDetails.splice(idx, 1);
                    $('#delAlert').modal('hide');
                }
            },
            getItem: function(item){
                $scope.selectedShipment = {};
                $scope.selectedShipment = item;            
            },  
            checkAll: function(){
                if($scope.selectAll){
                    for(let i = 0; i < $scope.shipmentDetails.length; i++){                    
                        $scope.listToDelete.listShipmentDetails.push($scope.shipmentDetails[i]);                
                    }            
                }
                else{                  
                    $scope.listToDelete.listShipmentDetails = [];
                }
            },
            delAllItem: function(){
                if($scope.isUpdate){
                    var controller = requestApiHelper.SHIPMENT.DEL_MULTI_SHIPMENT_DETAIL;
                    baseService.POST(controller, $scope.listToDelete.listShipmentDetails).then(function(response){
                        if(response){
                            $scope.listToDelete.listShipmentDetails = [];
                            baseService.showToast('Xóa nhiều mục thành công!', 'success');
                            $('#delAllAlert').modal('hide');
                            getShipmentDetails();
                        }
                        else if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }
                        else{
                            baseService.showToast('Xóa nhiều mục thất bại!', 'danger');
                        }
                    }, function(err){
                        baseService.showToast(err.data.message, 'danger');
                    });
                }
                else{
                    $scope.listToDelete.listShipmentDetails.forEach(item => {
                        let idx = $scope.shipmentDetails.findIndex(function (d) { return d.packageId === item.packageId });
                        $scope.shipmentDetails.splice(idx, 1);
                    });
                    $scope.listToDelete.listShipmentDetails = [];
                    $('#delAllAlert').modal('hide');
                }
            }
        };

        $scope.onPrint = () => {
            if (!!shipmentId) {
                window.open(
                    `Printcontroller/printShipment/${shipmentId}`,
                    "_blank"
                );
            }    
        }

        function getPackages() {
            baseService.GET(requestApiHelper.PACKAGE.GET).then(function (respone) {
                if (respone){
                    $scope.listPackages = [];
                    $scope.listPackages = respone;
                }
                else 
                    console.log('Không có dữ liệu chuyến thư');
            });
        };

        function getShipmentDetails() {
            baseService.GET(requestApiHelper.SHIPMENT.GET_DETAIL_BY_ID.concat('/', shipmentId)).then(function (respone) {
                if (respone){
                    $scope.shipmentDetails = respone;
                }
                else console.log('Không có dữ liệu chuyến thư');
            });
        }
}]);