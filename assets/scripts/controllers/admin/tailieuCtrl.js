angular.module('vpApp').controller('tailieuCtrl',['$scope', '$rootScope', '$window', 'baseService', 'requestApiHelper', function($scope, $rootScope, $window, baseService, requestApiHelper){
    $rootScope.currentPage = {
        parent: 'Trang Tài liệu',
        child: ''
    };
    $scope.filterLang = 'vi';
    $scope.isUpdate = false;
    $scope.numberPage = '10';
    $scope.numOfPage = 5;
    $scope.label = {
        title: ''        
    };
    $scope.notice = {
        class: '',
        message: ''
    };
    $scope.tlList = [];
    $scope.selectedTL = {};
    function init(){
        var controller = requestApiHelper.TAILIEU.ALL;
        baseService.GET(controller).then(function(response){
            $scope.tlList = response; 
        }, function(err){
            console.log(err);
        });
    };
    $rootScope.getLanguages();
    getMasterPlanType();
    init();    
    $scope.event = {
        edit: function(item){
            $scope.label.title = 'Sửa thông tin';
            $scope.isUpdate = true;
            $scope.selectedTL = {};            
            $scope.selectedTL = item;             
            $scope.notice.class = '';
            $scope.notice.message = '';  
        },
        add: function(){
            $scope.label.title = 'Thêm mặt bằng';
            $scope.isUpdate = false;
            $scope.selectedTL = {
                image: '',
                floor_name: '',
                name: '',
                description: '',
                path_name: '',
                sort: 0,
                Status: 1
            };
            $('#img').attr('src', '');
            $scope.notice.class = '';
            $scope.notice.message = '';  
        },
        submitForm: function(){
            //var noimage = 'assets/includes/upload/images/users/noimage.jpg';<span class="char1">G</span>
            if($scope.Form.$valid){
                if($scope.isUpdate){
                    var controller = requestApiHelper.TAILIEU.UPDATE_TL;
                    $scope.selectedTL.Titledisplay = '';
                    for (var i = 0; i < $scope.selectedTL.Title.length; i++) {
                        var j = i+1;
                        $scope.selectedTL.Titledisplay += '<span class="char' + j + '">' +$scope.selectedTL.Title.charAt(i)+'</span>';
                    }
                    baseService.POST(controller, $scope.selectedTL).then(function(response){
                        if(response){
                            init();
                            $('#myModal').modal('hide');  
                            baseService.showToast('Cập nhật thành công!', 'success');
                        }  
                        else{
                            $scope.notice.class = 'text-danger';
                            $scope.notice.message = 'Cập nhật thất bại!';
                        }
                    }, function(err){                        
                        console.log(err);
                    });
                }
                else{
                    var controller = requestApiHelper.MASTER_PLAN.ADD_MASTER_PLAN;
                   // $scope.selectedTL.image = ($scope.selectedTL.Avatar === '')? noimage : $scope.selectedTL.image;
                    baseService.POST(controller, $scope.selectedTL).then(function(response){
                        if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }
                        else if(response === 'true'){
                            init();
                            $('#myModal').modal('hide');  
                            baseService.showToast('Thêm thành công!', 'success');
                        } 
                        else{
                            $scope.notice.class = 'text-danger';
                            $scope.notice.message = 'Thêm thất bại!';
                        }
                    }, function(err){                        
                        console.log(err);
                    });
                }
            }          
        },
        del: function(){                        
            var controller = requestApiHelper.MASTER_PLAN.DELETE_MASTER_PLAN;
            var param = {id: $scope.selectedTL.id};
            baseService.POST(controller, param).then(function(response){
                if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else if(response === 'true'){
                    init();
                    $('#delAlert').modal('hide');  
                    baseService.showToast('Xóa thành công!', 'success');
                } 
                else{
                    baseService.showToast('Xóa thất bại!', 'danger');
                }
            }, function(err){                
                console.log(err);
            })
        },
        getItem: function(item){
            $scope.selectedTL = {};                   
            $scope.selectedTL = item;                     
        },
        browserImg: function(){
            CKFinder.popup({
                chooseFiles: true,  
                selectActionFunction: function(url){
                    var newUrl = baseService.getImageUrl(url);
                    $('#img').attr('src',url);
                    $('#txt_url').val(newUrl);
                    $scope.selectedTL.Image = newUrl;
                }
            });
            
        },
        browserBG: function(){
            CKFinder.popup({
                chooseFiles: true,  
                selectActionFunction: function(url){
                    var newUrl = baseService.getImageUrl(url);
                    $('#img2').attr('src',url);
                    $('#txt_url2').val(newUrl);
                    $scope.selectedTL.Background = newUrl;
                }
            });
        },
        browserFile: function(){
            CKFinder.popup({
                chooseFiles: true,  
                selectActionFunction: function(url){
                    var newUrl = baseService.getImageUrl(url);
                    $('#txt_url3').val(newUrl);
                    $scope.selectedTL.File = newUrl;
                }
            });
        },
    };
    function getMasterPlanType() {
        var controller = requestApiHelper.MASTER_PLAN_TYPE.ALL;
        baseService.GET(controller).then(function(response){
            $scope.masterplantype = response; 
        }, function(err){
            console.log(err);
        });
    };
}]);