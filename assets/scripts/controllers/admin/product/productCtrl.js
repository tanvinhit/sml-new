angular.module('vpApp').controller('productCtrl',['$scope', '$rootScope', '$window', '$location', 'baseService', 'requestApiHelper', function($scope, $rootScope, $window, $location, baseService, requestApiHelper){
    $scope.numberPage = '10';        
    $scope.numOfPage = 5;        
    $scope.filterLang = 'vi';
    $scope.products = [];
    $scope.selectedPro = {};
    $scope.listToDelete = [];
    var selectedAll = 0;
    $rootScope.currentPage = {
        parent: 'Product',
        child: '',
        class: 'fa fa-edit'
    };               
    function init(){ 
        var controller = requestApiHelper.PRODUCT.GET + '/' + $scope.filterLang;
        var param = {type: 'admin'};
        baseService.POST(controller,param).then(function(response){                           
            $scope.products = response;                     
        }, function(error){
            console.log(error.message);
        });
    };            
    init();
    $rootScope.getLanguages();                     
    $scope.event = {
        edit: function(item){                        
            $scope.selectedPro = {};
            $scope.selectedPro = item;   
            $rootScope.selectLang = $scope.filterLang;
            $location.path('admin/product/edit/' + item.ProID);
        },
        add: function(){                                      
            $rootScope.selectLang = $scope.filterLang;
            $location.path('admin/product/add');
        },
        del: function(){
            var idPro = $scope.selectedPro.ProID;
            var controller = requestApiHelper.PRODUCT.DELETE;
            var param = {
                id: idPro,
                user: $rootScope.user.IdUser
            };
            baseService.POST(controller, param).then(function(response){
                if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else if(response === 'true'){
                    init();
                    $('#delAlert').modal('hide');
                    baseService.showToast('Delete successful!', 'success');
                }
                else{
                    baseService.showToast('Delete fail!', 'danger');
                }
            }, function(err){
                console.log(err.message);
                baseService.showToast('Delete fail!', 'danger');
            });
        },
        getItem: function(item){
            $scope.selectedPro = {};
            $scope.selectedPro = item;            
        },             
        selectLang: function(code, id){    
            $scope.filterLang = code;
            if(id === 0){                
                init();
                $('#tab0').addClass('active');             
            }
            else{               
                init();
                $('#tab'+id).addClass('active');     
            }
            var checkTabAll = $('#tab0').hasClass('active');
            if(checkTabAll && id !== 0){
                $('#tab0').removeClass('active');
            }
            else{
                for(var i=0;i<$rootScope.languages.length;i++){
                    var checkClass = $('#tab'+$rootScope.languages[i].ID).hasClass('active');
                    var idLang = $rootScope.languages[i].ID;
                    if(checkClass && id !== idLang){
                        $('#tab'+$rootScope.languages[i].ID).removeClass('active');
                        break;
                    }
                }
            }
        },
        checkAll: function(){
            if(selectedAll === 0){
                selectedAll = 1;
                for(var i = 0; i < $scope.products.length; i++){                    
                    $scope.listToDelete.push($scope.products[i].ProID);                
                }            
            }
            else{
                selectedAll = 0;
                for(var i = 0; i < $scope.listToDelete.length; i++){                    
                    $scope.listToDelete.splice(i, 1);                
                } 
            }
        },
        delAllItem: function(){
            var controller = requestApiHelper.PRODUCT.DEL_MULTI;
            baseService.POST(controller, $scope.listToDelete).then(function(response){
                if(response === 'true'){
                    baseService.showToast('Xóa nhiều mục thành công!', 'success');
                    $('#delAllAlert').modal('hide');
                    init();
                }
                else if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else{
                    baseService.showToast('Xóa nhiều mục thất bại!', 'danger');
                }
            }, function(err){
                baseService.showToast(err.data.message, 'danger');
            })
        }
    };    
}]);