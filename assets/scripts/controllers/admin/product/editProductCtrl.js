angular.module('vpApp').controller('editProductCtrl',['$scope', '$rootScope', '$window', '$location', '$routeParams', 'baseService', 'requestApiHelper', function($scope, $rootScope, $window, $location, $routeParams, baseService, requestApiHelper){
    $scope.meta = $location.path();
    var ProID = $routeParams.id;
    $scope.options = baseService.ckeditorOptions;
    $scope.selectedPro = {};
    var title = 'Product';    
    $rootScope.currentPage = {
        parent:  title,
        child: ($scope.meta.indexOf('edit') !== -1)? 'Edit ' + title : 'Add ' + title,
        class: 'fa fa-edit'
    };  
    $scope.externalUrl = ''; 
    $scope.cateList = [];
    $rootScope.dataType = 2;
    function concatImageFromArr(arrImg) {
        var result = '';
        for(var i = 0; i < arrImg.length; i++){
            if(i === arrImg.length - 1)
                result += arrImg[i];
            else
                result += arrImg[i] + '|';
        }
        return result;
    };
    function init(){
        $scope.imageList = [];
        if($scope.meta.indexOf('add') !== -1){
            $('#img').attr('src', 'assets/admin/img/no_image.png');                    
            $scope.selectedPro = { 
                CatId: '',
                Author: $rootScope.user.IdUser,
                Username: $rootScope.user.Username,
                DateCreated: baseService.getDatetimeNow(),
                ViewCount: 0,
                ProLang: $rootScope.selectLang,
                Status: '1',                
                ImageList: [],
                Image: 'assets/admin/img/no_image.png'
            };      
        }
        else{
            var controller = requestApiHelper.PRODUCT.EDIT + '/' + ProID;
            baseService.GET(controller).then(function(response){
                $scope.selectedPro = response[0];
                var meta = $scope.selectedPro.ProMeta;
                $scope.selectedPro.ProMeta = meta.substring(0, meta.lastIndexOf('.'));
                if($scope.selectedPro.ImageList !== null && $scope.selectedPro.ImageList !== '' && $scope.selectedPro.ImageList !== undefined)
                    $scope.imageList = $scope.selectedPro.ImageList.split('|');                                
                if($scope.selectedPro.CatId !== null && $scope.selectedPro.CatId !== '' && $scope.selectedPro.CatId !== undefined){
                    $scope.cateList = $scope.selectedPro.CatId.split(',');
                    $scope.cateList.splice(0,1); $scope.cateList.splice($scope.cateList.length-1,1);                    
                }
            }, function(err){
                console.log(err);
            });
        }
    };
    init();
    $rootScope.getCategories($rootScope.selectLang, $rootScope.dataType);        
    $rootScope.getLanguages();
    $rootScope.getTemplates();
    $('.collapse-link:not(.binded)').addClass("binded").click(function () {
        var ibox = $(this).closest('div.ibox');
        var button = $(this).find('i');
        var content = ibox.find('div.ibox-content');
        content.slideToggle(200);
        button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        ibox.toggleClass('').toggleClass('border-bottom');
        setTimeout(function () {
            ibox.resize();
            ibox.find('[id^=map-]').resize();
        }, 50);
    });
    $scope.event = {
        submitForm: function(){            
            if($scope.Form.$valid){
                $scope.selectedPro.ImageList = concatImageFromArr($scope.imageList);                                                
                if($scope.cateList.length > 0){                     
                    $scope.selectedPro.CatId = ',' + $scope.cateList.join(',') + ',';
                }
                if ($scope.selectedPro.Username !== undefined)
                    delete $scope.selectedPro.Username;                  
                if($scope.meta.indexOf('edit') !== -1){
                    var controller = requestApiHelper.PRODUCT.UPDATE;                                        
                    baseService.POST(controller, $scope.selectedPro).then(function(response){
                        if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }
                        else if(response === 'true'){                            
                            //$window.history.back();
                            baseService.showToast('Update successful!', 'success');                       
                        }
                        else{
                            baseService.showToast('Update fail!', 'danger');
                        }
                    }, function(err){                        
                        console.log(err.message);
                        baseService.showToast('Update fail!', 'danger');
                    });
                }
                else{
                    var controller = requestApiHelper.PRODUCT.ADD;                                 
                    baseService.POST(controller, $scope.selectedPro).then(function(response){    
                        if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }
                        else if(response.success){                           
                            //init();
                            $location.path('admin/product/edit/'.concat(response.last));
                            baseService.showToast('Post successful!', 'success');                              
                        }
                        else{
                            baseService.showToast('Post fail!', 'danger');
                        }
                    }, function(err){                        
                        console.log(err.message);
                        baseService.showToast('Post fail!', 'danger');
                    }); 
                }
            }
        },        
        selectedChange: function(){            
            var Alias = '';              
            if($scope.selectedPro.ProName !== undefined || $scope.selectedPro.ProName !== ''){
                Alias = baseService.getAlias($scope.selectedPro.ProName);
                $scope.selectedPro.ProMeta = Alias;
                $scope.selectedPro.SeoTitle = $scope.selectedPro.ProName;
                $scope.selectedPro.SeoKeyword = $scope.selectedPro.ProName;
            }            
        },
        browserImg: function(){
            CKFinder.popup({
                chooseFiles: true,  
                selectActionFunction: function(url){
                    var newUrl = baseService.getImageUrl(url);
                    $('#img').attr('src',url);                    
                    $scope.selectedPro.Image = newUrl;
                }                
            });
        },
        browserImgItem: function () {
            var finder = new CKFinder();
            finder.selectActionFunction = function (url) {
                var length = (baseService.ROOT_URL).length;
                var link = baseService.getImageUrl(url);               
                $scope.imageList.push(link);
                $scope.$apply();
            };
            finder.popup();
        },
        editImg: function (index) {
            var finder = new CKFinder();
            finder.selectActionFunction = function (url) {
                var length = (baseService.ROOT_URL).length;
                var link = baseService.getImageUrl(url);
                $scope.imageList[index] = link;
                document.getElementById('Image' + index).src = $scope.imageList[index];
            };
            finder.popup();
        },
        delImg: function(index){
            $scope.imageList.splice(index, 1);
        },
        addExternalUrl: function () {
            if ($scope.externalUrl !== '') {
                $scope.imageList.push($scope.externalUrl);
                $scope.externalUrl = '';
            } else {
                alert('Vui lòng nhập đường dẫn hình ảnh!');
            }
        }
    };
    $scope.$watch('selectedPro.ProMeta', function(){
        if($scope.selectedPro.ProMeta !== undefined)
            $scope.selectedPro.SeoCanonica = baseService.ROOT_URL.concat($scope.selectedPro.ProMeta);
    });  
}]);