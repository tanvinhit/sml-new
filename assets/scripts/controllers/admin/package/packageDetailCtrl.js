angular.module('vpApp')
    .controller('packageDetailCtrl',['$scope', '$rootScope', '$location', '$routeParams', '$timeout', 'baseService', 'requestApiHelper', function ($scope, $rootScope, $location, $routeParams, $timeout, baseService, requestApiHelper) {
        $scope.meta = $location.path();
        var packageId = $routeParams.id;
        $scope.filterLang = "vi";
        $scope.serviceId = '';
        $scope.package = {};
        $scope.selectedPackDetail = {};
        $scope.isUpdate = false;
        $scope.fromPlace = '';
        $scope.toPlace = '';
        $scope.billCustomId = '';
        $scope.selectedBill = "-- Chọn 1 vận đơn --";
        $rootScope.currentPage = {
            parent: 'Bảng kê chi tiết',
            child: 'Tạo tải'
        };
        $scope.listBills = [];
        $scope.packageDetails = [];
        $scope.listToDelete = {
            listPackageDetails: []
        };
        function init(){
            if($scope.meta.indexOf('add') === -1){
                $scope.isUpdate = true;
                baseService.GET(requestApiHelper.PACKAGE.GET_BY_ID.concat('/', packageId)).then(function (respone) {
                    if (respone){
                        $scope.package = respone;
                    }
                    else console.log('Không có dữ liệu tải');
                });

                getPackageDetails();
            }
            else{
                $scope.isUpdate = false;
                $scope.package = {
                    packageDatetime: baseService.getDatetimeNow(),
                };
                $scope.serviceId = '';
                $scope.fromPlace = '';
                $scope.toPlace = '';
                $scope.packageDetails = [];
            }
        }

        getServices();
        getBills();
        init();

        $scope.submitForm = function () {
            if($scope.meta.indexOf('add') === -1){
                baseService.POST(requestApiHelper.PACKAGE.UPDATE, $scope.package).then(function (respone) {
                    if (respone) {
                        var message = 'Cập nhật tải thành công!';
                        baseService.showToast(message, 'success');
                    } else {
                        var message = 'Cập nhật tải không thành công!';
                        baseService.showToast(message, 'danger');
                    }
                });
            }
            else{
                baseService.POST(requestApiHelper.PACKAGE.INSERT, $scope.package).then(function (respone) {
                    if (respone.success) {
                        var message = 'Thêm tải thành công!';
                        baseService.showToast(message, 'success');
                        packageId = respone.data;
                        $scope.packageDetails.forEach(item => {
                            item.packageId = packageId;
                        });
                        baseService.POST(requestApiHelper.PACKAGE.ADD_PACKAGE_DETAIL, $scope.packageDetails).then(function (respone) {
                            if (respone.success) {
                                init();
                            } else {
                                var message = 'Cập nhật tải không thành công!';
                                baseService.showToast(message, 'danger');
                            }
                        });
                    } else {
                        var message = respone.message;
                        baseService.showToast(message, 'danger');
                    }
                });
            }
        };

        $scope.getDistrictFromById = (districtId) => {
            if(!!districtId){
                districtId = districtId.toUpperCase();
                var controller = requestApiHelper.ADDRESS.GET_HUB_BY_ID;
                baseService.GET(controller.concat('/', districtId)).then(function(response) {
                    if(!!response){
                        let district = response[0];
                        $scope.package.fromPlaceDetail = district.TNH;
                    }
                }, function(err){
                    console.log(err);
                });
            }
        }

        $scope.getDistrictToById = (districtId) => {
            if(!!districtId){
                districtId = districtId.toUpperCase();
                var controller = requestApiHelper.ADDRESS.GET_HUB_BY_ID;
                baseService.GET(controller.concat('/', districtId)).then(function(response) {
                    if(!!response){
                        let district = response[0];
                        $scope.package.toPlaceDetail = district.TNH;
                    }
                }, function(err){
                    console.log(err);
                });
            }
        };

        $scope.onEnterCode = () => {
            $scope.code = baseService.TO_UNSIGN_NO_LOWERCASE($scope.billCustomId);
    
            let ctrl = requestApiHelper.BILL.GET_BY_CODE;
            baseService.GET(ctrl.concat('/', $scope.billCustomId)).then((rsp) => {
                if (rsp) {
                    let bill = rsp;
                    $scope.selectBill(bill);
                }
                else {
                    baseService.showToast("Không tìm thấy vận đơn", "warning");
                }
            }, (err) => {
                console.log(err);
            });
    
            $timeout(function () {
                $scope.billCustomId = '';
            }, 1000);
        };

        $scope.selectBill = (x) => {
            $scope.selectedBill = x.billCustomId + '-' + x.fromPlace + '-' + x.CustomerName;
            let details = {
                packDetailId: 0,
                packageId: packageId,
                billId: x.billId,
                billCustomId: x.billCustomId,
                CustomerName: x.CustomerName,
                createdDate: x.createdDate,
                UserName: x.UserName,
                fromPlace: x.fromPlace,
                provinceTo: x.provinceTo,
                weight: x.weight
            };
            if($scope.isUpdate){
                let idx = $scope.packageDetails.find(function (d) { return d.billId === details.billId && d.packageId === details.packageId });
                if (idx === undefined){
                    let packDetails = []
                    packDetails.push(details);
                    baseService.POST(requestApiHelper.PACKAGE.ADD_PACKAGE_DETAIL, packDetails).then(response => {
                        if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }
                        
                        if(response.success){
                            getPackageDetails();
                        }
                        else
                            baseService.showToast(response.message, 'danger');
                    }, err => {
                        baseService.showToast("Xảy ra lỗi", 'danger');
                    });
                }
            }
            else{
                let idx = $scope.packageDetails.find(function (d) { return d.billId === details.billId && d.packageId === details.packageId });
                if (idx === undefined)
                    $scope.packageDetails.push(details);
            }
        };

        $scope.event = {
            del: function(){
                if($scope.isUpdate){
                    var controller = requestApiHelper.PACKAGE.DEL_PACKAGE_DETAIL.concat("/", $scope.selectedPackDetail.packDetailId);
                    baseService.GET(controller).then(function(response){
                        if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }
                        else if(response){
                            getPackageDetails();
                            $('#delAlert').modal('hide');
                            baseService.showToast('Delete successful!', 'success');
                        }
                        else{
                            baseService.showToast('Delete fail!', 'danger');
                        }
                    }, function(err){
                        console.log(err.message);
                        baseService.showToast('Delete fail!', 'danger');
                    });
                }
                else {
                    let idx = $scope.packageDetails.findIndex(function (d) { return d.billId === $scope.selectedPackDetail.billId });
                    $scope.packageDetails.splice(idx, 1);
                    $('#delAlert').modal('hide');
                }
            },
            getItem: function(item){
                $scope.selectedPackDetail = {};
                $scope.selectedPackDetail = item;            
            },  
            checkAll: function(){
                if($scope.selectAll){
                    for(let i = 0; i < $scope.packageDetails.length; i++){                    
                        $scope.listToDelete.listPackageDetails.push($scope.packageDetails[i]);                
                    }            
                }
                else{                  
                    $scope.listToDelete.listPackageDetails = [];
                }
            },
            delAllItem: function(){
                if($scope.isUpdate){
                    var controller = requestApiHelper.PACKAGE.DEL_MULTI_PACKAGE_DETAIL;
                    baseService.POST(controller, $scope.listToDelete.listPackageDetails).then(function(response){
                        if(response){
                            $scope.listToDelete.listPackageDetails = [];
                            baseService.showToast('Xóa nhiều mục thành công!', 'success');
                            $('#delAllAlert').modal('hide');
                            getPackageDetails();
                        }
                        else if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }
                        else{
                            baseService.showToast('Xóa nhiều mục thất bại!', 'danger');
                        }
                    }, function(err){
                        baseService.showToast(err.data.message, 'danger');
                    });
                }
                else{
                    $scope.listToDelete.listPackageDetails.forEach(item => {
                        let idx = $scope.packageDetails.findIndex(function (d) { return d.billId === item.billId });
                        $scope.packageDetails.splice(idx, 1);
                    });
                    $scope.listToDelete.listPackageDetails = [];
                    $('#delAllAlert').modal('hide');
                }
            }
        };

        $scope.onPrint = () => {
            if (!!packageId) {
                window.open(
                    `Printcontroller/printPackage/${packageId}`,
                    "_blank"
                );
            }    
        }

        function getServices() {
            var controller = requestApiHelper.SERVICE.GET + '/' + $scope.filterLang;
            var param = {type: 'admin'};
            baseService.POST(controller, param).then(function(response){
                $scope.services = response; 
            }, function(err){
                console.log(err);
            });
        };

        function getBills() {
            baseService.GET(requestApiHelper.BILL.ALL).then(function (respone) {
                if (respone){
                    $scope.listBills = [];
                    $scope.listBills = respone;
                }
                else 
                    console.log('Không có dữ liệu đơn hàng');
            });
        };

        function getPackageDetails() {
            baseService.GET(requestApiHelper.PACKAGE.GET_DETAIL_BY_ID.concat('/', packageId)).then(function (respone) {
                if (respone){
                    $scope.packageDetails = respone;
                }
                else console.log('Không có dữ liệu tải');
            });
        }
}]);