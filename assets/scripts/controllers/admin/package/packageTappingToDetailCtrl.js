angular.module('vpApp')
    .controller('packageTappingToDetailCtrl',['$scope', '$rootScope', '$location', '$routeParams', '$timeout', 'baseService', 'requestApiHelper', function ($scope, $rootScope, $location, $routeParams, $timeout, baseService, requestApiHelper) {
        $scope.meta = $location.path();
        var packageId = $routeParams.id;
        $scope.filterLang = "vi";
        $scope.serviceId = '';
        $scope.package = {};
        $scope.selectedPackDetail = {};
        $scope.isUpdate = false;
        $scope.fromPlace = '';
        $scope.toPlace = '';
        $scope.billCustomId = '';
        $scope.selectedBill = "-- Chọn 1 vận đơn --";
        $rootScope.currentPage = {
            parent: 'Tải đến',
            child: 'Chi tiết tải'
        };
        $scope.listBills = [];
        $scope.packageDetails = [];
        $scope.listToDelete = {
            listPackageDetails: []
        };
        function init(){
            $scope.isUpdate = true;
            baseService.GET(requestApiHelper.PACKAGE.GET_BY_ID.concat('/', packageId)).then(function (respone) {
                if (respone){
                    $scope.package = respone;
                }
                else console.log('Không có dữ liệu tải');
            });

            getPackageDetails();
        }

        getServices();
        init();

        $scope.submitForm = function () {
            baseService.POST(requestApiHelper.PACKAGE.UPDATE, $scope.package).then(function (respone) {
                if (respone) {
                    var message = 'Cập nhật tải thành công!';
                    baseService.showToast(message, 'success');
                } else {
                    var message = 'Cập nhật tải không thành công!';
                    baseService.showToast(message, 'danger');
                }
            });
        };

        $scope.event = {
            checkAll: function(){
                if($scope.selectAll){
                    for(let i = 0; i < $scope.packageDetails.length; i++){                    
                        $scope.listToDelete.listPackageDetails.push($scope.packageDetails[i]);                
                    }            
                }
                else{                  
                    $scope.listToDelete.listPackageDetails = [];
                }
            },
            saveAll: function(){
                var controller = requestApiHelper.PACKAGE.TAPPING_TO_ALL_BILL;
                baseService.POST(controller, $scope.listTappingTo.listPackageDetails).then(function(response){
                    if(response){
                        $scope.listTappingTo.listPackageDetails = [];
                        baseService.showToast('Thành công!', 'success');
                        $('#myModal').modal('hide');
                        getShipmentDetails();
                    }
                    else if(response.redirect !== undefined){
                        $window.location.href = response.redirect;
                    }
                    else{
                        baseService.showToast('Thất bại!', 'danger');
                    }
                }, function(err){
                    baseService.showToast(err.data.message, 'danger');
                });
            },
            save: function(packDetailId, isReceived){
                var controller = requestApiHelper.PACKAGE.TAPPING_TO_BILL.concat('/', packDetailId, '/', isReceived);
                baseService.GET(controller).then(function(response){
                    if(response){
                        baseService.showToast('Thành công!', 'success');
                        $('#myModal').modal('hide');
                        getPackageDetails();
                    }
                    else if(response.redirect !== undefined){
                        $window.location.href = response.redirect;
                    }
                    else{
                        baseService.showToast('Thất bại!', 'danger');
                    }
                }, function(err){
                    baseService.showToast(err.data.message, 'danger');
                });
            }
        };

        function getServices() {
            var controller = requestApiHelper.SERVICE.GET + '/' + $scope.filterLang;
            var param = {type: 'admin'};
            baseService.POST(controller, param).then(function(response){
                $scope.services = response; 
            }, function(err){
                console.log(err);
            });
        };

        function getPackageDetails() {
            baseService.GET(requestApiHelper.PACKAGE.GET_DETAIL_BY_ID.concat('/', packageId)).then(function (respone) {
                if (respone){
                    $scope.packageDetails = respone;
                }
                else console.log('Không có dữ liệu tải');
            });
        }
}]);