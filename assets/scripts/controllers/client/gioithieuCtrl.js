angular.module('vpApp').controller('gioithieuCtrl',
['$scope', '$rootScope', '$window', '$sce', 'baseService', 'requestApiHelper', 
function($scope, $rootScope, $window, $sce, baseService, requestApiHelper){    
    $scope.aboutList_page_1_1 = {};
    $scope.aboutList_page_1_2 = {};
    $scope.aboutList_page_1_3 = {};
    $scope.aboutList_page_1_4 = {};
    $scope.aboutList_page_2 = {};
    function init(){
        var controller1 = requestApiHelper.ABOUT.ALL1;
        baseService.GET(controller1).then(function(response1){
            $scope.aboutList_page_1_1  = response1[0];
            $scope.aboutList_page_1_2  = response1[1];
            $scope.aboutList_page_1_3  = response1[2];
            $scope.aboutList_page_1_4  = response1[3];
            console.log($scope.aboutList_page_1);
        }, function(err){
            console.log(err);
        });

        var controller = requestApiHelper.ABOUT.ALL2;
        baseService.GET(controller).then(function(response){
            $scope.aboutList_page_2  = response[0];
            // $scope.aboutList_page_2  = response[1];
        }, function(err){
            console.log(err);
        });
    };
    init();   
}]);