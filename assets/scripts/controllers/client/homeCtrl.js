angular.module("vpApp").controller("homeCtrl", [
  "$scope",
  "$rootScope",
  "baseService",
  "requestApiHelper",
  "$timeout",
  function ($scope, $rootScope, baseService, requestApiHelper,$timeout) {
    $scope.limitArticle = 3;
    $scope.articles = [];
    $scope.contentList_page_2 = {};
    $scope.contentList_page_4 = {};
    $scope.contentList_page_5 = {};
    $scope.contentList_page_7 = {};
    $scope.contactList = {};
    $scope.slideDVPT = [];
    $scope.slideDVTT = [];
    $scope.slideDVPP = [];
    $scope.slide_page_2 = [];
    $scope.slide_page_5 = [];
    $scope.slide_page_7 = [];
    $scope.connectYoutubeDA = {};
    $scope.backgroundHome = {};
    $scope.slogan = {};
    $scope.hinh2 = {};
    $scope.hinh4 = {};
    $scope.hinh5 = {};
    $scope.hinh6 = {};
    $scope.hinh7 = {};
    $scope.videoYT = `
            "<div class="control-youtube">
            <a class="play-button" id="Play" href="javascript:void(0)"></a>
            <a class="pause-button" id="Pause" href="javascript:void(0)"></a>
        </div>
        <div class="bg-video"
            style="background-image: url(assets/client/pictures/catalog/home/home_1_bg.jpg);">
        </div>
        
        <div class="youtube-video" data-embed="https://www.youtube.com/watch?v=uP2bjXWODUQ">
        </div>
        <div class="control">
            <span id="current-time">0:00</span><span>/</span><span id="duration">0:00</span>
            <button id="playpause" type="button" data-state="play"></button>
            <button id="mutetoggle" type="button" data-state="mute"></button>
            <button id="fullscreen" type="button" data-state="go-fullscreen"></button>
        </div>"
            `;
            
    function init() {
      // getConnect();
      // getArticle();
      // getContent();
      // getContact();
      // getDvPhatTrien();
      // getDvTongThau();
      // getDvPhanPhoi();
      // getSildePage2();
      // getSildePage5();
      // getSildePage7();
      // getBackground();
      // getSlogan();
      // getHinh2();
      // getHinh4();
      // getHinh5();
      // getHinh6();
      // getHinh7();
      // $timeout(function() {
      //   load_js();
      // }, 1500);
    }

    function getArticle(limit = $scope.limitArticle) {
      var controller = requestApiHelper.ARTICLE.GET;
      var param = { type: "admin" };
      baseService.POST(controller, param).then(
        function (response) {
          if ((response || []).length > limit) {
            for (let index = 0; index < limit; index++) {
              const article = response[index];
              $scope.articles.push(article);
            }
          } else {
            $scope.articles = response || [];
          }

          console.log('$scope.articles', $scope.articles);
        },
        function (err) {
          console.log(err);
        }
      );
    };
    function getContent(){
      var controller = requestApiHelper.CONTENT.ALL;
      baseService.GET(controller).then(function(response){
          $scope.contentList_page_2 = response[0];
          $scope.contentList_page_4 = response[1];
          $scope.contentList_page_5 = response[2];
          $scope.contentList_page_7 = response[3];
      }, function(err){
          console.log(err);
      });
  };

  
  function getContact(){
        var controller = requestApiHelper.CONTACT_PAGE.ALL;
        baseService.GET(controller).then(function(response){
            $scope.contactList = response[0]; 
        }, function(err){
            console.log(err);
        });
  };

  function getDvPhatTrien(){
    var controller = requestApiHelper.SLIDE.GET_BY_CATEID + '/' + 29;
    var param = {type: 'admin'};
    baseService.POST(controller, param).then(function(response){
        $scope.slideDVPT = response; 
    }, function(err){
        console.log(err);
    });
  };

  function getDvTongThau(){
    var controller = requestApiHelper.SLIDE.GET_BY_CATEID + '/' + 30;
    var param = {type: 'admin'};
    baseService.POST(controller, param).then(function(response){
        $scope.slideDVTT = response; 
    }, function(err){
        console.log(err);
    });
  };

  function getDvPhanPhoi(){
    var controller = requestApiHelper.SLIDE.GET_BY_CATEID + '/' + 31;
    var param = {type: 'admin'};
    baseService.POST(controller, param).then(function(response){
        $scope.slideDVPP = response; 
    }, function(err){
        console.log(err);
    });
  };

  function getSildePage2(){
    var controller = requestApiHelper.SLIDE.GET_BY_CATEID + '/' + 26;
    var param = {type: 'admin'};
    baseService.POST(controller, param).then(function(response){
        $scope.slide_page_2 = response; 
    }, function(err){
        console.log(err);
    });
  };

  function getSildePage5(){
    var controller = requestApiHelper.SLIDE.GET_BY_CATEID + '/' + 27;
    var param = {type: 'admin'};
    baseService.POST(controller, param).then(function(response){
        $scope.slide_page_5 = response; 
    }, function(err){
        console.log(err);
    });
  };

  function getSildePage7(){
    var controller = requestApiHelper.SLIDE.GET_BY_CATEID + '/' + 28;
    var param = {type: 'admin'};
    baseService.POST(controller, param).then(function(response){
        $scope.slide_page_7 = response; 
    }, function(err){
        console.log(err);
    });
  };

    function getBackground(){
      var controller = requestApiHelper.SLIDE.GET_BY_CATEID + '/' + 37;
      var param = {type: 'admin'};
      baseService.POST(controller, param).then(function(response){
          $scope.backgroundHome = response[0]; 
      }, function(err){
          console.log(err);
      });
    };

    function getSlogan(){
      var controller = requestApiHelper.SLIDE.GET_BY_CATEID + '/' + 38;
      var param = {type: 'admin'};
      baseService.POST(controller, param).then(function(response){
          $scope.slogan = response[0]; 
      }, function(err){
          console.log(err);
      });
    };

    function getHinh2(){
      var controller = requestApiHelper.SLIDE.GET_BY_CATEID + '/' + 39;
      var param = {type: 'admin'};
      baseService.POST(controller, param).then(function(response){
          $scope.hinh2 = response[0]; 
      }, function(err){
          console.log(err);
      });
    };

    function getHinh4(){
      var controller = requestApiHelper.SLIDE.GET_BY_CATEID + '/' + 40;
      var param = {type: 'admin'};
      baseService.POST(controller, param).then(function(response){
          $scope.hinh4 = response[0]; 
      }, function(err){
          console.log(err);
      });
    };

    function getHinh5(){
      var controller = requestApiHelper.SLIDE.GET_BY_CATEID + '/' + 41;
      var param = {type: 'admin'};
      baseService.POST(controller, param).then(function(response){
          $scope.hinh5 = response[0]; 
      }, function(err){
          console.log(err);
      });
    };

    function getHinh6(){
      var controller = requestApiHelper.SLIDE.GET_BY_CATEID + '/' + 42;
      var param = {type: 'admin'};
      baseService.POST(controller, param).then(function(response){
          $scope.hinh6 = response[0]; 
      }, function(err){
          console.log(err);
      });
    };

    function getHinh7(){
      var controller = requestApiHelper.SLIDE.GET_BY_CATEID + '/' + 43;
      var param = {type: 'admin'};
      baseService.POST(controller, param).then(function(response){
          $scope.hinh7 = response[0]; 
      }, function(err){
          console.log(err);
      });
    };

  function getConnect(){
    var controller = requestApiHelper.CONNECT.ALL;
    baseService.GET(controller).then(function(response){
        $scope.connectYoutubeDA = response[3];
    }, function(err){
        console.log(err);
    });
  };

    init();

    

  },
]);
