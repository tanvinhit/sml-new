angular.module('vpApp').controller('vitriCtrl',['$scope', '$rootScope', '$window', '$sce', 'baseService', 'requestApiHelper', function($scope, $rootScope, $window, $sce, baseService, requestApiHelper){    
    $scope.position_client = {};
    function init(){
        var controller = requestApiHelper.POSITION.ALL;
        baseService.GET(controller).then(function(response){
            $scope.position_client  = response[0];
        }, function(err){
            console.log(err);
        });
    };
    init();        
}]);