angular.module("vpApp").controller("indexCtrl", [
  "$http",
  "$scope",
  "$rootScope",
  "$window",
  "$sce",
  "baseService",
  "requestApiHelper",
  function (
    $http,
    $scope,
    $rootScope,
    $window,
    $sce,
    baseService,
    requestApiHelper
  ) {
    $scope.mailContactInfo = {
      Name: "",
      Email: "",
      Phone: "",
      Subject: "",
      Content: "",
      DateSent: getDate(new Date()),
      DeviceType: getDeviceType(),
      IP: "",
    };
    $scope.connectPhone = {};
    $scope.connectFB = {};
    $scope.connectYoutube = {};
    $scope.sendCustomerInfo = function addCustomer() {
      if (!$scope.mailContactInfo.Name) {
        return;
      }

      const numberCharPhone = ($scope.mailContactInfo.Phone || "").length;
      if (
        !$scope.mailContactInfo.Phone ||
        numberCharPhone < 10 ||
        numberCharPhone > 11
      ) {
        return;
      }

      if (!validateEmail($scope.mailContactInfo.Email)) {
        return;
      }
      var controller = requestApiHelper.MAIL_CONTACT.ADD;
      var param = $scope.mailContactInfo;

      const myobj = document.getElementsByClassName("loadx");
      myobj && myobj.length > 0 && myobj[0].setAttribute("style", "display: block;");
      baseService.POST(controller, param).then(
        function (response) {
          $('.register-form').removeClass('show');
          const myobj = document.getElementsByClassName("loadx");
          myobj && myobj.length > 0 && myobj[0].setAttribute("style", "display: none;");
          $("#btn-register-submit").css("pointer-events", "visible");
        },
        function (err) {
          console.log(err);
        }
      );
    };

    function getDate(date) {
      var dateStr =
        date.getFullYear() +
        "/" +
        ("00" + (date.getMonth() + 1)).slice(-2) +
        "/" +
        ("00" + date.getDate()).slice(-2) +
        " " +
        ("00" + date.getHours()).slice(-2) +
        ":" +
        ("00" + date.getMinutes()).slice(-2) +
        ":" +
        ("00" + date.getSeconds()).slice(-2);
      return dateStr;
    }

    function getDeviceType() {
      const ua = navigator.userAgent;
      if (/(tablet|ipad|playbook|silk)|(android(?!.*mobi))/i.test(ua)) {
        return "tablet";
      }
      if (
        /Mobile|iP(hone|od|ad)|Android|BlackBerry|IEMobile|Kindle|Silk-Accelerated|(hpw|web)OS|Opera M(obi|ini)/.test(
          ua
        )
      ) {
        return "mobile";
      }
      return "desktop";
    }

    function validateEmail(email) {
      const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
    }

    function getIP() {
      $http
        .get("https://api.ipify.org/?format=json")
        .success(function (data) {
          if (data) {
            $scope.mailContactInfo.IP = data.ip || "";
          }
        })
        .error(function (data, status) {
          console.log(data);
        });
    }
    function getConnect() {
      var controller = requestApiHelper.CONNECT.ALL;
      baseService.GET(controller).then(function (response) {
        $scope.connectPhone = response[0];
        $scope.connectFB = response[1];
        $scope.connectYoutube = response[2];
      }, function (err) {
        console.log(err);
      });
    };

    $scope.sendCustomerInfo = function addCustomer() {
      if (!$scope.mailContactInfo.Name) {
        return;
      }

      const numberCharPhone = ($scope.mailContactInfo.Phone || "").length;
      if (
        !$scope.mailContactInfo.Phone ||
        numberCharPhone < 10 ||
        numberCharPhone > 11
      ) {
        return;
      }

      if (!validateEmail($scope.mailContactInfo.Email)) {
        return;
      }
      var controller = requestApiHelper.MAIL_CONTACT.ADD;
      var param = $scope.mailContactInfo;

      const myobj = document.getElementsByClassName("loadx");
      myobj && myobj.length > 0 && myobj[0].setAttribute("style", "display: block;");
      baseService.POST(controller, param).then(
        function (response) {
          alert('a');
        },
        function (err) {
          console.log(err);
        }
      );
    };
    // getConnect();

    // getIP();
  },
]);
