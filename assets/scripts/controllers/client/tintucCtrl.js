angular.module("vpApp").controller("tintucCtrl", [
  "$scope",
  "$rootScope",
  "$window",
  "$sce",
  "baseService",
  "requestApiHelper",
  "$location",
  function ($scope, $rootScope, $window, $sce, baseService, requestApiHelper, $location) {
    function init() {
      var controller = requestApiHelper.ARTICLE.GET;
      var param = { type: "admin" };
      baseService.POST(controller, param).then(
        function (response) {
          $scope.articlesOrigin = response;
          $scope.articles = createGroupActicle(response || [], 4);
          console.log("$scope.articles", $scope.articles);
          checkConditionToOpenNews();
        },
        function (err) {
          console.log(err);
        }
      );
    }

    function checkConditionToOpenNews() {
      let pathname = location.pathname;
      // console.log('homePath', );
      var re = new RegExp('(\/(tin-tuc)\/[^/$]+)$');
      if (re.test(pathname)) {
        const artMeta = pathname.substring(pathname.lastIndexOf('/') + 1);
        $scope.onNewsDetailClick(artMeta);
        console.log(artMeta);
      }
    }


    $scope.onClickOpenNewsDetail = function onClickOpenNewsDetail(artMeta) {
      $location.replace();
      $location.path('tin-tuc/' + artMeta, false);
    }

    $scope.onClickCloseNewsDetail = function onClickCloseNewsDetail() {
      $location.path('tin-tuc');
    }

    function createGroupActicle(articles = [], itemInGroup = 4) {
      let results = [];
      let group = [];
      for (let index = 1; index <= articles.length; index++) {
        const article = articles[index - 1];
        group.push(article);
        if (index % itemInGroup === 0) {
          results.push(group);
          group = [];
        }
      }

      if (group.length > 0) {
        results.push(group);
      }

      return results;
    }
    init();

    var content = ``;

    $scope.onNewsDetailClick = function viewDetailActicle(artMeta) {
      item = $scope.articlesOrigin.filter((item) => item.ArtMeta === artMeta);
      if (!item || item.length === 0) {
        console.log('not find the article');
        $location.path('tin-tuc');
        return;
      }

      content = item[0]["Content"];
      // $location.path(item[0]["SeoCanonica"]);  
      $location.path('tin-tuc/' + artMeta);
      // console.log(id, content);
      // e.preventDefault(),
      (News = 1),
        $(window).width() > 1100 && requestAnimationFrame(BgEffect),
        $(".loadx").length ||
        $("body").append('<div class="loadx" style="display:block"></div>'),
        $(".link-page").removeClass("current"),
        $(".link-page").addClass("current");
      var t = $(".colum-box-news"),
        a =
          ($(".link-page").find(".head-text a").attr("data-name"),
            $(".link-page").find(".head-text a").attr("href")),
        o = $(".link-page").find(".head-text a").attr("data-title"),
        l = $(".link-page").find(".head-text a").attr("data-keyword"),
        i = $(".link-page").find(".head-text a").attr("data-description"),
        s = $(".link-page").find(".head-text a").attr("data-name");
      // changeUrl(a, o, i, l, s, o, i);
      var n = $(".link-page").find(".head-text a").attr("href");
      return (
        $(".news-content").removeClass("show"),
        $(".footer").removeClass("align"),
        $(".wave-ani").length && $(".stop-svg").trigger("click"),
        $(".news-list, .wheel").addClass("hide"),
        $(".scrollB").getNiceScroll().hide(),
        $(".news-content")
          .stop()
          .animate(
            {
              opacity: 0,
            },
            600,
            "linear",
            function () {
              NewsLoad(n, t, content);
            }
          ),
        !1
      );
    };

    function NewsLoad(e, t, content) {
      var docu = document.createElement("div");
      $(docu).addClass("news-text").append(content);

      $(".news-text").length && $(".news-text").remove(),
        $('.news-content').append(docu),
        $(window).width() <= 1100
          ? $(".news-text img").addClass("zoom-pic")
          : $(".news-text img").removeClass("zoom-pic"),
        ZoomPic(),
        $(".news-text a, .news-text p a").on("click", function (e) {
          e.preventDefault();
          var t = $(this).attr("href");
          return window.open(t, "_blank"), !1;
        }),
        $(".news-content")
          .stop()
          .animate(
            {
              opacity: 1,
            },
            1e3,
            "linear",
            function () {
              $('.colum-box-news').addClass("show"),
                $(".click-hover").fadeIn(600, "linear"),
                $(".loadx").fadeOut(400, "linear", function () {
                  $(".news-content").addClass("show"), $(".loadx").remove();
                }),
                $(window).width() > 1100
                  ? setTimeout(function () {
                    ScrollNiceC();
                  }, 500)
                  : detectBut();
            }
          ),
        $(".close-news, .click-hover").on("click", function () {
          var e = $(".nav li.current a").attr("data-href"),
            t = $(".nav li.current a").attr("data-title"),
            a = $(".nav li.current a").attr("data-keyword"),
            o = $(".nav li.current a").attr("data-description"),
            l = $(".nav li.current a").attr("data-name");

          // changeUrl(e, t, o, a, l, t, o),
          $(".colum-box-news").removeClass("show"),

            $(".news-content")
              .stop()
              .animate(
                {
                  opacity: 0,
                },
                500,
                "linear",
                function () {
                  $(".click-hover").fadeOut(600, "linear"),
                    $(".news-list").removeClass("hide").addClass("fadein"),
                    $(".link-page").addClass("show"),
                    $(".wheel").removeClass("hide"),
                    $(".scrollC").scrollTop(0),
                    $(".scrollC").getNiceScroll().remove(),
                    $(".news-content").children().remove(),
                    $(".footer").addClass("align"),
                    ScrollNiceB(),
                    $(".show-text .wave-ani").length &&
                    $(".play-svg").trigger("click");
                  $(".pic-img img").css('opacity', 1);
                }
              );
        });
    }
  },
]);