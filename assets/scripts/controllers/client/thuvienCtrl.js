angular.module('vpApp').controller('thuvienCtrl', ['$scope', '$rootScope', '$window', '$sce', 'baseService', 'requestApiHelper',
    function ($scope, $rootScope, $window, $sce, baseService, requestApiHelper) {
        $scope.listCate = [];
        $scope.TemplateSlide = '';
        $scope.filterLang = 'vi';
        $scope.tailieu = {};

        function init() {
            $scope.cate = {
                Language: $scope.filterLang
            };
            $scope.notice = {
                class: '',
                message: ''
            };
            var controller = requestApiHelper.CATEGORY.GET + '/' + $scope.filterLang;
            var data = {
                type: 'admin',
                dataType: 4
            };
            baseService.POST(controller, data).then(function (response) {
                $scope.listCate = response;
            }, function (err) {
                console.log(err);
            });
            var controller2 = requestApiHelper.TAILIEU.ALL;
            baseService.GET(controller2).then(function(response){
                $scope.tailieu = response[0]; 
            }, function(err){
                console.log(err);
            });
        };

        init();

        $scope.onDetailAlbum = (CatID) => {
            var t = $(this).attr("data-href") || $(this).attr("href");
            return $(".loadx").length || $("body").append('<div class="loadx" style="display:block"></div>'), $("html, body").addClass("no-scroll"), $(".overlay-dark").addClass("show"), $(".all-album").fadeIn(300, "linear", function () {
                AlbumLoad(t,CatID)
            }), !1
        }
        
        function AlbumLoad(e,CatID) {
            
            $scope.arrayImage = [];
            var controller = requestApiHelper.SLIDE.GET_BY_CATEID + '/' + CatID;
            var param = {type: 'admin'};
            baseService.POST(controller, param).then(function(response){
                var html_pic = "";
                var html_thumb = "";
                for (var i = 0; i < response.length; i++) {
                    html_pic += `
                    <div class="album-pic-center">
                                <div class="pic-name">
                                    <h3>`+ response[i].Title + `</h3>
                                </div>
                                <div class="container-zoom"><img src="`+ response[i].Image + `" alt="` + response[i].Title + `"></div>
                            </div>`;

                    html_thumb += ` <div class="thumb-item pic-img"><a href="javascript:void(0);"><img src="`+ response[i].Image + `" alt="` + response[i].Title + `"></a></div>`;
                }
                var html = `
                    "<div class="album-load">
                        <a href="javascript:void(0);" class="close-album"></a>
                        <div class="album-center">`
                        + html_pic +
                        `</div>
                        <div class="thumbs">`  
                        + html_thumb +
                        `</div>
                    </div>"
            `;
            function t(e) {
                var t = e.item.Count - 1
                    , a = e.item.index;
                0 > a && (a = t),
                    a > t && (a = 0),
                    $(".thumbs").find(".slide-item").removeClass("current").eq(a).addClass("current");
                var o = $(".thumbs").find(".slide-item.active").length - 1
                    , l = $(".thumbs").find(".slide-item.active").first().index()
                    , i = $(".thumbs").find(".slide-item.active").last().index();
                a >= i - 1 && $(".thumbs").data("btq.slidebox").to(a, 300, !0),
                    l >= a && $(".thumbs").data("btq.slidebox").to(a - o, 300, !0)
            }
            function a() {
                clearTimeout(timex),
                    $(".pic-name").removeClass("move"),
                    $(".pic-name h3").children().children().removeClass("move"),
                    $(".selected").find(".pic-name").addClass("move"),
                    $(".move h3").children().children().each(function (e) {
                        var t = $(this);
                        setTimeout(function () {
                            $(t).addClass("move")
                        }, 50 * (e + 1))
                    })
            }
            0 != TouchLenght && isTouchDevice || $(".slide-slidebox").length && $(".slide-slidebox").trigger("stop.btq.autoplay"),
                $(".slide-video-playing").length && $(".pause-button").trigger("click"),
                $(".all-album").append(html),
                $(".all-album .album-load").length > 1 && $(".all-album .album-load").last().remove(),
                Loadpic(),
                $(".pic-name > h3").lettering("words").children("span").lettering().children("span").lettering(),
                $(".album-center").on("initialized.btq.slidebox", function () {
                    $(".container-zoom").each(function (e, t) {
                        new PinchZoom["default"](t, {
                            draggableUnzoomed: !1
                        })
                    }),
                        $(".album-center").find(".slide-item.active").addClass("selected"),
                        a()
                }).BTQSlider({
                    items: 1,
                    margin: 0,
                    smartSpeed: 600,
                    loop: !1,
                    dots: !0,
                    nav: !0,
                    responsiveRefreshRate: 150
                }).on("changed.btq.slidebox", function (e) {
                    $(".thumbs").length && t(e)
                }).on("translate.btq.slidebox", function () {
                    $(".album-center").find(".slide-item").removeClass("selected")
                }).on("translated.btq.slidebox", function () {
                    $(".album-center").find(".slide-item.active").addClass("selected"),
                        a()
                }),
                $(".thumbs").on("initialized.btq.slidebox", function () {
                    var e = $(".thumbs").find(".slide-item").length;
                    $(window).width() >= 600 ? 6 >= e ? $(".thumbs").addClass("center-slidebox") : $(".thumbs").removeClass("center-slidebox") : 3 >= e ? $(".thumbs").addClass("center-slidebox") : $(".thumbs").removeClass("center-slidebox"),
                        $(".thumbs").find(".slide-item").eq(0).addClass("current")
                }).BTQSlider({
                    margin: 5,
                    smartSpeed: 300,
                    dots: !1,
                    nav: !1,
                    responsiveRefreshRate: 100,
                    responsive: {
                        0: {
                            items: 3,
                            slideBy: 3
                        },
                        600: {
                            items: 6,
                            slideBy: 6
                        }
                    }
                }),
                $(".thumbs").on("click", ".slide-item", function (e) {
                    e.preventDefault();
                    var t = $(this).index();
                    $(".album-center").data("btq.slidebox").to(t, 600, !0)
                }),
                $(".all-album").on("mousewheel", ".album-center", function (e) {
                    if (e.deltaY > 0) {
                        if (!doWheel)
                            return;
                        doWheel = !1,
                            $(".album-center").trigger("prev.btq.slidebox"),
                            setTimeout(turnWheelTouch, 500)
                    } else {
                        if (!doWheel)
                            return;
                        doWheel = !1,
                            $(".album-center").trigger("next.btq.slidebox"),
                            setTimeout(turnWheelTouch, 500)
                    }
                    e.preventDefault()
                }),
                $(".album-load").animate({
                    opacity: 1
                }, 100, "linear", function () {
                    $(".loadx").fadeOut(400, "linear", function () {
                        $(".loadx").remove()
                    })
                }),
                $(".close-album").on("click", function () {
                    return $(".all-album").fadeOut(500, "linear", function () {
                        $(".overlay-dark").removeClass("show"),
                            $(".album-load").remove()
                    }),
                        $("html, body").removeClass("no-scroll"),
                        !1
                })
            }, function(err){
                console.log(err);
            });
            console.log($scope.arrayImage.length);
            

        }

    }]);