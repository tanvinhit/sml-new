angular.module("vpApp").controller("matbangCtrl", [
  "$scope",
  "$rootScope",
  "$window",
  "$sce",
  "baseService",
  "requestApiHelper",
  "$location",
  function (
    $scope,
    $rootScope,
    $window,
    $sce,
    baseService,
    requestApiHelper,
    $location
  ) {
    // const getMasterPlanAllClient = new Promise((resolve, reject) => {
    //   var controller = requestApiHelper.MASTER_PLAN.ALL_CLIENT;
    //   var param = { type: "admin" };
    //   baseService.POST(controller, param).then(
    //     function (response) {
    //       console.log('MASTER_PLAN ALL_CLIENT', response);
    //       $scope.masterPlan = response;
    //       resolve(response);
    //     },
    //     function (err) {
    //       console.log(err);
    //       reject(err);
    //     }
    //   );
    // });
    $scope.currentIndex = 0;
    $scope.currentDetailIndex = 0;
    $scope.allowLoadNewDetail = true;
    const getMasterPlan = new Promise((resolve, reject) => {
      var controller = requestApiHelper.MASTER_PLAN.ALL;
      var param = { type: "admin" };
      baseService.POST(controller, param).then(
        function (response) {
          console.log("MASTER_PLAN", response);
          $scope.masterPlan = response;
          console.log("MASTER_PLANMASTER_PLANMASTER_PLAN", $scope.masterPlan);
          resolve(response);
        },
        function (err) {
          console.log(err);
          reject(err);
        }
      );
    });
    const getMasterPlanDetail = new Promise((resolve, reject) => {
      var controller = requestApiHelper.DETAIL_MASTER_PLAN.ALL;
      var param = { type: "admin" };
      baseService.POST(controller, param).then(
        function (response) {
          console.log("getMasterPlanDetail", response);
          $scope.masterPlanDetail = response;
          resolve(response);
        },
        function (err) {
          console.log(err);
          reject(err);
        }
      );
    });

    function createData(masterPlan = [], masterPlanDetail = []) {
      for (let i = 0; i < masterPlan.length; i++) {
        const mp = masterPlan[i];
        mp["detail"] = [];
        // for (let j = 0; j < masterPlanDetail.length; j++) {
        //   const mpd = masterPlanDetail[j];
        //   if (mpd.master_plan_id === mp.id) {
        //     mp["detail"].push(mpd);
        //     masterPlanDetail.splice(j, 1);
        //   }
        // }

        for (let j = masterPlanDetail.length - 1; j >= 0; j--) {
          const mpd = masterPlanDetail[j];
          if (mpd.master_plan_id === mp.id) {
            mp["detail"].push(mpd);
            masterPlanDetail.splice(j, 1);
          }
        }
      }
      console.log("masterPlan", masterPlan);
      return masterPlan;
    }

    function createTemplateFoor30(element, floor_name) {
      if (+element.position === 10) {
        return `<h3>Căn hộ <strong>PH-1</strong></h3>
        <p>Diện tích: <strong>${element.carpet_area}</strong> m<sup>2</sup></p>`
      }
      if (+element.position === 11) {
        return `<h3>Căn hộ <strong>PH-2</strong></h3>
        <p>Diện tích: <strong>${element.carpet_area}</strong> m<sup>2</sup></p>`
      }
      if (+element.position === 12) {
        return `<h3>Căn hộ <strong>PH-3</strong></h3>
        <p>Diện tích: <strong>${element.carpet_area}</strong> m<sup>2</sup></p>`
      }

      if (element.garden !== null) {
        return `<h3>Căn hộ <strong>${floor_name}-${element.position < 10 ? '0' + element.position : element.position}</strong></h3>
        <p><strong>${element.garden}</strong></p>
        <p>Diện tích: <strong>${element.carpet_area}</strong> m<sup>2</sup></p>
        <p>Số phòng ngủ: <strong>${element.number_bedrooms}</strong></p>`;
      }else{
        return `<h3>Căn hộ <strong>${floor_name}-${element.position < 10 ? '0' + element.position : element.position}</strong></h3>
        <p>Diện tích: <strong>${element.carpet_area}</strong> m<sup>2</sup></p>
        <p>Số phòng ngủ: <strong>${element.number_bedrooms}</strong></p>`;
      }


      return `<h3>Căn hộ <strong>${floor_name}-${element.position < 10 ? '0' + element.position : element.position}</strong></h3>
      <p>Diện tích: <strong>${element.carpet_area}</strong> m<sup>2</sup></p>
      <p>Số phòng ngủ: <strong>${element.number_bedrooms}</strong></p>`;
    }

    function createTemplateDetail(
      detail = [],
      master_plan_type_id,
      floor_name,
      index
    ) {
      let result = {
        dataMap: "",
        dataSvg: "",
        dataInfoHouse: "",
      };

      detail.forEach((element, detailIndex) => {
        result.dataMap += `<area class="onarea " index="${index}" detailIndex="${detailIndex}" data-name="${element.position}" alt="" title="10"
            href="javascript:void(0)" coords="${element.coords}"
            shape="${element.shape}">`;
        var positions = element.matrix.split(" ");
        positions[4] = positions[4] - 10;
        result.dataSvg += `<g class="num" data-num="${element.position}">
            <circle class="bg-circle" cx="${element.x_axis}" cy="${element.y_axis}" r="${element.radius}" />
            <text transform="matrix(${positions.join(" ")})"
                class="house-number">${element.position < 10 ? '0' + element.position : element.position}</text>
        </g>`;

        result.dataInfoHouse += `
        <div class="house-text" data-block="${element.position}">
            <a class="go-link" href="01-4.html"
                tppabs="">
                <div class="num-block">${element.position}</div>
                <div class="small-box">
                ${+master_plan_type_id !== 9 ? (+master_plan_type_id === 1
            ? `<h3>${element.name}</h3>`
            : `<h3>Căn hộ <strong>${floor_name}-${element.position < 10 ? '0' + element.position : element.position}</strong></h3>
                <p>Diện tích: <strong>${element.carpet_area}</strong> m<sup>2</sup></p>
                <p>Số phòng ngủ: <strong>${element.number_bedrooms}</strong></p>`) : createTemplateFoor30(element, floor_name)



          }
                    
                </div>

                <span class="shape"></span>
            </a>
        </div>`;
      });

      return result;
    }

    function createTemplate() {
      let result = "";
      $scope.masterPlanData.forEach((element, index) => {
        let string = `
        <div class="group-central" data-name="apartment-floor-01" data-href="${element.path_name}"
            tppabs="" data-title="" data-description=""
            data-keyword="">
            <div class="content-main white">
                <div class="title-main">
                    <h2>${element.name}</h2>
                </div>
                <div class="apartment-map">
                    <div class="apartment-bg">
                        <img class="map" src="${element.image}"
                            tppabs="" alt="Tầng 3,4,5"
                            usemap="#Map${element.id}">
                        <map name="Map${element.id}">
                                
								<replace-data-map/>
								
								
								
                        </map>
                        <div class="typical">
                            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 2400 1400">
                                
								
                            <replace-data-svg/>
								
								
								
                            </svg>
                        </div>
                        <div class="all-dot">
                            <div class="compass2"></div>
                        </div>
                    </div>
                </div>
                <!--INFO HOUSE-->
                <div class="info-house">
                    
					
                <replace-data-info-house/>
					
					
                </div>
                <!--INFO HOUSE-->
            </div>


            
        </div>
        `;

        const stringDetail = createTemplateDetail(
          element.detail || [],
          element.id,
          element.floor_name,
          index
        );
        string = string.replace("<replace-data-map/>", stringDetail.dataMap);
        string = string.replace("<replace-data-svg/>", stringDetail.dataSvg);
        string = string.replace(
          "<replace-data-info-house/>",
          stringDetail.dataInfoHouse
        );
        result += string;
      });
      return result;
    }
    function load_js() {
      var head = document.getElementsByTagName("head")[0];
      var script = document.createElement("script");
      script.src = "assets/client/catalog/view/theme/default/js/load.js";
      head.appendChild(script);
    }

    function onClickArea() {
      // console.log($('.house-detail'));
      // $('.house-detail').on('click', (e) => {alert('keyup')});
      document.addEventListener("keydown", function (e) {
        setTimeout(() => {
          $scope.allowLoadNewDetail = true;
        }, 3000);
        const isHaveModal = document.querySelector(".house-detail");
        if (!$scope.allowLoadNewDetail || !isHaveModal) {
          return;
        }
        if (e.keyCode === 40) {
          console.log("40", e);
          // $(e.currentTarget).closest('#apartment-page').
          // console.log(e.currentTarget.querySelector('.slide-pic-nav .next-pic'));
          // $(e.currentTarget.querySelector('.slide-pic-nav .next-pic')).trigger('click');
          loadModal(e, false, true);
        }
        if (e.keyCode === 38) {
          console.log("38", e);
          // $(e.currentTarget.querySelector('.slide-pic-nav .prev-pic')).trigger('click');
          loadModal(e, false, false);
        }
        $scope.allowLoadNewDetail = false;
      });
    }

    function init() {
      Promise.all([getMasterPlan, getMasterPlanDetail])
        .then((data) => {
          console.log(data);
          $scope.masterPlanData = createData(data[0], data[1]);
          // $scope.templateHtml =  createTemplate();
          console.log($("#templateHtml"));
          $(createTemplate()).appendTo("#templateHtml");
          load_js();
          // loadModal();
          //not detail
          // $("area.onarea").on("click", (e) => {
          //   loadModal(e);
          //   setTimeout(() => {
          //     const handaleNextPic = (e) => {
          //       const isHaveModal = document.querySelector(".house-detail");
          //       if (!isHaveModal) {
          //         return;
          //       }

          //       loadModal(e, false, true);
          //     };
          //     const handalePrevPic = (e) => {
          //       const isHaveModal = document.querySelector(".house-detail");
          //       if (!isHaveModal) {
          //         return;
          //       }
          //       loadModal(e, false, false);
          //     };
          //     document
          //       .querySelector(".next-pic")
          //       .removeEventListener("click", handaleNextPic);
          //     document
          //       .querySelector(".prev-pic")
          //       .removeEventListener("click", handalePrevPic);
          //     document
          //       .querySelector(".next-pic")
          //       .addEventListener("click", handaleNextPic);
          //     document
          //       .querySelector(".prev-pic")
          //       .addEventListener("click", handalePrevPic);

          //     const handaleModalWheel = (e) => {
          //       setTimeout(() => {
          //         $scope.allowLoadNewDetail = true;
          //       }, 1000);
          //       if (e.deltaY > 0 && $scope.allowLoadNewDetail) {
          //         $scope.allowLoadNewDetail = false;
          //         console.log("modal bottom");
          //         loadModal(e, false, true);
          //         return;
          //         // $(document.querySelector('.next-pic')).trigger('click');
          //       }
          //       if (e.deltaY < 0 && $scope.allowLoadNewDetail) {
          //         $scope.allowLoadNewDetail = false;
          //         console.log("modal top");
          //         loadModal(e, false, false);
          //         return;
          //       }
          //       console.log("wheel", e);
          //     };

          //     const handaleModalSwipeUp = (e) => {
          //       setTimeout(() => {
          //         $scope.allowLoadNewDetail = true;
          //       }, 500);
          //       if ($scope.allowLoadNewDetail) {
          //         $scope.allowLoadNewDetail = false;
          //         console.log("modal bottom");
          //         loadModal(e, false, true);
          //         return;
          //         // $(document.querySelector('.next-pic')).trigger('click');
          //       }
          //       if (e.deltaY < 0 && $scope.allowLoadNewDetail) {
          //         $scope.allowLoadNewDetail = false;
          //         console.log("modal top");
          //         loadModal(e, false, false);
          //         return;
          //       }
          //       console.log("wheel", e);
          //     };

          //     const handaleModalSwipeDown = (e) => {
          //       setTimeout(() => {
          //         $scope.allowLoadNewDetail = true;
          //       }, 500);
          //       if ($scope.allowLoadNewDetail) {
          //         $scope.allowLoadNewDetail = false;
          //         console.log("modal top");
          //         loadModal(e, false, false);
          //         return;
          //       }
          //       console.log("wheel", e);
          //     };

          //     document.removeEventListener("wheel", handaleModalWheel);
          //     document.addEventListener("wheel", handaleModalWheel);
          //     $("body").on("swipeup", handaleModalSwipeUp);
          //     $("body").on("swipedown", handaleModalSwipeDown);
          //   }, 1000);
          // });
          setTimeout(() => {
            onClickArea();
            $("#apartment-page >.box-nav.show").attr(
              "style",
              "display: block;"
            );
            console.log("run");
            document.addEventListener("wheel", (e) => {
              if (e.deltaY > 0) {
                console.log("bottom");
                onScrollMain(false);
              } else {
                console.log("top");
                onScrollMain();
              }
              console.log("wheel", e);
            });
            $("body").on("swipeup", function (e) {
              onScrollMain();
            });
            $("body").on("swipedown", function (e) {
              onScrollMain(false);
            });
          }, 5000);
        })
        .catch((err) => {
          console.log(err);
        });
      // getMasterPlanAllClient();
    }

    function onScrollMain(isUp = true) {
      const isHaveModal = document.querySelector(".house-detail");
      if (isHaveModal) {
        return;
      }

      const itag = document.querySelectorAll(".box-nav.show ul li");
      for (let index = 0; index < itag.length; index++) {
        const element = itag[index];
        if (
          !isUp &&
          element.classList.contains("current") &&
          index < itag.length - 1
        ) {
          $(itag[index + 1].querySelector("a")).trigger("click");
          console.log(element);
          return;
        }
        if (isUp && element.classList.contains("current") && index > 0) {
          $(itag[index - 1].querySelector("a")).trigger("click");
          console.log(element);
          return;
        }
      }
    }

    init();

    // function BoxSlide() {
    //   function e() {
    //     setTimeout(function () {
    //       TweenLite.set($(".group-central").not($(".group-central")[l]), {
    //         y: "100%",
    //       }),
    //         (i = !1);
    //     }, 1e3);
    //   }

    //   function t() {
    //     (i = !0),
    //       TweenLite.set($(".group-central"), {
    //         zIndex: "",
    //       }),
    //       $(".footer").removeClass("end"),
    //       $(".wheel").addClass("show"),
    //       $(".go-top, .copyright").removeClass("show"),
    //       $(".box-nav li").removeClass("current"),
    //       $(".box-nav").removeClass("blue"),
    //       clearTimeout(timer),
    //       $(".dot-num").removeClass("show"),
    //       $(".num").removeClass("fadeinup"),
    //       $(".youtube-video iframe").length &&
    //         ($(".pause-button").trigger("click"), clearInterval(timer)),
    //       $(".logo").removeClass("out"),
    //       $(".logo, .hotline, .nav-click").removeClass("brown"),
    //       $(".map-svg").removeClass("show"),
    //       $(".wave-ani").hasClass("in-play") && $(".stop-svg").trigger("click"),
    //       $("#about-page").length && requestAnimationFrame(SvgCollapse),
    //       TweenLite.fromTo(
    //         $(".group-central")[l],
    //         0.8,
    //         {
    //           zIndex: 2,
    //         },
    //         {
    //           y: "0%",
    //           ease: Quad.easeOut,
    //           onComplete: function () {
    //             if (
    //               ($("div").removeClass("sunlight lighting rotatenew"),
    //               $(".group-central").removeClass("show-text"),
    //               $(".group-central").eq(l).addClass("show-text"),
    //               $(".box-nav li").eq(l).addClass("current"),
    //               $(".grid-item-bg canvas").removeClass("show"),
    //               $(".group-central .thumb-image").length && CancelMove(),
    //               $(".show-text .wave-ani").length &&
    //                 $(".play-svg").trigger("click"),
    //               $('.group-central[data-name="home-banner"]').hasClass(
    //                 "show-text"
    //               )
    //                 ? ($(".logo").addClass("scale"),
    //                   $(".logo, .hotline, .nav-click").addClass("white"))
    //                 : ($(".logo").removeClass("scale"),
    //                   $(".logo, .hotline, .nav-click").removeClass("white")),
    //               ($('.group-central[data-name="home-intro"]').hasClass(
    //                 "show-text"
    //               ) ||
    //                 $('.group-central[data-name="home-library"]').hasClass(
    //                   "show-text"
    //                 )) &&
    //                 $(".logo, .hotline, .nav-click").addClass("brown"),
    //               $(".group-central.show-text .slide-pic").length &&
    //                 setTimeout(function () {
    //                   $(".slide-pic").trigger("next.btq.slidebox", 1500);
    //                 }, 800),
    //               $('.group-central[data-name="home-location"]').hasClass(
    //                 "show-text"
    //               ) &&
    //                 setTimeout(function () {
    //                   $(".map-svg").addClass("show");
    //                 }, 800),
    //               $('.group-central[data-name="home-contact"]').hasClass(
    //                 "show-text"
    //               ) && $(".logo").addClass("out"),
    //               ($('.group-central[data-name="home-facilities"]').hasClass(
    //                 "show-text"
    //               ) ||
    //                 $('.group-central[data-name="home-contact"]').hasClass(
    //                   "show-text"
    //                 )) &&
    //                 $(".box-nav").addClass("blue"),
    //               $("#apartment-page").length &&
    //                 ($(".group-central:nth-child(odd)").hasClass("show-text")
    //                   ? $(".fix-footer, .wheel, .go-top").addClass("brown")
    //                   : $(".fix-footer, .wheel, .go-top").removeClass("brown")),
    //               $(".group-central.show-text .thumb-image").length &&
    //                 MoveBackground(),
    //               $(".group-central.show-text .grid-item-bg").length &&
    //                 requestAnimationFrame(BgEffect),
    //               $(".youtube-video iframe").length &&
    //                 $('.group-central[data-name="video-home"]').hasClass(
    //                   "show-text"
    //                 ) &&
    //                 (clearInterval(timer),
    //                 (timer = setInterval(function () {
    //                   $(".play-button").trigger("click");
    //                 }, 800))),
    //               $("#facilities-page").length &&
    //                 $(".group-central").hasClass("show-text") &&
    //                 $(".show-text .all-dot-top")
    //                   .children()
    //                   .each(function (e) {
    //                     var t = $(this);
    //                     timer = setTimeout(function () {
    //                       $(t).addClass("show");
    //                     }, 100 * (e + 1));
    //                   }),
    //               $("#facilities-page").length &&
    //                 ($(".group-central:nth-child(odd)").hasClass("show-text")
    //                   ? ($(".fix-footer, .wheel, .go-top")
    //                       .removeClass("brown")
    //                       .addClass("blue"),
    //                     $(".box-nav").addClass("blue"))
    //                   : ($(".fix-footer, .wheel, .go-top")
    //                       .removeClass("blue")
    //                       .addClass("brown"),
    //                     $(".box-nav").removeClass("blue"))),
    //               $("#about-page").length &&
    //                 $(".group-central").hasClass("show-text") &&
    //                 requestAnimationFrame(SvgExpand),
    //               $("#library-page").length &&
    //                 $(".group-central").hasClass("show-text") &&
    //                 AniTitle(),
    //               $(".group-central:last-child").hasClass("show-text") &&
    //                 ($(".wheel").removeClass("show"),
    //                 $(".go-top").addClass("show"),
    //                 $(".copyright").addClass("show")),
    //               $(
    //                 "#about-page, #facilities-page, #apartment-page, #library-page"
    //               ).length)
    //             ) {
    //               var t =
    //                   ($(".group-central.show-text").attr("data-name"),
    //                   $(".box-nav li, .sub-nav li")
    //                     .eq(l)
    //                     .find("a")
    //                     .attr("href")),
    //                 a = $(".box-nav li, .sub-nav li")
    //                   .eq(l)
    //                   .find("a")
    //                   .attr("data-title"),
    //                 o = $(".box-nav li, .sub-nav li")
    //                   .eq(l)
    //                   .find("a")
    //                   .attr("data-keyword"),
    //                 i = $(".box-nav li, .sub-nav li")
    //                   .eq(l)
    //                   .find("a")
    //                   .attr("data-description"),
    //                 s = $(".box-nav li, .sub-nav li")
    //                   .eq(l)
    //                   .find("a")
    //                   .attr("data-page");
    //               // changeUrl(t, a, i, o, s, a, i)
    //             }
    //             e();
    //           },
    //         }
    //       );
    //   }

    //   function a() {
    //     (i = !0),
    //       TweenLite.set($(".group-central"), {
    //         zIndex: "",
    //       }),
    //       $(".footer").removeClass("end"),
    //       $(".wheel").addClass("show"),
    //       $(".go-top, .copyright").removeClass("show"),
    //       $(".box-nav li").removeClass("current"),
    //       $(".box-nav").removeClass("blue"),
    //       clearTimeout(timer),
    //       $(".dot-num").removeClass("show"),
    //       $(".num").removeClass("fadeinup"),
    //       $(".youtube-video iframe").length &&
    //         ($(".pause-button").trigger("click"), clearInterval(timer)),
    //       $(".logo").removeClass("out"),
    //       $(".logo, .hotline, .nav-click").removeClass("brown"),
    //       $(".map-svg").removeClass("show"),
    //       $(".wave-ani").hasClass("in-play") && $(".stop-svg").trigger("click"),
    //       $("#about-page").length && requestAnimationFrame(SvgCollapse),
    //       TweenLite.fromTo(
    //         $(".group-central")[l],
    //         0.8,
    //         {
    //           y: "-100%",
    //           zIndex: 2,
    //         },
    //         {
    //           y: "0%",
    //           ease: Quad.easeOut,
    //           onComplete: function () {
    //             if (
    //               ($("div").removeClass("sunlight lighting rotatenew"),
    //               $(".group-central").removeClass("show-text"),
    //               $(".group-central").eq(l).addClass("show-text"),
    //               $(".box-nav li").eq(l).addClass("current"),
    //               $(".grid-item-bg canvas").removeClass("show"),
    //               $(".group-central .thumb-image").length && CancelMove(),
    //               $(".show-text .wave-ani").length &&
    //                 $(".play-svg").trigger("click"),
    //               $('.group-central[data-name="home-banner"]').hasClass(
    //                 "show-text"
    //               )
    //                 ? ($(".logo").addClass("scale"),
    //                   $(".logo, .hotline, .nav-click").addClass("white"))
    //                 : ($(".logo").removeClass("scale"),
    //                   $(".logo, .hotline, .nav-click").removeClass("white")),
    //               ($('.group-central[data-name="home-intro"]').hasClass(
    //                 "show-text"
    //               ) ||
    //                 $('.group-central[data-name="home-library"]').hasClass(
    //                   "show-text"
    //                 )) &&
    //                 $(".logo, .hotline, .nav-click").addClass("brown"),
    //               $(".group-central.show-text .slide-pic").length &&
    //                 setTimeout(function () {
    //                   $(".slide-pic").trigger("next.btq.slidebox", 1500);
    //                 }, 800),
    //               $('.group-central[data-name="home-location"]').hasClass(
    //                 "show-text"
    //               ) &&
    //                 setTimeout(function () {
    //                   $(".map-svg").addClass("show");
    //                 }, 800),
    //               $('.group-central[data-name="home-contact"]').hasClass(
    //                 "show-text"
    //               ) && $(".logo").addClass("out"),
    //               ($('.group-central[data-name="home-facilities"]').hasClass(
    //                 "show-text"
    //               ) ||
    //                 $('.group-central[data-name="home-contact"]').hasClass(
    //                   "show-text"
    //                 )) &&
    //                 $(".box-nav").addClass("blue"),
    //               $("#apartment-page").length &&
    //                 ($(".group-central:nth-child(odd)").hasClass("show-text")
    //                   ? $(".fix-footer, .wheel, .go-top").addClass("brown")
    //                   : $(".fix-footer, .wheel, .go-top").removeClass("brown")),
    //               $(".group-central.show-text .thumb-image").length &&
    //                 MoveBackground(),
    //               $(".group-central.show-text .grid-item-bg").length &&
    //                 requestAnimationFrame(BgEffect),
    //               $(".youtube-video iframe").length &&
    //                 $('.group-central[data-name="video-home"]').hasClass(
    //                   "show-text"
    //                 ) &&
    //                 (clearInterval(timer),
    //                 (timer = setInterval(function () {
    //                   $(".play-button").trigger("click");
    //                 }, 800))),
    //               $("#facilities-page").length &&
    //                 $(".group-central").hasClass("show-text") &&
    //                 $(".show-text .all-dot-top")
    //                   .children()
    //                   .each(function (e) {
    //                     var t = $(this);
    //                     timer = setTimeout(function () {
    //                       $(t).addClass("show");
    //                     }, 100 * (e + 1));
    //                   }),
    //               $("#facilities-page").length &&
    //                 ($(".group-central:nth-child(odd)").hasClass("show-text")
    //                   ? ($(".fix-footer, .wheel, .go-top")
    //                       .removeClass("brown")
    //                       .addClass("blue"),
    //                     $(".box-nav").addClass("blue"))
    //                   : ($(".fix-footer, .wheel, .go-top")
    //                       .removeClass("blue")
    //                       .addClass("brown"),
    //                     $(".box-nav").removeClass("blue"))),
    //               $("#about-page").length &&
    //                 $(".group-central").hasClass("show-text") &&
    //                 requestAnimationFrame(SvgExpand),
    //               $("#library-page").length &&
    //                 $(".group-central").hasClass("show-text") &&
    //                 AniTitle(),
    //               $(".group-central:last-child").hasClass("show-text") &&
    //                 ($(".wheel").removeClass("show"),
    //                 $(".go-top").addClass("show"),
    //                 $(".copyright").addClass("show")),
    //               $(
    //                 "#about-page, #facilities-page, #apartment-page, #library-page"
    //               ).length)
    //             ) {
    //               var t =
    //                   ($(".group-central.show-text").attr("data-name"),
    //                   $(".box-nav li, .sub-nav li")
    //                     .eq(l)
    //                     .find("a")
    //                     .attr("href")),
    //                 a = $(".box-nav li, .sub-nav li")
    //                   .eq(l)
    //                   .find("a")
    //                   .attr("data-title"),
    //                 o = $(".box-nav li, .sub-nav li")
    //                   .eq(l)
    //                   .find("a")
    //                   .attr("data-keyword"),
    //                 i = $(".box-nav li, .sub-nav li")
    //                   .eq(l)
    //                   .find("a")
    //                   .attr("data-description"),
    //                 s = $(".box-nav li, .sub-nav li")
    //                   .eq(l)
    //                   .find("a")
    //                   .attr("data-page");
    //               // changeUrl(t, a, i, o, s, a, i)
    //             }
    //             e();
    //           },
    //         }
    //       );
    //   }
    //   var o = $(".group-central").length,
    //     l = $(".group-central").index(),
    //     i = !1;
    //   TweenLite.set($(".group-central").not($(".group-central")[l]), {
    //     y: "100%",
    //   });
    //   $(window).width() > 1100 &&
    //     !$("body").hasClass("fullscreen") &&
    //     ($(".box-slider:not(.single)").on("mousewheel", function (e) {
    //       // alert('Done')
    //       var s;
    //       i === !1 &&
    //         (s = (function () {
    //           var t = Math.max(
    //             -1,
    //             Math.min(1, e.wheelDelta || -e.deltaY || -e.detail)
    //           );
    //           return t;
    //         })()),
    //         $(window).width() > 1100 &&
    //           !$("body").hasClass("fullscreen") &&
    //           (null != $(".group-central")[l] && 1 === parseInt(s)
    //             ? (l >= o - 1 ? (l = 0) : l++, t())
    //             : null != $(".group-central")[l] &&
    //               -1 === parseInt(s) &&
    //               (0 >= l ? (l = o - 1) : l--, a())),
    //         (window.location.pathname = "/bds/mat-bang/tang-1.html");
    //     }),
    //     $(".box-slider:not(.single)")
    //       .on("swipeup", function () {
    //         doTouch &&
    //           ((doTouch = !1),
    //           $(window).width() > 1100 &&
    //             !$("body").hasClass("fullscreen") &&
    //             ($(".box-nav li.current").next().trigger("click"),
    //             setTimeout(turnWheelTouch, 500)));
    //       })
    //       .on("swipedown", function () {
    //         doTouch &&
    //           ((doTouch = !1),
    //           $(window).width() > 1100 &&
    //             !$("body").hasClass("fullscreen") &&
    //             ($(".box-nav li.current").prev().trigger("click"),
    //             setTimeout(turnWheelTouch, 500)));
    //       })),
    //     $(".box-nav li").on("click", function () {
    //       var e = $(this).index();
    //       return i
    //         ? !1
    //         : (!i && e > l ? ((l = e), t()) : !i && l > e && ((l = e), a()),
    //           !1);
    //     }),
    //     setTimeout(function () {
    //       $(".group-central:first-child").addClass("show-text"),
    //         $(".box-nav li:first-child").addClass("current"),
    //         $('.group-central[data-name="home-banner"]').hasClass(
    //           "show-text"
    //         ) &&
    //           ($(".logo").addClass("scale"),
    //           $(".logo, .hotline, .nav-click").addClass("white")),
    //         $('.group-central[data-name="contact"]').hasClass("show-text") &&
    //           $(".logo").addClass("out");
    //     }, 500),
    //     setTimeout(function () {
    //       $(".show-text .wave-ani").length && $(".play-svg").trigger("click"),
    //         $("#about-page").length &&
    //           $(".group-central").hasClass("show-text") &&
    //           requestAnimationFrame(SvgExpand),
    //         $("#library-page").length &&
    //           $(".group-central").hasClass("show-text") &&
    //           AniTitle(),
    //         $("#location-page").length &&
    //           $(".group-central").hasClass("show-text") &&
    //           $(".map-svg").addClass("show"),
    //         $("#facilities-page").length &&
    //           $(".group-central").hasClass("show-text") &&
    //           $(".show-text .all-dot-top")
    //             .children()
    //             .each(function (e) {
    //               var t = $(this);
    //               timer = setTimeout(function () {
    //                 $(t).addClass("show");
    //               }, 100 * (e + 1));
    //             }),
    //         $("#facilities-page").length &&
    //           ($(".group-central:nth-child(odd)").hasClass("show-text")
    //             ? ($(".fix-footer, .wheel, .go-top")
    //                 .removeClass("brown")
    //                 .addClass("blue"),
    //               $(".box-nav").addClass("blue"))
    //             : ($(".fix-footer, .wheel, .go-top")
    //                 .removeClass("blue")
    //                 .addClass("brown"),
    //               $(".box-nav").removeClass("blue"))),
    //         $("#apartment-page").length &&
    //           ($(".group-central:nth-child(odd)").hasClass("show-text")
    //             ? $(".fix-footer, .wheel, .go-top").addClass("brown")
    //             : $(".fix-footer, .wheel, .go-top").removeClass("brown"));
    //     }, 2e3);
    // }
    // BoxSlide();

    function loadModal(e, isClick = true, isPlus = true) {
      // $(".onarea").on("click", function (e) {
      console.log(e);
      //  document.querySelector('.prev-pic').classList.remove('disabled');
      //       document.querySelector('.next-pic').classList.remove('disabled');
      if (isClick) {
        $scope.currentIndex = $(e.target).attr("index");
        $scope.currentDetailIndex = $(e.target).attr("detailindex");
      } else {
        let isFirstLast = false;
        if (
          isPlus &&
          +$scope.currentDetailIndex + 1 ===
          $scope.masterPlanData[$scope.currentIndex].detail.length - 1
        ) {
          document.querySelector(".next-pic").classList.add("disabled");
          document.querySelector(".prev-pic").classList.remove("disabled");
          isFirstLast = true;
        }

        if (!isPlus && +$scope.currentDetailIndex - 1 === 0) {
          document.querySelector(".prev-pic").classList.add("disabled");
          document.querySelector(".next-pic").classList.remove("disabled");
          isFirstLast = true;
        }

        if (!isFirstLast) {
          document.querySelector(".prev-pic").classList.remove("disabled");
          document.querySelector(".next-pic").classList.remove("disabled");
        }

        if (!isPlus && +$scope.currentDetailIndex === 0) {
          document.querySelector(".prev-pic").classList.add("disabled");
          // document.querySelector('.next-pic').classList.remove('disabled');
          return;
        }
        if (
          isPlus &&
          +$scope.currentDetailIndex ===
          $scope.masterPlanData[$scope.currentIndex].detail.length - 1
        ) {
          document.querySelector(".next-pic").classList.add("disabled");
          // document.querySelector('.prev-pic').classList.remove('disabled');
          return;
        }

        // document.querySelector('.prev-pic').classList.remove('disabled');
        // document.querySelector('.next-pic').classList.remove('disabled');
        // $scope.currentIndex = isPlus ? +$scope.currentIndex + 1 : + $scope.currentIndex - 1;
        $scope.currentDetailIndex = isPlus
          ? +$scope.currentDetailIndex + 1
          : +$scope.currentDetailIndex - 1;
      }
      console.log("currentIndex", $scope.currentIndex);
      console.log("currentDetailIndex", $scope.currentDetailIndex);
      const item = $scope.masterPlanData[$scope.currentIndex];
      const itemDetail = item.detail[$scope.currentDetailIndex];
      console.log(item);
      console.log(itemDetail);
      e.preventDefault();
      var s = $(this).attr("href"),
        n = $(this).attr("data-name"),
        r = $(this)
          .parent()
          .parent()
          .parent()
          .parent()
          .find(".title-main h2")
          .text(),
        c = $(".house-text[data-block='" + n + "'] .num-block").text();
      return (
        $(".num").addClass("staying"),
        $(".house-text").removeClass("current"),
        $(".house-text[data-block='" + n + "']").addClass("current"),
        $(".house-text.current").parent().parent().addClass("on-slide"),
        $(".loadx").length ||
        $("body").append('<div class="loadx" style="display:block"></div>'),
        $("html, body").addClass("no-scroll"),
        $(".overlay-dark").addClass("wave-bg show"),
        $(".nav-click").addClass("hide"),
        $(".mouse").addClass("show"),
        $(".fix-footer").addClass("go-bottom"),
        $(".load-apartment")
          .stop()
          .fadeIn(300, "linear", function () {
            $(".house-detail").length && $(".house-detail").remove(),
              ApartmentLoad(s, c, r, item, itemDetail);
          }),
        !1
      );
      // }
      // )

      function ApartmentLoad(e, t, a, item, itemDetail) {
        var html = `
            <div class="house-detail">
                <div class="content-house">
                <div class="title-box">
                <div class="title-tel">[ <span>${item.name}</span> ]</div>
                <h2 class="house-style">Suite</h2><h2>Vị trí #  <strong>${itemDetail.position}</strong></h2>
                <div class="house-des">
                <h3><strong>${itemDetail.number_bedrooms}</strong> PN, <strong>${itemDetail.number_bedrooms}</strong> WC</h3>
                <ul>
                <li>
                <p>Diện tích tim tường: <strong>${itemDetail.carpet_area}</strong> m<sup>2</sup></p>
                </li>
                <li>
                <p>Diện tích thông thủy: <strong>${itemDetail.built_up_area}</strong> m<sup>2</sup></p>
                </li>
                </ul></div>
                </div>
                <div class="apartment-pic">
                <div class="scale-pic">
                <img src="" alt="Vị trí #  18">
                </div>
                </div>
                <div class="house-bottom">
                <div class="keyplan">
                <div class="compass2"></div>
                <p>Hướng và vị trí</p>
                <img src="" alt="Vị trí #  18">
                </div>
                <div class="share">
                <span>Share</span>
                <ul>
                <li>
                <a class="facebook" href="https://www.facebook.com/sharer.php?u=http://thesong.vn/vi/mat-bang/tang-02/18.html" target="_blank">
                <svg  xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 30 50">
                <path   fill="currentColor" d="M20.7,19h-4.2v-2.8c0-1,0.7-1.3,1.2-1.3s3,0,3,0v-4.7h-4.2c-4.6,0-5.7,3.5-5.7,5.7V19H8.1v4.8h2.6
                c0,6.1,0,13.5,0,13.5h5.6c0,0,0-7.5,0-13.5H20L20.7,19z"></path>
                </svg>
                </a>
                </li>
                </ul>
                </div>
                </div>
                <div class="description">
                <p>(*) Thông tin và hình ảnh trên website chỉ thể hiện thông số kỹ thuật, tính thẩm mỹ và sự sáng tạo tại thời điểm cập nhật. Các thông tin chính thức được quy định rõ trên hợp đồng mua bán căn hộ. Các diễn giải trực quan của bản vẽ chỉ là phối cảnh, không phải là sự thể hiện thực tế. 
                </p>
                <p>
                Những thay đổi (nếu có) là quyết định cuối cùng thuộc về chủ đầu tư.</p></div>
                </div>
                </div>
            `;

        $(".load-apartment").append(html),
          $(".slide-pic-nav").length ||
          ($(".load-apartment").append(
            '<div class="slide-pic-nav"><div class="next-pic"></div><div class="prev-pic"></div></div>'
          ),
            $(".slide-pic-nav").fadeIn(400, "linear")),
          $(".load-apartment .house-detail").length > 1 &&
          $(".load-apartment .house-detail").last().remove(),
          $(".scale-pic").addClass("pinch-zoom"),
          $(".pinch-zoom").each(function (e, t) {
            new PinchZoom["default"](t, {
              draggableUnzoomed: !1,
            });
          });
        var o = $(".house-text.current");
        LoadNext(o),
          $(".loadx")
            .stop()
            .fadeOut(400, "linear", function () {
              $(".bg-house").hasClass("show") ||
                requestAnimationFrame(SvgExpand),
                $(".house-detail").addClass("show-house"),
                $(".go-back").addClass("show"),
                $(".logo").addClass("center");
              var e = $(".content-house").offset().left;
              $(".logo.center").css({
                left: e - 10,
              }),
                $(".loadx").remove();
            }),
          $(".go-back").on("click", function () {
            return (
              $(".logo").removeClass("center"),
              $(".logo").removeAttr("style"),
              $(".house-text.current")
                .parent()
                .parent()
                .removeClass("on-slide"),
              $(".house-text").removeClass("current"),
              requestAnimationFrame(SvgCollapse),
              $(".load-apartment")
                .stop()
                .fadeOut(500, "linear", function () {
                  $(".overlay-dark").removeClass("wave-bg show"),
                    $(".go-back, .mouse").removeClass("show"),
                    $(".nav-click").removeClass("hide"),
                    $(".house-detail, .slide-pic-nav").remove(),
                    $("html, body").removeClass("no-scroll"),
                    $(".num").removeClass("staying"),
                    $(".fix-footer").removeClass("go-bottom");
                }),
              !1
            );
          });
      }
    }
  },
]);
