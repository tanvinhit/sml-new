;(function() {
  'use strict';

  angular
    .module('vpApp')
    .config(AppConfig);

  AppConfig.$inject = ['$stateProvider', '$urlRouterProvider','$locationProvider'];

  function AppConfig($stateProvider, $urlRouterProvider,$locationProvider) {

    $stateProvider
      .state({
        name: '/',
        url: '/',
        templateUrl: 'assets/scripts/app/views/home/home.html',
        controller: 'HomeController',
        controllerAs: 'vm'
      })
      .state({
        name: 'biet-thu',
        url: '/biet-thu',
        templateUrl: 'assets/scripts/app/views/bietthu/bietthu.html',
        controller: 'BietthuController',
        controllerAs: 'vm'
      })
      .state({
        name: 'can-ho',
        url: '/can-ho',
        templateUrl: 'assets/scripts/app/views/canho/canho.html',
        controller: 'CanhoController',
        controllerAs: 'vm'
      })
      .state({
        name: 'tin-tuc',
        url: '/tin-tuc',
        templateUrl: 'assets/scripts/app/views/tintuc/tintuc.html',
        controller: 'TintucController',
        controllerAs: 'vm'
      })
      .state({
        name: 'tin-tuc-detail',
        url: '/tin-tuc/:cateArt',
        templateUrl: 'assets/scripts/app/views/tintuc-detail/tintuc-detail.html',
        controller: 'TintucDetailController',
        controllerAs: 'vm'
      }) 
      .state({
        name: 'tien-ich-tang',
        url: '/tien-ich-tang',
        templateUrl: 'assets/scripts/app/views/tienich-tang/tienichtang.html',
        controller: 'TienichtangController',
        controllerAs: 'vm'
      });

    $urlRouterProvider.otherwise('/');
    $locationProvider.html5Mode(true);

  }

})();
