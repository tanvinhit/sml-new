(function () {
    'use strict';
    angular
      .module('vpApp')
      .controller('BietthuController', BietthuController);
  
    BietthuController.$inject = ['fullPageService', "ngMeta", "requestApiHelper", "$scope", "$rootScope", "baseService","$timeout",];
  
    function BietthuController(fullPageService, ngMeta, requestApiHelper, $scope, $rootScope, baseService,$timeout) {
      var options = {
        // anchors: ['tinh-hoa-cua-bien-ca', 'mat-bang-tong-the', 'dai-cong-vien-nuoc', 'cac-tien-ich-noi-troi', 'hinh-anh-noi-that'],
        anchors: ['tinh-hoa-cua-bien-ca', 'mat-bang-tong-the', 'cac-tien-ich-noi-troi', 'hinh-anh-noi-that'],
        menu: '#bietthu-nav',
        lockAnchors: false,
        css3: false,
        responsiveWidth: 1100,
        verticalCentered: false,
        navigation: false,
        navigationPosition: 'right',
        resetSliders:true,
        responsive: 1025,
        afterResponsive: function (isResponsive) {
          if (isResponsive) {
            $('html, body').css('overflow-x', 'hidden')
          }
        },
        afterLoad: function (origin, destination, direction) {
  
        },
        onLeave: function (index, nextIndex, direction) {
          $('.main-nav ').removeClass('active-nav')
          $('.body-wrapper').removeClass('active-body-wrapper')
          $('.wrapper-detail').removeClass('active-wrapper-detail')
          $('.menu').removeClass('show-menu')
          $('.button-nav').removeClass('active-button')
        },
      };

      var rebuild = function() {
        destroyFullPage();
  
        $('#fullpage2').fullpage(options);
      };
  
      var destroyFullPage = function() {
        if ($.fn.fullpage.destroy) {
          $.fn.fullpage.destroy('all');
        }
      };
      rebuild();

      function getSeo(){
        var controllerSeo = requestApiHelper.TEMPLATE.SEO;
          var data = {
              meta: 'biet-thu',
          };
          baseService.POST(controllerSeo, data).then(function (response) {
  
              ngMeta.setTitle(response.SeoTitle);
              ngMeta.setTag('description', response.SeoDescribes);
              ngMeta.setTag('keywords',  response.SeoKeyword);
              ngMeta.setTag('og:image',  baseService.ROOT_URL.concat("assets/includes/upload/files/Panorama_Pool.png"));
              ngMeta.setTag('og:locale',  'vi');
              ngMeta.setTag('author', 'summerland');
          }, function (err) {
              console.log(err);
          });
        
      };
  
      getSeo();

      $scope.section = {
        section_TIEN_ICH: [],
        section_CATESLIDE_IMAGE: [],
        section_SLIDE_IMAGE: [],
        section_SLIDE_IMAGE_TI : [],
        sectionTH: {},
      };
      $scope.dataLoaded = false;
      $scope.dataLoadedTI = false;

      function init(){
        var cSection4_2 = requestApiHelper.CATEGORY.GET + '/' + 'vi';
        var data_Section4_2 = {
            type: 'admin',
            dataType: 7
        };
        baseService.POST(cSection4_2, data_Section4_2).then(function (res4) {
          $scope.section.section_CATESLIDE_IMAGE = res4;
          var param_a = {
            type: 'admin'
          };
          var tabsRequest = [];
          for (let i = 0; i < $scope.section.section_CATESLIDE_IMAGE.length; i++) {
            var controller_a = requestApiHelper.SLIDE.GET_BY_CATEID + '/' + $scope.section.section_CATESLIDE_IMAGE[i].CatID;
            tabsRequest.push(baseService.POST(controller_a, param_a));
          }
      
          Promise.all(tabsRequest).then(values => {
            values.forEach((res_a, i) => {
              var oList = {
                tab: "tab" + $scope.section.section_CATESLIDE_IMAGE[i].CatID,
                list: res_a
              }
              $scope.section.section_SLIDE_IMAGE.push(oList);
            })
      
            $timeout(function () {
              $scope.itemAB = $scope.section.section_SLIDE_IMAGE;
              loadData($scope.itemAB);
            }, 0);
          }, function (err) {
            console.log(err);
          })
        }, function (err) {
          console.log(err);
        });

        var tabsImage = [];
        var param_Image = {
          type: 'admin'
        };
        var controllerImage = requestApiHelper.SLIDE.GET_BY_CATEID + '/' + 61;
        tabsImage.push(baseService.POST(controllerImage, param_Image));
      
          Promise.all(tabsImage).then(values => {
            values.forEach((res_a, i) => {
              var oList = {
                list: res_a
              }
              $scope.section.section_SLIDE_IMAGE_TI.push(oList);
            })
      
            $timeout(function () {
              $scope.itemABC = $scope.section.section_SLIDE_IMAGE_TI;
              loadDataTI($scope.itemABC);
            }, 0);
          }, function (err) {
            console.log(err);
          })

          var cSectionTH = requestApiHelper.TINH_HOA.ALL;
          baseService.GET(cSectionTH).then(function(resth){
              $scope.section.sectionTH = resth[0]; 
          }, function(err){
              console.log(err);
          });
      };
      init(); 

      

      function loadData(x){
        $timeout(function(){
          $scope.itemAs = x;
          $scope.dataLoaded = true;
          console.log($scope.itemAs);
        }, 0);
      };

      function loadDataTI(x){
        $timeout(function(){
          $scope.itemAsTI = x;
          $scope.dataLoadedTI = true;
        }, 0);
      };

    }
  
  })();