-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 27, 2022 at 04:31 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `admin_summerland`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `Id` int(11) NOT NULL,
  `Title` text CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Content` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Image` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `background` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`Id`, `Title`, `Content`, `Image`, `background`) VALUES
(1, 'assets/clients/imgs/tropical-paradise/title-paradise.png', '<p dir=\"ltr\">Sắp tới đ&acirc;y, Phan Thiết sẽ s&ocirc;i động, thu h&uacute;t v&agrave; n&oacute;ng bỏng hơn khi được điểm sắc bởi những gam m&agrave;u nhiệt đới rực rỡ của thi&ecirc;n đường giải tr&iacute; Khu đ&ocirc; thị dịch vụ du lịch Mũi N&eacute; Summerland.</p>\n\n<p>&nbsp;</p>\n\n<p dir=\"ltr\">Với quy m&ocirc; 31,5ha nằm ngay tọa độ v&agrave;ng kết nối &ldquo;thủ phủ resort&rdquo; Mũi N&eacute; v&agrave; th&agrave;nh phố Phan Thiết nhộn nhịp, Mũi N&eacute; Summerland được kỳ vọng sẽ trở th&agrave;nh tổ hợp vui chơi giải tr&iacute; cao cấp bậc nhất B&igrave;nh Thuận. Khi đi v&agrave;o hoạt động, sẽ phục vụ trọn g&oacute;i mọi nhu cầu của du kh&aacute;ch trong v&agrave; ngo&agrave;i nước bằng hệ thống tiện &iacute;ch độc đ&aacute;o &amp; đẳng cấp: g&acirc;y ấn tượng với c&ocirc;ng vi&ecirc;n tương t&aacute;c đa thế hệ Summer Ocean Park với những tr&ograve; chơi hiện đại v&agrave; c&oacute; t&iacute;nh tương t&aacute;c cao, gắn kết c&aacute;c th&agrave;nh vi&ecirc;n trong gia đ&igrave;nh. Dự &aacute;n c&ograve;n sở hữu tuyến phố đi bộ trải d&agrave;i 2.000m kết hợp shopping v&agrave; c&aacute;c hoạt động biểu diễn đường phố; chuỗi c&ocirc;ng vi&ecirc;n chủ đề, clubhouse, nh&agrave; h&agrave;ng ngo&agrave;i trời&hellip;</p>\n\n<p>&nbsp;</p>\n\n<p dir=\"ltr\">Mũi N&eacute; Summerland, điểm đến &quot;check in&quot; giải tr&iacute; h&agrave;ng đầu của du kh&aacute;ch khi đến Phan Thiết, nơi trải nghiệm những khoảng khắc b&ugrave;ng ch&aacute;y v&agrave; đầy m&agrave;u sắc</p>\n', 'assets/includes/upload/files/thien-duong-mien--nhiet-doi.png', 'assets/clients/imgs/tropical-paradise/tropical-paradise.png');

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `ArtID` int(11) NOT NULL,
  `ArtName` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `ArtDescribes` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `ArtMeta` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Image` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `DateCreated` datetime DEFAULT NULL,
  `CatId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Author` int(11) DEFAULT NULL,
  `ArtLang` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ViewCount` int(11) DEFAULT 0,
  `Content` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` tinyint(4) DEFAULT 1,
  `TempId` int(11) DEFAULT NULL,
  `Video` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SeoTitle` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SeoDescribes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SeoCanonica` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MetaRobot` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SeoKeyword` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`ArtID`, `ArtName`, `ArtDescribes`, `ArtMeta`, `Image`, `DateCreated`, `CatId`, `Author`, `ArtLang`, `ViewCount`, `Content`, `Status`, `TempId`, `Video`, `SeoTitle`, `SeoDescribes`, `SeoCanonica`, `MetaRobot`, `SeoKeyword`) VALUES
(14, 'CẬP NHẬT TIẾN ĐỘ DỰ ÁN MŨI NÉ SUMMERLAND THÁNG 5/2021', 'Hưng Lộc Phát Group cập nhật tiến độ dự án Mũi Né Summerland tháng 05/2021: Các tuyến đường giao thông nội bộ đã hoàn thiện; Hệ thống hạ tầng trang thiết bị kỹ thuật đang được triển khai; Các dãy nhà phố, nhà phố thương mại thi công đến tầng 3, 4 và đang dần hoàn thiện mặt ngoài…', 'cap-nhat-tien-do-du-an-mui-ne-summerland-thang-5-2021.html', 'assets/includes/upload/files/thumb-2-min.jpg', '2021-07-15 05:06:00', ',1,', 7, 'vi', 289, '<p><strong>Hưng Lộc Phát Group cập nhật tiến độ dự án Mũi Né Summerland tháng 05/2021: Các tuyến đường giao thông nội bộ đã hoàn thiện; Hệ thống hạ tầng trang thiết bị kỹ thuật đang được triển khai; Các dãy nhà phố, nhà phố thương mại thi công đến tầng 3, 4 và đang dần hoàn thiện mặt ngoài…</strong></p>\n\n<p>Các hình ảnh tại công trình dự án:</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://summerland.swap.vn/assets/includes/upload/files/thumb-1-min.jpg\" /></p>\n\n<p style=\"text-align: center;\">Các dãy nhà phố đã thi công đến tầng 3, 4</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://summerland.swap.vn/assets/includes/upload/files/thumb-3-min.jpg\" /></p>\n\n<p style=\"text-align: center;\">Các dãy nhà phố đang dần hoàn thiện mặt ngoài</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://summerland.swap.vn/assets/includes/upload/images/thumb-2-min.jpg\" /></p>\n\n<p style=\"text-align: center;\">Các dãy nhà phố đang dần hoàn thiện mặt ngoài</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://summerland.swap.vn/assets/includes/upload/files/thumb-4-min.jpg\" /></p>\n\n<p style=\"text-align: center;\">Đội ngũ kỹ sư, công nhân tại công trường</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://summerland.swap.vn/assets/includes/upload/files/thumb-5-min.jpg\" /></p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://summerland.swap.vn/assets/includes/upload/files/thumb-6-min.jpg\" /></p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://summerland.swap.vn/assets/includes/upload/files/thumb-7-min.jpg\" /></p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://summerland.swap.vn/assets/includes/upload/files/thumb-8-min.jpg\" /></p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://summerland.swap.vn/assets/includes/upload/files/thumb-9-min.jpg\" /></p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://summerland.swap.vn/assets/includes/upload/files/thumb-10-min.jpg\" /></p>\n', 1, 0, '', 'CẬP NHẬT TIẾN ĐỘ DỰ ÁN MŨI NÉ SUMMERLAND THÁNG 5/2', 'Hưng Lộc Phát Group cập nhật tiến độ dự án Mũi Né Summerland tháng 05/2021: Các tuyến đường giao thông nội bộ đã hoàn thiện; Hệ thống hạ tầng trang thiết bị kỹ thuật đang được triển khai; Các dãy nhà phố, nhà phố thương mại thi công đến tầng 3, 4 và đang ', 'http://summerland.swap.vn/cap-nhat-tien-do-du-an-mui-ne-summerland-thang-5-2021.html', '', ''),
(15, 'CẬP NHẬT TIẾN ĐỘ DỰ ÁN MŨI NÉ SUMMERLAND THÁNG 2/2021', 'Các tuyến đường giao thông nội bộ đã hoàn thiện, hệ thống hạ tầng – kĩ thuật (hệ thống điện, nước, PCCC, camera & báo cháy…) đang được triển khai thực hiện, các dãy nhà phố, nhà phố thương mại đang được thi công móng, ép cọc, một số dãy phố hoàn thiện đến tầng 3, tầng 4.', 'cap-nhat-tien-do-du-an-mui-ne-summerland-thang-2-2021.html', 'assets/includes/upload/files/zoom-2.png', '2021-02-01 02:10:00', ',1,', 7, 'vi', 96, '<p><strong>Các tuyến đường giao thông nội bộ đã hoàn thiện, hệ thống hạ tầng – kĩ thuật (hệ thống điện, nước, PCCC, camera &amp; báo cháy…) đang được triển khai thực hiện, các dãy nhà phố, nhà phố thương mại đang được thi công móng, ép cọc, một số dãy phố hoàn thiện đến tầng 3, tầng 4.</strong></p>\n\n<p style=\"text-align: center;\">Các hình ảnh tại công trình dự án:</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://summerland.swap.vn/assets/includes/upload/files/zoom-2.png\" /></p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://summerland.swap.vn/assets/includes/upload/files/zoom-6.png\" /></p>\n\n<p style=\"text-align: center;\">Dãy nhà phố 1</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://summerland.swap.vn/assets/includes/upload/files/zoom-8.png\" /></p>\n\n<p style=\"text-align: center;\">Dãy Nhà phố 2</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://summerland.swap.vn/assets/includes/upload/files/zoom-7.png\" /></p>\n\n<p style=\"text-align: center;\">Dãy Nhà phố 2 hướng đồi</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://summerland.swap.vn/assets/includes/upload/files/zoom-9.png\" /></p>\n\n<p style=\"text-align: center;\">Dãy Nhà phố 4</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://summerland.swap.vn/assets/includes/upload/files/zoom-10.png\" /></p>\n\n<p style=\"text-align: center;\">Dãy Nhà phố 5</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://summerland.swap.vn/assets/includes/upload/files/zoom-11.png\" /></p>\n\n<p style=\"text-align: center;\">Dãy Nhà phố thương mại 2</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://summerland.swap.vn/assets/includes/upload/files/zoom-1.png\" /></p>\n\n<p style=\"text-align: center;\">Đội ngũ kỹ sư, công nhân tại công trường</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://summerland.swap.vn/assets/includes/upload/files/zoom-5.png\" /></p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://summerland.swap.vn/assets/includes/upload/files/zoom-4.png\" /></p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://summerland.swap.vn/assets/includes/upload/files/zoom-3.png\" /></p>\n', 1, 0, '', 'CẬP NHẬT TIẾN ĐỘ DỰ ÁN MŨI NÉ SUMMERLAND THÁNG 2/2', 'Các tuyến đường giao thông nội bộ đã hoàn thiện, hệ thống hạ tầng – kĩ thuật (hệ thống điện, nước, PCCC, camera & báo cháy…) đang được triển khai thực hiện, các dãy nhà phố, nhà phố thương mại đang được thi công móng, ép cọc, một số dãy phố hoàn thiện đến', 'http://summerland.swap.vn/cap-nhat-tien-do-du-an-mui-ne-summerland-thang-2-2021.html', '', ''),
(16, 'Cận cảnh khu công viên nước hiện đại bậc nhất Phan Thiết', 'Hưng Lộc Phát Corp đang đẩy mạnh thi công khu công viên nước Summerland hiện đại bậc nhất Phan Thiết, tích hợp hàng trăm trò chơi cảm giác mạnh và giải trí, được tư vấn thiết kế, vận hành bởi các tập đoàn quốc tế uy tín.', 'can-canh-khu-cong-vien-nuoc-hien-dai-bac-nhat-phan-thiet.html', 'assets/includes/upload/files/hung-loc-phat-1a_ngof.jpg', '2020-06-10 05:06:00', ',1,', 7, 'vi', 61, '<p><strong>Hưng Lộc Phát Corp đang đẩy mạnh thi công khu công viên nước Summerland hiện đại bậc nhất Phan Thiết, tích hợp hàng trăm trò chơi cảm giác mạnh và giải trí, được tư vấn thiết kế, vận hành bởi các tập đoàn quốc tế uy tín.</strong></p>\n\n<p>Khu công viên nước đầu tiên tại Phan Thiết, Summer Ocean Park thuộc khu đô thị nghỉ dưỡng và giải trí Mũi Né Summerland tọa lạc trên đường Võ Nguyên Giáp, phường Phú Hài, thành phố Phan Thiết, có quy mô 31,5ha.</p>\n\n<p>“Lấy ý tưởng từ Rồng Thanh Long là linh vật của dự án, Hưng Lộc Phát muốn phát triển một khu công viên nước không chỉ quy mô, hiện đại, mà còn mang những nét đặc trưng riêng biệt của vùng đất nhiệt đới Bình Thuận, tạo nên những trải nghiệm mới mẻ và thú vị cho du khách”, đại diện Hưng Lộc Phát cho biết.</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://summerland.swap.vn/assets/includes/upload/files/hung-loc-phat-1a_ngof.jpg\" /></p>\n\n<p style=\"text-align: center;\">Summer Ocean Park được phát triển bởi ý tưởng độc đáo, mang nét đặc trưng riêng biệt</p>\n\n<p>Hưng Lộc Phát Corp dự kiến sẽ chi cả nghìn tỉ đồng để phát triển khu công viên nước này, tích hợp đầy đủ các trò chơi cảm giác mạnh và giải trí đầu tiên tại Bình Thuận. Bao quanh công viên nước là chuỗi trung tâm thương mại, tổ hợp ẩm thực biển và fastfood.Dự kiến khi đi vào hoạt động trong 2 năm tới, mỗi năm khu công viên nước này sẽ đón khoảng 3 triệu khách du lịch.</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://summerland.swap.vn/assets/includes/upload/files/hung-loc-phat-2a_jqij.jpg\" /></p>\n\n<p style=\"text-align: center;\">Đại công viên nước hiện đại bậc nhất Phan Thiết với sức chứa lên đến 4.500 người</p>\n\n<p>Tại tổ hợp công viên nước với sức chứa lên đến 4.500 người, lần đầu tiên khách du lịch sẽ được trải nghiệm những trò chơi cảm giác cực mạnh, những đợt tăng tốc đột ngột, những cú rơi tự do với: boomerang khổng lồ, đường trượt 6 làn dốc đứng cao, lướt sóng rồng, hố trượt tử thần, dòng sông sóng thần,…Tính sơ bộ, tổng cộng toàn bộ hệ thống ống trượt mạo hiểm của công viên này dài hàng ngàn mét.</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://summerland.swap.vn/assets/includes/upload/files/hung-loc-phat-3ajpg_jjtz.png\" /></p>\n\n<p style=\"text-align: center;\">Phối cảnh tổ hợp giải trí nước cho trẻ nhỏ tại công viên nước Summerland</p>\n\n<p>Ngoài hệ thống trò chơi mạo hiểm, công viên nước của Mũi Né Summerland Resort còn đầu tư tổ hợp vui chơi giải trí cho gia đình và trẻ em với công viên nước cho trẻ nhỏ, hồ bơi resort giật tầng, hồ đôi tạo sóng quy mô.Tại khu hồ đôi tạo sóng kết hợp với sân khấu nổi, là nơi tổ chức các pool party, sự kiện âm nhạc vô cùng sôi động và hấp dẫn mỗi dịp cuối tuần và lễ tết.Công viên nước còn sở hữu dòng sông sóng thần đầu tiên của Bình Thuận dài 300m, sức chứa lên đến hàng ngàn người, cho cả gia đình có những phút giây chơi đùa thỏa thích cùng nhau.</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://summerland.swap.vn/assets/includes/upload/files/hung-loc-phat-4a_tkxu.png\" /></p>\n\n<p style=\"text-align: center;\">Khu công viên nước còn tích hợp các tiện ích nghỉ dưỡng cao cấp</p>\n\n<p>Tối đến, tại FlowRider, du khách có thể vừa ăn uống, tiệc tùng vừa xem biểu diễn lướt sóng nghệ thuật. Hoặc có thể nghỉ ngơi và tận hưởng những phút giây thư giãn tại khu spa, tắm sauna, muối khoáng, xông hơi... hiện đại. Tất cả sẽ biến công viên nước Summer Ocean Park trở thành điểm nhấn riêng biệt của dự án Mũi Né Summerland.</p>\n\n<p>Đáng chú ý, theo chia sẻ từ Hưng Lộc Phát, White Water West và Proslide sẽ thực hiện khâu tư vấn thiết kế, cung cấp thiết bị cho đại công trình này.Được biết, trong hơn 40 năm qua, 2 tập đoàn này đã thi công gần 1.000 công viên nước, trong đó có các công viên nước lớn nhất thế giới như: Epic Waters Indoor Water Park(Mỹ); Happy Magic Water Cube Nanjing(Trung Quốc), SeaWorld Gold Coast(Australia),… Sắp tới Hưng Lộc Phát sẽ tiếp tục hợp tác với một đối tác quốc tế về việc vận hành và phát triển công viên nước này.Cho thấy quyết tâm biến Summer Ocean Park trở thành đại công viên nước quy mô và hiện đại bậc nhất Phan Thiết của chủ đầu tư.</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://summerland.swap.vn/assets/includes/upload/files/hung-loc-phat-5a_psub.jpg\" /></p>\n\n<p style=\"text-align: center;\">Hưng Lộc Phát đẩy mạnh thi công, công trường nhộn nhịp</p>\n\n<p>Bên cạnh đó, khu đô thị nghỉ dưỡng - giải trí Mũi Né Summerland của Công ty Hưng Lộc Phát còn là dự án đón đầu chủ trương phát triển “nền kinh tế đêm” của Bình Thuận, nhằm tạo ra điểm nhấn du lịch mới, giúp du khách có thêm lựa chọn vui chơi giải trí và xài tiền.</p>\n\n<p>Được biết, Hưng Lộc Phát dự kiến sẽ đưa các hạng mục tiện ích của Mũi Né Summerland vào hoạt động trong năm 2021. Khi hoàn thành, ngoài khu công viên nước Summer Ocean Park, Mũi Né Summerland còn sở hữu chuỗi tiện ích đặc biệt như: tuyến phố đi bộ hơn 2.000m; quảng trường Summer Square; khu ẩm thực đa quốc gia; khu shopping, chuỗi công viên đa chủ đề… Tất cả sẽ biến Mũi Né Summerland thành một điểm đến hấp dẫn khó cưỡng dành cho mọi du khách.</p>\n\n<p>Chỉ một thời gian ngắn nữa, không khí hội hè nhộn nhịp như Pattaya hay LasVegas sẽ hiện diện tại Mũi Né Summerland.Du khách đến đây không chỉ đơn thuần nghỉ dưỡng mà còn để trải nghiệm những khoảnh khắc bùng cháy và đầy màu sắc.</p>\n\n<p>Theo đánh giá của một chuyên gia địa ốc, Mũi Né Summerland thu hút đông đảo khách hàng nhờ những ưu thế riêng. Trong những đợt mở bán gần đây, giỏ hàng của chủ đầu tư công bố đều nhanh chóng được thị trường hấp thụ và tạo nên những đợt sóng cho cả thị trường BĐS Phan Thiết.</p>\n\n<p>&nbsp;</p>\n', 1, 0, '', 'Cận cảnh khu công viên nước hiện đại bậc nhất Phan', 'Hưng Lộc Phát Corp đang đẩy mạnh thi công khu công viên nước Summerland hiện đại bậc nhất Phan Thiết, tích hợp hàng trăm trò chơi cảm giác mạnh và giải trí, được tư vấn thiết kế, vận hành bởi các tập đoàn quốc tế uy tín.', 'http://summerland.swap.vn/can-canh-khu-cong-vien-nuoc-hien-dai-bac-nhat-phan-thiet.html', '', ''),
(17, 'Phan Thiết nhiều cơ hội phát triển nghỉ dưỡng trọn gói', 'Các khu phức hợp nghỉ dưỡng - giải trí - mua sắm nghìn tỷ tại địa phương này tạo ra sự sôi động thị trường và thể hiện tầm nhìn dài hạn của các \'ông lớn\'.', 'phan-thiet-nhieu-co-hoi-phat-trien-nghi-duong-tron-goi.html', 'assets/includes/upload/files/1212-8149-1597161257.jpg', '2020-04-15 01:43:57', ',1,', 7, 'vi', 49, '<p><strong>Các khu phức hợp nghỉ dưỡng - giải trí - mua sắm nghìn tỷ tại địa phương này tạo ra sự sôi động thị trường và thể hiện tầm nhìn dài hạn của các \'ông lớn\'.</strong></p>\n\n<p>Chị Ngọc Hà, giám đốc một công ty thiết kế nội thất tại TP HCM, có sở thích du lịch nghỉ dưỡng mỗi khi hoàn tất các công trình lớn. Hè năm nào, chị cũng chọn điểm đến quen thuộc ở Nha Trang và ở liền hơn một tuần hơn. Bởi nơi đây có khu nghỉ dưỡng tọa lạc ở hòn đảo tách biệt với thành phố nhộn nhịp, nhà hàng ẩm thực phục vụ từ sáng sớm đến tối mịt, có công viên nước, khu thể thao biển để vận động. Kể cả du lịch ở Phuket, Bali, Hàn Quốc... chị cũng chọn mô hình nghỉ dưỡng tương tự để ở dài ngày mà không cảm giác nhàm chán.</p>\n\n<p>\"Đến những khu nghỉ dưỡng ‘tất cả trong một’ này, mình không cần phải bước chân ra ngoài, vì mọi nhu cầu từ nghỉ ngơi, tập luyện, mua sắm hay ăn uống đều được đáp ứng tại chỗ\", chị Hà cho biết. \"Nhưng ở Việt Nam thì những điểm đến dạng này chỉ đến trên đầu ngón tay, chưa có nhiều lựa chọn cho những du khách như mình\".</p>\n\n<p><strong>Điểm đến \'tất cả trong một\'</strong></p>\n\n<p>Khu nghỉ dưỡng trọn gói - all-inclusive resort hay ‘tất cả trong một’ – all-in-one resort theo mô tả của chị Ngọc Hà là mô hình nghỉ dưỡng vốn rất quen thuộc tại các điểm đến du lịch nổi tiếng trên thế giới như Caribbean, Hawaii, Mexico, Nam Phi... Tại Đông Nam Á, Thái Lan hay Indonesia cũng sớm phát triển loại hình này.</p>\n\n<p>Nắm bắt đặc điểm thời tiết ấm áp quanh năm mang đến khả năng khai thác du lịch dài ngày, các khu nghỉ dưỡng trọn gói, ngoài những phòng nghỉ sang trọng, còn mang đến đa dạng dịch vụ cho du khách. Khi thời gian ở kéo dài vài tuần lễ đến cả tháng, thì họ không cảm thấy hết hứng thú, bởi ngay tại nội khu có đầy đủ các dịch vụ từ tập luyện, thể thao dưới nước, khu vui chơi - giải trí, chăm sóc sức khỏe, khám phá ẩm thực bản địa, mua sắm... Kể cả phòng nghỉ cũng được bố trí nhiều dạng, từ khách sạn cao tầng, khách sạn nghệ thuật (boutique) hay villa ven biển để du khách trải nghiệm suốt thời gian lưu trú.</p>\n\n<p>Theo chuyên gia tài chính của tờ &lt;em&gt;US Weekly&lt;/em&gt;, các khu nghỉ dưỡng trọn gói trên thế giới ngày càng phát triển, do mang đến sự hài lòng cho đa dạng phân khúc khách hàng. Đối với các du khách hạng sang với hầu bao \"rủng rỉnh\", họ sẽ cảm giác thư giãn, thoải mái hơn khi ở trong không gian đầy đủ tiện ích, cảm giác được phục vụ mọi sở thích, nhu cầu. Trong khi đối với các nhóm khách có tài chính ở tầm trung, các điểm nghỉ dưỡng trọn gói giúp họ cảm thấy an tâm hơn với dịch vụ, dự trù các khoản sẽ chi trong kỳ nghỉ, tránh chọn nhầm những điểm đến không rõ chất lượng, giá tiền...</p>\n\n<p>Bối cảnh Covid-19 càng tăng thêm sức hấp dẫn cho các resort phức hợp, có đa dạng tiện ích nội khu, dịch vụ ngay trong tầm di chuyển của du khách. \"Đại dịch khiến những du khách có nhu cầu nghỉ dưỡng tách biệt, không di chuyển nhiều, hạn chế tiếp xúc. Đây là thế mạnh của các điểm đến \'trọn gói\'. Một số khu nghỉ dưỡng truyền thống cũng sẵn sàng chuyển mình theo mô hình này để khách hàng tăng thêm thời gian lưu trú\", chuyên trang đánh giá các dịch vụ khách sạn - hội nghị</p>\n\n<p>Theo giới du lịch, các khu nghỉ trọn gói thường được bố trí tại những vùng có bãi biển đẹp, không gian yên tĩnh và môi trường sống gần với thiên nhiên. Việt Nam với nhiều bãi biển đẹp trải dài dọc theo các tỉnh là điều kiện thuận lợi để du lịch Việt Nam phát triển loại hình du lịch này. Tuy nhiên, hạn chế về quỹ đất và tiềm lực tài chính khiến số lượng các khu nghỉ dưỡng trọn gói trở thành hàng hiếm trên thị trường.</p>\n\n<p><strong>Cơ hội bứt phá cho Phan Thiết</strong></p>\n\n<p>Mũi Né - Phan Thiết có nhiều tiềm năng để hình thành điểm đến nghỉ dưỡng quốc tế nhờ bãi biển trong xanh, cát trắng, khí hậu ấm áp quanh năm. Cảnh quan tại đây cũng giữ được sự nguyên sơ, tạo hứng thú cho du khách trong nước lẫn quốc tế. Nơi đây được đánh giá là \"thủ phủ resort\" khi sở hữu các khu nghỉ dưỡng từ nhỏ đến lớn. Theo báo Bình Thuận, đến nay địa bàn tỉnh có 387 dự án du lịch đang hoạt động, với tổng diện tích đất 6.249 ha và tổng vốn đầu tư 69.845 tỷ đồng.</p>\n\n<p>\"Dù vậy, thị trường đến nay vẫn vắng bóng các dự án nghỉ dưỡng phức hợp, cung cấp cho khách hàng trải nghiệm nghỉ dưỡng nhiều ngày, tối ưu chi tiêu của du khách\", một chuyên gia địa ốc cho biết.</p>\n\n<p>Tuy nhiên, chuyên gia này nhận định, dự án thành phần cao tốc Dầu Giây - Phan Thiết có tổng chiều dài 99km cũng như kế hoạch xây dựng sân bay Phan Thiết đã trở thành đòn bẩy thu hút cuộc đổ bộ của hàng loạt dự án nghỉ dưỡng nghìn tỷ. Trong đó không ít các dự án được đầu tư theo hình thức đô thị nghỉ dưỡng, khu phức hợp nghỉ dưỡng - giải trí và có cả khu nghỉ dưỡng trọn gói.</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://summerland.swap.vn/assets/includes/upload/files/1212-8149-1597161257.jpg\" /></p>\n\n<p style=\"text-align: center;\">Mũi Né Summerland - tổ hợp all-inclusive resort tại Phan Thiết.</p>\n\n<p>Điển hình như dự án Mũi Né Summerland của chủ đầu tư Hưng Lộc Phát chọn phát triển theo hướng tổ hợp nghỉ dưỡng đa tiện ích, vui chơi - giải trí - thương mại cao cấp bậc nhất Bình Thuận. Khi đi vào hoạt động, sẽ phục vụ trọn gói mọi nhu cầu của du khách trong và ngoài nước, từ phòng nghỉ chuẩn 5 sao, tham dự các lễ hội sắc màu cho đến các trò chơi vận động và trải nghiệm ẩm thực hay mua sắm... Với quy mô 31,5ha, dự án bố trí đến 60 biệt thự, 332 nhà phố, 251 nhà phố thương mại, hơn 2.000 căn hộ biển và khu khách sạn 5 sao.</p>\n\n<p>Thực tế, Mũi Né Summerland sở hữu chuỗi tiện ích lần đầu tiên xuất hiện tại Phan Thiết như tuyến phố đi bộ hơn 2.000m; quảng trường sôi động Summer Square; khu ẩm thực hơn 100 nhà hàng đa quốc gia; khu shopping, khu BBQ sân vườn, clubhouse, cà phê, gym, spa hiện đại...</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://summerland.swap.vn/assets/includes/upload/files/1212-8914-1597161257.jpg\" /></p>\n\n<p style=\"text-align: center;\">Mũi Né Summerland sở hữu công viên nước hơn 20.000 m2 và loạt các tiện ích hiện đại</p>\n\n<p>Nhằm giúp du khách tận hưởng các dịch vụ thể thao nước, chủ đầu tư chi hàng trăm tỷ đồng xây dựng công viên nước Summer Ocean Park hơn 20.000 m2, du khách đến đây sẽ được trải nghiệm hàng trăm trò chơi cảm giác mạnh và giải trí như đường trượt 6 làn dốc đứng cao, lướt sóng rồng, hố trượt tử thần, dòng sông sóng thần, hồ đôi tạo sóng...</p>\n\n<p>Bên cạnh đó, công viên nước Mũi Né Summerland còn có tổ hợp vui chơi giải trí nước cho trẻ em, hồ bơi resort giật tầng, FlowRider không gian để du khách vừa ăn uống, tiệc tùng vừa xem biểu diễn lướt sóng nghệ thuật vào buổi tối. Ngoài ra chuỗi công viên đa chủ đề là không gian cho các buổi picnic, cây cầu Rainbow Bridge còn là điểm check-in cho du khách trẻ.</p>\n\n<p>Phố mua sắm nội khu dự án của chủ đầu tư Hưng Lộc Phát cũng mang phong cách kiến trúc châu Âu. Với chiều dài hơn 2.000m, khu vực này tập trung các cửa hiệu shopping và điểm trải nghiệm văn hóa bản địa cho khách du lịch. Trong đó, phố đi bộ quy tụ chuỗi ẩm thực đa văn hóa, các thương hiệu thời trang hàng đầu. Chủ đầu tư kỳ vọng đây sẽ là tổ hợp giải trí về đêm hiện đại bậc nhất Phan Thiết với những tuyến phố đi bộ, khu phố ẩm thực, giải trí, con đường lễ hội, tiệc tùng chuyên hoạt động về đêm.</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://summerland.swap.vn/assets/includes/upload/files/2122-2834-1597161257.png\" /></p>\n\n<p style=\"text-align: center;\">Tổ hợp giải trí về đêm hiện đại bậc nhất Phan Thiết.</p>\n\n<p>\"Mũi Né Summerland sẽ được quản lý, vận hành bởi các tập đoàn quốc tế hàng đầu. Tất cả nhằm níu chân du khách ở lại Phan Thiết lâu hơn và chi tiêu nhiều hơn\", đại diện chủ đầu tư Hưng Lộc Phát chia sẻ và đồng thời cho rằng việc hình thành tổ hợp đa tiện ích sẽ giúp Phan Thiết trở thành điểm đến đầu tư tiềm năng, tạo động lực cho thị trường bất động sản ven biển Mũi Né.</p>\n\n<p>Hiện toàn bộ hệ thống hạ tầng của dự án cơ bản hoàn thiện. Ngoài ra, hai ngân hàng là VietinBank và Vietcombank bảo trợ vốn đầu tư phát triển dự án lẫn vốn vay cho khách hàng.</p>\n\n<p>Với chị Ngọc Hà, sau khi biết đến thông tin tổ hợp Mũi Né Summerland, vị khách này tỏ ra hào hứng bởi thời gian tới, ngoài Nha Trang, chị và gia đình có thêm địa điểm để nghỉ dưỡng, giải trí, vui chơi theo từng sở thích ngay trong nước mà không phải ra nước ngoài.</p>\n\n<p>\"Việc các ông chủ địa ốc nhanh nhạy với xu hướng của thế giới bằng các tổ hợp dự án lớn đáp ứng nhu cầu của du khách trong và ngoài nước chứng tỏ Phan Thiết và nhiều thành phố biển của Việt Nam có nhiều tiềm năng phát triển phân khúc nghỉ dưỡng\", nữ CEO này nói.</p>\n\n<p>&nbsp;</p>\n', 1, 0, '', 'Phan Thiết nhiều cơ hội phát triển nghỉ dưỡng trọn', 'Các khu phức hợp nghỉ dưỡng - giải trí - mua sắm nghìn tỷ tại địa phương này tạo ra sự sôi động thị trường và thể hiện tầm nhìn dài hạn của các \'ông lớn\'.', 'http://summerland.swap.vn/phan-thiet-nhieu-co-hoi-phat-trien-nghi-duong-tron-goi.html', '', ''),
(18, 'Cao tốc Phan Thiết - Dầu Giây tạo \'cú hích\' thu hút đầu tư', 'TTO - Đó là khẳng định của Phó thủ tướng Trương Hòa Bình tại lễ khởi công dự án cao tốc Phan Thiết - Dầu Giây vào sáng 30-9.', 'cao-toc-phan-thiet-dau-giay-tao-cu-hich-thu-hut-dau-tu.html', 'assets/includes/upload/files/caotoc-1605516126-8562-1605516197.jpg', '2019-09-15 01:51:16', ',1,', 7, 'vi', 33, '<p><strong>TTO - Đó là khẳng định của Phó thủ tướng Trương Hòa Bình tại lễ khởi công dự án cao tốc Phan Thiết - Dầu Giây vào sáng 30-9.</strong></p>\n\n<p style=\"text-align: center;\"><strong><img alt=\"\" src=\"http://summerland.swap.vn/assets/includes/upload/files/2-16014375257901080201956-1601438980938373471548.png\" style=\"width: 586px; height: 180px;\" /></strong></p>\n\n<p style=\"text-align: center;\">Phó Thủ tướng Trương Hòa Bình cùng các bộ ngành và các địa phương ấn nút khởi công dự án - Ảnh: H.M</p>\n\n<p>Sáng 30-9, tại tỉnh Đồng Nai, Phó thủ tướng Trương Hòa Bình phát lệnh lễ khởi công dự án <a href=\"https://tuoitre.vn/cao-toc.html\" target=\"_blank\" title=\"cao tốc\">cao tốc</a> Phan Thiết - Dầu Giây.</p>\n\n<p>Đây là dự án thành phần cao tốc đi qua địa phận 2 tỉnh Bình Thuận, Đồng Nai có chiều dài khoảng 99km. Trong giai đoạn 1 được xây dựng theo quy mô 4 làn xe với tổng mức đầu tư hơn 12.577 tỉ đồng, trong đó chi phí xây dựng là hơn 7.200 tỉ đồng.</p>\n\n<p>Phát biểu tại buổi lễ, Phó thủ tướng Trương Hòa Bình, nói: \"Tuyến cao tốc Phan Thiết - Dầu Giây hoàn thành sẽ là cú hích cho các tỉnh và khu vực dự án đi qua và trực tiếp là tỉnh Đồng Nai, Bình Thuận phát triển kinh tế -xã hội, thu hút đầu tư và góp phần khai thác hết tiềm năng và thế mạnh của các địa phương\".</p>\n\n<p>Theo phó thủ tướng, trước nhu cầu phát triển của đất nước, Đảng và Nhà nước đã quyết định đầu tư xây dựng tuyến đường cao tốc Bắc - Nam, lựa chọn các đoạn ưu tiên làm trước với chiều dài 654km và trong đó có 3 dự án cao tốc Phan Thiết - Dầu Giây, Vĩnh Hảo - Phan Thiết, Mai Sơn - quốc lộ 45.</p>\n\n<p>Nhấn mạnh đến vai trò kết nối của cao tốc Phan Thiết - Dầu Giây, ông Bình nói dự án này sẽ kết nối với các đường bộ cao tốc đã và đang đầu tư.</p>\n\n<p>Dự án rút ngắn hành trình từ TPHCM đi đến các trung tâm kinh tế, mở ra cơ hội đầu tư vào khu vực Nam trung bộ, khắc phục tình trạng ùn tắc giao thông…</p>\n\n<p>Ông Bình cũng lưu ý chủ đầu tư và các nhà thầu xây dựng đúng tiến độ, đảm bảo chất lượng, không để thất thoát và vi phạm pháp luật.</p>\n\n<p>&nbsp;</p>\n', 1, 0, '', 'Cao tốc Phan Thiết - Dầu Giây tạo \'cú hích\' thu hú', 'TTO - Đó là khẳng định của Phó thủ tướng Trương Hòa Bình tại lễ khởi công dự án cao tốc Phan Thiết - Dầu Giây vào sáng 30-9.', 'http://summerland.swap.vn/cao-toc-phan-thiet-dau-giay-tao-cu-hich-thu-hut-dau-tu.html', '', ''),
(19, 'CẬP NHẬT TIẾN ĐỘ DỰ ÁN MŨI NÉ SUMMERLAND THÁNG 10/2021', 'Hưng Lộc Phát Group cập nhật tiến độ dự án Mũi Né Summerland tháng 10/2021: Các tuyến đường giao thông nội bộ đã được hoàn thiện; Hệ thống hạ tầng trang thiết bị kỹ thuật đang được triển khai; Các dãy nhà phố thương mại đang thi công và hoàn thiện phần thô; Các dãy nhà phố đang đang dần hoàn thiện mặt ngoài…', 'cap-nhat-tien-do-du-an-mui-ne-summerland-thang-10-2021.html', 'assets/includes/upload/images/NP1.jpg', '2021-11-27 20:32:01', ',1,', 7, 'vi', 76, '<p>&nbsp;</p>\n\n<p>Hưng Lộc Phát Group cập nhật tiến độ dự án Mũi Né Summerland tháng 10/2021: Các tuyến đường giao thông nội bộ đã được hoàn thiện; Hệ thống hạ tầng trang thiết bị kỹ thuật đang được triển khai; Các dãy nhà phố thương mại đang thi công và hoàn thiện phần thô; Các dãy nhà phố đang đang dần hoàn thiện mặt ngoài…</p>\n\n<p>Các hình ảnh tại công trình dự án:</p>\n\n<p style=\"text-align: center;\"><img alt=\"Dãy nhà phố 1\" src=\"http://muinesummerland.vn/assets/includes/upload/images/NP1.jpg\" style=\"height: 338px; width: 600px;\" /></p>\n\n<p style=\"text-align: center;\">Dãy nhà phố 1</p>\n\n<p style=\"text-align: center;\">&nbsp;</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://muinesummerland.vn/assets/includes/upload/images/NP1-2.jpg\" style=\"height: 338px; width: 601px;\" /></p>\n\n<p style=\"text-align: center;\">Dãy nhà phố 2</p>\n\n<p style=\"text-align: center;\">&nbsp;</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://muinesummerland.vn/assets/includes/upload/images/NP2-3.jpg\" style=\"width: 600px; height: 338px;\" /></p>\n\n<p style=\"text-align: center;\">Dãy nhà phố 2</p>\n\n<p style=\"text-align: center;\">&nbsp;</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://muinesummerland.vn/assets/includes/upload/images/NP4-4.jpg\" style=\"width: 600px; height: 338px;\" /></p>\n\n<p style=\"text-align: center;\">Dãy nhà phố 4</p>\n\n<p style=\"text-align: center;\">&nbsp;</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://muinesummerland.vn/assets/includes/upload/images/NP4-5.jpg\" style=\"height: 338px; width: 600px;\" /></p>\n\n<p style=\"text-align: center;\">Dãy nhà phố 4</p>\n\n<p style=\"text-align: center;\">&nbsp;</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://muinesummerland.vn/assets/includes/upload/images/NP6-1.jpg\" style=\"width: 600px; height: 338px;\" /></p>\n\n<p style=\"text-align: center;\">Dãy nhà phố 6</p>\n\n<p style=\"text-align: center;\">&nbsp;</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://muinesummerland.vn/assets/includes/upload/images/NP6-4.jpg\" style=\"width: 600px; height: 338px;\" /></p>\n\n<p style=\"text-align: center;\">Dãy nhà phố 6</p>\n\n<p style=\"text-align: center;\">&nbsp;</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://muinesummerland.vn/assets/includes/upload/images/Doi%20ngu%20cong%20nhan.jpg\" style=\"width: 600px; height: 338px;\" /></p>\n\n<p style=\"text-align: center;\">Đội ngũ kỹ sư, công nhân tại công trường</p>\n\n<p style=\"text-align: center;\">&nbsp;</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://muinesummerland.vn/assets/includes/upload/images/NP8.jpg\" style=\"width: 600px; height: 338px;\" /></p>\n\n<p style=\"text-align: center;\">Dãy nhà phố thương mại 8</p>\n\n<p style=\"text-align: center;\">&nbsp;</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://muinesummerland.vn/assets/includes/upload/images/NP8-1.jpg\" style=\"width: 599px; height: 338px;\" /></p>\n\n<p style=\"text-align: center;\">Dãy nhà phố thương mại 8</p>\n\n<p style=\"text-align: center;\">&nbsp;</p>\n', 1, 0, '', 'CẬP NHẬT TIẾN ĐỘ DỰ ÁN MŨI NÉ SUMMERLAND THÁNG 10/', 'CẬP NHẬT TIẾN ĐỘ DỰ ÁN MŨI NÉ SUMMERLAND THÁNG 10/2021', 'https://muinesummerland.vn/cap-nhat-tien-do-du-an-mui-ne-summerland-thang-10-2021.html', 'CẬP NHẬT TIẾN ĐỘ DỰ ÁN MŨI NÉ SUMMERLAND THÁNG 10/2021', 'CẬP NHẬT TIẾN ĐỘ DỰ ÁN MŨI NÉ SUMMERLAND THÁNG 10/2021'),
(20, 'CẬP NHẬT TIẾN ĐỘ DỰ ÁN MŨI NÉ SUMMERLAND THÁNG 01/2022', 'Hưng Lộc Phát Group cập nhật tiến độ dự án Mũi Né Summerland tháng 01/2022: Các tuyến đường giao thông nội bộ đã được hoàn thiện; Hệ thống hạ tầng trang thiết bị kỹ thuật đang được triển khai; Các dãy nhà phố thương mại đang thi công và hoàn thiện phần thô; Mặt ngoài các dãy nhà phố đang dần hoàn thiện mặt ngoài.', 'cap-nhat-tien-do-du-an-mui-ne-summerland-thang-01-2022.html', 'assets/includes/upload/files/NP2-2(1).jpg', '2022-01-11 20:45:17', ',1,', 9, 'vi', 74, '<p style=\"margin-left: 0cm;\"><span style=\"font-family:Times New Roman,Times,serif;\"><span style=\"font-size:18px;\"><span lang=\"EN-US\">Hưng Lộc Phát Group cập nhật tiến độ dự án Mũi Né Summerland tháng 01/2022: </span>Các tuyến đường giao thông nội bộ đã được hoàn thiện; Hệ thống hạ tầng trang thiết bị kỹ thuật đang được triển khai; Các dãy nhà phố thương mại đang thi công và hoàn thiện phần thô; <span lang=\"EN-US\">Mặt ngoài các dãy nhà phố đang dần </span>hoàn thiện mặt ngoài<span lang=\"EN-US\">.</span></span></span></p>\n\n<p style=\"margin-left: 0cm;\"><span style=\"font-family:Times New Roman,Times,serif;\"><span style=\"font-size:18px;\"><span lang=\"EN-US\">Các hình ảnh tại công trình dự án: </span></span></span></p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><img alt=\"Dãy nhà phố 1\" src=\"http://muinesummerland.vn/assets/includes/upload/images/2022_01_11_NP1.jpg\" /></p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><span style=\"font-size:16px;\">Dãy nhà phố 1</span></p>\n\n<p style=\"margin-left: 0cm; text-align: center;\">&nbsp;</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><img alt=\"Dãy nhà phố 2\" src=\"http://muinesummerland.vn/assets/includes/upload/images/2022_01_11_NP2.jpg\" /></p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><span style=\"font-size:16px;\">Dãy nhà phố 2</span></p>\n\n<p style=\"margin-left: 0cm; text-align: center;\">&nbsp;</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><img alt=\"Dãy nhà phố 2\" src=\"http://muinesummerland.vn/assets/includes/upload/images/2022_01_11_NP2-2.jpg\" /></p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><span style=\"font-size:16px;\">Dãy nhà phố 2</span></p>\n\n<p style=\"margin-left: 0cm; text-align: center;\">&nbsp;</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><img alt=\"Dãy nhà phố 3\" src=\"http://muinesummerland.vn/assets/includes/upload/images/2022_01_11_NP3.jpg\" /></p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><span style=\"font-size:16px;\">Dãy nhà phố 3</span></p>\n\n<p style=\"margin-left: 0cm; text-align: center;\">&nbsp;</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><img alt=\"\" src=\"http://muinesummerland.vn/assets/includes/upload/images/2022_01_11_NP4.jpg\" /></p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><span style=\"font-size:16px;\">Dãy nhà phố 4</span></p>\n\n<p style=\"margin-left: 0cm; text-align: center;\">&nbsp;</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><span style=\"font-size:16px;\"><img alt=\"\" src=\"http://muinesummerland.vn/assets/includes/upload/images/2022_01_11_NP4-2.jpg\" /></span></p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><span style=\"font-size:16px;\">Dãy nhà phố 4</span></p>\n\n<p style=\"margin-left: 0cm; text-align: center;\">&nbsp;</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><span style=\"font-size:16px;\"><img alt=\"\" src=\"http://muinesummerland.vn/assets/includes/upload/images/2022_01_11_NP5.jpg\" /></span></p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><span style=\"font-size:16px;\">Dãy nhà phố 5</span></p>\n\n<p style=\"margin-left: 0cm; text-align: center;\">&nbsp;</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><span style=\"font-size:16px;\"><img alt=\"\" src=\"http://muinesummerland.vn/assets/includes/upload/images/2022_01_11_NP6-1.jpg\" /></span></p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><span style=\"font-size:16px;\">Dãy nhà phố 6</span></p>\n\n<p style=\"margin-left: 0cm; text-align: center;\">&nbsp;</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><span style=\"font-size:16px;\"><img alt=\"\" src=\"http://muinesummerland.vn/assets/includes/upload/images/2022_01_11_NPTM1.jpg\" /></span></p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><span style=\"font-size:16px;\">Dãy nhà phố thương mại 1</span></p>\n\n<p style=\"margin-left: 0cm; text-align: center;\">&nbsp;</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><span style=\"font-size:16px;\"><img alt=\"\" src=\"http://muinesummerland.vn/assets/includes/upload/images/2022_01_11_NPTM2.jpg\" /></span></p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><span style=\"font-size:16px;\">Dãy nhà phố thương mại 2</span></p>\n\n<p style=\"margin-left: 0cm; text-align: center;\">&nbsp;</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><span style=\"font-size:16px;\"><img alt=\"\" src=\"http://muinesummerland.vn/assets/includes/upload/images/2022_01_11_NPTM7.jpg\" /></span></p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><span style=\"font-size:16px;\">Dãy nhà phố thương mại 7</span></p>\n\n<p style=\"margin-left: 0cm; text-align: center;\">&nbsp;</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><span style=\"font-size:16px;\"><img alt=\"\" src=\"http://muinesummerland.vn/assets/includes/upload/images/2022_01_11_NPTM8.jpg\" />Dãy nhà phố thương mại 8</span></p>\n\n<p style=\"margin-left: 0cm; text-align: center;\">&nbsp;</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><span style=\"font-size:16px;\"><img alt=\"\" src=\"http://muinesummerland.vn/assets/includes/upload/images/2022_01_11_CV.jpg\" /></span></p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><span style=\"font-size:16px;\">Công viên</span></p>\n\n<p style=\"margin-left: 0cm; text-align: right;\"><a href=\"https://hunglocphat.vn\"><strong><span style=\"font-size:16px;\">Hưng Lộc Phát</span></strong></a></p>\n', 1, 0, '', 'CẬP NHẬT TIẾN ĐỘ DỰ ÁN MŨI NÉ SUMMERLAND THÁNG 01/', 'Hưng Lộc Phát Group cập nhật tiến độ dự án Mũi Né Summerland tháng 01/2022: Các tuyến đường giao thông nội bộ đã được hoàn thiện; Hệ thống hạ tầng trang thiết bị kỹ thuật đang được triển khai; Các dãy nhà phố thương mại đang thi công và hoàn thiện phần th', 'https://muinesummerland.vn/cap-nhat-tien-do-du-an-mui-ne-summerland-thang-01-2022.html', '', 'CẬP NHẬT TIẾN ĐỘ DỰ ÁN MŨI NÉ SUMMERLAND THÁNG 01/2022'),
(21, 'Hưng Lộc Phát ký hợp tác chiến lược với Hải Phát Land', '', 'hung-loc-phat-ky-hop-tac-chien-luoc-voi-hai-phat-land.html', 'assets/includes/upload/files/Ch%E1%BB%A7%20%C4%91%E1%BA%A7u%20t%C6%B0%20H%C6%B0ng%20L%E1%BB%99c%20Ph%C3%A1t%20k%C3%BD%20k%E1%BA%BFt%20h%E1%BB%A3p%20t%C3%A1c%20chi%E1%BA%BFn%20l%C6%B0%E1%BB%A3c%20v%C3%A0%20ph%C3%A2n%20ph%E1%BB%91i%20%C4%91%E1%BB%99c%20quy%E1%BB%81n%20ph%C3%A2n%20khu%20The%20Island%20d%E1%BB%B1%20%C3%A1n%20M%C5%A9i%20N%C3%A9%20Summerland%20v%E1%BB%9Bi%20H%E1%BA%A3i%20Ph%C3%A1t%20Land%20t%E1%BA%A1i%20kh%C3%A1ch%20s%E1%BA%A1n%20Sheraton%2C%20TP%20HCM_%20%E1%BA%A2nh%20-%20H%C6%B0ng%20L%E1%BB%99c%20Ph%C3%A1t.png', '2022-02-24 09:22:53', ',1,', 9, 'vi', 38, '<p>Phía Hưng Lộc Phát chọn Hải Phát Land phân phối độc quyền phân khu The Island thuộc dự án Mũi Né Summerland trong sự kiện ngày 23/2 tại TP HCM.</p>\n\n<article>\n<p>Đại diện chủ đầu tư dự án Mũi Né Summerland cho biết, lý do lựa chọn Hải Phát Land trở thành tổng đại lý phân phối phân khu The Island nhờ tiềm lực và kinh nghiệm phân phối, môi giới lâu năm của đơn vị.</p>\n\n<p>&nbsp;</p>\n\n<p><a href=\"https://hunglocphat.vn/wp-content/uploads/2022/02/Chu-dau-tu-Hung-Loc-Phat-ky-ket-hop-tac-chien-luoc-va-phan-phoi-doc-quyen-phan-khu-The-Island-du-an-Mui-Ne-Summerland-voi-Hai-Phat-Land-tai-khach-san-Sheraton-TP-HCM.-Anh-Hung-Loc-Phat.png\"><img alt=\"Chủ đầu tư Hưng Lộc Phát ký kết hợp tác chiến lược và phân phối độc quyền phân khu The Island dự án Mũi Né Summerland với Hải Phát Land tại khách sạn Sheraton, TP HCM. Ảnh: Hưng Lộc Phát\" src=\"https://hunglocphat.vn/wp-content/uploads/2022/02/Chu-dau-tu-Hung-Loc-Phat-ky-ket-hop-tac-chien-luoc-va-phan-phoi-doc-quyen-phan-khu-The-Island-du-an-Mui-Ne-Summerland-voi-Hai-Phat-Land-tai-khach-san-Sheraton-TP-HCM.-Anh-Hung-Loc-Phat.png\" style=\"width: 680px; height: 408px;\" /></a></p>\n\n<p>Chủ đầu tư Hưng Lộc Phát ký kết hợp tác chiến lược và phân phối độc quyền phân khu The Island dự án Mũi Né Summerland với Hải Phát Land tại khách sạn Sheraton, TP HCM. Ảnh: Hưng Lộc Phát</p>\n\n<p>&nbsp;</p>\n</article>\n\n<p>Khu đô thị dịch vụ du lịch Mũi Né Summerland có quy mô 31,5 ha, tọa lạc tại Mũi Né, Phan Thiết. Là dự án gần sân bay Phan Thiết, Summerland sở hữu lợi thế nằm đối diện sân golf Sealink, trên ngọn đồi Hải Vọng - một trong những nơi khách du lịch thường đến ngắm hoàng hôn trên biển. Xung quanh dự án bao bọc bởi hai tuyến đường huyết mạch Võ Nguyên Giáp và Quốc lộ 1A. Dự kiến sau khi cao tốc Dầu Giây - Phan Thiết đi vào hoạt động, thời gian di chuyển từ TP HCM đến Phan Thiết tốn trung bình khoảng 3h.</p>\n\n<p>Khi đi vào hoạt động, dự án hứa hẹn đáp ứng nhu cầu của du khách trong và ngoài nước với hệ thống tiện ích độc đáo. Nơi đây được kỳ vọng trở thành điểm đến thu hút với những gam màu nhiệt đới, mang đến những trải nghiệm nghỉ dưỡng tiện nghi, sang trọng đúng chất \"vùng đất mùa hè\".</p>\n\n<p>&nbsp;</p>\n\n<p><a href=\"https://hunglocphat.vn/wp-content/uploads/2022/02/Hinh-anh-tai-le-ky-ket.-Anh-Hung-Loc-Phat.png\"><img alt=\"Hình ảnh tại lễ ký kết. Ảnh: Hưng Lộc Phát\" src=\"https://hunglocphat.vn/wp-content/uploads/2022/02/Hinh-anh-tai-le-ky-ket.-Anh-Hung-Loc-Phat.png\" style=\"width: 680px; height: 430px;\" /></a></p>\n\n<p>Hình ảnh tại lễ ký kết. Ảnh: Hưng Lộc Phát</p>\n\n<p>&nbsp;</p>\n\n<p>\"Thừa hưởng hệ tiện ích hiện đại và sôi động, phân khu The Island sẽ là điểm dừng chân lý tưởng cho những tâm hồn yêu mùa hè, tận hưởng những khoảnh khắc đáng nhớ\", đại diện chủ đầu tư cho biết.</p>\n\n<p>Với thiết kế đương đại, tinh tế, mỗi căn nhà phố nghỉ dưỡng tại đây sẽ đáp ứng tiêu chí tối ưu mặt thoáng, tràn ngập ánh sáng tự nhiên giúp chủ nhân có thể tận hưởng hương vị biển cả ngay khi mở cửa.</p>\n\n<p>Đặc biệt, phân khu còn được thừa hưởng tiện ích công viên tương tác đa thế hệ, không gian vui chơi dành riêng cho các gia đình có trẻ nhỏ, giúp gia tăng tương tác và sự gắn kết giữa bố mẹ, ông bà với con trẻ. Mảng xanh của chuỗi công viên chủ đề là điểm nhấn giúp tạo không khí trong lành, thư thái, giao hòa giữa không gian thiên nhiên với sự náo nhiệt của phố thị hiện đại.</p>\n\n<p>&nbsp;</p>\n\n<p><a href=\"https://hunglocphat.vn/wp-content/uploads/2022/02/Phoi-canh-phan-khu-The-Island-thua-huong-he-tien-ich-soi-dong-va-hien-dai-cua-du-an-Summerland.-Anh-Hung-Loc-Phat.png\"><img alt=\"Phối cảnh phân khu The Island thừa hưởng hệ tiện ích sôi động và hiện đại của dự án Summerland. Ảnh: Hưng Lộc Phát\" src=\"https://hunglocphat.vn/wp-content/uploads/2022/02/Phoi-canh-phan-khu-The-Island-thua-huong-he-tien-ich-soi-dong-va-hien-dai-cua-du-an-Summerland.-Anh-Hung-Loc-Phat.png\" style=\"width: 680px; height: 244px;\" /></a></p>\n\n<p>Phối cảnh phân khu The Island thừa hưởng hệ tiện ích sôi động và hiện đại của dự án Summerland. Ảnh: Hưng Lộc Phát</p>\n\n<p>&nbsp;</p>\n\n<p>Năm 2020, Mũi Né - Phan Thiết được công nhận là khu du lịch quốc gia và trở thành một trong những điểm đến đắt giá của Việt Nam. Ngoài đường bờ biển dài ngát xanh và những đồi dương, cát trập trùng, Mũi Né còn nổi tiếng với khí hậu nhiệt đới quanh năm, thuận lợi khai thác du lịch.</p>\n\n<p>\"Thị trường bất động sản nghỉ dưỡng đã chứng kiến sự xuất hiện của hàng loạt chủ đầu tư lớn như Vingroup, Sungroup, FLC.. khiến thị trường thêm phần sôi động và hấp dẫn các nhà đầu tư. Đặc biệt, tại thị trường Mũi Né, các dự án chất lượng quốc tế đang trở thành mục tiêu săn tìm của các nhà đầu tư bởi tính thanh khoản cao, tiềm năng sinh lợi lớn\" đại diện Hải Phát Land cho biết.</p>\n\n<p style=\"text-align: right;\"><a href=\"https://vnexpress.net/hung-loc-phat-ky-hop-tac-chien-luoc-voi-hai-phat-land-4431122.html\" rel=\"noopener\" target=\"_blank\"><strong>VnExpress.net</strong></a></p>\n', 1, 0, '', 'Hưng Lộc Phát ký hợp tác chiến lược với Hải Phát L', 'Phía Hưng Lộc Phát chọn Hải Phát Land phân phối độc quyền phân khu The Island thuộc dự án Mũi Né Summerland trong sự kiện ngày 23/2 tại TP HCM.', 'https://muinesummerland.vn/hung-loc-phat-ky-hop-tac-chien-luoc-voi-hai-phat-land.html', '', 'Hưng Lộc Phát ký hợp tác chiến lược với Hải Phát Land'),
(22, 'CẬP NHẬT TIẾN ĐỘ DỰ ÁN MŨI NÉ SUMMERLAND THÁNG 03/2022', 'Hưng Lộc Phát Group cập nhật tiến độ dự án Mũi Né Summerland tháng 03/2022. Các hình ảnh tại công trình dự án:', 'cap-nhat-tien-do-du-an-mui-ne-summerland-thang-03-2022.html', 'assets/includes/upload/files/NP3(1).jpg', '2022-03-09 09:48:47', ',1,', 9, 'vi', 69, '<p style=\"margin-left:0cm\"><span lang=\"EN-US\" style=\"font-family:&quot;Times New Roman&quot;,serif\">Hưng Lộc Phát Group cập nhật tiến độ dự án Mũi Né Summerland tháng 03/2022. Các hình ảnh tại công trình dự án: </span></p>\n\n<p style=\"margin-left:0cm\">&nbsp;</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><img alt=\"Dãy nhà phố 1\" src=\"http://muinesummerland.vn/assets/includes/upload/files/NP1(2).jpg\" />Dãy nhà phố 1</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\">&nbsp;</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><img alt=\"Dãy nhà phố 2\" src=\"http://muinesummerland.vn/assets/includes/upload/files/NP2_1.jpg\" />Dãy nhà phố 2</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\">&nbsp;</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><img alt=\"Dãy nhà phố 2\" src=\"http://muinesummerland.vn/assets/includes/upload/files/NP2(1).jpg\" />Dãy nhà phố 2</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\">&nbsp;</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><img alt=\"Dãy nhà phố 3\" src=\"http://muinesummerland.vn/assets/includes/upload/files/NP3(1).jpg\" />Dãy nhà phố 3</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\">&nbsp;</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><img alt=\"Dãy nhà phố 4\" src=\"http://muinesummerland.vn/assets/includes/upload/files/NP4_1.jpg\" />Dãy nhà phố 4</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\">&nbsp;</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><img alt=\"Dãy nhà phố 5\" src=\"http://muinesummerland.vn/assets/includes/upload/files/NP5_1.jpg\" />Dãy nhà phố 5</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\">&nbsp;</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><img alt=\"Dãy nhà phố 5\" src=\"http://muinesummerland.vn/assets/includes/upload/files/NP5(1).jpg\" />Dãy nhà phố 5</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\">&nbsp;</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><img alt=\"Dãy nhà phố 6\" src=\"http://muinesummerland.vn/assets/includes/upload/files/NP6_1.jpg\" />Dãy nhà phố 6</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\">&nbsp;</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><img alt=\"Dãy nhà phố 6\" src=\"http://muinesummerland.vn/assets/includes/upload/files/NP6_3.jpg\" />Dãy nhà phố 6</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\">&nbsp;</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><img alt=\"Dãy nhà phố 10\" src=\"http://muinesummerland.vn/assets/includes/upload/files/NP10_1.jpg\" />Dãy nhà phố 10</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\">&nbsp;</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><img alt=\"Dãy nhà phố 10\" src=\"http://muinesummerland.vn/assets/includes/upload/files/NP10_2.jpg\" />Dãy nhà phố 10</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\">&nbsp;</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><img alt=\"Dãy nhà phố 10\" src=\"http://muinesummerland.vn/assets/includes/upload/files/NP10.jpg\" />Dãy nhà phố 10</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\">&nbsp;</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><img alt=\"Dãy nhà phố thương mại 1\" src=\"http://muinesummerland.vn/assets/includes/upload/files/NPTM1(1).jpg\" />Dãy nhà phố thương mại 1</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\">&nbsp;</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><img alt=\"Dãy nhà phố thương mại 2\" src=\"http://muinesummerland.vn/assets/includes/upload/files/NPTM2(1).jpg\" />Dãy nhà phố thương mại 2</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\">&nbsp;</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><img alt=\"Dãy nhà phố thương mại 7\" src=\"http://muinesummerland.vn/assets/includes/upload/files/NPTM7_1.jpg\" />Dãy nhà phố thương mại 7</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\">&nbsp;</p>\n\n<p style=\"margin-left: 0cm; text-align: center;\"><img alt=\"Dãy nhà phố thương mại 8\" src=\"http://muinesummerland.vn/assets/includes/upload/files/NPTM8(1).jpg\" />Dãy nhà phố thương mại 8</p>\n\n<p style=\"margin-left: 0cm; text-align: right;\"><a href=\"https://hunglocphat.vn/\" target=\"_blank\"><strong>Hưng Lộc Phát</strong></a></p>\n', 1, 0, '', 'CẬP NHẬT TIẾN ĐỘ DỰ ÁN MŨI NÉ SUMMERLAND THÁNG 03/', 'Hưng Lộc Phát Group cập nhật tiến độ dự án Mũi Né Summerland tháng 03/2022. Các hình ảnh tại công trình dự án:', 'https://muinesummerland.vn/cap-nhat-tien-do-du-an-mui-ne-summerland-thang-03-2022.html', '', 'CẬP NHẬT TIẾN ĐỘ DỰ ÁN MŨI NÉ SUMMERLAND THÁNG 03/2022');

-- --------------------------------------------------------

--
-- Table structure for table `article_tq`
--

CREATE TABLE `article_tq` (
  `id` int(11) NOT NULL,
  `background` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `article_tq`
--

INSERT INTO `article_tq` (`id`, `background`, `title`) VALUES
(1, 'assets/clients/imgs/tintucduan/bg_tintucduan.png', 'TIN TỨC DỰ ÁN');

-- --------------------------------------------------------

--
-- Table structure for table `assignment`
--

CREATE TABLE `assignment` (
  `id` int(11) NOT NULL,
  `billCustomId` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status_assign` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `waitDate` date DEFAULT NULL,
  `assignDate` date DEFAULT NULL,
  `approveDate` date DEFAULT NULL,
  `area` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `assignment`
--

INSERT INTO `assignment` (`id`, `billCustomId`, `user_id`, `status_assign`, `status`, `waitDate`, `assignDate`, `approveDate`, `area`) VALUES
(57, 'TEST01', 33, 2, 3, '2020-09-05', '2020-09-06', '2020-09-05', 'Hồ Chí Minh'),
(58, 'aaaaaaaaaaaaa', 0, 0, 0, NULL, '0000-00-00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `benefit_list`
--

CREATE TABLE `benefit_list` (
  `id` int(11) NOT NULL,
  `title_1` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `title_2` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci NOT NULL,
  `icon` longtext COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `benefit_list`
--

INSERT INTO `benefit_list` (`id`, `title_1`, `title_2`, `title`, `icon`, `content`) VALUES
(1, 'Pháp Lý', 'chuẩn chỉnh', 'Pháp Lý chuẩn chỉnh', 'assets/clients/imgs/loiich/icon/phap-li-chuan-chinh.png', 'Pháp lý chuẩn chỉnh và minh bạch nhất thị trường Phan Thiết, sở hữu lâu dài'),
(2, 'Vị trí', 'đắt giá', 'Vị trí đắt giá', 'assets/clients/imgs/loiich/icon/vi-tri-dat-gia.png', 'Tọa lạc ngay trung tâm Thủ Phủ Resort, gần sân bay Phan Thiết nhất trong bán kính 2km'),
(3, 'Concept', 'độc đáo', 'Concept độc đáo', 'assets/clients/imgs/loiich/icon/doc-dao.png', 'Dự án được định hướng phát triển thành thiên đường giải trí hiện đại hàng đầu Phan Thiết'),
(4, 'Sức bật', 'hạ tầng', 'Sức bật hạ tầng', 'assets/clients/imgs/loiich/icon/suc-bat-ha-tang.png', 'Đón đầu cơ hội khi Sân bay Phan Thiết và Cao tốc Dầu Giây - Phan Thiết sẽ hoàn thành năm 2022'),
(5, 'Thế mạnh', 'du lịch', 'Thế mạnh du lịch', 'assets/clients/imgs/loiich/icon/the-manh-du-lich.png', 'Phan Thiết định hướng trở thành Đô thị du lịch biển cấp quốc gia thu hút du khách  trong & ngoài nước');

-- --------------------------------------------------------

--
-- Table structure for table `benefit_tq`
--

CREATE TABLE `benefit_tq` (
  `id` int(11) NOT NULL,
  `background` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `benefit_tq`
--

INSERT INTO `benefit_tq` (`id`, `background`, `title`) VALUES
(1, 'assets/clients/imgs/loiich/bg_connect.png', 'LỢI ÍCH ĐẦU TƯ');

-- --------------------------------------------------------

--
-- Table structure for table `billdetails`
--

CREATE TABLE `billdetails` (
  `billDetailId` int(11) NOT NULL,
  `billId` int(11) DEFAULT NULL,
  `productName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `productUnit` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `productQuantity` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bills`
--

CREATE TABLE `bills` (
  `billId` int(11) NOT NULL,
  `createdDate` datetime DEFAULT NULL,
  `fromPlace` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight` float NOT NULL DEFAULT 0,
  `userName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `customerSend` int(11) DEFAULT NULL,
  `receiveName` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `receivePhone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provinceTo` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `districtTo` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wardTo` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `addressTo` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `unitPrice` decimal(10,0) NOT NULL DEFAULT 0,
  `fuelFee` decimal(10,0) NOT NULL DEFAULT 0,
  `totalPrice` decimal(10,0) NOT NULL DEFAULT 0,
  `vat` decimal(10,0) NOT NULL DEFAULT 0,
  `totalDiscount` decimal(10,0) NOT NULL DEFAULT 0,
  `isDeleted` bit(1) NOT NULL DEFAULT b'0',
  `billCustomId` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customerSendName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customerSendPhone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `companyName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customerSendAddress` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serviceId` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serviceNote` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serviceCOD` float NOT NULL DEFAULT 0,
  `typeOfParcel` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payments` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `valuesOfParcel` float NOT NULL DEFAULT 0,
  `addressDetail` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `additionalFee` decimal(10,0) NOT NULL DEFAULT 0,
  `plusServices` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `addressFrom` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fromPlaceDetail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `districtToDetail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `otherFee` decimal(10,0) NOT NULL DEFAULT 0,
  `provinceFrom` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `receiveIsName` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Comment` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bills`
--

INSERT INTO `bills` (`billId`, `createdDate`, `fromPlace`, `weight`, `userName`, `user_id`, `customerSend`, `receiveName`, `receivePhone`, `provinceTo`, `districtTo`, `wardTo`, `addressTo`, `status`, `unitPrice`, `fuelFee`, `totalPrice`, `vat`, `totalDiscount`, `isDeleted`, `billCustomId`, `customerSendName`, `customerSendPhone`, `companyName`, `customerSendAddress`, `serviceId`, `serviceNote`, `serviceCOD`, `typeOfParcel`, `payments`, `valuesOfParcel`, `addressDetail`, `additionalFee`, `plusServices`, `addressFrom`, `fromPlaceDetail`, `districtToDetail`, `otherFee`, `provinceFrom`, `image`, `receiveIsName`, `Comment`) VALUES
(121, '2020-09-05 10:39:50', 'HCQ1', 3500, 'Thế Hệ Mới', 33, 5, 'ÈEEF', '34343434', 'HNI', 'HNHT', NULL, '123', 3, '96342', '14451', '121873', '11079', '0', b'0', 'TEST01', 'CÔNG TY TNHH GIẢI TRÍ CJ HK', '0971169169', 'CÔNG TY TNHH GIẢI TRÍ CJ HK', NULL, 'CPN', NULL, 0, 'Hàng hoá', 'Theo hợp đồng', 0, '123, QUẬN HAI BÀ TRƯNG, HNI', '0', NULL, '2-4-6 BIS ĐƯỜNG LÊ THÁNH TÔN, P BẾN NGHÉ', 'Hồ Chí Minh, QUẬN 1', 'QUẬN HAI BÀ TRƯNG', '0', 'HCM', 'assets/includes/upload/files/1596120029867.JPEG', 'aaa', 'aaaa');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `CatID` int(11) NOT NULL,
  `CatName` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `CatMeta` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `CatDescribes` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `TempId` int(11) DEFAULT NULL,
  `CatLang` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Type` int(11) DEFAULT 1,
  `CatParent` int(11) DEFAULT NULL,
  `Image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SeoTitle` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SeoDescribes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SeoKeyword` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SeoCanonica` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MetaRobot` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`CatID`, `CatName`, `CatMeta`, `CatDescribes`, `TempId`, `CatLang`, `Type`, `CatParent`, `Image`, `SeoTitle`, `SeoDescribes`, `SeoKeyword`, `SeoCanonica`, `MetaRobot`, `Status`) VALUES
(1, 'Tin tức', 'tin-tuc', '', 14, 'vi', 1, 0, NULL, 'Tin tức', 'Tin tức - B.A.T', 'Tin tức', NULL, NULL, 1),
(26, 'Slide 2', 'slide-2', '', NULL, 'vi', 3, 0, '', '', '', '', '', '', 1),
(27, 'Slide 5', 'slide-5', '', NULL, 'vi', 3, 0, '', '', '', '', '', '', 1),
(28, 'Slide 7', 'slide-7', '', NULL, 'vi', 3, 0, '', '', '', '', '', '', 1),
(29, 'Đơn vị phát triển', 'don-vi-phat-trien', '', 0, 'vi', 3, 0, '', '', '', '', '', '', 1),
(30, 'Đơn vị tổng thầu', 'don-vi-tong-thau', '', NULL, 'vi', 3, 0, '', '', '', '', '', '', 1),
(31, 'Phân phối', 'phan-phoi', '', NULL, 'vi', 3, 0, '', '', '', '', '', '', 1),
(37, 'Background Trang chủ', 'background-trang-chu', '', NULL, 'vi', 3, 0, '', '', '', '', '', '', 1),
(38, 'Slogan', 'slogan', '', NULL, 'vi', 3, 0, '', '', '', '', '', '', 1),
(39, 'Hình nhỏ Slide 2', 'hinh-nho-slide-2', '', NULL, 'vi', 3, 0, '', '', '', '', '', '', 1),
(40, 'Hình nhỏ Slide 4', 'hinh-nho-slide-4', '', NULL, 'vi', 3, 0, '', '', '', '', '', '', 1),
(41, 'Hình nhỏ Slide 5', 'hinh-nho-slide-5', '', NULL, 'vi', 3, 0, '', '', '', '', '', '', 1),
(42, 'Hình nhỏ Slide 6', 'hinh-nho-slide-6', '', NULL, 'vi', 3, 0, '', '', '', '', '', '', 1),
(43, 'Hình nhỏ Slide 7', 'hinh-nho-slide-7', '', NULL, 'vi', 3, 0, '', '', '', '', '', '', 1),
(44, 'Background Youtube', 'bg-youtube', '', NULL, 'vi', 3, 0, '', '', '', '', '', '', 1),
(51, 'Summer Walking Street', 'tien-ich', '', NULL, 'vi', 4, 0, '', '', '', '', '', '', 1),
(52, 'Summer Paradise', 'tien-ich', '', NULL, 'vi', 4, 0, '', '', '', '', '', '', 1),
(53, 'Summer Garden', 'tien-ich', '', NULL, 'vi', 4, 0, '', '', '', '', '', '', 1),
(54, '02/2021', 'tien-do', '', 0, 'vi', 5, 0, '', '', '', '', '', '', 1),
(55, '05/2021', 'tien-do', '', NULL, 'vi', 5, 0, '', '', '', '', '', '', 1),
(56, '10/2021', 'tien-do', '', 0, 'vi', 5, 0, '', '', '', '', '', '', 1),
(58, '01/2022', 'tien-do', '', NULL, 'vi', 5, 0, '', '', '', '', '', '', 1),
(59, '03/2022', 'tien-do', '', NULL, 'vi', 5, 0, '', '', '', '', '', '', 1),
(60, 'Summer Ocean Park', 'tien-ich', '', NULL, 'vi', 4, 0, '', '', '', '', '', '', 1),
(61, 'Tiện ích Biệt Thự', 'bt-tien-ich', NULL, NULL, 'vi', 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(62, 'Biệt thự', 'bt-nt', '', NULL, 'vi', 7, 0, '', '', '', '', '', '', 1),
(63, 'Nhà phố', 'bt-nt', '', NULL, 'vi', 7, 0, '', '', '', '', '', '', 1),
(64, 'Loại hình sản phẩm', 'loai-hinh-san-pham', NULL, NULL, 'vi', 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `categoryhistory`
--

CREATE TABLE `categoryhistory` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categoryhistory`
--

INSERT INTO `categoryhistory` (`id`, `name`) VALUES
(1, 'BƯU PHẨM PHÁT THÀNH CÔNG'),
(2, 'BƯU PHẨM PHÁT KHÔNG THÀNH CÔNG'),
(3, 'BƯU PHẨM PHÁT KHÔNG THÀNH CÔNG - KHÁCH HÀNG BỔ SUNG THÔNG TIN GIAO LẠI THÀNH CÔNG'),
(4, 'BƯU PHẨM ĐANG TIẾN HÀNH PHÁT');

-- --------------------------------------------------------

--
-- Table structure for table `connect`
--

CREATE TABLE `connect` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `detail` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `connect`
--

INSERT INTO `connect` (`id`, `name`, `detail`) VALUES
(1, 'SĐT', '0965986468'),
(2, 'FANGPAGE FACEBOOK', 'https://www.facebook.com/TapdoanHungLocPhat'),
(3, 'Zalo', 'https://zalo.me/1747415237432370179'),
(4, 'Email', 'sales@muinesummerland.vn');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `Id` int(11) NOT NULL,
  `Title` text CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Titledisplay` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Content` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Content_2` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`Id`, `Title`, `Titledisplay`, `Content`, `Content_2`) VALUES
(1, 'CÔNG TY CỔ PHẦN ATTLAND', '<span class=\"char1\">C</span><span class=\"char2\">Ô</span><span class=\"char3\">N</span><span class=\"char4\">G</span><span class=\"char5\"> </span><span class=\"char6\">T</span><span class=\"char7\">Y</span><span class=\"char8\"> </span><span class=\"char9\">C</span><span class=\"char10\">Ổ</span><span class=\"char11\"> </span><span class=\"char12\">P</span><span class=\"char13\">H</span><span class=\"char14\">Ầ</span><span class=\"char15\">N</span><span class=\"char16\"> </span><span class=\"char17\">A</span><span class=\"char18\">T</span><span class=\"char19\">T</span><span class=\"char20\">L</span><span class=\"char21\">A</span><span class=\"char22\">N</span><span class=\"char23\">D</span>', '<p>L9, Khu phố Ng&atilde;i Thắng, Phường B&igrave;nh Thắng, TP. Dĩ An, Tỉnh B&igrave;nh Dương</p>\n\n<p>Điện thoại: <a href=\"tel:+8428 7778 8989\">(028) 7778 8989</a></p>\n\n<p>Email: <a href=\"mailto:info@angia.com.vn\">info@attland.com.vn</a></p>\n', '<p>Dự &aacute;n<strong> </strong>ARTELLA: Đường Thống Nhất, TP. Dĩ An</p>\n');

-- --------------------------------------------------------

--
-- Table structure for table `contact_tq`
--

CREATE TABLE `contact_tq` (
  `id` int(11) NOT NULL,
  `background` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contact_tq`
--

INSERT INTO `contact_tq` (`id`, `background`, `title`) VALUES
(1, 'assets/clients/imgs/footer/home-footer-bg.jpg', 'Liên hệ');

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE `content` (
  `Id` int(11) NOT NULL,
  `Silde_Name` varchar(1) DEFAULT NULL,
  `Silde` int(11) DEFAULT NULL,
  `Title` text CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Titledisplay` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Content` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`Id`, `Silde_Name`, `Silde`, `Title`, `Titledisplay`, `Content`) VALUES
(1, 'A', 2, 'ARTELLA', '<span class=\"char1\">A</span><span class=\"char2\">R</span><span class=\"char3\">T</span><span class=\"char4\">E</span><span class=\"char5\">L</span><span class=\"char6\">L</span><span class=\"char7\">A</span>', '<p>BIỂU TƯỢNG THÀNH PH&Ocirc;́ THỦ ĐỨC</p>\n'),
(2, 'T', 4, 'MỘT BƯỚC XUỐNG PHỐ', '<span class=\"char1\">M</span><span class=\"char2\">Ộ</span><span class=\"char3\">T</span><span class=\"char4\"> </span><span class=\"char5\">B</span><span class=\"char6\">Ư</span><span class=\"char7\">Ớ</span><span class=\"char8\">C</span><span class=\"char9\"> </span><span class=\"char10\">X</span><span class=\"char11\">U</span><span class=\"char12\">Ố</span><span class=\"char13\">N</span><span class=\"char14\">G</span><span class=\"char15\"> </span><span class=\"char16\">P</span><span class=\"char17\">H</span><span class=\"char18\">Ố</span>', '<p>Từ Artella chỉ cần một bước ch&acirc;n xuống phố sẽ tiếp cận ngay tuyến Metro, n&acirc;ng tầm vị thế cho chủ nh&acirc;n &ndash; những người trẻ năng động, s&aacute;ng tạo v&agrave; th&agrave;nh đạt.</p>\n'),
(3, 'E', 5, 'TIỆN ÍCH', '<span class=\"char1\">T</span><span class=\"char2\">I</span><span class=\"char3\">Ệ</span><span class=\"char4\">N</span><span class=\"char5\"> </span><span class=\"char6\">Í</span><span class=\"char7\">C</span><span class=\"char8\">H</span>', '<p>HƠN 20+ TIỆN &Iacute;CH ĐẲNG CẤP KH&Aacute;C BIỆT</p>\n'),
(4, 'L', 7, 'HÌNH ẢNH', '<span class=\"char1\">H</span><span class=\"char2\">Ì</span><span class=\"char3\">N</span><span class=\"char4\">H</span><span class=\"char5\"> </span><span class=\"char6\">Ả</span><span class=\"char7\">N</span><span class=\"char8\">H</span>', '<p>T&acirc;m đi&ecirc;̉m Thành ph&ocirc;́ Thủ Đức</p>\n');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `ID` int(11) NOT NULL,
  `Name` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Estimate` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Image` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Address` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Language` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `companyName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `district` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `districtDetail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `province` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`ID`, `Name`, `Estimate`, `Image`, `Address`, `Phone`, `Language`, `Email`, `companyName`, `district`, `districtDetail`, `province`) VALUES
(1, 'Phạm Quang Huy', '', '', '', '0917 63 61 69', 'vi', 'info@modunsoft.com', 'Công ty TNHH ModunSoft', NULL, NULL, 'HCM'),
(3, 'A TIẾN', 'bản test', '', 'ĐƯỜNG 47 PHƯỜNG HIỆP BÌNH CHÁNH', '0971169169', 'vi', 'info@thehemoiexpress.vn', 'SHOP TIÊU XANH', 'HCTD', 'Hồ Chí Minh, QUẬN THỦ ĐỨC', 'HCM'),
(4, 'MR ĐẠT', '', '', '2-4-6Bis Lê Thánh Tôn, P.Bến nghé, Quận 1, Tp.HCM', '028 38282224', 'vi', '', 'CÔNG TY TNHH GIẢI TRÍ CJ HK', 'HCQ1', 'Hồ Chí Minh, QUẬN 1', 'HCM'),
(5, 'CÔNG TY TNHH GIẢI TRÍ CJ HK', '', '', '2-4-6 BIS ĐƯỜNG LÊ THÁNH TÔN, P BẾN NGHÉ', '0971169169', 'vi', '', 'CÔNG TY TNHH GIẢI TRÍ CJ HK', 'HCQ1', 'Hồ Chí Minh, QUẬN 1', 'HCM'),
(25, 'Vinh Plus', '', '', '123 nguyễn công trứ', '0941783796', 'vi', 'v@gmail.com', 'VP Solutions', 'HCQ1', 'Hồ Chí Minh, QUẬN 1', 'HCM'),
(26, 'aaa', 'aaa', '', 'aaa', '01234', 'vi', 'aaa', 'sssss', 'HNHT', 'Hà Nội, QUẬN HAI BÀ TRƯNG', 'HNI'),
(27, 'aaaa', 'aaaa', '', 'cscsw', 'sscc', 'vi', 'v@gmail.com', 'aaaa', 'HNHT', 'Hà Nội, QUẬN HAI BÀ TRƯNG', 'HNI');

-- --------------------------------------------------------

--
-- Table structure for table `devvn_tinhthanhpho`
--

CREATE TABLE `devvn_tinhthanhpho` (
  `matp` int(2) NOT NULL,
  `prov_id` varchar(5) CHARACTER SET utf8 NOT NULL,
  `prov_name` varchar(50) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `devvn_tinhthanhpho`
--

INSERT INTO `devvn_tinhthanhpho` (`matp`, `prov_id`, `prov_name`) VALUES
(0, 'HNI', 'Hà Nội'),
(1, 'HGG', 'Hà Giang'),
(2, 'CBG', 'Cao Bằng'),
(3, 'BKN', 'Bắc Kạn'),
(4, 'TQG', 'Tuyên Quang'),
(5, 'LCI', 'Lào Cai'),
(6, 'DBN', 'Điện Biên'),
(7, 'LCU', 'Lai Châu'),
(8, 'SLA', 'Sơn La'),
(9, 'YBI', 'Yên Bái'),
(10, 'HBH', 'Hòa Bình'),
(11, 'TNN', 'Thái Nguyên'),
(12, 'LSN', 'Lạng Sơn'),
(13, 'QNH', 'Quảng Ninh'),
(14, 'BGG', 'Bắc Giang'),
(15, 'PTO', 'Phú Thọ'),
(16, 'VPC', 'Vĩnh Phúc'),
(17, 'BNH', 'Bắc Ninh'),
(18, 'HDG', 'Hải Dương'),
(19, 'HPG', 'Hải Phòng'),
(20, 'HYN', 'Hưng Yên'),
(21, 'TBH', 'Thái Bình'),
(22, 'HNM', 'Hà Nam'),
(23, 'NDH', 'Nam Định'),
(24, 'NBH', 'Ninh Bình'),
(25, 'THA', 'Thanh Hóa'),
(26, 'NAN', 'Nghệ An'),
(27, 'HTH', 'Hà Tĩnh'),
(28, 'QBH', 'Quảng Bình'),
(29, 'QTI', 'Quảng Trị'),
(30, 'TTH', 'Thừa Thiên Huế'),
(31, 'DNG', 'Đà Nẵng'),
(32, 'QNM', 'Quảng Nam'),
(33, 'QNI', 'Quảng Ngãi'),
(34, 'BDH', 'Bình Định'),
(35, 'PYN', 'Phú Yên'),
(36, 'KHA', 'Khánh Hòa'),
(37, 'NTN', 'Ninh Thuận'),
(38, 'BTN', 'Bình Thuận'),
(39, 'KTM', 'Kon Tum'),
(40, 'GLI', 'Gia Lai'),
(41, 'DLK', 'Đắk Lắk'),
(42, 'DNG', 'Đắk Nông'),
(43, 'LDG', 'Lâm Đồng'),
(44, 'BPC', 'Bình Phước'),
(45, 'TNH', 'Tây Ninh'),
(46, 'BDG', 'Bình Dương'),
(47, 'DNI', 'Đồng Nai'),
(48, 'BVT', 'Bà Rịa - Vũng Tàu'),
(49, 'HCM', 'Hồ Chí Minh'),
(50, 'LAN', 'Long An'),
(51, 'TGG', 'Tiền Giang'),
(52, 'BTE', 'Bến Tre'),
(53, 'TVH', 'Trà Vinh'),
(54, 'VLG', 'Vĩnh Long'),
(55, 'DTP', 'Đồng Tháp'),
(56, 'AGG', 'An Giang'),
(57, 'KGG', 'Kiên Giang'),
(58, 'CTO', 'Cần Thơ'),
(59, 'HGG', 'Hậu Giang'),
(60, 'STG', 'Sóc Trăng'),
(61, 'BLU', 'Bạc Liêu'),
(62, 'CMU', 'Cà Mau');

-- --------------------------------------------------------

--
-- Table structure for table `enable_caotang`
--

CREATE TABLE `enable_caotang` (
  `id` int(11) NOT NULL,
  `enable` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `enable_caotang`
--

INSERT INTO `enable_caotang` (`id`, `enable`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE `file` (
  `id` int(11) NOT NULL,
  `File` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`id`, `File`) VALUES
(1, '');

-- --------------------------------------------------------

--
-- Table structure for table `header`
--

CREATE TABLE `header` (
  `HeaderId` int(11) NOT NULL,
  `Logo` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Banner` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `header`
--

INSERT INTO `header` (`HeaderId`, `Logo`, `Banner`) VALUES
(40, 'assets/includes/upload/files/logo1.jpg', 'assets/includes/upload/files/logo1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `id` int(11) NOT NULL,
  `assignment_Id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `billCustomId` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `name_detail` varchar(500) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`id`, `assignment_Id`, `category_id`, `billCustomId`, `date`, `time`, `name`, `name_detail`) VALUES
(221, 0, 4, 'TEST01', '2020-09-05', '10:40:00', 'Đang đóng gói', 'Nhập máy phiếu gửi'),
(222, 57, 4, 'TEST01', '2020-09-05', '10:40:00', 'Phân công cho bưu tá', ''),
(223, 57, 4, 'TEST01', '2020-09-05', '10:41:00', 'Đang vận chuyển', ''),
(224, 57, 4, 'TEST01', '2020-09-05', '12:14:00', 'Đang giao hàng', ''),
(225, 57, 4, 'TEST01', '2020-09-05', '12:14:00', 'Đang giao hàng', ''),
(226, 57, 2, 'TEST01', '2020-09-05', '12:16:00', 'Tồn - Thông báo chuyển hoàn', 'Lí do: aaaa');

-- --------------------------------------------------------

--
-- Table structure for table `homepage`
--

CREATE TABLE `homepage` (
  `id` int(11) NOT NULL,
  `background` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `homepage`
--

INSERT INTO `homepage` (`id`, `background`) VALUES
(1, 'assets/includes/upload/files/web-SML.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `ID` int(11) NOT NULL,
  `Name` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Code` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` tinyint(4) DEFAULT 1,
  `Icon` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`ID`, `Name`, `Code`, `Status`, `Icon`) VALUES
(1, 'Tiếng Việt', 'vi', 1, 'assets/includes/upload/images/icons/vn-flag.png'),
(2, 'English', 'en', 1, 'assets/includes/upload/images/icons/uk-flag.png');

-- --------------------------------------------------------

--
-- Table structure for table `mailcontact`
--

CREATE TABLE `mailcontact` (
  `ID` int(11) NOT NULL,
  `Name` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Email` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Subject` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Content` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `DateSent` datetime DEFAULT NULL,
  `Phone` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `DeviceType` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IP` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mailcontact`
--

INSERT INTO `mailcontact` (`ID`, `Name`, `Email`, `Subject`, `Content`, `DateSent`, `Phone`, `DeviceType`, `IP`) VALUES
(1, 'Fooh Choon Hee', 'foohch@yahoo.com', 'Thông tin user của Summerland', '', '2022-03-14 20:39:01', '60122876407', 'mobile', '');

-- --------------------------------------------------------

--
-- Table structure for table `map`
--

CREATE TABLE `map` (
  `MapID` int(11) NOT NULL,
  `Address` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Lattitude` double DEFAULT NULL,
  `Longitude` double DEFAULT NULL,
  `Phone` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `map`
--

INSERT INTO `map` (`MapID`, `Address`, `Lattitude`, `Longitude`, `Phone`) VALUES
(1, '1868 Võ Nguyên Giáp, Phước Tân, Biên Hòa, Đồng Nai', 10.898187, 106.892018, '0917 63 61 69 - 0917 63 61 69');

-- --------------------------------------------------------

--
-- Table structure for table `master_plan`
--

CREATE TABLE `master_plan` (
  `id` int(11) NOT NULL,
  `floor_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path_name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL,
  `Status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master_plan`
--

INSERT INTO `master_plan` (`id`, `floor_name`, `name`, `description`, `image`, `path_name`, `sort`, `Status`) VALUES
(1, 'A01', 'Tầng trệt', '', 'assets/client/pictures/catalog/background/tang_1.jpg', 'tang-tret.html', 1, 0),
(2, 'A03', 'Tầng 03', '', 'assets/client/pictures/catalog/background/tang_3_v2.jpg', 'tang-03.html', 2, 1),
(3, 'A04', 'Tầng 04', '', 'assets/client/pictures/catalog/background/tang_4_v2.jpg', 'tang-04.html', 3, 1),
(4, 'A05', 'Tầng điển hình', '', 'assets/client/pictures/catalog/background/tang_5-7_14-18_24-29_v2.jpg', 'tang-dien-hinh.html', 4, 1),
(5, 'A09', 'Tầng 08-12', '', 'assets/client/pictures/catalog/background/tang_8_12_v2.jpg', 'tang-09-12.html', 6, 1),
(6, 'A13', 'Tầng 13', '', 'assets/client/pictures/catalog/background/tang_13_v2.jpg', 'tang-13.html', 7, 1),
(7, 'A19', 'Tầng 19-22', '', 'assets/client/pictures/catalog/background/tang_19_22_v2.jpg', 'tang-19-22.html', 8, 1),
(8, 'A23', 'Tầng 23', '', 'assets/client/pictures/catalog/background/tang_23_v2.jpg', 'tang-23.html', 9, 1),
(9, 'A30', 'Tầng 30', '', 'assets/client/pictures/catalog/background/tang_30.jpg', 'tang-30.html', 10, 1),
(10, 'A08', 'Tầng 8&12 Bỏ', '', 'assets/client/pictures/catalog/background/tang_8_12_v2.jpg', 'tang-08-18.html', 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `master_plan_detail`
--

CREATE TABLE `master_plan_detail` (
  `id` int(11) NOT NULL,
  `master_plan_id` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `carpet_area` decimal(10,2) NOT NULL,
  `built_up_area` decimal(10,2) NOT NULL,
  `Image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `shape` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `coords` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `path_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `MetaRobot` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SeoTitle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SeoDescribes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SeoKeyword` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SeoCanonica` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `x_axis` decimal(10,2) NOT NULL,
  `y_axis` decimal(10,2) NOT NULL,
  `radius` decimal(10,2) NOT NULL,
  `matrix` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kind_of_room` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `number_bedrooms` int(11) NOT NULL,
  `garden` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort` int(11) NOT NULL,
  `Status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master_plan_detail`
--

INSERT INTO `master_plan_detail` (`id`, `master_plan_id`, `position`, `carpet_area`, `built_up_area`, `Image`, `shape`, `coords`, `path_name`, `MetaRobot`, `SeoTitle`, `SeoDescribes`, `SeoKeyword`, `SeoCanonica`, `x_axis`, `y_axis`, `radius`, `matrix`, `name`, `description`, `kind_of_room`, `number_bedrooms`, `garden`, `sort`, `Status`) VALUES
(1, 1, 10, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '976,972,976,1216,917,1225,870,1229,847,1229,817,1223,787,1208,760,1180,749,1157,743,1131,741,1104,741,947,741,790,940,790,940,972', '', '', '', '', '', '', '1020.00', '895.00', '16.60', '1 0 0 1 1019 901', 'KHU PHỤ TRỢ', 'h-10', '', 2, NULL, 0, 1),
(2, 1, 1, '75.10', '66.30', 'assets/client/pictures/catalog/room/room.png', 'poly', '1076,859,1101,859,1135,861,1135,1198,1052,1207,970,1219,966,952,1076,946', '', '', '', '', '', '', '863.00', '1161.00', '16.60', '1 0 0 1 863 1167', 'Khu thương mại dịch vụ', 'h-01', '', 2, NULL, 0, 1),
(3, 1, 1, '75.10', '66.30', 'assets/client/pictures/catalog/room/room.png', 'poly', '741,1100,968,1098,968,1217,891,1229,847,1234,830,1234,796,1221,777,1208,760,1191,747,1170,741,1151', '', '', '', '', '', '', '1060.00', '1057.00', '16.60', '1 0 0 1 1060 1063', 'Khu thương mại dịch vụ', 'h-01', '', 2, NULL, 0, 1),
(4, 1, 1, '75.10', '66.30', 'assets/client/pictures/catalog/room/room.png', 'poly', '1135,862,1161,867,1186,869,1214,873,1239,886,1264,899,1281,918,1296,941,1311,973,1322,1009,1347,1096,1269,1170,1254,1183,1235,1185,1131,1200', '', '', '', '', '', '', '1232.00', '1037.00', '16.60', '1 0 0 1 1232 1043', 'Khu thương mại dịch vụ', 'h-01', '', 2, NULL, 0, 1),
(5, 1, 1, '75.10', '66.30', 'assets/client/pictures/catalog/room/room.png', 'poly', '1387,524,1652,526,1663,590,1672,641,1674,672,1674,702,1674,745,1669,768,1655,789,1534,908,1398,857,1373,836,1364,815', '', '', '', '', '', '', '1528.00', '669.00', '16.60', '1 0 0 1 1528 675', 'Khu thương mại dịch vụ', 'h-01', '', 2, NULL, 0, 1),
(6, 1, 1, '75.10', '66.30', 'assets/client/pictures/catalog/room/room.png', 'poly', '1390,412,1633,414,1652,523,1390,523', '', '', '', '', '', '', '1522.00', '469.00', '16.60', '1 0 0 1 1522 475', 'Khu thương mại dịch vụ', 'h-01', '', 2, NULL, 0, 1),
(7, 1, 1, '75.10', '66.30', 'assets/client/pictures/catalog/room/room.png', 'poly', '1390,226,1392,413,1633,411,1610,283,1602,258,1585,241,1549,228', '', '', '', '', '', '', '1505.00', '311.00', '16.60', '1 0 0 1 1505 317', 'Khu thương mại dịch vụ', 'h-01', '', 2, NULL, 0, 1),
(8, 1, 5, '48.50', '42.90', 'assets/client/pictures/catalog/room/room.png', 'poly', '1271,227,1394,227,1394,460,1390,519,1334,521,1275,458', '', '', '', '', '', '', '1340.00', '351.00', '16.60', '1 0 0 1 1340 357', 'HÀNH LANG', 'h-05', '', 1, NULL, 0, 1),
(9, 1, 3, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '1273,416,1076,416,925,571,929,713,741,715,739,297,747,263,762,252,781,238,800,231,1271,229', '', '', '', '', '', '', '925.00', '386.00', '16.60', '1 0 0 1 925 392', 'NHÀ TRẺ', 'h-03', '', 2, NULL, 0, 1),
(10, 1, 7, '92.00', '81.20', 'assets/client/pictures/catalog/room/room.png', 'poly', '741,714,743,794,968,794,966,752,929,754,929,718', '', '', '', '', '', '', '854.00', '752.00', '16.60', '1 0 0 1 854 758', 'KHU VỆ SINH', 'h-07', '', 3, NULL, 0, 1),
(11, 1, 6, '48.50', '42.90', 'assets/client/pictures/catalog/room/room.png', 'poly', '766,826,968,826,966,952,770,954', '', '', '', '', '', '', '1108.00', '591.00', '16.60', '1 0 0 1 1108 597', 'SINH HOẠT CỘNG ĐỒNG', 'h-04', '', 1, NULL, 0, 1),
(12, 1, 2, '66.70', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '1004,543,1152,698,1209,641,1057,492', '', '', '', '', '', '', '1265.00', '774.00', '16.60', '1 0 0 1 1265 780', 'SẢNH THANG MÁY', 'h-06', '', 2, NULL, 0, 1),
(13, 1, 4, '75.10', '66.30', 'assets/client/pictures/catalog/room/room.png', 'poly', '1326,518,1390,522,1364,815,1379,847,1409,862,1534,908,1349,1093,1307,970,1292,934,1273,908,1247,889,1218,874,1154,864,1108,857,1078,853,1029,847,1027,819', '', '', '', '', '', '', '865.00', '885.00', '16.60', '1 0 0 1 865 891', 'SẢNH CHÍNH', 'h-02', '', 2, NULL, 0, 1),
(14, 3, 1, '75.10', '66.30', 'assets/client/pictures/catalog/room/room.png', 'poly', '971, 1040, 972, 1209, 942, 1214, 911, 1218, 876, 1222, 855, 1223, 834, 1223, 812, 1218, 791, 1208, 772, 1196, 755, 1178, 741, 1157, 735, 1138, 729, 1116, 728, 1093, 729, 1073, 932, 1071, 933, 1040', '', '', '', '', '', '', '852.00', '1140.00', '16.60', '1 0 0 1 852 1146', 'Khu thương mại dịch vụ', 'A-01', '', 2, NULL, 0, 1),
(15, 3, 2, '66.70', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '731, 931, 936, 931, 936, 1074, 730, 1073', '', '', '', '', '', '', '852.00', '998.00', '16.60', '1 0 0 1 852 1004', 'Khu thương mại dịch vụ', 'A-02', '', 2, NULL, 0, 1),
(16, 3, 3, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '732, 787, 937, 787, 937, 933, 732, 933', '', '', '', '', '', '', '852.00', '862.00', '16.60', '1 0 0 1 852 868', 'Khu thương mại dịch vụ', 'A-03', '', 2, NULL, 0, 1),
(17, 3, 4, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '730, 647, 937, 646, 936, 788, 730, 789', '', '', '', '', '', '', '852.00', '712.00', '16.60', '1 0 0 1 852 718', 'Khu thương mại dịch vụ', 'A-04', '', 2, NULL, 0, 1),
(18, 3, 5, '48.50', '42.90', 'assets/client/pictures/catalog/room/room.png', 'poly', '731, 540, 933, 540, 934, 644, 731, 644', '', '', '', '', '', '', '852.00', '582.00', '16.60', '1 0 0 1 852 588', 'Khu thương mại dịch vụ', 'A-05', '', 1, NULL, 0, 1),
(19, 3, 6, '48.50', '42.90', 'assets/client/pictures/catalog/room/room.png', 'poly', '732, 436, 936, 436, 935, 541, 732, 541', '', '', '', '', '', '', '852.00', '492.00', '16.60', '1 0 0 1 852 498', 'Khu thương mại dịch vụ', 'A-06', '', 1, NULL, 0, 1),
(20, 3, 7, '92.00', '81.20', 'assets/client/pictures/catalog/room/room.png', 'poly', '933, 234, 935, 436, 731, 436, 731, 374, 731, 317, 731, 293, 737, 272, 750, 255, 764, 244, 779, 238, 797, 233', '', '', '', '', '', '', '852.00', '336.00', '16.60', '1 0 0 1 852 342', 'Khu thương mại dịch vụ', 'A-07', '', 3, NULL, 0, 1),
(21, 3, 8, '67.90', '60.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '972, 237, 1115, 237, 1116, 436, 972, 435', '', '', '', '', '', '', '1048.00', '324.00', '16.60', '1 0 0 1 1048 330', 'Khu thương mại dịch vụ', 'A-08', '', 2, NULL, 0, 1),
(22, 3, 9, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '1117, 234, 1266, 235, 1263, 436, 1115, 436', '', '', '', '', '', '', '1205.00', '324.00', '16.60', '1 0 0 1 1205 330', 'Khu thương mại dịch vụ', 'A-09', '', 2, NULL, 0, 1),
(23, 3, 10, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '1262, 235, 1412, 234, 1411, 436, 1262, 435', '', '', '', '', '', '', '1349.00', '324.00', '16.60', '1 0 0 1 1349 330', 'Khu thương mại dịch vụ', 'A-10', '', 2, NULL, 0, 1),
(24, 3, 11, '86.80', '77.00', 'assets/client/pictures/catalog/room/room.png', 'poly', '1444, 434, 1410, 434, 1410, 234, 1540, 234, 1564, 235, 1579, 242, 1592, 254, 1602, 270, 1610, 291, 1614, 311, 1619, 333, 1624, 356, 1628, 379, 1633, 406, 1635, 420, 1444, 419', '', '', '', '', '', '', '1517.00', '324.00', '16.60', '1 0 0 1 1517 330', 'Khu thương mại dịch vụ', 'A-11', '', 3, NULL, 0, 1),
(25, 3, 12, '51.70', '44.90', 'assets/client/pictures/catalog/room/room.png', 'poly', '1417, 470, 1445, 470, 1446, 423, 1628, 422, 1652, 530, 1416, 530', '', '', '', '', '', '', '1545.00', '474.00', '16.60', '1 0 0 1 1545 480', 'Khu thương mại dịch vụ', 'A-12', '', 1, NULL, 0, 1),
(26, 3, 13, '57.20', '50.70', 'assets/client/pictures/catalog/room/room.png', 'poly', '1415, 530, 1649, 531, 1667, 634, 1416, 633', '', '', '', '', '', '', '1545.00', '578.00', '16.60', '1 0 0 1 1545 584', 'Khu thương mại dịch vụ', 'A-13', '', 1, NULL, 0, 1),
(27, 3, 14, '59.80', '52.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '1415, 633, 1667, 634, 1671, 677, 1672, 708, 1671, 743, 1415, 742', '', '', '', '', '', '', '1545.00', '682.00', '16.60', '1 0 0 1 1545 688', 'Khu thương mại dịch vụ', 'A-14', '', 1, NULL, 0, 1),
(28, 3, 15, '53.80', '47.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '1074, 968, 1180, 968, 1181, 1183, 1074, 1198', '', '', '', '', '', '', '1146.00', '1076.00', '16.60', '1 0 0 1 1146 1082', 'Khu thương mại dịch vụ', 'A-15', '', 1, NULL, 0, 1),
(29, 3, 16, '57.30', '50.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '969, 968, 1076, 968, 1075, 1198, 969, 1211', '', '', '', '', '', '', '1045.00', '1076.00', '16.60', '1 0 0 1 1045 1082', 'Khu thương mại dịch vụ', 'A-16', '', 1, NULL, 0, 1),
(46, 5, 1, '75.10', '66.30', 'assets/client/pictures/catalog/room/room.png', 'poly', '936, 1041, 972, 1041, 972, 1210, 857, 1224, 829, 1221, 804, 1213, 779, 1197, 763, 1184, 747, 1160, 738, 1133, 734, 1109, 733, 1073, 936, 1072', '', '', '', '', '', '', '852.00', '1140.00', '16.60', '1 0 0 1 852 1146', 'Khu thương mại dịch vụ', 'A-01', '', 2, NULL, 0, 1),
(47, 5, 2, '66.70', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '735, 931, 935, 929, 936, 1075, 736, 1073', '', '', '', '', '', '', '852.00', '998.00', '16.60', '1 0 0 1 852 1004', 'Khu thương mại dịch vụ', 'A-02', '', 2, NULL, 0, 1),
(48, 5, 3, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '734, 788, 935, 788, 936, 932, 736, 931', '', '', '', '', '', '', '852.00', '862.00', '16.60', '1 0 0 1 852 868', 'Khu thương mại dịch vụ', 'A-03', '', 2, NULL, 0, 1),
(49, 5, 4, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '733, 645, 936, 644, 935, 788, 734, 788', '', '', '', '', '', '', '852.00', '712.00', '16.60', '1 0 0 1 852 718', 'Khu thương mại dịch vụ', 'A-04', '', 2, NULL, 0, 1),
(50, 5, 5, '48.50', '42.90', 'assets/client/pictures/catalog/room/room.png', 'poly', '735, 541, 934, 540, 936, 645, 734, 645', '', '', '', '', '', '', '852.00', '582.00', '16.60', '1 0 0 1 852 588', 'Khu thương mại dịch vụ', 'A-05', '', 1, NULL, 0, 1),
(51, 5, 6, '48.50', '42.90', 'assets/client/pictures/catalog/room/room.png', 'poly', '735, 438, 936, 436, 936, 540, 736, 541', '', '', '', '', '', '', '852.00', '492.00', '16.60', '1 0 0 1 852 498', 'Khu thương mại dịch vụ', 'A-06', '', 1, NULL, 0, 1),
(52, 5, 7, '92.00', '81.20', 'assets/client/pictures/catalog/room/room.png', 'poly', '935, 437, 735, 437, 733, 294, 740, 273, 755, 254, 773, 243, 800, 236, 936, 236', '', '', '', '', '', '', '852.00', '336.00', '16.60', '1 0 0 1 852 342', 'Khu thương mại dịch vụ', 'A-07', '', 3, NULL, 0, 1),
(53, 5, 8, '67.90', '60.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '973, 238, 1119, 237, 1120, 434, 973, 434', '', '', '', '', '', '', '1048.00', '324.00', '16.60', '1 0 0 1 1048 330', 'Khu thương mại dịch vụ', 'A-08', '', 2, NULL, 0, 1),
(54, 5, 9, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '1119, 237, 1268, 238, 1267, 435, 1120, 435', '', '', '', '', '', '', '1205.00', '324.00', '16.60', '1 0 0 1 1205 330', 'Khu thương mại dịch vụ', 'A-09', '', 2, NULL, 0, 1),
(55, 5, 10, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '1265, 236, 1412, 236, 1414, 435, 1266, 434', '', '', '', '', '', '', '1349.00', '324.00', '16.60', '1 0 0 1 1349 330', 'Khu thương mại dịch vụ', 'A-10', '', 2, NULL, 0, 1),
(56, 5, 11, '86.80', '77.00', 'assets/client/pictures/catalog/room/room.png', 'poly', '1412, 436, 1411, 235, 1565, 237, 1582, 244, 1597, 260, 1607, 292, 1630, 422, 1448, 422, 1448, 436', '', '', '', '', '', '', '1517.00', '324.00', '16.60', '1 0 0 1 1517 330', 'Khu thương mại dịch vụ', 'A-11', '', 3, NULL, 0, 1),
(57, 5, 12, '51.70', '44.90', 'assets/client/pictures/catalog/room/room.png', 'poly', '1448, 423, 1628, 423, 1650, 530, 1419, 530, 1418, 471, 1449, 473', '', '', '', '', '', '', '1545.00', '474.00', '16.60', '1 0 0 1 1545 480', 'Khu thương mại dịch vụ', 'A-12', '', 1, NULL, 0, 1),
(58, 5, 13, '57.20', '50.70', 'assets/client/pictures/catalog/room/room.png', 'poly', '1419, 530, 1648, 531, 1666, 634, 1420, 634', '', '', '', '', '', '', '1545.00', '578.00', '16.60', '1 0 0 1 1545 584', 'Khu thương mại dịch vụ', 'A-13', '', 1, NULL, 0, 1),
(59, 5, 14, '59.80', '52.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '1419, 634, 1666, 634, 1671, 690, 1669, 737, 1420, 737', '', '', '', '', '', '', '1545.00', '682.00', '16.60', '1 0 0 1 1545 688', 'Khu thương mại dịch vụ', 'A-14', '', 1, NULL, 0, 1),
(60, 5, 15, '95.90', '84.80', 'assets/client/pictures/catalog/room/room.png', 'poly', '1418, 740, 1670, 738, 1662, 805, 1642, 881, 1631, 912, 1613, 924, 1508, 921, 1518, 901, 1518, 883, 1396, 883, 1355, 842, 1400, 797, 1419, 777\r\n', '', '', '', '', '', '', '1545.00', '804.00', '16.60', '1 0 0 1 1545 810', 'Khu thương mại dịch vụ', 'A-15', '', 3, NULL, 0, 1),
(61, 5, 16, '69.00', '60.80', 'assets/client/pictures/catalog/room/room.png', 'poly', '1396, 883, 1519, 883, 1518, 902, 1490, 952, 1457, 993, 1422, 1024, 1393, 1044, 1330, 1077, 1328, 987, 1268, 928, 1355, 840\r\n', '', '', '', '', '', '', '1390.00', '943.00', '16.60', '1 0 0 1 1390 949', 'Khu thương mại dịch vụ', 'A-16', '', 2, NULL, 0, 1),
(62, 5, 17, '77.50', '69.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '1182, 970, 1225, 970, 1263, 932, 1330, 985, 1328, 1080, 1359, 1063, 1372, 1073, 1374, 1140, 1359, 1160, 1182, 1183', '', '', '', '', '', '', '1268.00', '1074.00', '16.60', '1 0 0 1 1268 1080', 'Khu thương mại dịch vụ', 'A-17', '', 2, NULL, 0, 1),
(63, 5, 18, '53.80', '47.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '1076, 969, 1183, 969, 1183, 1183, 1076, 1196', '', '', '', '', '', '', '1146.00', '1076.00', '16.60', '1 0 0 1 1146 1082', 'Khu thương mại dịch vụ', 'A-18', '', 1, NULL, 0, 1),
(64, 5, 19, '57.30', '50.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '971, 970, 1079, 969, 1077, 1193, 967, 1209', '', '', '', '', '', '', '1045.00', '1076.00', '16.60', '1 0 0 1 1045 1082', 'Khu thương mại dịch vụ', 'A-19', '', 1, NULL, 0, 1),
(65, 2, 1, '75.10', '66.30', 'assets/client/pictures/catalog/room/room.png', 'poly', '935, 1039, 976, 1040, 972, 1209, 948, 1212, 914, 1216, 889, 1220, 858, 1223, 831, 1221, 808, 1215, 785, 1204, 768, 1189, 757, 1175, 745, 1155, 736, 1133, 734, 1115, 733, 1097, 732, 1070, 935, 1069', '', '', '', '', '', '', '852.00', '1140.00', '16.60', '1 0 0 1 852 1146', 'Khu thương mại dịch vụ', 'A-01', '', 2, NULL, 0, 1),
(66, 2, 2, '66.70', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '733, 929, 936, 930, 935, 1070, 733, 1070', '', '', '', '', '', '', '852.00', '998.00', '16.60', '1 0 0 1 852 1004', 'Khu thương mại dịch vụ', 'A-02', '', 2, NULL, 0, 1),
(67, 2, 3, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '734, 786, 936, 786, 936, 928, 733, 929', '', '', '', '', '', '', '852.00', '862.00', '16.60', '1 0 0 1 852 868', 'Khu thương mại dịch vụ', 'A-03', '', 2, NULL, 0, 1),
(68, 2, 4, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '733, 644, 936, 644, 937, 787, 733, 787', '', '', '', '', '', '', '852.00', '712.00', '16.60', '1 0 0 1 852 718', 'Khu thương mại dịch vụ', 'A-04', '', 2, NULL, 0, 1),
(69, 2, 5, '48.50', '42.90', 'assets/client/pictures/catalog/room/room.png', 'poly', '733, 540, 937, 538, 937, 642, 732, 645', '', '', '', '', '', '', '852.00', '582.00', '16.60', '1 0 0 1 852 588', 'Khu thương mại dịch vụ', 'A-05', '', 1, NULL, 0, 1),
(70, 2, 6, '48.50', '42.90', 'assets/client/pictures/catalog/room/room.png', 'poly', '735, 436, 936, 436, 936, 539, 734, 540', '', '', '', '', '', '', '852.00', '492.00', '16.60', '1 0 0 1 852 498', 'Khu thương mại dịch vụ', 'A-06', '', 1, NULL, 0, 1),
(71, 2, 7, '92.00', '81.20', 'assets/client/pictures/catalog/room/room.png', 'poly', '938, 236, 936, 436, 733, 435, 733, 314, 735, 284, 741, 269, 749, 255, 758, 248, 775, 240, 794, 236, 823, 235', '', '', '', '', '', '', '852.00', '336.00', '16.60', '1 0 0 1 852 342', 'Khu thương mại dịch vụ', 'A-07', '', 3, NULL, 0, 1),
(72, 2, 8, '67.90', '60.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '972, 236, 1118, 236, 1119, 435, 972, 434', '', '', '', '', '', '', '1048.00', '324.00', '16.60', '1 0 0 1 1048 330', 'Khu thương mại dịch vụ', 'A-08', '', 2, NULL, 0, 1),
(73, 2, 9, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '1120, 236, 1267, 236, 1265, 435, 1120, 435', '', '', '', '', '', '', '1205.00', '324.00', '16.60', '1 0 0 1 1205 330', 'Khu thương mại dịch vụ', 'A-09', '', 2, NULL, 0, 1),
(74, 2, 10, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '1265, 235, 1413, 235, 1414, 435, 1265, 435', '', '', '', '', '', '', '1349.00', '324.00', '16.60', '1 0 0 1 1349 330', 'Khu thương mại dịch vụ', 'A-10', '', 2, NULL, 0, 1),
(75, 2, 11, '86.80', '77.00', 'assets/client/pictures/catalog/room/room.png', 'poly', '1452, 434, 1412, 434, 1412, 235, 1475, 234, 1519, 234, 1547, 234, 1570, 237, 1586, 245, 1599, 261, 1608, 286, 1616, 323, 1624, 361, 1630, 390, 1635, 421, 1452, 420', '', '', '', '', '', '', '1517.00', '324.00', '16.60', '1 0 0 1 1517 330', 'Khu thương mại dịch vụ', 'A-11', '', 3, NULL, 0, 1),
(76, 4, 1, '75.10', '66.30', 'assets/client/pictures/catalog/room/room.png', 'poly', '934, 1038, 971, 1038, 971, 1207, 859, 1222, 834, 1220, 810, 1214, 785, 1202, 766, 1186, 749, 1164, 741, 1148, 733, 1125, 732, 1069, 933, 1068', '', '', '', '', '', '', '852.00', '1140.00', '16.60', '1 0 0 1 852 1146', 'Khu thương mại dịch vụ', 'A-01', '', 2, NULL, 0, 1),
(77, 4, 2, '66.70', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '738,1067,941,1070,941,925,735,928', '', '', '', '', '', '', '852.00', '998.00', '16.60', '1 0 0 1 852 1004', 'Khu thương mại dịch vụ', 'A-02', '', 2, NULL, 0, 1),
(78, 4, 3, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '735,928,941,928,941,782,732,784', '', '', '', '', '', '', '852.00', '862.00', '16.60', '1 0 0 1 852 868', 'Khu thương mại dịch vụ', 'A-03', '', 2, NULL, 0, 1),
(79, 4, 4, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '735,784,735,637,941,635,941,784', '', '', '', '', '', '', '852.00', '712.00', '16.60', '1 0 0 1 852 718', 'Khu thương mại dịch vụ', 'A-04', '', 2, NULL, 0, 1),
(80, 4, 5, '48.50', '42.90', 'assets/client/pictures/catalog/room/room.png', 'poly', '738,637,941,637,941,534,738,534', '', '', '', '', '', '', '852.00', '582.00', '16.60', '1 0 0 1 852 588', 'Khu thương mại dịch vụ', 'A-05', '', 1, NULL, 0, 1),
(81, 4, 6, '48.50', '42.90', 'assets/client/pictures/catalog/room/room.png', 'poly', '733, 434, 936, 435, 935, 540, 733, 539', '', '', '', '', '', '', '852.00', '492.00', '16.60', '1 0 0 1 852 498', 'Khu thương mại dịch vụ', 'A-06', '', 1, NULL, 0, 1),
(82, 4, 7, '92.00', '81.20', 'assets/client/pictures/catalog/room/room.png', 'poly', '729, 433, 936, 435, 936, 232, 795, 232, 770, 236, 753, 245, 738, 263, 730, 288', '', '', '', '', '', '', '852.00', '336.00', '16.60', '1 0 0 1 852 342', 'Khu thương mại dịch vụ', 'A-07', '', 3, NULL, 0, 1),
(83, 4, 8, '67.90', '60.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '972, 236, 1119, 235, 1120, 434, 973, 433', '', '', '', '', '', '', '1048.00', '324.00', '16.60', '1 0 0 1 1048 330', 'Khu thương mại dịch vụ', 'A-08', '', 2, NULL, 0, 1),
(84, 4, 9, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '1119, 235, 1266, 234, 1264, 433, 1118, 433', '', '', '', '', '', '', '1205.00', '324.00', '16.60', '1 0 0 1 1205 330', 'Khu thương mại dịch vụ', 'A-09', '', 2, NULL, 0, 1),
(85, 4, 10, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '1264, 235, 1412, 235, 1410, 434, 1265, 434', '', '', '', '', '', '', '1349.00', '324.00', '16.60', '1 0 0 1 1349 330', 'Khu thương mại dịch vụ', 'A-10', '', 2, NULL, 0, 1),
(86, 4, 11, '86.80', '44.90', 'assets/client/pictures/catalog/room/room.png', 'poly', '1445, 433, 1410, 433, 1410, 235, 1544, 235, 1564, 237, 1575, 242, 1586, 249, 1597, 260, 1604, 274, 1611, 305, 1621, 358, 1631, 419, 1445, 420', '', '', '', '', '', '', '1517.00', '324.00', '16.60', '1 0 0 1 1517 330', 'Khu thương mại dịch vụ', 'A-11', '', 3, NULL, 0, 1),
(87, 4, 12, '51.70', '44.90', 'assets/client/pictures/catalog/room/room.png', 'poly', '1447, 420, 1630, 420, 1651, 529, 1417, 528, 1416, 471, 1447, 470', '', '', '', '', '', '', '1545.00', '474.00', '16.60', '1 0 0 1 1545 480', 'Khu thương mại dịch vụ', 'A-12', '', 1, NULL, 0, 1),
(88, 4, 13, '57.20', '50.70', 'assets/client/pictures/catalog/room/room.png', 'poly', '1417, 526, 1652, 528, 1668, 633, 1418, 632', '', '', '', '', '', '', '1545.00', '578.00', '16.60', '1 0 0 1 1545 584', 'Khu thương mại dịch vụ', 'A-13', '', 1, NULL, 0, 1),
(89, 4, 14, '59.80', '52.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '1417, 631, 1669, 631, 1674, 689, 1673, 738, 1416, 740', '', '', '', '', '', '', '1545.00', '682.00', '16.60', '1 0 0 1 1545 688', 'Khu thương mại dịch vụ', 'A-14', '', 1, NULL, 0, 1),
(90, 4, 15, '95.90', '84.80', 'assets/client/pictures/catalog/room/room.png', 'poly', '1354, 839, 1418, 776, 1417, 735, 1671, 736, 1663, 803, 1643, 881, 1634, 902, 1625, 915, 1610, 923, 1564, 924, 1563, 882, 1395, 880', '', '', '', '', '', '', '1545.00', '804.00', '16.60', '1 0 0 1 1545 810', 'Khu thương mại dịch vụ', 'A-15', '', 3, NULL, 0, 1),
(91, 4, 16, '78.40', '69.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '1182, 967, 1221, 966, 1268, 923, 1331, 985, 1330, 1112, 1372, 1111, 1373, 1135, 1365, 1151, 1351, 1161, 1267, 1171, 1179, 1181', '', '', '', '', '', '', '1268.00', '1074.00', '16.60', '1 0 0 1 1268 1080', 'Khu thương mại dịch vụ', 'A-16', '', 2, NULL, 0, 1),
(92, 4, 17, '53.80', '47.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '1075, 966, 1183, 966, 1182, 1179, 1076, 1191', '', '', '', '', '', '', '1146.00', '1076.00', '16.60', '1 0 0 1 1146 1082', 'Khu thương mại dịch vụ', 'A-17', '', 1, NULL, 0, 1),
(93, 4, 18, '57.30', '50.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '971, 967, 1079, 967, 1076, 1192, 971, 1205', '', '', '', '', '', '', '1045.00', '1076.00', '16.60', '1 0 0 1 1045 1082', 'Khu thương mại dịch vụ', 'A-18', '', 1, NULL, 0, 1),
(94, 6, 1, '75.10', '66.30', 'assets/client/pictures/catalog/room/room.png', 'poly', '936, 1041, 972, 1041, 972, 1210, 857, 1224, 829, 1221, 804, 1213, 779, 1197, 763, 1184, 747, 1160, 738, 1133, 734, 1109, 733, 1073, 936, 1072', '', '', '', '', '', '', '852.00', '1140.00', '16.60', '1 0 0 1 852 1146', 'Khu thương mại dịch vụ', 'A-01', '', 2, NULL, 0, 1),
(95, 6, 2, '66.70', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '735, 931, 935, 929, 936, 1075, 736, 1073', '', '', '', '', '', '', '852.00', '998.00', '16.60', '1 0 0 1 852 1004', 'Khu thương mại dịch vụ', 'A-02', '', 2, NULL, 0, 1),
(96, 6, 3, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '734, 788, 935, 788, 936, 932, 736, 931', '', '', '', '', '', '', '852.00', '862.00', '16.60', '1 0 0 1 852 868', 'Khu thương mại dịch vụ', 'A-03', '', 2, NULL, 0, 1),
(97, 6, 4, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '733, 645, 936, 644, 935, 788, 734, 788', '', '', '', '', '', '', '852.00', '712.00', '16.60', '1 0 0 1 852 718', 'Khu thương mại dịch vụ', 'A-04', '', 2, NULL, 0, 1),
(98, 6, 5, '48.50', '42.90', 'assets/client/pictures/catalog/room/room.png', 'poly', '735, 541, 934, 540, 936, 645, 734, 645', '', '', '', '', '', '', '852.00', '582.00', '16.60', '1 0 0 1 852 588', 'Khu thương mại dịch vụ', 'A-05', '', 1, NULL, 0, 1),
(99, 6, 6, '48.50', '42.90', 'assets/client/pictures/catalog/room/room.png', 'poly', '735, 438, 936, 436, 936, 540, 736, 541', '', '', '', '', '', '', '852.00', '492.00', '16.60', '1 0 0 1 852 498', 'Khu thương mại dịch vụ', 'A-06', '', 1, NULL, 0, 1),
(100, 6, 7, '92.00', '81.20', 'assets/client/pictures/catalog/room/room.png', 'poly', '935, 437, 735, 437, 733, 294, 740, 273, 755, 254, 773, 243, 800, 236, 936, 236', '', '', '', '', '', '', '852.00', '336.00', '16.60', '1 0 0 1 852 342', 'Khu thương mại dịch vụ', 'A-07', '', 3, NULL, 0, 1),
(101, 6, 8, '67.90', '60.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '973, 238, 1119, 237, 1120, 434, 973, 434', '', '', '', '', '', '', '1048.00', '324.00', '16.60', '1 0 0 1 1048 330', 'Khu thương mại dịch vụ', 'A-08', '', 2, NULL, 0, 1),
(102, 6, 9, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '1119, 237, 1268, 238, 1267, 435, 1120, 435', '', '', '', '', '', '', '1205.00', '324.00', '16.60', '1 0 0 1 1205 330', 'Khu thương mại dịch vụ', 'A-09', '', 2, NULL, 0, 1),
(103, 6, 10, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '1265, 236, 1412, 236, 1414, 435, 1266, 434', '', '', '', '', '', '', '1349.00', '324.00', '16.60', '1 0 0 1 1349 330', 'Khu thương mại dịch vụ', 'A-10', '', 2, NULL, 0, 1),
(104, 6, 11, '86.80', '77.00', 'assets/client/pictures/catalog/room/room.png', 'poly', '1412, 436, 1411, 235, 1565, 237, 1582, 244, 1597, 260, 1607, 292, 1630, 422, 1448, 422, 1448, 436', '', '', '', '', '', '', '1517.00', '324.00', '16.60', '1 0 0 1 1517 330', 'Khu thương mại dịch vụ', 'A-11', '', 3, NULL, 0, 1),
(105, 6, 12, '51.70', '44.90', 'assets/client/pictures/catalog/room/room.png', 'poly', '1448, 423, 1628, 423, 1650, 530, 1419, 530, 1418, 471, 1449, 473', '', '', '', '', '', '', '1545.00', '474.00', '16.60', '1 0 0 1 1545 480', 'Khu thương mại dịch vụ', 'A-12', '', 1, NULL, 0, 1),
(106, 6, 13, '57.20', '50.70', 'assets/client/pictures/catalog/room/room.png', 'poly', '1419, 530, 1648, 531, 1666, 634, 1420, 634', '', '', '', '', '', '', '1545.00', '578.00', '16.60', '1 0 0 1 1545 584', 'Khu thương mại dịch vụ', 'A-13', '', 1, NULL, 0, 1),
(107, 6, 14, '59.80', '52.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '1419, 634, 1666, 634, 1671, 690, 1669, 737, 1420, 737', '', '', '', '', '', '', '1545.00', '682.00', '16.60', '1 0 0 1 1545 688', 'Khu thương mại dịch vụ', 'A-14', '', 1, NULL, 0, 1),
(108, 6, 15, '95.90', '84.80', 'assets/client/pictures/catalog/room/room.png', 'poly', '1418, 740, 1670, 738, 1662, 805, 1642, 881, 1631, 912, 1613, 924, 1508, 921, 1518, 901, 1518, 883, 1396, 883, 1355, 842, 1400, 797, 1419, 777', '', '', '', '', '', '', '1545.00', '804.00', '16.60', '1 0 0 1 1545 810', 'Khu thương mại dịch vụ', 'A-15', '', 3, NULL, 0, 1),
(109, 6, 16, '77.50', '69.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '1182, 970, 1225, 970, 1263, 932, 1330, 985, 1328, 1080, 1359, 1063, 1372, 1073, 1374, 1140, 1359, 1160, 1182, 1183', '', '', '', '', '', '', '1268.00', '1074.00', '16.60', '1 0 0 1 1268 1080', 'Khu thương mại dịch vụ', 'A-16', '', 2, NULL, 0, 1),
(110, 6, 17, '53.80', '47.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '1076, 969, 1183, 969, 1183, 1183, 1076, 1196', '', '', '', '', '', '', '1146.00', '1076.00', '16.60', '1 0 0 1 1146 1082', 'Khu thương mại dịch vụ', 'A-17', '', 1, NULL, 0, 1),
(111, 6, 18, '57.30', '50.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '971, 970, 1079, 969, 1077, 1193, 967, 1209', '', '', '', '', '', '', '1045.00', '1076.00', '16.60', '1 0 0 1 1045 1082', 'Khu thương mại dịch vụ', 'A-18', '', 1, NULL, 0, 1),
(112, 7, 1, '75.10', '66.30', 'assets/client/pictures/catalog/room/room.png', 'poly', '936, 1041, 972, 1041, 972, 1210, 857, 1224, 829, 1221, 804, 1213, 779, 1197, 763, 1184, 747, 1160, 738, 1133, 734, 1109, 733, 1073, 936, 1072', '', '', '', '', '', '', '852.00', '1140.00', '16.60', '1 0 0 1 852 1146', 'Khu thương mại dịch vụ', 'A-01', '', 2, NULL, 0, 1),
(113, 7, 2, '66.70', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '735, 931, 935, 929, 936, 1075, 736, 1073', '', '', '', '', '', '', '852.00', '998.00', '16.60', '1 0 0 1 852 1004', 'Khu thương mại dịch vụ', 'A-02', '', 2, NULL, 0, 1),
(114, 7, 3, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '734, 788, 935, 788, 936, 932, 736, 931', '', '', '', '', '', '', '852.00', '862.00', '16.60', '1 0 0 1 852 868', 'Khu thương mại dịch vụ', 'A-03', '', 2, NULL, 0, 1),
(115, 7, 4, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '733, 645, 936, 644, 935, 788, 734, 788', '', '', '', '', '', '', '852.00', '712.00', '16.60', '1 0 0 1 852 718', 'Khu thương mại dịch vụ', 'A-04', '', 2, NULL, 0, 1),
(116, 7, 5, '48.50', '42.90', 'assets/client/pictures/catalog/room/room.png', 'poly', '735, 541, 934, 540, 936, 645, 734, 645', '', '', '', '', '', '', '852.00', '582.00', '16.60', '1 0 0 1 852 588', 'Khu thương mại dịch vụ', 'A-05', '', 1, NULL, 0, 1),
(117, 7, 6, '48.50', '42.90', 'assets/client/pictures/catalog/room/room.png', 'poly', '735, 438, 936, 436, 936, 540, 736, 541', '', '', '', '', '', '', '852.00', '492.00', '16.60', '1 0 0 1 852 498', 'Khu thương mại dịch vụ', 'A-06', '', 1, NULL, 0, 1),
(118, 7, 7, '92.00', '81.20', 'assets/client/pictures/catalog/room/room.png', 'poly', '935, 437, 735, 437, 733, 294, 740, 273, 755, 254, 773, 243, 800, 236, 936, 236', '', '', '', '', '', '', '852.00', '336.00', '16.60', '1 0 0 1 852 342', 'Khu thương mại dịch vụ', 'A-07', '', 3, NULL, 0, 1),
(119, 7, 8, '67.90', '60.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '973, 238, 1119, 237, 1120, 434, 973, 434', '', '', '', '', '', '', '1048.00', '324.00', '16.60', '1 0 0 1 1048 330', 'Khu thương mại dịch vụ', 'A-08', '', 2, NULL, 0, 1),
(120, 7, 9, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '1119, 237, 1268, 238, 1267, 435, 1120, 435', '', '', '', '', '', '', '1205.00', '324.00', '16.60', '1 0 0 1 1205 330', 'Khu thương mại dịch vụ', 'A-09', '', 2, NULL, 0, 1),
(121, 7, 10, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '1265, 236, 1412, 236, 1414, 435, 1266, 434', '', '', '', '', '', '', '1349.00', '324.00', '16.60', '1 0 0 1 1349 330', 'Khu thương mại dịch vụ', 'A-10', '', 2, NULL, 0, 1),
(122, 7, 11, '86.80', '77.00', 'assets/client/pictures/catalog/room/room.png', 'poly', '1412, 436, 1411, 235, 1565, 237, 1582, 244, 1597, 260, 1607, 292, 1630, 422, 1448, 422, 1448, 436', '', '', '', '', '', '', '1517.00', '324.00', '16.60', '1 0 0 1 1517 330', 'Khu thương mại dịch vụ', 'A-11', '', 3, NULL, 0, 1),
(123, 7, 12, '51.70', '44.90', 'assets/client/pictures/catalog/room/room.png', 'poly', '1448, 423, 1628, 423, 1650, 530, 1419, 530, 1418, 471, 1449, 473', '', '', '', '', '', '', '1545.00', '474.00', '16.60', '1 0 0 1 1545 480', 'Khu thương mại dịch vụ', 'A-12', '', 1, NULL, 0, 1),
(124, 7, 13, '57.20', '50.70', 'assets/client/pictures/catalog/room/room.png', 'poly', '1419, 530, 1648, 531, 1666, 634, 1420, 634', '', '', '', '', '', '', '1545.00', '578.00', '16.60', '1 0 0 1 1545 584', 'Khu thương mại dịch vụ', 'A-13', '', 1, NULL, 0, 1),
(125, 7, 14, '59.80', '52.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '1419, 634, 1666, 634, 1671, 690, 1669, 737, 1420, 737', '', '', '', '', '', '', '1545.00', '682.00', '16.60', '1 0 0 1 1545 688', 'Khu thương mại dịch vụ', 'A-14', '', 1, NULL, 0, 1),
(126, 7, 15, '77.40', '75.00', 'assets/client/pictures/catalog/room/room.png', 'poly', '1417, 737, 1669, 737, 1663, 798, 1648, 856, 1642, 880, 1435, 881, 1418, 862', '', '', '', '', '', '', '1545.00', '804.00', '16.60', '1 0 0 1 1545 810', 'Khu thương mại dịch vụ', 'A-15', '', 0, NULL, 0, 1),
(127, 7, 16, '81.10', '71.80', 'assets/client/pictures/catalog/room/room.png', 'poly', '1321, 873, 1376, 820, 1418, 820, 1418, 863, 1436, 880, 1641, 879, 1623, 924, 1603, 964, 1558, 1025, 1521, 1069', '', '', '', '', '', '', '1498.00', '942.00', '16.60', '1 0 0 1 1498 948', 'Khu thương mại dịch vụ', 'A-16', '', 2, NULL, 0, 1),
(128, 7, 17, '78.40', '69.70', 'assets/client/pictures/catalog/room/room.png', 'poly', '1268, 927, 1322, 873, 1520, 1068, 1476, 1107, 1417, 1144, 1372, 1159, 1327, 1164, 1329, 986', '', '', '', '', '', '', '1411.00', '1042.00', '16.60', '1 0 0 1 1411 1048', 'Khu thương mại dịch vụ', 'A-17', '', 2, NULL, 0, 1),
(129, 7, 18, '67.40', '69.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '1182, 969, 1225, 968, 1266, 927, 1329, 987, 1328, 1163, 1265, 1170, 1182, 1182', '', '', '', '', '', '', '1268.00', '1074.00', '16.60', '1 0 0 1 1268 1080', 'Khu thương mại dịch vụ', 'A-18', '', 2, NULL, 0, 1),
(130, 7, 19, '53.80', '47.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '1076, 969, 1183, 969, 1183, 1183, 1076, 1196', '', '', '', '', '', '', '1146.00', '1076.00', '16.60', '1 0 0 1 1146 1082', 'Khu thương mại dịch vụ', 'A-19', '', 1, NULL, 0, 1),
(131, 7, 20, '57.30', '50.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '971, 970, 1079, 969, 1077, 1193, 967, 1209', '', '', '', '', '', '', '1045.00', '1076.00', '16.60', '1 0 0 1 1045 1082', 'Khu thương mại dịch vụ', 'A-20', '', 1, NULL, 0, 1),
(132, 8, 1, '75.10', '66.30', 'assets/client/pictures/catalog/room/room.png', 'poly', '732, 1073, 936, 1074, 936, 1041, 973, 1041, 972, 1211, 861, 1224, 834, 1221, 804, 1212, 783, 1200, 759, 1179, 739, 1148, 734, 1114', '', '', '', '', '', '', '852.00', '1140.00', '16.60', '1 0 0 1 852 1146', 'Khu thương mại dịch vụ', 'A-01', '', 2, NULL, 0, 1),
(133, 8, 2, '66.70', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '736, 931, 937, 930, 937, 1076, 735, 1075', '', '', '', '', '', '', '852.00', '998.00', '16.60', '1 0 0 1 852 1004', 'Khu thương mại dịch vụ', 'A-02', '', 2, NULL, 0, 1),
(134, 8, 3, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '735, 787, 937, 789, 936, 931, 735, 932', '', '', '', '', '', '', '852.00', '862.00', '16.60', '1 0 0 1 852 868', 'Khu thương mại dịch vụ', 'A-03', '', 2, NULL, 0, 1),
(135, 8, 4, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '736, 645, 936, 644, 936, 788, 736, 787', '', '', '', '', '', '', '852.00', '712.00', '16.60', '1 0 0 1 852 718', 'Khu thương mại dịch vụ', 'A-04', '', 2, NULL, 0, 1),
(136, 8, 5, '48.50', '42.90', 'assets/client/pictures/catalog/room/room.png', 'poly', '735, 541, 937, 541, 936, 645, 736, 643', '', '', '', '', '', '', '852.00', '582.00', '16.60', '1 0 0 1 852 588', 'Khu thương mại dịch vụ', 'A-05', '', 1, NULL, 0, 1),
(137, 8, 6, '48.50', '42.90', 'assets/client/pictures/catalog/room/room.png', 'poly', '735, 437, 937, 438, 936, 541, 736, 543', '', '', '', '', '', '', '852.00', '492.00', '16.60', '1 0 0 1 852 498', 'Khu thương mại dịch vụ', 'A-06', '', 1, NULL, 0, 1),
(138, 8, 7, '92.00', '81.20', 'assets/client/pictures/catalog/room/room.png', 'poly', '937, 436, 735, 437, 735, 315, 735, 293, 740, 278, 749, 260, 765, 247, 791, 237, 823, 235, 937, 235', '', '', '', '', '', '', '852.00', '336.00', '16.60', '1 0 0 1 852 342', 'Khu thương mại dịch vụ', 'A-07', '', 3, NULL, 0, 1),
(139, 8, 8, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '975, 238, 1120, 237, 1119, 435, 975, 435', '', '', '', '', '', '', '1048.00', '336.00', '16.60', '1 0 0 1 1048 342', 'Khu thương mại dịch vụ', 'A-08', '', 2, NULL, 0, 1),
(140, 8, 9, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '1121, 235, 1269, 237, 1267, 436, 1119, 435', '', '', '', '', '', '', '1205.00', '324.00', '16.60', '1 0 0 1 1205 330', 'Khu thương mại dịch vụ', 'A-09', '', 2, NULL, 0, 1),
(141, 8, 10, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '1263, 235, 1414, 235, 1415, 435, 1268, 435', '', '', '', '', '', '', '1349.00', '324.00', '16.60', '1 0 0 1 1349 330', 'Khu thương mại dịch vụ', 'A-10', '', 2, NULL, 0, 1),
(142, 8, 11, '86.80', '77.00', 'assets/client/pictures/catalog/room/room.png', 'poly', '1449, 435, 1414, 435, 1412, 234, 1552, 236, 1572, 238, 1588, 249, 1601, 267, 1629, 421, 1449, 421', '', '', '', '', '', '', '1517.00', '324.00', '16.60', '1 0 0 1 1517 330', 'Khu thương mại dịch vụ', 'A-11', '', 3, NULL, 0, 1),
(143, 8, 12, '51.70', '44.90', 'assets/client/pictures/catalog/room/room.png', 'poly', '1420, 532, 1419, 472, 1449, 472, 1449, 423, 1629, 422, 1650, 530', '', '', '', '', '', '', '1545.00', '474.00', '16.60', '1 0 0 1 1545 480', 'Khu thương mại dịch vụ', 'A-12', '', 1, NULL, 0, 1),
(144, 8, 13, '57.20', '50.70', 'assets/client/pictures/catalog/room/room.png', 'poly', '1417,532,1419,633,1667,634,1646,532', '', '', '', '', '', '', '1545.00', '578.00', '16.60', '1 0 0 1 1545 584', 'Khu thương mại dịch vụ', 'A-13', '', 1, NULL, 0, 1),
(145, 8, 14, '59.80', '52.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '1420, 633, 1420, 739, 1670, 738, 1673, 690, 1666, 632', '', '', '', '', '', '', '1545.00', '682.00', '16.60', '1 0 0 1 1545 688', 'Khu thương mại dịch vụ', 'A-14', '', 1, NULL, 0, 1),
(146, 8, 15, '95.90', '84.80', 'assets/client/pictures/catalog/room/room.png', 'poly', '1419, 737, 1669, 737, 1662, 808, 1651, 854, 1640, 885, 1632, 908, 1623, 922, 1603, 927, 1567, 927, 1567, 883, 1397, 884, 1356, 844, 1401, 797, 1419, 777', '', '', '', '', '', '', '1545.00', '804.00', '16.60', '1 0 0 1 1545 810', 'Khu thương mại dịch vụ', 'A-15', '', 3, NULL, 0, 1),
(147, 8, 16, '77.50', '69.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '1183, 969, 1227, 969, 1273, 925, 1329, 985, 1328, 1114, 1374, 1115, 1375, 1137, 1367, 1153, 1354, 1163, 1183, 1184', '', '', '', '', '', '', '1268.00', '1074.00', '16.60', '1 0 0 1 1268 1080', 'Khu thương mại dịch vụ', 'A-16', '', 2, NULL, 0, 1),
(148, 8, 17, '53.80', '47.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '1078,967,1180,969,1184,1184,1076,1190', '', '', '', '', '', '', '1146.00', '1076.00', '16.60', '1 0 0 1 1146 1082', 'Khu thương mại dịch vụ', 'A-17', '', 1, NULL, 0, 1),
(149, 8, 18, '57.30', '50.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '970,967,1076,967,1076,1192,970,1207', '', '', '', '', '', '', '1045.00', '1076.00', '16.60', '1 0 0 1 1045 1082', 'Khu thương mại dịch vụ', 'A-18', '', 1, NULL, 0, 1),
(159, 9, 5, '250.00', '66.30', 'assets/client/pictures/catalog/room/room.png', 'poly', '966,968,970,1201,855,1216,817,1212,792,1203,770,1184,756,1169,745,1146,737,1127,737,1076,734,790,936,790,934,970', '', '', '', '', '', '', '852.00', '998.00', '16.60', '1 0 0 1 852 1004', 'Khu thương mại dịch vụ', 'Penhouse A30-05', '', 3, NULL, 0, 1),
(160, 9, 2, '66.70', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '732,934,936,932,936,1074,737,1070', '', '', '', '', '', '', '852.00', '998.00', '16.60', '1 0 0 1 852 1004', 'Khu thương mại dịch vụ', 'A-02', '', 2, NULL, 0, 0),
(161, 9, 3, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '732,785,938,787,932,932,724,932', '', '', '', '', '', '', '852.00', '862.00', '16.60', '1 0 0 1 852 868', 'Khu thương mại dịch vụ', 'A-03', '', 2, NULL, 0, 0),
(162, 9, 6, '160.00', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '726,435,932,433,932,783,728,785', '', '', '', '', '', '', '852.00', '622.00', '16.60', '1 0 0 1 852 626', 'Khu thương mại dịch vụ', 'Penhouse A30-06', '', 3, 'Tầng sân vườn', 0, 1),
(163, 9, 5, '48.50', '42.90', 'assets/client/pictures/catalog/room/room.png', 'poly', '733, 787, 934, 786, 936, 969, 972, 968, 972, 1207, 860, 1221, 833, 1221, 797, 1209, 772, 1191, 751, 1167, 739, 1144, 732, 1118', '', '', '', '', '', '', '852.00', '582.00', '16.60', '1 0 0 1 852 588', 'Khu thương mại dịch vụ', 'A-05', '', 1, NULL, 0, 0),
(164, 9, 6, '48.50', '42.90', 'assets/client/pictures/catalog/room/room.png', 'poly', '733, 435, 935, 436, 935, 787, 733, 786', '', '', '', '', '', '', '852.00', '492.00', '16.60', '1 0 0 1 852 498', 'Khu thương mại dịch vụ', 'A-06', '', 1, 'Tầng sân vườn', 0, 0),
(165, 9, 4, '230.00', '81.20', 'assets/client/pictures/catalog/room/room.png', 'poly', '971, 968, 1227, 968, 1274, 920, 1379, 1025, 1380, 1134, 1372, 1152, 1356, 1163, 971, 1208', '', '', '', '', '', '', '1146.00', '1076.00', '16.60', '1 0 0 1 1146 1082', 'Khu thương mại dịch vụ', 'Penhouse A30-04', '', 3, NULL, 0, 1),
(166, 9, 8, '53.80', '47.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '1080,1188,1080,967,1186,965,1188,1181,1121,1188,1157,1183', '', '', '', '', '', '', '1146.00', '1076.00', '16.60', '1 0 0 1 1146 1082', 'Khu thương mại dịch vụ', 'A-8', '', 1, NULL, 0, 0),
(167, 9, 9, '57.30', '50.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '977,1204,977,962,1080,965,1083,1194', '', '', '', '', '', '', '1045.00', '1076.00', '16.60', '1 0 0 1 1038 1082', 'Khu thương mại dịch vụ', 'A-9', '', 1, NULL, 0, 0),
(168, 9, 1, '232.20', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '734, 436, 733, 300, 736, 282, 745, 264, 756, 252, 775, 240, 797, 234, 1266, 236, 1267, 435', '', '', '', '', '', '', '1038.00', '336.00', '16.60', '1 0 0 1 1038 342', 'Khu thương mại dịch vụ', 'Penhouse A30-01', '', 4, 'Tầng sân vườn', 0, 1),
(169, 9, 2, '202.40', '77.00', 'assets/client/pictures/catalog/room/room.png', 'poly', '1265, 236, 1560, 237, 1578, 241, 1590, 250, 1601, 263, 1605, 279, 1650, 529, 1421, 530, 1420, 469, 1389, 434, 1266, 433', '', '', '', '', '', '', '1464.00', '352.00', '16.60', '1 0 0 1 1464 358', 'Khu thương mại dịch vụ', 'Penhouse A30-02', '', 4, NULL, 0, 1),
(170, 9, 3, '259.60', '44.90', 'assets/client/pictures/catalog/room/room.png', 'poly', '1420, 529, 1649, 529, 1668, 637, 1672, 687, 1672, 705, 1668, 772, 1659, 829, 1643, 890, 1634, 911, 1620, 922, 1604, 927, 1506, 925, 1483, 929, 1460, 938, 1440, 945, 1416, 965, 1400, 985, 1388, 1005, 1380, 1027, 1275, 923, 1421, 776\r\n', '', '', '', '', '', '', '1536.00', '761.00', '16.60', '1 0 0 1 1536 767', 'Khu thương mại dịch vụ', 'Penhouse A30-03', '', 4, 'Tầng sân vườn', 0, 1),
(171, 10, 1, '75.10', '66.30', 'assets/client/pictures/catalog/room/room.png', 'poly', '977,1210,866,1220,838,1217,817,1212,794,1203,779,1193,768,1183,758,1170,750,1154,743,1132,740,1112,740,1073,938,1071,938,1037,977,1035', '', '', '', '', '', '', '852.00', '1140.00', '16.60', '1 0 0 1 852 1146', 'Khu thương mại dịch vụ', 'A-01', '', 2, NULL, 0, 1),
(172, 4, 2, '66.70', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '738,1067,941,1070,941,925,735,928', '', '', '', '', '', '', '852.00', '998.00', '16.60', '1 0 0 1 852 1004', 'Khu thương mại dịch vụ', 'A-02', '', 2, NULL, 0, 1),
(173, 4, 3, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '735,928,941,928,941,782,732,784', '', '', '', '', '', '', '852.00', '862.00', '16.60', '1 0 0 1 852 868', 'Khu thương mại dịch vụ', 'A-03', '', 2, NULL, 0, 1),
(174, 4, 4, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '735,784,735,637,941,635,941,784', '', '', '', '', '', '', '852.00', '712.00', '16.60', '1 0 0 1 852 718', 'Khu thương mại dịch vụ', 'A-04', '', 2, NULL, 0, 1),
(175, 4, 5, '48.50', '42.90', 'assets/client/pictures/catalog/room/room.png', 'poly', '738,637,941,637,941,534,738,534', '', '', '', '', '', '', '852.00', '582.00', '16.60', '1 0 0 1 852 588', 'Khu thương mại dịch vụ', 'A-05', '', 1, NULL, 0, 1),
(176, 4, 6, '48.50', '42.90', 'assets/client/pictures/catalog/room/room.png', 'poly', '740,532,941,534,941,429,738,431', '', '', '', '', '', '', '852.00', '492.00', '16.60', '1 0 0 1 852 498', 'Khu thương mại dịch vụ', 'A-06', '', 1, NULL, 0, 1),
(177, 4, 7, '92.00', '81.20', 'assets/client/pictures/catalog/room/room.png', 'poly', '812,230,792,230,779,235,761,248,753,258,745,273,740,291,738,307,740,428,941,430,936,230', '', '', '', '', '', '', '852.00', '336.00', '16.60', '1 0 0 1 852 342', 'Khu thương mại dịch vụ', 'A-07', '', 3, NULL, 0, 1),
(178, 4, 8, '67.90', '60.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '977,430,977,230,1124,232,1124,430', '', '', '', '', '', '', '1048.00', '324.00', '16.60', '1 0 0 1 1048 330', 'Khu thương mại dịch vụ', 'A-08', '', 2, NULL, 0, 1),
(179, 4, 9, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '1126,229,1271,230,1271,429,1126,429', '', '', '', '', '', '', '1205.00', '324.00', '16.60', '1 0 0 1 1205 330', 'Khu thương mại dịch vụ', 'A-09', '', 2, NULL, 0, 1),
(180, 10, 2, '66.70', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '738,1067,941,1070,941,925,735,928', '', '', '', '', '', '', '852.00', '998.00', '16.60', '1 0 0 1 852 1004', 'Khu thương mại dịch vụ', 'A-02', '', 2, NULL, 0, 1),
(181, 10, 3, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '735,928,941,928,941,782,732,784', '', '', '', '', '', '', '852.00', '862.00', '16.60', '1 0 0 1 852 868', 'Khu thương mại dịch vụ', 'A-03', '', 2, NULL, 0, 1),
(182, 10, 4, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '735,784,735,637,941,635,941,784', '', '', '', '', '', '', '852.00', '712.00', '16.60', '1 0 0 1 852 718', 'Khu thương mại dịch vụ', 'A-04', '', 2, NULL, 0, 1),
(183, 10, 5, '48.50', '42.90', 'assets/client/pictures/catalog/room/room.png', 'poly', '738,637,941,637,941,534,738,534', '', '', '', '', '', '', '852.00', '582.00', '16.60', '1 0 0 1 852 588', 'Khu thương mại dịch vụ', 'A-05', '', 1, NULL, 0, 1),
(184, 10, 6, '48.50', '42.90', 'assets/client/pictures/catalog/room/room.png', 'poly', '740,532,941,534,941,429,738,431', '', '', '', '', '', '', '852.00', '492.00', '16.60', '1 0 0 1 852 498', 'Khu thương mại dịch vụ', 'A-06', '', 1, NULL, 0, 1),
(185, 10, 7, '92.00', '81.20', 'assets/client/pictures/catalog/room/room.png', 'poly', '812,230,792,230,779,235,761,248,753,258,745,273,740,291,738,307,740,428,941,430,936,230', '', '', '', '', '', '', '852.00', '336.00', '16.60', '1 0 0 1 852 342', 'Khu thương mại dịch vụ', 'A-07', '', 3, NULL, 0, 1),
(186, 10, 8, '67.90', '60.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '977,430,977,230,1124,232,1124,430', '', '', '', '', '', '', '1048.00', '324.00', '16.60', '1 0 0 1 1048 330', 'Khu thương mại dịch vụ', 'A-08', '', 2, NULL, 0, 1),
(187, 10, 9, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '1126,229,1271,230,1271,429,1126,429', '', '', '', '', '', '', '1205.00', '324.00', '16.60', '1 0 0 1 1205 330', 'Khu thương mại dịch vụ', 'A-09', '', 2, NULL, 0, 1),
(188, 10, 10, '66.90', '61.10', 'assets/client/pictures/catalog/room/room.png', 'poly', '1271,433,1417,430,1415,230,1271,230', '', '', '', '', '', '', '1349.00', '324.00', '16.60', '1 0 0 1 1349 330', 'Khu thương mại dịch vụ', 'A-10', '', 2, NULL, 0, 1),
(189, 10, 11, '86.80', '44.90', 'assets/client/pictures/catalog/room/room.png', 'poly', '1420,428,1453,428,1453,417,1634,415,1616,291,1610,273,1605,260,1595,245,1585,235,1574,230,1564,230,1551,230,1420,230,1420,271', '', '', '', '', '', '', '1517.00', '324.00', '16.60', '1 0 0 1 1517 330', 'Khu thương mại dịch vụ', 'A-11', '', 3, NULL, 0, 1),
(190, 10, 12, '51.70', '44.90', 'assets/client/pictures/catalog/room/room.png', 'poly', '1456,415,1636,415,1654,523,1425,528,1425,466,1453,466', '', '', '', '', '', '', '1545.00', '474.00', '16.60', '1 0 0 1 1545 480', 'Khu thương mại dịch vụ', 'A-12', '', 1, NULL, 0, 1),
(191, 10, 13, '57.20', '50.70', 'assets/client/pictures/catalog/room/room.png', 'poly', '1422,526,1657,523,1672,631,1425,631', '', '', '', '', '', '', '1545.00', '578.00', '16.60', '1 0 0 1 1545 584', 'Khu thương mại dịch vụ', 'A-13', '', 1, NULL, 0, 1),
(192, 10, 14, '59.80', '52.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '1422,631,1422,734,1677,734,1677,708,1677,688,1677,670,1677,649,1675,628', '', '', '', '', '', '', '1545.00', '682.00', '16.60', '1 0 0 1 1545 688', 'Khu thương mại dịch vụ', 'A-14', '', 1, NULL, 0, 1),
(193, 10, 15, '95.90', '84.80', 'assets/client/pictures/catalog/room/room.png', 'poly', '1569,880,1572,893,1574,906,1580,914,1598,921,1616,921,1631,911,1641,896,1647,878,1667,808,1675,746,1677,728,1422,728,1425,772,1358,839,1402,878', '', '', '', '', '', '', '1545.00', '804.00', '16.60', '1 0 0 1 1545 810', 'Khu thương mại dịch vụ', 'A-15', '', 3, NULL, 0, 1),
(194, 10, 16, '78.40', '69.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '1379,1112,1338,1109,1335,983,1276,921,1235,963,1186,965,1191,1179,1276,1168,1307,1166,1332,1163,1348,1161,1366,1153,1379,1138', '', '', '', '', '', '', '1268.00', '1074.00', '16.60', '1 0 0 1 1268 1080', 'Khu thương mại dịch vụ', 'A-16', '', 2, NULL, 0, 1),
(195, 10, 17, '53.80', '47.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '1080,1188,1080,967,1186,965,1188,1181,1121,1188,1157,1183', '', '', '', '', '', '', '1146.00', '1076.00', '16.60', '1 0 0 1 1146 1082', 'Khu thương mại dịch vụ', 'A-17', '', 1, NULL, 0, 1),
(196, 10, 18, '57.30', '50.60', 'assets/client/pictures/catalog/room/room.png', 'poly', '977,1204,977,962,1080,965,1083,1194', '', '', '', '', '', '', '1045.00', '1076.00', '16.60', '1 0 0 1 1045 1082', 'Khu thương mại dịch vụ', 'A-18', '', 1, NULL, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `master_plan_type`
--

CREATE TABLE `master_plan_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master_plan_type`
--

INSERT INTO `master_plan_type` (`id`, `name`, `description`, `Status`) VALUES
(1, '33333', 'CCC', 1),
(2, 'XXXX', 'XXXX', 0),
(3, 'ccccc', 'cccc', 1);

-- --------------------------------------------------------

--
-- Table structure for table `navigates`
--

CREATE TABLE `navigates` (
  `NavID` int(11) NOT NULL,
  `NavName` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `NavMeta` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `ParentId` int(11) DEFAULT 0,
  `Status` tinyint(4) DEFAULT 1,
  `NavLang` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Position` int(11) DEFAULT 0,
  `Type` tinyint(4) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `navigates`
--

INSERT INTO `navigates` (`NavID`, `NavName`, `NavMeta`, `ParentId`, `Status`, `NavLang`, `Position`, `Type`) VALUES
(1, 'Giới thiệu', 'gioi-thieu.html', 0, 1, 'vi', 1, 0),
(3, 'Liên hệ', 'lien-he.html', 0, 1, 'vi', 7, 0),
(8, 'Tin tức', 'tin-tuc', 0, 1, 'vi', 6, 0);

-- --------------------------------------------------------

--
-- Table structure for table `orderdetail`
--

CREATE TABLE `orderdetail` (
  `orderDetailId` int(11) NOT NULL,
  `orderId` int(11) DEFAULT NULL,
  `proId` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL DEFAULT 1,
  `proPrice` float NOT NULL DEFAULT 0,
  `proDiscount` float NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orderdetail`
--

INSERT INTO `orderdetail` (`orderDetailId`, `orderId`, `proId`, `quantity`, `proPrice`, `proDiscount`) VALUES
(1, 1, 4, 5, 4000000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `orderId` int(11) NOT NULL,
  `fullName` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `address` varchar(400) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`orderId`, `fullName`, `phone`, `address`, `email`, `content`, `createdDate`, `status`) VALUES
(1, 'asd', '6456465', '', '', '', '2019-03-19 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `packagedetail`
--

CREATE TABLE `packagedetail` (
  `packDetailId` int(11) NOT NULL,
  `packageId` int(11) DEFAULT NULL,
  `billId` int(11) DEFAULT NULL,
  `isReceived` bit(1) NOT NULL DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `packagedetail`
--

INSERT INTO `packagedetail` (`packDetailId`, `packageId`, `billId`, `isReceived`) VALUES
(1, 1, 4, b'0'),
(5, 3, 5, b'0'),
(6, 1, 5, b'0'),
(7, 4, 5, b'1'),
(8, 4, 4, b'1'),
(9, 12, 20, b'0'),
(10, 12, 20, b'0'),
(11, 12, 19, b'0'),
(12, 19, 48, b'0'),
(14, 22, 49, b'0'),
(15, 24, 66, b'0'),
(16, 24, 80, b'0'),
(17, 24, 81, b'0'),
(18, 24, 66, b'0');

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `packageId` int(11) NOT NULL,
  `packageCustomId` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serviceId` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isDeleted` bit(1) NOT NULL DEFAULT b'0',
  `packageDatetime` datetime DEFAULT NULL,
  `weight` float NOT NULL DEFAULT 0,
  `note` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fromPlace` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fromPlaceDetail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `toPlace` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `toPlaceDetail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`packageId`, `packageCustomId`, `serviceId`, `isDeleted`, `packageDatetime`, `weight`, `note`, `fromPlace`, `fromPlaceDetail`, `toPlace`, `toPlaceDetail`) VALUES
(1, 'sdfsdf', 'PNL', b'1', '2019-04-20 10:20:59', 12, '', 'HCPN', 'Hồ Chí Minh, QUẬN PHÚ NHUẬN', 'HCHM', 'Hồ Chí Minh, HUYỆN HÓC MÔN'),
(3, 'gjkjk', 'PNL', b'1', '2019-04-20 12:17:14', 12, NULL, 'HCPN', 'Hồ Chí Minh, QUẬN PHÚ NHUẬN', 'HCHM', 'Hồ Chí Minh, HUYỆN HÓC MÔN'),
(4, 'LKJH', 'VHT', b'1', '2019-05-09 21:39:18', 10, 'TẢI GẤP', 'HCM', 'TP HCM', 'HCM', 'TP HCM'),
(5, 'HCM01', 'VHT', b'0', '2019-08-26 03:54:26', 10000, 'TẢI TIÊU XANH', 'HCM', 'TP HCM', 'HCM', 'TP HCM'),
(6, 'HCM02', 'VHT', b'0', '2019-08-26 03:54:26', 10000, 'TẢI TIÊU XANH', 'HCM', 'TP HCM', 'HCM', 'TP HCM'),
(7, 'HCM03', 'VHT', b'0', '2019-08-26 03:54:26', 10000, 'TẢI TIÊU XANH', 'hcbh', 'TP HCM', 'Hcbh', 'TP HCM'),
(8, 'TEST020901', 'VHT', b'0', '2019-09-02 19:35:09', 10000, 'TẢI NHANH', 'HCM', 'TP HCM', 'HNI', 'Hà Nội'),
(9, 'TESTTAI0709', 'PHT', b'1', '2019-09-07 11:42:55', 10000, 'QUÀ TẶNG', 'HCM', 'TP HCM', 'HNI', 'Hà Nội'),
(10, 'TESTMATAI07092019', 'PHT', b'1', '2019-09-07 11:47:08', 1000, 'QUÀ TẶNG', 'HCM', 'TP HCM', 'HNI', 'Hà Nội'),
(11, 'TESTMATAI0709', 'PHT', b'1', '2019-09-07 11:47:08', 1000, 'QUÀ TẶNG', 'HCM', 'TP HCM', 'HNI', 'Hà Nội'),
(12, 'TESTMATAI0710', 'PHT', b'1', '2019-09-07 12:32:57', 10, '', 'HCM', 'TP HCM', 'DNg', 'Đà Nẵng'),
(13, 'TESTTAI0709', 'CPN', b'1', '2019-09-07 15:39:35', 1000, 'TẢI QUÀ TẶNG', 'HCM', 'TP HCM', 'HNI', 'Hà Nội'),
(14, 'MATAI0709', 'CPN', b'1', '2019-09-07 15:45:22', 1000, 'TẢI QUÀ TẶNG', 'HCM', 'TP HCM', 'HNI', 'Hà Nội'),
(15, 'MATAI07090907', 'CPN', b'1', '2019-09-07 15:48:01', 10000, 'TẢI QUÀ TẶNG', 'HCM', 'TP HCM', 'HCM', 'TP HCM'),
(16, 'TEST0709001', 'CPN', b'1', '2019-09-07 15:53:31', 1000, NULL, 'HCM', 'TP HCM', 'HCM', 'TP HCM'),
(17, 'TEST0709001', 'CPN', b'1', '2019-09-08 10:53:28', 1000, NULL, 'HCM', 'TP HCM', 'HCM', 'TP HCM'),
(18, 'TEST0709001', 'cpn', b'1', '2019-09-08 10:56:15', 1000, NULL, 'hcm', 'TP HCM', 'hcm', 'TP HCM'),
(19, 'TEST0709001', 'CPN', b'1', '2019-09-08 10:59:32', 1000, '', 'hcm', 'TP HCM', 'hcm', 'TP HCM'),
(20, 'QUANGHUY0809', 'CPN', b'1', '2019-09-08 12:29:10', 1000, 'QUÀ TẶNG', 'HCM', 'TP HCM', 'HNI', 'Hà Nội'),
(21, 'MATAI01', 'CPN', b'0', '2019-09-08 12:37:17', 1000, 'QUÀ TẶNG', 'HCM', 'TP HCM', 'HCM', 'TP HCM'),
(22, 'MATAI02', 'CPN', b'0', '2019-09-08 12:38:14', 1000, 'QUÀ TẶNG', 'HCM', 'TP HCM', 'HNI', 'Hà Nội'),
(23, 'MATAI03', 'CPN', b'0', '2019-09-08 12:44:14', 1000, 'QUÀ', 'HCM', 'TP HCM', 'HNI', 'Hà Nội'),
(24, 'VN001', 'PST', b'0', '2020-08-24 22:27:54', 1, '', '001', '007', '001', '007');

-- --------------------------------------------------------

--
-- Table structure for table `partner`
--

CREATE TABLE `partner` (
  `id` int(11) NOT NULL,
  `icon` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `partner`
--

INSERT INTO `partner` (`id`, `icon`) VALUES
(1, 'assets/clients/imgs/footer/logoparner1.png'),
(2, 'assets/clients/imgs/footer/logoparner2.png'),
(3, 'assets/clients/imgs/footer/logoparner3.png'),
(4, 'assets/clients/imgs/footer/logoparner4.png');

-- --------------------------------------------------------

--
-- Table structure for table `plusservices`
--

CREATE TABLE `plusservices` (
  `plusServId` int(11) NOT NULL,
  `plusServName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unitPrice` decimal(10,0) NOT NULL DEFAULT 0,
  `isDeleted` bit(1) NOT NULL DEFAULT b'0',
  `typePrice` bit(1) NOT NULL DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `plusservices`
--

INSERT INTO `plusservices` (`plusServId`, `plusServName`, `unitPrice`, `isDeleted`, `typePrice`) VALUES
(1, 'HÀNG GIÁ TRỊ CAO (GTC)', '2000', b'0', b'1'),
(2, 'BÁO PHÁT (BP)', '5000', b'0', b'1'),
(3, 'PHÁT TẬN TAY (PTT)', '5000', b'0', b'1'),
(4, 'ĐỒNG KIỂM (DK)', '15000', b'0', b'1'),
(5, 'BẢO HIỂM (BH)', '1', b'0', b'0'),
(7, 'PHỤ PHÍ ĐÓNG GÓI (PP)', '15000', b'0', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `position`
--

CREATE TABLE `position` (
  `Id` int(11) NOT NULL,
  `Title` text CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Content` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Image` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `background` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `position`
--

INSERT INTO `position` (`Id`, `Title`, `Content`, `Image`, `background`) VALUES
(1, 'VỊ TRÍ ĐẮT GIÁ', '<p>Mũi N&eacute; Summerland nằm tại trung t&acirc;m &ldquo;thủ phủ resort&rdquo;, l&agrave; dự &aacute;n nằm gần s&acirc;n bay Phan Thiết nhất trong b&aacute;n k&iacute;nh 2km đ&oacute;n đầu sự ph&aacute;t triển của hạ tầng cho c&aacute;c nh&agrave; đầu tư<br />\n<br />\nNgo&agrave;i ra, dự &aacute;n c&ograve;n nằm đối diện s&acirc;n golf Sealink, tr&ecirc;n ngọn đồi Hải Vọng &ndash; nơi mệnh danh l&agrave; điểm ngắm ho&agrave;ng h&ocirc;n tr&ecirc;n biển đẹp nhất Việt Nam. Được bao bọc bởi 2 tuyến đường huyết mạch của Phan Thiết l&agrave; đường V&otilde; Nguy&ecirc;n Gi&aacute;p v&agrave; Quốc lộ 1A.</p>\n', 'assets/includes/upload/files/map-no-logo.png', 'assets/clients/imgs/vitri/bg3.png');

-- --------------------------------------------------------

--
-- Table structure for table `positon_ct`
--

CREATE TABLE `positon_ct` (
  `id` int(11) NOT NULL,
  `name` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `positon_ct`
--

INSERT INTO `positon_ct` (`id`, `name`) VALUES
(1, 'Đối diện sân golf Sealink'),
(2, 'Sân bay Phan Thiết: 5 phút'),
(3, 'Cao tốc Dầu Giây - Phan Thiết: 5 phút'),
(4, 'Bãi đá Ông Địa: 2 phút'),
(5, 'Trung tâm TP. Phan Thiết: 3 Phút');

-- --------------------------------------------------------

--
-- Table structure for table `positon_from_to`
--

CREATE TABLE `positon_from_to` (
  `id` int(11) NOT NULL,
  `fromPlace` longtext COLLATE utf8_unicode_ci NOT NULL,
  `toPlace` longtext COLLATE utf8_unicode_ci NOT NULL,
  `detailPlace` longtext COLLATE utf8_unicode_ci NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `positon_from_to`
--

INSERT INTO `positon_from_to` (`id`, `fromPlace`, `toPlace`, `detailPlace`, `time`) VALUES
(1, 'TP.HCM', 'PHAN THIẾT', 'Cao tốc Dầu Giây - Phan Thiết', 180),
(2, 'HÀ NỘI', 'PHAN THIẾT', 'Sân bay Nội Bài - Phan Thiết', 90);

-- --------------------------------------------------------

--
-- Table structure for table `prices`
--

CREATE TABLE `prices` (
  `priceId` int(11) NOT NULL,
  `toPlace` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `service` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `dcpWeight` float NOT NULL DEFAULT 0,
  `dcpPrice` decimal(10,0) NOT NULL DEFAULT 0,
  `fromPlace` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dcpToWeight` float NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `prices`
--

INSERT INTO `prices` (`priceId`, `toPlace`, `service`, `dcpWeight`, `dcpPrice`, `fromPlace`, `dcpToWeight`) VALUES
(945, 'HNI', 'CPN', 1001, '58064', 'HCM', 1500),
(946, 'HNI', 'CPN', 1501, '69891', 'HCM', 2000),
(947, 'HNI', 'CPN', 2001, '78708', 'HCM', 2500),
(948, 'HNI', 'CPN', 2501, '87525', 'HCM', 3000),
(949, 'HNI', 'CPN', 3001, '96342', 'HCM', 3500),
(950, 'HNI', 'CPN', 3501, '105159', 'HCM', 4000),
(951, 'HNI', 'CPN', 4001, '113977', 'HCM', 4500),
(952, 'HNI', 'CPN', 4501, '122794', 'HCM', 5000),
(953, 'HNI', 'CPN', 5001, '131611', 'HCM', 5500),
(954, 'HNI', 'CPN', 5501, '140428', 'HCM', 6000),
(955, 'HNI', 'CPN', 6001, '149245', 'HCM', 6500),
(956, 'HNI', 'CPN', 6501, '158062', 'HCM', 7000),
(957, 'HNI', 'CPN', 7001, '166879', 'HCM', 7500),
(958, 'HNI', 'CPN', 7501, '175696', 'HCM', 8000),
(959, 'HNI', 'CPN', 8001, '184513', 'HCM', 8500),
(960, 'HNI', 'CPN', 8501, '193330', 'HCM', 9000),
(961, 'HNI', 'CPN', 9001, '202147', 'HCM', 9500),
(962, 'HNI', 'CPN', 9501, '210964', 'HCM', 10000),
(963, 'HNI', 'CPN', 10001, '219781', 'HCM', 10500),
(964, 'HNI', 'CPN', 10501, '228598', 'HCM', 11000),
(965, 'HNI', 'CPN', 11001, '237415', 'HCM', 11500),
(966, 'HNI', 'CPN', 11501, '246232', 'HCM', 12000),
(967, 'HNI', 'CPN', 12001, '255049', 'HCM', 12500),
(968, 'HNI', 'CPN', 12501, '263866', 'HCM', 13000),
(969, 'HNI', 'CPN', 13001, '272683', 'HCM', 13500),
(970, 'HNI', 'CPN', 13501, '281500', 'HCM', 14000),
(971, 'HNI', 'CPN', 14001, '290318', 'HCM', 14500),
(972, 'HNI', 'CPN', 14501, '299135', 'HCM', 15000),
(973, 'HNI', 'CPN', 15001, '307952', 'HCM', 15500),
(974, 'HNI', 'CPN', 15501, '316769', 'HCM', 16000),
(975, 'HNI', 'CPN', 16001, '325586', 'HCM', 16500),
(976, 'HNI', 'CPN', 16501, '334403', 'HCM', 17000),
(977, 'HNI', 'CPN', 17001, '343220', 'HCM', 17500),
(978, 'HNI', 'CPN', 17501, '352037', 'HCM', 18000),
(979, 'HNI', 'CPN', 18001, '360854', 'HCM', 18500),
(980, 'HNI', 'CPN', 18501, '369671', 'HCM', 19000),
(981, 'HNI', 'CPN', 19001, '378488', 'HCM', 19500),
(982, 'HNI', 'CPN', 19501, '387305', 'HCM', 20000),
(983, 'HNI', 'CPN', 20001, '396122', 'HCM', 20500),
(984, 'HNI', 'CPN', 20501, '404939', 'HCM', 21000),
(985, 'HNI', 'CPN', 21001, '413756', 'HCM', 21500),
(986, 'HNI', 'CPN', 21501, '422573', 'HCM', 22000),
(987, 'HNI', 'CPN', 22001, '431390', 'HCM', 22500),
(988, 'HNI', 'CPN', 22501, '440207', 'HCM', 23000),
(989, 'HNI', 'CPN', 23001, '449024', 'HCM', 23500),
(990, 'HNI', 'CPN', 23501, '457841', 'HCM', 24000),
(991, 'HNI', 'CPN', 24001, '466659', 'HCM', 24500),
(992, 'HNI', 'CPN', 24501, '475476', 'HCM', 25000),
(993, 'HNI', 'CPN', 25001, '484293', 'HCM', 25500),
(994, 'HNI', 'CPN', 25501, '493110', 'HCM', 26000),
(995, 'HNI', 'CPN', 26001, '501927', 'HCM', 26500),
(996, 'HNI', 'CPN', 26501, '510744', 'HCM', 27000),
(997, 'HNI', 'CPN', 27001, '519561', 'HCM', 27500),
(998, 'HNI', 'CPN', 27501, '528378', 'HCM', 28000),
(999, 'HNI', 'CPN', 28001, '537195', 'HCM', 28500),
(1000, 'HNI', 'CPN', 28501, '546012', 'HCM', 29000),
(1001, 'HNI', 'CPN', 29001, '554829', 'HCM', 29500),
(1002, 'HNI', 'CPN', 29501, '563646', 'HCM', 30000),
(1003, 'HNI', 'CPN', 30001, '572463', 'HCM', 30500),
(1004, 'HNI', 'CPN', 30501, '581280', 'HCM', 31000),
(1005, 'HNI', 'CPN', 31001, '590097', 'HCM', 31500),
(1006, 'HNI', 'CPN', 31501, '598914', 'HCM', 32000),
(1007, 'HNI', 'CPN', 32001, '607731', 'HCM', 32500),
(1008, 'HNI', 'CPN', 32501, '616548', 'HCM', 33000),
(1009, 'HNI', 'CPN', 33001, '625365', 'HCM', 33500),
(1010, 'HNI', 'CPN', 33501, '634182', 'HCM', 34000),
(1011, 'HNI', 'CPN', 34001, '643000', 'HCM', 34500),
(1012, 'HNI', 'CPN', 34501, '651817', 'HCM', 35000),
(1013, 'HNI', 'CPN', 35001, '660634', 'HCM', 35500),
(1014, 'HNI', 'CPN', 35501, '669451', 'HCM', 36000),
(1015, 'HNI', 'CPN', 36001, '678268', 'HCM', 36500),
(1016, 'HNI', 'CPN', 36501, '687085', 'HCM', 37000),
(1017, 'HNI', 'CPN', 37001, '695902', 'HCM', 37500),
(1018, 'HNI', 'CPN', 37501, '704719', 'HCM', 38000),
(1019, 'HNI', 'CPN', 38001, '713536', 'HCM', 38500),
(1020, 'HNI', 'CPN', 38501, '722353', 'HCM', 39000),
(1021, 'HNI', 'CPN', 39001, '731170', 'HCM', 39500),
(1022, 'HNI', 'CPN', 39501, '739987', 'HCM', 40000),
(1023, 'HNI', 'CPN', 40001, '748804', 'HCM', 40500),
(1024, 'HNI', 'CPN', 40501, '757621', 'HCM', 41000),
(1025, 'HNI', 'CPN', 41001, '766438', 'HCM', 41500),
(1026, 'HNI', 'CPN', 41501, '775255', 'HCM', 42000),
(1027, 'HNI', 'CPN', 42001, '784072', 'HCM', 42500),
(1028, 'HNI', 'CPN', 42501, '792889', 'HCM', 43000),
(1029, 'HNI', 'CPN', 43001, '801706', 'HCM', 43500),
(1030, 'HNI', 'CPN', 43501, '810523', 'HCM', 44000),
(1031, 'HNI', 'CPN', 44001, '819341', 'HCM', 44500),
(1032, 'HNI', 'CPN', 44501, '828158', 'HCM', 45000),
(1033, 'HNI', 'CPN', 45001, '836975', 'HCM', 45500),
(1034, 'HNI', 'CPN', 45501, '845792', 'HCM', 46000),
(1035, 'HNI', 'CPN', 46001, '854609', 'HCM', 46500),
(1036, 'HNI', 'CPN', 46501, '863426', 'HCM', 47000),
(1037, 'HNI', 'CPN', 47001, '872243', 'HCM', 47500),
(1038, 'HNI', 'CPN', 47501, '881060', 'HCM', 48000),
(1039, 'HNI', 'CPN', 48001, '889877', 'HCM', 48500),
(1040, 'HNI', 'CPN', 48501, '898694', 'HCM', 49000),
(1041, 'HNI', 'CPN', 49001, '907511', 'HCM', 49500),
(1042, 'HNI', 'CPN', 49501, '916328', 'HCM', 50000),
(1043, 'HNI', 'CPN', 50001, '925145', 'HCM', 50500),
(1044, 'HNI', 'CPN', 50501, '933962', 'HCM', 51000),
(1045, 'HNI', 'CPN', 51001, '942779', 'HCM', 51500),
(1046, 'HNI', 'CPN', 51501, '951596', 'HCM', 52000),
(1047, 'HNI', 'CPN', 52001, '960413', 'HCM', 52500),
(1048, 'HNI', 'CPN', 52501, '969230', 'HCM', 53000),
(1049, 'HNI', 'CPN', 53001, '978047', 'HCM', 53500),
(1050, 'HNI', 'CPN', 53501, '986864', 'HCM', 54000),
(1051, 'HNI', 'CPN', 54001, '995682', 'HCM', 54500),
(1052, 'HNI', 'CPN', 54501, '1004499', 'HCM', 55000),
(1053, 'HNI', 'CPN', 55001, '1013316', 'HCM', 55500),
(1054, 'HNI', 'CPN', 55501, '1022133', 'HCM', 56000),
(1055, 'HNI', 'CPN', 56001, '1030950', 'HCM', 56500),
(1056, 'HNI', 'CPN', 56501, '1039767', 'HCM', 57000),
(1057, 'HNI', 'CPN', 57001, '1048584', 'HCM', 57500),
(1058, 'HNI', 'CPN', 57501, '1057401', 'HCM', 58000),
(1059, 'HNI', 'CPN', 58001, '1066218', 'HCM', 58500),
(1060, 'HNI', 'CPN', 58501, '1075035', 'HCM', 59000),
(1061, 'HNI', 'CPN', 59001, '1083852', 'HCM', 59500),
(1062, 'HNI', 'CPN', 59501, '1092669', 'HCM', 60000),
(1063, 'HNI', 'CPN', 60001, '1101486', 'HCM', 60500),
(1064, 'HNI', 'CPN', 60501, '1110303', 'HCM', 61000),
(1065, 'HNI', 'CPN', 61001, '1119120', 'HCM', 61500),
(1066, 'HNI', 'CPN', 61501, '1127937', 'HCM', 62000),
(1067, 'HNI', 'CPN', 62001, '1136754', 'HCM', 62500),
(1068, 'HNI', 'CPN', 62501, '1145571', 'HCM', 63000),
(1069, 'HNI', 'CPN', 63001, '1154388', 'HCM', 63500),
(1070, 'HNI', 'CPN', 63501, '1163205', 'HCM', 64000),
(1071, 'HNI', 'CPN', 64001, '1172023', 'HCM', 64500),
(1072, 'HNI', 'CPN', 64501, '1180840', 'HCM', 65000),
(1073, 'HNI', 'CPN', 65001, '1189657', 'HCM', 65500),
(1074, 'HNI', 'CPN', 65501, '1198474', 'HCM', 66000),
(1075, 'HNI', 'CPN', 66001, '1207291', 'HCM', 66500),
(1076, 'HNI', 'CPN', 66501, '1216108', 'HCM', 67000),
(1077, 'HNI', 'CPN', 67001, '1224925', 'HCM', 67500),
(1078, 'HNI', 'CPN', 67501, '1233742', 'HCM', 68000),
(1079, 'HNI', 'CPN', 68001, '1242559', 'HCM', 68500),
(1080, 'HNI', 'CPN', 68501, '1251376', 'HCM', 69000),
(1081, 'HNI', 'CPN', 69001, '1260193', 'HCM', 69500),
(1082, 'HNI', 'CPN', 69501, '1269010', 'HCM', 70000),
(1083, 'HNI', 'CPN', 70001, '1277827', 'HCM', 70500),
(1084, 'HNI', 'CPN', 70501, '1286644', 'HCM', 71000),
(1085, 'HNI', 'CPN', 71001, '1295461', 'HCM', 71500),
(1086, 'HNI', 'CPN', 71501, '1304278', 'HCM', 72000),
(1087, 'HNI', 'CPN', 72001, '1313095', 'HCM', 72500),
(1088, 'HNI', 'CPN', 72501, '1321912', 'HCM', 73000),
(1089, 'HNI', 'CPN', 73001, '1330729', 'HCM', 73500),
(1090, 'HNI', 'CPN', 73501, '1339546', 'HCM', 74000),
(1091, 'HNI', 'CPN', 74001, '1348364', 'HCM', 74500),
(1092, 'HNI', 'CPN', 74501, '1357181', 'HCM', 75000),
(1093, 'HNI', 'CPN', 75001, '1365998', 'HCM', 75500),
(1094, 'HNI', 'CPN', 75501, '1374815', 'HCM', 76000),
(1095, 'HNI', 'CPN', 76001, '1383632', 'HCM', 76500),
(1096, 'HNI', 'CPN', 76501, '1392449', 'HCM', 77000),
(1097, 'HNI', 'CPN', 77001, '1401266', 'HCM', 77500),
(1098, 'HNI', 'CPN', 77501, '1410083', 'HCM', 78000),
(1099, 'HNI', 'CPN', 78001, '1418900', 'HCM', 78500),
(1100, 'HNI', 'CPN', 78501, '1427717', 'HCM', 79000),
(1101, 'HNI', 'CPN', 79001, '1436534', 'HCM', 79500),
(1102, 'HNI', 'CPN', 79501, '1445351', 'HCM', 80000),
(1103, 'HNI', 'CPN', 80001, '1454168', 'HCM', 80500),
(1104, 'HNI', 'CPN', 80501, '1462985', 'HCM', 81000),
(1105, 'HNI', 'CPN', 81001, '1471802', 'HCM', 81500),
(1106, 'HNI', 'CPN', 81501, '0', 'HCM', 82000),
(1608, 'HNI', 'CPN', 0, '9', 'HCM', 50),
(1609, 'HNI', 'CPN', 50, '13', 'HCM', 100),
(1610, 'HNI', 'CPN', 101, '23', 'HCM', 250),
(1611, 'HNI', 'CPN', 251, '34', 'HCM', 500),
(1612, 'HNI', 'CPN', 501, '46', 'HCM', 1),
(1613, 'HNI', 'CPN', 1, '58', 'HCM', 1),
(1614, 'HNI', 'CPN', 1, '69', 'HCM', 2),
(1615, 'HNI', 'CPN', 2, '78', 'HCM', 2),
(1616, 'HNI', 'CPN', 2, '87', 'HCM', 3),
(1617, 'HNI', 'CPN', 3, '96', 'HCM', 3),
(1618, 'HNI', 'CPN', 3, '105', 'HCM', 4),
(1619, 'HNI', 'CPN', 4, '113', 'HCM', 4),
(1620, 'HNI', 'CPN', 4, '122', 'HCM', 5),
(1621, 'HNI', 'CPN', 5, '131', 'HCM', 5),
(1622, 'HNI', 'CPN', 5, '140', 'HCM', 6),
(1623, 'HNI', 'CPN', 6, '149', 'HCM', 6),
(1624, 'HNI', 'CPN', 6, '158', 'HCM', 7),
(1625, 'HNI', 'CPN', 7, '166', 'HCM', 7),
(1626, 'HNI', 'CPN', 7, '175', 'HCM', 8),
(1627, 'HNI', 'CPN', 8, '184', 'HCM', 8),
(1628, 'HNI', 'CPN', 8, '193', 'HCM', 9),
(1629, 'HNI', 'CPN', 9, '202', 'HCM', 9),
(1630, 'HNI', 'CPN', 9, '210', 'HCM', 10),
(1631, 'HNI', 'CPN', 10, '219', 'HCM', 10),
(1632, 'HNI', 'CPN', 10, '228', 'HCM', 11),
(1633, 'HNI', 'CPN', 11, '237', 'HCM', 11),
(1634, 'HNI', 'CPN', 11, '246', 'HCM', 12),
(1635, 'HNI', 'CPN', 12, '255', 'HCM', 12),
(1636, 'HNI', 'CPN', 12, '263', 'HCM', 13),
(1637, 'HNI', 'CPN', 13, '272', 'HCM', 13),
(1638, 'HNI', 'CPN', 13, '281', 'HCM', 14),
(1639, 'HNI', 'CPN', 14, '290', 'HCM', 14),
(1640, 'HNI', 'CPN', 14, '299', 'HCM', 15),
(1641, 'HNI', 'CPN', 15, '307', 'HCM', 15),
(1642, 'HNI', 'CPN', 15, '316', 'HCM', 16),
(1643, 'HNI', 'CPN', 16, '325', 'HCM', 16),
(1644, 'HNI', 'CPN', 16, '334', 'HCM', 17),
(1645, 'HNI', 'CPN', 17, '343', 'HCM', 17),
(1646, 'HNI', 'CPN', 17, '352', 'HCM', 18),
(1647, 'HNI', 'CPN', 18, '360', 'HCM', 18),
(1648, 'HNI', 'CPN', 18, '369', 'HCM', 19),
(1649, 'HNI', 'CPN', 19, '378', 'HCM', 19),
(1650, 'HNI', 'CPN', 19, '387', 'HCM', 20),
(1651, 'HNI', 'CPN', 20, '396', 'HCM', 20),
(1652, 'HNI', 'CPN', 20, '404', 'HCM', 21),
(1653, 'HNI', 'CPN', 21, '413', 'HCM', 21),
(1654, 'HNI', 'CPN', 21, '422', 'HCM', 22),
(1655, 'HNI', 'CPN', 22, '431', 'HCM', 22),
(1656, 'HNI', 'CPN', 22, '440', 'HCM', 23),
(1657, 'HNI', 'CPN', 23, '449', 'HCM', 23),
(1658, 'HNI', 'CPN', 23, '457', 'HCM', 24),
(1659, 'HNI', 'CPN', 24, '466', 'HCM', 24),
(1660, 'HNI', 'CPN', 24, '475', 'HCM', 25),
(1661, 'HNI', 'CPN', 25, '484', 'HCM', 25),
(1662, 'HNI', 'CPN', 25, '493', 'HCM', 26),
(1663, 'HNI', 'CPN', 26, '501', 'HCM', 26),
(1664, 'HNI', 'CPN', 26, '510', 'HCM', 27),
(1665, 'HNI', 'CPN', 27, '519', 'HCM', 27),
(1666, 'HNI', 'CPN', 27, '528', 'HCM', 28),
(1667, 'HNI', 'CPN', 28, '537', 'HCM', 28),
(1668, 'HNI', 'CPN', 28, '546', 'HCM', 29),
(1669, 'HNI', 'CPN', 29, '554', 'HCM', 29),
(1670, 'HNI', 'CPN', 29, '563', 'HCM', 30),
(1671, 'HNI', 'CPN', 30, '572', 'HCM', 30),
(1672, 'HNI', 'CPN', 30, '581', 'HCM', 31),
(1673, 'HNI', 'CPN', 31, '590', 'HCM', 31),
(1674, 'HNI', 'CPN', 31, '598', 'HCM', 32),
(1675, 'HNI', 'CPN', 32, '607', 'HCM', 32),
(1676, 'HNI', 'CPN', 32, '616', 'HCM', 33),
(1677, 'HNI', 'CPN', 33, '625', 'HCM', 33),
(1678, 'HNI', 'CPN', 33, '634', 'HCM', 34),
(1679, 'HNI', 'CPN', 34, '643', 'HCM', 34),
(1680, 'HNI', 'CPN', 34, '651', 'HCM', 35),
(1681, 'HNI', 'CPN', 35, '660', 'HCM', 35),
(1682, 'HNI', 'CPN', 35, '669', 'HCM', 36),
(1683, 'HNI', 'CPN', 36, '678', 'HCM', 36),
(1684, 'HNI', 'CPN', 36, '687', 'HCM', 37),
(1685, 'HNI', 'CPN', 37, '695', 'HCM', 37),
(1686, 'HNI', 'CPN', 37, '704', 'HCM', 38),
(1687, 'HNI', 'CPN', 38, '713', 'HCM', 38),
(1688, 'HNI', 'CPN', 38, '722', 'HCM', 39),
(1689, 'HNI', 'CPN', 39, '731', 'HCM', 39),
(1690, 'HNI', 'CPN', 39, '739', 'HCM', 40),
(1691, 'HNI', 'CPN', 40, '748', 'HCM', 40),
(1692, 'HNI', 'CPN', 40, '757', 'HCM', 41),
(1693, 'HNI', 'CPN', 41, '766', 'HCM', 41),
(1694, 'HNI', 'CPN', 41, '775', 'HCM', 42),
(1695, 'HNI', 'CPN', 42, '784', 'HCM', 42),
(1696, 'HNI', 'CPN', 42, '792', 'HCM', 43),
(1697, 'HNI', 'CPN', 43, '801', 'HCM', 43),
(1698, 'HNI', 'CPN', 43, '810', 'HCM', 44),
(1699, 'HNI', 'CPN', 44, '819', 'HCM', 44),
(1700, 'HNI', 'CPN', 44, '828', 'HCM', 45),
(1701, 'HNI', 'CPN', 45, '836', 'HCM', 45),
(1702, 'HNI', 'CPN', 45, '845', 'HCM', 46),
(1703, 'HNI', 'CPN', 46, '854', 'HCM', 46),
(1704, 'HNI', 'CPN', 46, '863', 'HCM', 47),
(1705, 'HNI', 'CPN', 47, '872', 'HCM', 47),
(1706, 'HNI', 'CPN', 47, '881', 'HCM', 48),
(1707, 'HNI', 'CPN', 48, '889', 'HCM', 48),
(1708, 'HNI', 'CPN', 48, '898', 'HCM', 49),
(1709, 'HNI', 'CPN', 49, '907', 'HCM', 49),
(1710, 'HNI', 'CPN', 49, '916', 'HCM', 50),
(1711, 'HNI', 'CPN', 50, '925', 'HCM', 50),
(1712, 'HNI', 'CPN', 50, '933', 'HCM', 51),
(1713, 'HNI', 'CPN', 51, '942', 'HCM', 51),
(1714, 'HNI', 'CPN', 51, '951', 'HCM', 52),
(1715, 'HNI', 'CPN', 52, '960', 'HCM', 52),
(1716, 'HNI', 'CPN', 52, '969', 'HCM', 53),
(1717, 'HNI', 'CPN', 53, '978', 'HCM', 53),
(1718, 'HNI', 'CPN', 53, '986', 'HCM', 54),
(1719, 'HNI', 'CPN', 54, '995', 'HCM', 54),
(1720, 'HNI', 'CPN', 54, '1', 'HCM', 55),
(1721, 'HNI', 'CPN', 55, '1', 'HCM', 55),
(1722, 'HNI', 'CPN', 55, '1', 'HCM', 56),
(1723, 'HNI', 'CPN', 56, '1', 'HCM', 56),
(1724, 'HNI', 'CPN', 56, '1', 'HCM', 57),
(1725, 'HNI', 'CPN', 57, '1', 'HCM', 57),
(1726, 'HNI', 'CPN', 57, '1', 'HCM', 58),
(1727, 'HNI', 'CPN', 58, '1', 'HCM', 58),
(1728, 'HNI', 'CPN', 58, '1', 'HCM', 59),
(1729, 'HNI', 'CPN', 59, '1', 'HCM', 59),
(1730, 'HNI', 'CPN', 59, '1', 'HCM', 60),
(1731, 'HNI', 'CPN', 60, '1', 'HCM', 60),
(1732, 'HNI', 'CPN', 60, '1', 'HCM', 61),
(1733, 'HNI', 'CPN', 61, '1', 'HCM', 61),
(1734, 'HNI', 'CPN', 61, '1', 'HCM', 62),
(1735, 'HNI', 'CPN', 62, '1', 'HCM', 62),
(1736, 'HNI', 'CPN', 62, '1', 'HCM', 63),
(1737, 'HNI', 'CPN', 63, '1', 'HCM', 63),
(1738, 'HNI', 'CPN', 63, '1', 'HCM', 64),
(1739, 'HNI', 'CPN', 64, '1', 'HCM', 64),
(1740, 'HNI', 'CPN', 64, '1', 'HCM', 65),
(1741, 'HNI', 'CPN', 65, '1', 'HCM', 65),
(1742, 'HNI', 'CPN', 65, '1', 'HCM', 66),
(1743, 'HNI', 'CPN', 66, '1', 'HCM', 66),
(1744, 'HNI', 'CPN', 66, '1', 'HCM', 67),
(1745, 'HNI', 'CPN', 67, '1', 'HCM', 67),
(1746, 'HNI', 'CPN', 67, '1', 'HCM', 68),
(1747, 'HNI', 'CPN', 68, '1', 'HCM', 68),
(1748, 'HNI', 'CPN', 68, '1', 'HCM', 69),
(1749, 'HNI', 'CPN', 69, '1', 'HCM', 69),
(1750, 'HNI', 'CPN', 69, '1', 'HCM', 70),
(1751, 'HNI', 'CPN', 70, '1', 'HCM', 70),
(1752, 'HNI', 'CPN', 70, '1', 'HCM', 71),
(1753, 'HNI', 'CPN', 71, '1', 'HCM', 71),
(1754, 'HNI', 'CPN', 71, '1', 'HCM', 72),
(1755, 'HNI', 'CPN', 72, '1', 'HCM', 72),
(1756, 'HNI', 'CPN', 72, '1', 'HCM', 73),
(1757, 'HNI', 'CPN', 73, '1', 'HCM', 73),
(1758, 'HNI', 'CPN', 73, '1', 'HCM', 74),
(1759, 'HNI', 'CPN', 74, '1', 'HCM', 74),
(1760, 'HNI', 'CPN', 74, '1', 'HCM', 75),
(1761, 'HNI', 'CPN', 75, '1', 'HCM', 75),
(1762, 'HNI', 'CPN', 75, '1', 'HCM', 76),
(1763, 'HNI', 'CPN', 76, '1', 'HCM', 76),
(1764, 'HNI', 'CPN', 76, '1', 'HCM', 77),
(1765, 'HNI', 'CPN', 77, '1', 'HCM', 77),
(1766, 'HNI', 'CPN', 77, '1', 'HCM', 78),
(1767, 'HNI', 'CPN', 78, '1', 'HCM', 78),
(1768, 'HNI', 'CPN', 78, '1', 'HCM', 79),
(1769, 'HNI', 'CPN', 79, '1', 'HCM', 79),
(1770, 'HNI', 'CPN', 79, '1', 'HCM', 80),
(1771, 'HNI', 'CPN', 80, '1', 'HCM', 80),
(1772, 'HNI', 'CPN', 80, '1', 'HCM', 81),
(1773, 'HNI', 'CPN', 81, '1', 'HCM', 81),
(1774, 'HNI', 'CPN', 81, '0', 'HCM', 82);

-- --------------------------------------------------------

--
-- Table structure for table `process_tq`
--

CREATE TABLE `process_tq` (
  `id` int(11) NOT NULL,
  `background` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `process_tq`
--

INSERT INTO `process_tq` (`id`, `background`, `title`) VALUES
(0, 'assets/clients/imgs/tiendoduan/bg_tiendoduan.jpg', 'Tiến độ dự án');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `ProID` int(11) NOT NULL,
  `ProName` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `ProDescribes` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `ProMeta` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Image` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `DateCreated` datetime DEFAULT NULL,
  `CatId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Author` int(11) DEFAULT NULL,
  `ProLang` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ViewCount` int(11) DEFAULT 0,
  `Content` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` tinyint(4) DEFAULT 1,
  `TempId` int(11) DEFAULT NULL,
  `Price` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `Discount` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `Manufacture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Estimate` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `ProductCode` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ImageList` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Video` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SeoTitle` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SeoDescribes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SeoCanonica` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MetaRobot` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SeoKeyword` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `resourcecate`
--

CREATE TABLE `resourcecate` (
  `ID` int(11) NOT NULL,
  `Name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Language` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `resources`
--

CREATE TABLE `resources` (
  `ResID` int(11) NOT NULL,
  `ResName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ResCate` int(11) DEFAULT NULL,
  `ResLang` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CateLink` int(11) DEFAULT NULL,
  `ProLink` int(11) DEFAULT NULL,
  `Image` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Frame` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `File` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Describes` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `resourcetype`
--

CREATE TABLE `resourcetype` (
  `TypeID` int(11) NOT NULL,
  `TypeName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` tinyint(4) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `resourcetype`
--

INSERT INTO `resourcetype` (`TypeID`, `TypeName`, `Status`) VALUES
(1, 'Ảnh', 1),
(2, 'Video', 1),
(3, 'Tập tin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `script`
--

CREATE TABLE `script` (
  `id` int(11) NOT NULL,
  `script` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `script`
--

INSERT INTO `script` (`id`, `script`) VALUES
(1, '');

-- --------------------------------------------------------

--
-- Table structure for table `section_tinhhoa`
--

CREATE TABLE `section_tinhhoa` (
  `id` int(11) NOT NULL,
  `background` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title_section` longtext COLLATE utf8_unicode_ci NOT NULL,
  `image` longtext COLLATE utf8_unicode_ci NOT NULL,
  `count_bt` int(11) NOT NULL,
  `count_np` int(11) NOT NULL,
  `count_nptm` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `section_tinhhoa`
--

INSERT INTO `section_tinhhoa` (`id`, `background`, `title`, `content`, `title_section`, `image`, `count_bt`, `count_np`, `count_nptm`) VALUES
(1, 'assets/clients/imgs/tinhhoa/bg_tinhhoa.jpg', 'assets/clients/imgs/tinhhoa/texttinhhoa.png', '<p>Lấy cảm hứng kiến tr&uacute;c từ những bờ biển trải d&agrave;i v&ocirc; tận với l&agrave;n s&oacute;ng xanh dịu d&agrave;ng, b&atilde;i c&aacute;t trắng muốt v&agrave; nắng v&agrave;ng &oacute;ng ả, Mũi N&eacute; Summerland kế thừa tinh hoa từ hương vị biển cả v&agrave; con người miền đất Phan Thiết. Với gam m&agrave;u rực rỡ v&ugrave;ng nhiệt đới kết hợp kiến tr&uacute;c hiện đại nhưng kh&ocirc;ng k&eacute;m phần tinh tế v&agrave; sang trọng. Sự kết hợp độc đ&aacute;o tạo n&ecirc;n sắc m&agrave;u ri&ecirc;ng biệt của một thi&ecirc;n đường miền nhiệt đới mang t&ecirc;n Mũi N&eacute; Summerland. L&agrave; nơi để bạn thỏa sức h&ograve;a m&igrave;nh trong thi&ecirc;n nhi&ecirc;n c&ugrave;ng những thiết kế độc bản đầy tinh xảo. Mũi N&eacute; Summerland, nơi d&agrave;nh tặng cho bạn những dịch vụ ri&ecirc;ng tư xa xỉ, t&ocirc;n vinh gi&aacute; trị duy nhất.</p>\n', 'Biệt thự / Nhà phố', 'assets/clients/imgs/tinhhoa/img_tinhhoa.jpg', 60, 332, 251);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `ID` int(11) NOT NULL,
  `Name` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Describes` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Image` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Language` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Url` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ServiceId` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`ID`, `Name`, `Describes`, `Image`, `Language`, `Url`, `ServiceId`) VALUES
(1, 'Phát Siêu tốc trong ngày', 'Phát Siêu tốc là dịch vụ đặc biệt dành cho khách hàng có nhu cầu nhận hàng hóa, tài liệu trong ngày hoặc theo thời gian thỏa thuận.', 'assets/includes/upload/images/Capture4.PNG', 'vi', '#', 'PST'),
(2, 'Phát Hỏa tốc', 'Phát Hỏa tốc là dịch vụ vận chuyển hàng hóa, tài liệu đến tay người nhận trong thời gian nhanh nhất, luôn được kết nối ưu tiên và phát ngay cho người nhận.', 'assets/includes/upload/images/project/basket_buy-512.png', 'vi', '#', 'PHT'),
(5, 'Chuyển phát nhanh', 'Dịch vụ Chuyển phát nhanh của Thế Hệ Mới Express giúp vận chuyển hàng hóa, tài liệu của khách hàng đến tay người nhận một cách nhanh chóng.', 'assets/includes/upload/images/project/beauty-spa-78-512.png', 'vi', '#', 'CPN'),
(9, 'Chuyển phát thường', 'Dịch vụ chuyển phát thường là dịch vụ chuyển phát tiết kiệm bằng đường bộ, vận chuyển hàng hóa, tài liệu của khách hàng tới toàn bộ 63 tỉnh/thành phố trên Toàn quốc.', 'assets/includes/upload/images/project/laptop.png', 'vi', '#', 'CPT'),
(10, 'aaaaa', 'aaaaaaa', '', 'vi', 'aaaa', 'aaaa'),
(11, 'aaaaa', 'aaaaaaa', 'assets/includes/upload/files/thumuc.PNG', 'vi', 'aaaa', 'aaaa'),
(12, 'aaaaa', 'aaaaaaa', 'assets/includes/upload/files/thumuc.PNG', 'vi', 'aaaa', 'aaaa'),
(13, 'aaaaa', 'aaaaaaa', 'assets/includes/upload/files/thumuc.PNG', 'vi', 'aaaa', 'aaaa'),
(14, 'aaaa', 'aaaaa', '', 'vi', 'aaaa', 'aaaa');

-- --------------------------------------------------------

--
-- Table structure for table `shipment`
--

CREATE TABLE `shipment` (
  `shipmentId` int(11) NOT NULL,
  `shipmentCustomId` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fromPlace` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `toPlace` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `typeShip` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight` float NOT NULL DEFAULT 0,
  `isDeleted` bit(1) NOT NULL DEFAULT b'0',
  `fromPlaceDetail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `toPlaceDetail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `shipment`
--

INSERT INTO `shipment` (`shipmentId`, `shipmentCustomId`, `fromPlace`, `toPlace`, `note`, `typeShip`, `weight`, `isDeleted`, `fromPlaceDetail`, `toPlaceDetail`) VALUES
(1, NULL, NULL, NULL, NULL, 'Chuyển nhanh', 0, b'1', NULL, NULL),
(2, 'dfgdfg', 'HCPN', 'HCHM', 'asd', 'Chuyển nhanh', 12, b'1', 'Hồ Chí Minh, QUẬN PHÚ NHUẬN', 'Hồ Chí Minh, HUYỆN HÓC MÔN'),
(3, 'sfgdfg', 'hcm', 'HCM', NULL, 'Chuyển nhanh', 100, b'1', 'TP HCM', 'TP HCM'),
(4, 'CT0209_001', 'HCTD', 'HCTD', NULL, 'Chuyển nhanh', 10000, b'1', NULL, NULL),
(5, 'CHUYENTHU01', 'HCM', 'HNI', 'QUÀ TẶNG', 'Chuyển nhanh', 1000, b'0', 'TP HCM', 'Hà Nội');

-- --------------------------------------------------------

--
-- Table structure for table `shipmentdetail`
--

CREATE TABLE `shipmentdetail` (
  `shipDetailId` int(11) NOT NULL,
  `shipmentId` int(11) DEFAULT NULL,
  `packageId` int(11) DEFAULT NULL,
  `isReceived` bit(1) NOT NULL DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `shipmentdetail`
--

INSERT INTO `shipmentdetail` (`shipDetailId`, `shipmentId`, `packageId`, `isReceived`) VALUES
(1, 2, 3, b'0'),
(2, NULL, NULL, b'0'),
(3, NULL, NULL, b'0'),
(4, NULL, NULL, b'0'),
(5, NULL, NULL, b'0'),
(6, NULL, NULL, b'0'),
(7, NULL, NULL, b'0'),
(8, NULL, NULL, b'0'),
(9, NULL, NULL, b'0'),
(10, NULL, NULL, b'0'),
(11, NULL, NULL, b'0'),
(12, NULL, NULL, b'0'),
(13, NULL, NULL, b'0'),
(14, NULL, NULL, b'0'),
(15, NULL, NULL, b'0'),
(16, NULL, NULL, b'0'),
(17, NULL, NULL, b'0'),
(18, 2, 1, b'0'),
(19, 3, 4, b'1'),
(20, 5, 21, b'0'),
(21, 5, 22, b'0');

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE `slides` (
  `ID` int(11) NOT NULL,
  `Image` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Describes` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `ProLink` int(11) DEFAULT NULL,
  `CateLink` int(11) DEFAULT NULL,
  `Language` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CateId` int(11) DEFAULT NULL,
  `code` char(20) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`ID`, `Image`, `Describes`, `ProLink`, `CateLink`, `Language`, `Title`, `CateId`, `code`, `sort`) VALUES
(16, 'assets/client/pictures/catalog/facilities/1BR_1.jpg', '', 0, 0, 'vi', 'PHỐI CẢNH TIỆN ÍCH 1', 34, '', 0),
(17, 'assets/client/pictures/catalog/facilities/1BR_1.jpg', '', 0, 0, 'vi', 'PHỐI CẢNH TIỆN ÍCH 2', 34, '', 0),
(18, 'assets/includes/upload/files/0363bf7bde182e467709.jpg', '', 0, 0, 'vi', 'PHỐI CẢNH TỔNG THỂ', 33, '', 0),
(19, 'assets/includes/upload/files/22044418257bd5258c6a.jpg', '', 0, 0, 'vi', 'PHỐI CẢNH TỔNG THỂ', 33, '', 0),
(20, 'assets/includes/upload/files/30a80bcc56a2a6fcffb3.jpg', '', 0, 0, 'vi', 'Hình phối cảnh nội thất', 32, '', 0),
(21, 'assets/includes/upload/files/f16a0403596da933f07c.jpg', '', 0, 0, 'vi', 'Hình phối cảnh nội thất', 32, '', 0),
(22, 'assets/client/pictures/catalog/contact/coteccons_logo.svg', '', 0, 0, 'vi', 'Phân phối 1', 31, '', 0),
(23, 'assets/client/pictures/catalog/contact/coteccons_logo.svg', '', 0, 0, 'vi', 'Phân phối 2', 31, '', 0),
(24, 'assets/client/pictures/catalog/contact/coteccons_logo.svg', '', 0, 0, 'vi', 'Phân phối 3', 31, '', 0),
(25, 'assets/client/pictures/catalog/contact/coteccons_logo.svg', '', 0, 0, 'vi', 'Phân phối 4', 31, '', 0),
(26, 'assets/client/pictures/catalog/contact/coteccons_logo.svg', '', 0, 0, 'vi', 'Phân phối 5', 31, '', 0),
(27, 'assets/client/pictures/catalog/contact/coteccons_logo.svg', '', 0, 0, 'vi', 'Phân phối 6', 31, '', 0),
(28, 'assets/client/pictures/catalog/contact/coteccons_logo.svg', '', 0, 0, 'vi', 'ĐƠN VỊ TỔNG THẦU', 30, '', 0),
(29, 'assets/client/pictures/catalog/logo/dau-tu-phat-trien.svg', '', 0, 0, 'vi', 'Đơn vị phát triên', 29, '', 0),
(30, 'assets/includes/upload/files/Chu%20A%20.jpg', '', 0, 0, 'vi', 'Silde 2', 26, '', 0),
(31, 'assets/includes/upload/files/ATA%20HO%20BOI%201.png', '', 0, 0, 'vi', 'Slide 5-1', 27, '', 0),
(32, 'assets/includes/upload/files/hb3.png', '', 0, 0, 'vi', 'Slide 5-2', 27, '', 0),
(33, 'assets/includes/upload/files/1ae3148a49e4b9bae0f5.jpg', '', 0, 0, 'vi', 'Slide 7-1', 28, '', 0),
(34, 'assets/includes/upload/files/f16a0403596da933f07c.jpg', '', 0, 0, 'vi', 'Slide 7-2', 28, '', 0),
(36, 'assets/includes/upload/files/5e0b550d346ec4309d7f.jpg', '', 0, 0, 'vi', 'Phối cảnh tổng thể', 33, '', 0),
(37, 'assets/includes/upload/files/ceb67eac1fcfef91b6de.jpg', '', 0, 0, 'vi', 'Phối cảnh tổng thể', 33, '', 0),
(38, 'assets/includes/upload/files/f6d699b3c4dd34836dcc.jpg', '', 0, 0, 'vi', 'Hình phối cảnh nội thất', 32, '', 0),
(39, 'assets/includes/upload/files/fab90ad357bda7e3feac.jpg', '', 0, 0, 'vi', 'Hình phối cảnh nội thất', 32, '', 0),
(40, 'assets/includes/upload/files/1BR_1.jpg', '', 0, 0, 'vi', 'Căn 1PN', 36, '', 0),
(41, 'assets/includes/upload/files/1BR_2.jpg', '', 0, 0, 'vi', 'Căn 1PN', 36, '', 0),
(42, 'assets/includes/upload/files/1BR_4.jpg', '', 0, 0, 'vi', 'Căn 1PN', 36, '', 0),
(43, 'assets/includes/upload/files/1BR_BEDROOM.jpg', '', 0, 0, 'vi', 'Căn 1PN', 36, '', 0),
(44, 'assets/includes/upload/files/1BR_WC.jpg', '', 0, 0, 'vi', 'Căn 1PN', 36, '', 0),
(45, 'assets/includes/upload/files/2BR_BEDROOM.jpg', '', 0, 0, 'vi', 'Căn 2PN', 35, '', 0),
(46, 'assets/includes/upload/files/2BR_KIDBEDROOM.jpg', '', 0, 0, 'vi', 'Căn 2PN', 35, '', 0),
(47, 'assets/includes/upload/files/2BR_LIVING%20ROOM%201.jpg', '', 0, 0, 'vi', 'Căn 2PN', 35, '', 0),
(48, 'assets/includes/upload/files/2BR_LIVING%20ROOM%202.jpg', '', 0, 0, 'vi', 'Căn 2PN', 35, '', 0),
(49, 'assets/includes/upload/files/2BR_WC.jpg', '', 0, 0, 'vi', 'Căn 2PN', 35, '', 0),
(50, 'assets/includes/upload/files/fab90ad357bda7e3feac.jpg', '', 0, 0, 'vi', 'slide 7-3', 28, '', 0),
(51, 'assets/includes/upload/files/Sanh%2001%20.png', '', 0, 0, 'vi', 'Sảnh 1', 27, '', 0),
(52, 'assets/includes/upload/files/Slide-Website-2.jpg', '', 0, 0, 'vi', 'Home', 37, '', 0),
(53, 'assets/client/pictures/catalog/logo/text-slogan-01.svg', '', 0, 0, 'vi', 'Slogan', 38, '', 0),
(54, 'assets/includes/upload/files/1.jpg', '', 0, 0, 'vi', 'Hình nhỏ Slide 2', 39, '', 0),
(55, 'assets/includes/upload/files/3.jpg', '', 0, 0, 'vi', 'Hình nhỏ Slide 4', 40, '', 0),
(56, 'assets/includes/upload/files/4.jpg', '', 0, 0, 'vi', 'Hình nhỏ Slide 5', 41, '', 0),
(57, 'assets/includes/upload/files/5.jpg', '', 0, 0, 'vi', 'Hình nhỏ Slide 6', 42, '', 0),
(58, 'assets/includes/upload/files/6.jpg', '', 0, 0, 'vi', 'Hình nhỏ Slide 7', 43, '', 0),
(59, 'assets/includes/upload/files/1ae3148a49e4b9bae0f5.jpg', '', 0, 0, 'vi', 'Background YTB', 44, '', 0),
(60, 'assets/includes/upload/files/Sanh%2002%20%20.jpg', '', 0, 0, 'vi', 'sảnh 2', 27, '', 0),
(61, 'assets/includes/upload/files/Sanh%2003%20.jpg', '', 0, 0, 'vi', 'Sảnh 3', 27, '', 0),
(62, 'assets/includes/upload/files/Chu%20A%20.jpg', '', 0, 0, 'vi', 'Tượng đài Check-in', 27, '', 0),
(63, 'assets/includes/upload/files/BBQ-garden.jpg', '', 0, 0, 'vi', 'BBQ Garden', 27, '', 0),
(64, 'assets/includes/upload/files/GYM.jpg', '', 0, 0, 'vi', 'GYM', 27, '', 0),
(65, 'assets/includes/upload/files/shopping-mall.jpg', '', 0, 0, 'vi', 'Shopping', 27, '', 0),
(66, 'assets/includes/upload/files/Spa.jpg', '', 0, 0, 'vi', 'Spa', 27, '', 0),
(67, 'assets/includes/upload/files/Kids-zone.jpg', '', 0, 0, 'vi', 'Kids zone', 27, '', 0),
(68, 'assets/includes/upload/files/ATA_MD5.png', '', 0, 0, 'vi', 'Phối cảnh tổng thể', 45, '', 0),
(69, 'assets/includes/upload/files/4B.png', '', 0, 0, 'vi', 'Panorama Pool', 47, '', 0),
(70, 'assets/includes/upload/files/A_1%20-%20Photo_1%20-%20Photo.png', '', 0, 0, 'vi', 'Luxury lobby', 47, '', 0),
(71, 'assets/includes/upload/files/ATA%20HO%20BOI%201.png', '', 0, 0, 'vi', 'Panorama Pool', 47, '', 0),
(74, 'assets/includes/upload/files/z2289630274063_be36d6ac30018f490f40f23a5207426e.jpg', '', 0, 0, 'vi', 'Phối cảnh tổng thể', 45, '', 0),
(75, 'assets/includes/upload/files/ATA-_TANG-23e1b-post.jpg', '', 0, 0, 'vi', 'Vườn chân mây', 47, '', 0),
(76, 'assets/includes/upload/files/CHECKIN.jpg', '', 0, 0, 'vi', 'Central Fountain', 47, '', 0),
(77, 'assets/includes/upload/files/HB1_9%20-%20Photo.png', '', 0, 0, 'vi', 'Panorama Pool', 47, '', 0),
(79, 'assets/includes/upload/files/hb3(1).png', '', 0, 0, 'vi', 'KID ZONE', 47, '', 0),
(80, 'assets/includes/upload/files/A_1%20-%20Photo_6%20-%20Photo.jpg', '', 0, 0, 'vi', 'Luxury Lobby', 47, '', 0),
(81, 'assets/includes/upload/files/S1.jpg', '', 0, 0, 'vi', 'Luxury Lobby', 47, '', 0),
(82, 'assets/includes/upload/files/s5.jpg', '', 0, 0, 'vi', 'Lift Lobby', 47, '', 0),
(84, 'assets/clients/imgs/tienich/slide.png', '', 0, 0, 'vi', '', 50, '', 0),
(85, 'assets/clients/imgs/tienich/SummerGarden/DragonPark.png', '', 0, 0, 'vi', 'Dragon Park', 53, '', 0),
(86, 'assets/clients/imgs/tienich/SummerGarden/Ochardgarden.jpg', '', 0, 0, 'vi', 'Ochard Garden', 53, '', 1),
(87, 'assets/clients/imgs/tienich/SummerGarden/Wellnesspark.jpg', '', 0, 0, 'vi', 'Wellness Park', 53, '', 2),
(91, 'assets/includes/upload/files/SML-CLUBHOUSE.jpg', '', 0, 0, 'vi', 'Club House', 52, '', 1),
(92, 'assets/clients/imgs/tienich/SummerParadise/Gym.jpg', '', 0, 0, 'vi', 'Gym', 52, '', 2),
(93, 'assets/includes/upload/files/Panorama_Pool.png', '', 0, 0, 'vi', 'Panorama Pool', 52, '', 0),
(94, 'assets/clients/imgs/tienich/SummerParadise/Poolbar.jpg', '', 0, 0, 'vi', 'Pool Bar', 52, '', 3),
(95, 'assets/clients/imgs/tienich/SummerParadise/Spa.jpg', '', 0, 0, 'vi', 'Spa', 52, '', 4),
(99, 'assets/clients/imgs/tienich/SummerOceanPark/tienich-1.jpg', '', 0, 0, 'vi', 'Dragon Fruit Bridge', 49, '', 1),
(100, 'assets/clients/imgs/tienich/SummerOceanPark/tienich-2.jpg', '', 0, 0, 'vi', 'Dual Wave Pool', 49, '', 2),
(101, 'assets/clients/imgs/tienich/SummerOceanPark/tienich-3.jpg', '', 0, 0, 'vi', 'Dual Wave Pool', 49, '', 3),
(102, 'assets/clients/imgs/tienich/SummerOceanPark/tienich-4.jpg', '', 0, 0, 'vi', 'Extreme River', 49, '', 4),
(103, 'assets/clients/imgs/tienich/SummerOceanPark/tienich-5.jpg', '', 0, 0, 'vi', 'F&B', 49, '', 5),
(104, 'assets/includes/upload/files/FLOW_RIDE_THUAN_PHUC_MANH_LONG.jpg', '', 0, 0, 'vi', 'Flow Ride', 49, '', 6),
(105, 'assets/clients/imgs/tienich/SummerOceanPark/tienich-7.jpg', '', 0, 0, 'vi', 'Hawaiian Fall', 49, '', 7),
(106, 'assets/includes/upload/files/KID_SPLASH_VUONG_QUOC_TRE_THO.jpg', '', 0, 0, 'vi', 'Kid Splash', 49, '', 8),
(107, 'assets/clients/imgs/tienich/SummerOceanPark/tienich-9.jpg', '', 0, 0, 'vi', 'Photo Zone', 49, '', 9),
(108, 'assets/clients/imgs/tienich/SummerOceanPark/tienich-10.jpg', '', 0, 0, 'vi', 'Play Pool', 49, '', 10),
(109, 'assets/clients/imgs/tienich/SummerOceanPark/tienich-11.jpg', '', 0, 0, 'vi', 'Rental', 49, '', 11),
(110, 'assets/clients/imgs/tienich/slide.png', '', 0, 0, 'vi', 'a', 50, '', 0),
(111, 'assets/clients/imgs/tienich/slide.png', '', 0, 0, 'vi', 'a', 50, '', 0),
(112, 'assets/clients/imgs/tienich/slide.png', '', 0, 0, 'vi', '4', 50, '', 0),
(113, 'assets/clients/imgs/tienich/slide.png', '', 0, 0, 'vi', '5', 50, '', 0),
(114, 'assets/clients/imgs/tienich/slide.png', '', 0, 0, 'vi', '6', 50, '', 0),
(115, 'assets/clients/imgs/tienich/slide.png', '', 0, 0, 'vi', '7', 50, '', 0),
(116, 'assets/clients/imgs/tienich/slide.png', '', 0, 0, 'vi', '8', 50, '', 0),
(117, 'assets/includes/upload/files/Tien-do-du-an-Mui-Ne-Summerland-thang-2-nam-2021-10.jpg', '', 0, 0, 'vi', '1', 54, '', 0),
(118, 'assets/includes/upload/files/Tien-do-du-an-Mui-Ne-Summerland-thang-2-nam-2021-9.jpg', '', 0, 0, 'vi', '2', 54, '', 0),
(119, 'assets/includes/upload/files/Tien-do-du-an-Mui-Ne-Summerland-thang-2-nam-2021-8.jpg', '', 0, 0, 'vi', '4', 54, '', 0),
(120, 'assets/includes/upload/files/Tien-do-du-an-Mui-Ne-Summerland-thang-2-nam-2021-6.jpg', '', 0, 0, 'vi', '3', 54, '', 0),
(122, 'assets/includes/upload/files/Tien-do-du-an-Mui-Ne-Summerland-thang-2-nam-2021-5.jpg', '', 0, 0, 'vi', '5', 54, '', 0),
(123, 'assets/includes/upload/files/Tien-do-du-an-Mui-Ne-Summerland-thang-2-nam-2021-4.jpg', '', 0, 0, 'vi', '6', 54, '', 0),
(124, 'assets/includes/upload/files/Tien-do-du-an-Mui-Ne-Summerland-thang-2-nam-2021-2.jpg', '', 0, 0, 'vi', '7', 54, '', 0),
(125, 'assets/includes/upload/files/Tien-do-du-an-Mui-Ne-Summerland-thang-2-nam-2021-1.jpg', '', 0, 0, 'vi', '8', 54, '', 0),
(126, 'assets/includes/upload/files/tien-do-du-an-mui-ne-summerland-thang5-2021-hinh8.JPG', '', 0, 0, 'vi', '1', 55, '', 0),
(127, 'assets/includes/upload/files/tien-do-du-an-mui-ne-summerland-thang5-2021-hinh7.JPG', '', 0, 0, 'vi', '2', 55, '', 0),
(128, 'assets/includes/upload/files/tien-do-du-an-mui-ne-summerland-thang5-2021-hinh6.JPG', '', 0, 0, 'vi', '3', 55, '', 0),
(129, 'assets/includes/upload/files/tien-do-du-an-mui-ne-summerland-thang5-2021-hinh5.JPG', '', 0, 0, 'vi', '4', 55, '', 0),
(130, 'assets/includes/upload/files/tien-do-du-an-mui-ne-summerland-thang5-2021-hinh4.JPG', '', 0, 0, 'vi', '5', 55, '', 0),
(131, 'assets/includes/upload/files/tien-do-du-an-mui-ne-summerland-thang5-2021-hinh3.JPG', '', 0, 0, 'vi', '6', 55, '', 0),
(132, 'assets/includes/upload/files/tien-do-du-an-mui-ne-summerland-thang5-2021-hinh2.JPG', '', 0, 0, 'vi', '7', 55, '', 0),
(134, 'assets/includes/upload/files/tien-do-du-an-mui-ne-summerland-thang5-2021-hinh1.JPG', '', 0, 0, 'vi', '8', 55, '', 0),
(136, 'assets/includes/upload/files/Tien-do-du-an-Mui-Ne-Summerland-thang-2-nam-2021-11.jpg', '', 0, 0, 'vi', '9', 54, '', 0),
(137, 'assets/includes/upload/files/Tien-do-du-an-Mui-Ne-Summerland-thang-2-nam-2021-7.jpg', '', 0, 0, 'vi', '10', 54, '', 0),
(138, 'assets/includes/upload/files/tien-do-du-an-mui-ne-summerland-thang5-2021-hinh9.JPG', '', 0, 0, 'vi', '9', 55, '', 0),
(139, 'assets/includes/upload/files/tien-do-du-an-mui-ne-summerland-thang5-2021-hinh10.JPG', '', 0, 0, 'vi', '10', 55, '', 0),
(141, 'assets/includes/upload/files/01-%20Tornado%20Wave.jpg', '', 0, 0, 'vi', 'Tornado Wave', 49, '', 0),
(142, 'assets/includes/upload/files/TANG5-13-HO-BOI-RESORT.jpg', '', 0, 0, 'vi', 'Chill pool', 52, '', 5),
(143, 'assets/includes/upload/files/TANG5-18-HO-BOI-CAU-KINH.jpg', '', 0, 0, 'vi', 'Pool Bridge', 52, '', 7),
(144, 'assets/includes/upload/files/Doi%20ngu%20cong%20nhan.jpg', '', 0, 0, 'vi', 'Đội ngũ công nhân', 56, '', 0),
(145, 'assets/includes/upload/files/NP1.jpg', '', 0, 0, 'vi', 'NP1', 56, '', 0),
(146, 'assets/includes/upload/files/NP1-2.jpg', '', 0, 0, 'vi', 'NP1-2', 56, '', 0),
(147, 'assets/includes/upload/files/NP2-2.jpg', '', 0, 0, 'vi', 'NP2-2', 56, '', 0),
(148, 'assets/includes/upload/files/NP2-3.jpg', '', 0, 0, 'vi', 'NP2-3', 56, '', 0),
(149, 'assets/includes/upload/files/NP4-4.jpg', '', 0, 0, 'vi', 'NP4-4', 56, '', 0),
(150, 'assets/includes/upload/files/NP4-5.jpg', '', 0, 0, 'vi', 'NP4-5', 56, '', 0),
(151, 'assets/includes/upload/files/NP6-1.jpg', '', 0, 0, 'vi', 'NP6-1', 56, '', 0),
(152, 'assets/includes/upload/files/NP6-4.jpg', '', 0, 0, 'vi', 'NP6-4', 56, '', 0),
(153, 'assets/includes/upload/files/NP8.jpg', '', 0, 0, 'vi', 'NP8', 56, '', 0),
(154, 'assets/includes/upload/files/NP8-1.jpg', '', 0, 0, 'vi', 'NP8-1', 56, '', 0),
(155, 'assets/includes/upload/files/CV.jpg', '', 0, 0, 'vi', '', 58, '', 0),
(156, 'assets/includes/upload/files/NP1(1).jpg', '', 0, 0, 'vi', '', 58, '', 0),
(157, 'assets/includes/upload/files/NP2-2(1).jpg', '', 0, 0, 'vi', '', 58, '', 0),
(158, 'assets/includes/upload/files/NP2.jpg', '', 0, 0, 'vi', '', 58, '', 0),
(159, 'assets/includes/upload/files/NP3.jpg', '', 0, 0, 'vi', '', 58, '', 0),
(160, 'assets/includes/upload/files/NP4.jpg', '', 0, 0, 'vi', '', 58, '', 0),
(161, 'assets/includes/upload/files/NP4-2.jpg', '', 0, 0, 'vi', '', 58, '', 0),
(162, 'assets/includes/upload/files/NP5.jpg', '', 0, 0, 'vi', '', 58, '', 0),
(163, 'assets/includes/upload/files/NP6-1(1).jpg', '', 0, 0, 'vi', '', 58, '', 0),
(164, 'assets/includes/upload/files/NPTM1.jpg', '', 0, 0, 'vi', '', 58, '', 0),
(165, 'assets/includes/upload/files/NPTM2.jpg', '', 0, 0, 'vi', '', 58, '', 0),
(166, 'assets/includes/upload/files/NPTM7.jpg', '', 0, 0, 'vi', '', 58, '', 0),
(167, 'assets/includes/upload/files/NPTM8.jpg', '', 0, 0, 'vi', '', 58, '', 0),
(168, 'assets/includes/upload/files/NP1(2).jpg', '', 0, 0, 'vi', '', 59, '', 0),
(169, 'assets/includes/upload/files/NP2_1.jpg', '', 0, 0, 'vi', '', 59, '', 0),
(170, 'assets/includes/upload/files/NP2(1).jpg', '', 0, 0, 'vi', '', 59, '', 0),
(171, 'assets/includes/upload/files/NP3(1).jpg', '', 0, 0, 'vi', '', 59, '', 0),
(172, 'assets/includes/upload/files/NP4_1.jpg', '', 0, 0, 'vi', '', 59, '', 0),
(173, 'assets/includes/upload/files/NP5_1.jpg', '', 0, 0, 'vi', '', 59, '', 0),
(174, 'assets/includes/upload/files/NP5(1).jpg', '', 0, 0, 'vi', '', 59, '', 0),
(175, 'assets/includes/upload/files/NP6_1.jpg', '', 0, 0, 'vi', '', 59, '', 0),
(176, 'assets/includes/upload/files/NP6_3.jpg', '', 0, 0, 'vi', '', 59, '', 0),
(177, 'assets/includes/upload/files/NP10_1.jpg', '', 0, 0, 'vi', '', 59, '', 0),
(178, 'assets/includes/upload/files/NP10_2.jpg', '', 0, 0, 'vi', '', 59, '', 0),
(179, 'assets/includes/upload/files/NP10.jpg', '', 0, 0, 'vi', '', 59, '', 0),
(181, 'assets/includes/upload/files/NPTM2(1).jpg', '', 0, 0, 'vi', '', 59, '', 0),
(182, 'assets/includes/upload/files/NPTM7_1.jpg', '', 0, 0, 'vi', '', 59, '', 0),
(183, 'assets/includes/upload/files/NPTM8(1).jpg', '', 0, 0, 'vi', '', 59, '', 0),
(184, 'assets/includes/upload/files/SML-CV-NUOC.jpg', '', 0, 0, 'vi', '', 60, '', 0),
(185, 'assets/includes/upload/files/SML-PDB-DAY.jpg', '', 0, 0, 'vi', 'Walking Street', 51, '', 1),
(186, 'assets/includes/upload/files/SML-SH2-NIGHT.jpg', '', 0, 0, 'vi', 'Shopping Night Market', 51, '', 2),
(187, 'assets/includes/upload/files/SML-KH-NHA-HANG.jpg', '', 0, 0, 'vi', 'Summer Restaurant', 52, '', 0),
(189, 'assets/clients/imgs/service_hot/SML-CV-NUOC.jpg', '', 0, 0, 'vi', 'Kid water (Coming soon)', 61, '0', 2),
(190, 'assets/clients/imgs/service_hot/SML-SH2-NIGHT.jpg', '', 0, 0, 'vi', 'Shopping night market', 61, '0', 3),
(191, 'assets/clients/imgs/service_hot/4.png', '', 0, 0, 'vi', 'Chuỗi các công viên chủ đề', 61, '0', 4),
(192, 'assets/clients/imgs/service_hot/SML-CLUBHOUSE.jpg', '', 0, 0, 'vi', 'Club house sang trọng', 61, '0', 5),
(193, 'assets/clients/imgs/bietthu/bietthu-img/1.jpg', NULL, 0, 0, 'vi', '1', 62, '', 1),
(194, 'assets/clients/imgs/bietthu/bietthu-img/2.jpg', NULL, 0, 0, 'vi', '2', 62, '', 2),
(195, 'assets/clients/imgs/bietthu/bietthu-img/3.jpg', NULL, 0, 0, 'vi', '3', 62, '', 3),
(196, 'assets/clients/imgs/bietthu/bietthu-img/4.jpg', NULL, 0, 0, 'vi', '4', 62, '', 4),
(197, 'assets/clients/imgs/bietthu/bietthu-img/5.jpg', NULL, 0, 0, 'vi', '5', 62, '', 5),
(198, 'assets/clients/imgs/bietthu/bietthu-img/6.jpg', NULL, 0, 0, 'vi', '6', 62, '', 6),
(199, 'assets/clients/imgs/bietthu/bietthu-img/7.jpg', NULL, 0, 0, 'vi', '7', 62, '', 7),
(200, 'assets/clients/imgs/bietthu/bietthu-img/8.jpg', NULL, 0, 0, 'vi', '8', 62, '', 8),
(201, 'assets/clients/imgs/bietthu/bietthu-img/9.jpg', NULL, 0, 0, 'vi', '9', 62, '', 9),
(216, 'assets/clients/imgs/bietthu/bietthu-img/10.jpg', NULL, 0, 0, 'vi', '10', 62, '', 10),
(203, 'assets/clients/imgs/bietthu/bietthu-img/11.jpg', NULL, 0, 0, 'vi', '11', 62, '', 11),
(204, 'assets/clients/imgs/bietthu/bietthu-img/12.jpg', NULL, 0, 0, 'vi', '12', 62, '', 12),
(214, 'assets/clients/imgs/bietthu/nhapho-nhaphothuongmai/1.jpg', NULL, 0, 0, 'vi', '1', 63, '', 1),
(213, 'assets/clients/imgs/bietthu/nhapho-nhaphothuongmai/2.jpg', NULL, 0, 0, 'vi', '2', 63, '', 2),
(212, 'assets/clients/imgs/bietthu/nhapho-nhaphothuongmai/3.jpg', NULL, 0, 0, 'vi', '3', 63, '', 3),
(205, 'assets/clients/imgs/bietthu/nhapho-nhaphothuongmai/4.jpg', NULL, 0, 0, 'vi', '4', 63, '', 4),
(206, 'assets/clients/imgs/bietthu/nhapho-nhaphothuongmai/5.jpg', NULL, 0, 0, 'vi', '5', 63, '', 5),
(207, 'assets/clients/imgs/bietthu/nhapho-nhaphothuongmai/6.jpg', NULL, 0, 0, 'vi', '6', 63, '', 6),
(208, 'assets/clients/imgs/bietthu/nhapho-nhaphothuongmai/7.jpg', NULL, 0, 0, 'vi', '7', 63, '', 7),
(209, 'assets/clients/imgs/bietthu/nhapho-nhaphothuongmai/8.jpg', NULL, 0, 0, 'vi', '8', 63, '', 8),
(210, 'assets/clients/imgs/bietthu/nhapho-nhaphothuongmai/9.jpg', NULL, 0, 0, 'vi', '9', 63, '', 9),
(211, 'assets/clients/imgs/bietthu/nhapho-nhaphothuongmai/10.jpg', NULL, 0, 0, 'vi', '10', 63, '', 10),
(212, 'assets/clients/imgs/bietthu/nhapho-nhaphothuongmai/11.jpg', NULL, 0, 0, 'vi', '11', 63, '', 11),
(215, 'assets/clients/imgs/service_hot/SML-PDB-DAY.jpg\r\n', '', 0, 0, 'vi', 'Walking street hơn 2000m', 61, '', 1),
(0, NULL, NULL, 0, 0, 'vi', NULL, NULL, '', 0),
(217, 'assets/clients/imgs/overall-type/biet-thu-nha-pho.jpg', NULL, 0, 0, 'vi', 'BIỆT THỰ/NHÀ PHỐ', 64, '', 1),
(218, 'assets/clients/imgs/overall-type/can-ho-dich-vu.jpg', NULL, 0, 0, 'vi', 'CĂN HỘ DỊCH VỤ', 64, '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tailieu`
--

CREATE TABLE `tailieu` (
  `Id` int(11) NOT NULL,
  `Title` text CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Titledisplay` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Background` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Image` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `File` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tailieu`
--

INSERT INTO `tailieu` (`Id`, `Title`, `Titledisplay`, `Background`, `Image`, `File`) VALUES
(1, 'E-Leaflet', '<span class=\"char1\">E</span><span class=\"char2\">-</span><span class=\"char3\">L</span><span class=\"char4\">e</span><span class=\"char5\">a</span><span class=\"char6\">f</span><span class=\"char7\">l</span><span class=\"char8\">e</span><span class=\"char9\">t</span>', 'assets/includes/upload/files/background.jpg', 'assets/includes/upload/files/f16a0403596da933f07c.jpg', 'assets/includes/upload/files/aa413e20634e9310ca5f.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `templates`
--

CREATE TABLE `templates` (
  `TempID` int(11) NOT NULL,
  `TempName` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Filename` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` tinyint(4) DEFAULT 1,
  `Type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Meta` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Language` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SeoTitle` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SeoKeyword` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SeoDescribes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IsLanding` tinyint(4) DEFAULT NULL,
  `SeoCanonica` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MetaRobot` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `templates`
--

INSERT INTO `templates` (`TempID`, `TempName`, `Filename`, `Status`, `Type`, `Meta`, `Language`, `SeoTitle`, `SeoKeyword`, `SeoDescribes`, `IsLanding`, `SeoCanonica`, `MetaRobot`) VALUES
(11, 'Trang chủ', 'home', 1, 'page', 'trang-chu.html', 'en', 'Summerland - Điểm đến giải trí của Phan Thiết', '', 'Phan Thiết xưa vốn dĩ vẫn hút khách du lịch theo nét riêng mà vùng đất này vốn có: dịu dàng, bình yên và có phần trầm lắng.', 0, 'https://muinesummerland.vn.html', ''),
(16, 'Căn hộ dịch vụ', 'can-ho', 1, 'page', 'can-ho.html', 'vi', 'Căn hộ dịch vụ', 'Căn hộ dịch vụ', 'Căn hộ dịch vụ', 0, 'http://localhost:8080/bds/mat-bang.html', ''),
(37, 'Biệt thự - Nhà Phố', 'biet-thu', 1, 'page', 'biet-thu.html', 'vi', 'Biệt thự - Nhà Phố', 'Biệt thự - Nhà Phố', 'Biệt thự - Nhà Phố', 0, 'http://localhost:8080/bds/thu-vien.html', 'Biệt thự - Nhà Phố'),
(38, 'Tin tức', 'tin-tuc', 1, 'page', 'tin-tuc.html', 'vi', 'Tin tức', 'Tin tức', 'Tin tức', 0, 'http://localhost:8080/bds/tin-tuc.html', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `UserID` int(11) NOT NULL,
  `Account` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UserName` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Address` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `Email` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Phone` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Avatar` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Password` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` tinyint(4) DEFAULT 1,
  `Permission` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `IsContact` tinyint(4) DEFAULT 0,
  `Facebook` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Youtube` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Google` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Twitter` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `prov_id` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prov_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`UserID`, `Account`, `UserName`, `Address`, `Email`, `Phone`, `Avatar`, `Password`, `Status`, `Permission`, `IsContact`, `Facebook`, `Youtube`, `Google`, `Twitter`, `prov_id`, `prov_name`) VALUES
(7, 'admin', 'Summerland', '', 'info@sumerland.vn', '', 'assets/includes/upload/images/users/noimage.jpg', 'c93ccd78b2076528346216b3b2f701e6', 1, 'Admin', 0, '', '', '', '', '', NULL),
(8, 'hunglocphat', 'Hung Loc Phat', '', 'hungphatlandcorp@gmail.com', '', 'assets/includes/upload/images/users/noimage.jpg', '34053bdee5f0738a3d565309a6a92972', 1, '', 0, NULL, NULL, NULL, NULL, '', ''),
(9, 'Tris Lee', 'Tris Lee', '', 'tri.le@hlp.vn', '0908200798', 'assets/includes/upload/images/users/noimage.jpg', 'df49c29d4dd86abd7b52cb58d4619d11', 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `utilities_tq`
--

CREATE TABLE `utilities_tq` (
  `id` int(11) NOT NULL,
  `background` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `utilities_tq`
--

INSERT INTO `utilities_tq` (`id`, `background`, `title`) VALUES
(1, 'assets/clients/imgs/tienich/bg_tienich.png', 'TIỆN ÍCH');

-- --------------------------------------------------------

--
-- Table structure for table `visitor`
--

CREATE TABLE `visitor` (
  `ID` int(11) NOT NULL,
  `Ip` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `TimeVisit` datetime DEFAULT NULL,
  `NumOfTime` int(11) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `widgets`
--

CREATE TABLE `widgets` (
  `ID` int(11) NOT NULL,
  `Title` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Area` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Language` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` tinyint(4) DEFAULT 1,
  `Describes` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Position` int(11) DEFAULT 1,
  `CateID` int(11) DEFAULT NULL,
  `Limits` int(11) DEFAULT NULL,
  `Method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Type` int(11) DEFAULT NULL,
  `Content` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `widgets`
--

INSERT INTO `widgets` (`ID`, `Title`, `Area`, `Language`, `Status`, `Describes`, `Position`, `CateID`, `Limits`, `Method`, `Type`, `Content`) VALUES
(21, 'Danh mục bài viết', 'sidebar', 'vi', 1, NULL, 1, NULL, 0, 'wd_articleCategories', 0, NULL),
(37, 'Bản đồ', 'noArea', 'vi', 1, NULL, 16, NULL, 0, 'wd_markerList', 0, NULL),
(40, 'Tin tức nổi bật', 'noArea', 'vi', 1, NULL, 15, NULL, 5, 'wd_featureArt', 0, NULL),
(44, 'Sản phẩm nổi bật', 'noArea', 'vi', 1, NULL, 14, NULL, 5, 'wd_featurePro', 0, NULL),
(46, 'Chia sẻ', 'footer', 'vi', 1, '', 4, 0, 1, 'wd_text', 0, '<p><a href=\"#\" target=\"_blank\"><img alt=\"icon facebook\" src=\"assets/includes/upload/images/icons/facebook2.png\" style=\"height: 48px; width: 48px; border-width: 0px; border-style: solid;\" title=\"facebook\" /></a>&nbsp;<a href=\"https://www.youtube.com/channel/UCAIVe0Qn5bKlIxAItu1-uOA\" target=\"_blank\"><img alt=\"icon youtube\" src=\"assets/includes/upload/images/icons/youtube-icon.png\" style=\"border-width: 0px; border-style: solid; width: 48px; height: 48px;\" title=\"youtube\" />&nbsp;<img alt=\"icon linkedin\" src=\"assets/includes/upload/images/icons/linkedin.png\" style=\"width: 48px; height: 48px;\" title=\"linkedin\" /></a></p>\n\n<p>&nbsp;</p>\n'),
(50, 'Theo dõi', 'noArea', 'vi', 1, NULL, 13, NULL, 1, 'wd_subscribe', 0, '<ul>\n                    <li><a href=\"index.html\">Home</a></li>\n                    <li><a href=\"about.html\">About</a></li>\n                    <li><a href=\"services.html\">Services</a></li>\n                    <li><a href=\"blog.html\">Blog</a></li>\n                    <li><a href=\"mail.html\">Mail Us</a></li>\n                </ul>\n                <form>\n                    <input type=\"text\" placeholder=\"Text...\">\n                    <input type=\"submit\" value=\"Subscribe\">\n                </form>'),
(51, 'Bài viết mới nhất', 'sidebar', 'vi', 1, NULL, 3, NULL, 5, 'wd_newArticle', 0, NULL),
(52, 'Tất cả sản phẩm', 'noArea', 'vi', 1, NULL, 12, NULL, 0, 'wd_allProduct', 0, NULL),
(53, 'Tất cả bài viết', 'noArea', 'vi', 1, NULL, 10, NULL, 0, 'wd_allArticle', 0, NULL),
(55, 'Thống kê truy cập', 'sidebar', 'vi', 1, NULL, 5, NULL, 0, 'wd_visitorStatistic', 0, 'total,today,yesterday,month,year'),
(58, 'Về chúng tôi', 'footer', 'vi', 1, 'Về chúng tôi', 2, 2, 5, 'wd_articleByCate', 1, ''),
(65, 'Tin tức - Khuyến mãi', 'body', 'vi', 1, '', 3, 0, 9, 'wd_newArticle', 0, ''),
(66, 'Thống kê truy cập', 'noArea', 'vi', 1, NULL, 11, NULL, 0, 'wd_visitorStatistic', 0, 'total,today,yesterday,month,year'),
(67, 'Fan Page', 'noArea', 'vi', 1, NULL, 9, NULL, 1, 'wd_fanPage', 0, NULL),
(69, 'Tin tức', 'footer', 'vi', 1, '', 3, 1, 5, 'wd_articleByCate', 1, ''),
(70, 'Tại sao chọn TheHeMoi Express?', 'body', 'vi', 1, '', 1, 0, 0, 'wd_text', 0, '<div class=\"list-about animated fadeIn row\" data-animation-delay=\"300\" data-animation-duration=\"1s\">\n<div class=\"list-about-item col-md-6 text-left\">\n<div class=\"icon-large wow zoomIn animated\" data-animation-delay=\"300\" data-animation-duration=\"1s\"><img alt=\"Mạng lưới giao hàng rộng khắp\" src=\"assets/client/Uploads/DigitalSystem/bd/bd189407-bc62-4e9d-b6a8-204897c67b61.jpg\" /></div>\n\n<h3>Mạng lưới giao hàng rộng khắp</h3>\n\n<p>TheHeMoi Express vận chuyển hàng hóa tới 63 tỉnh/thành phố Toàn Quốc cùng 220 quốc gia và vùng lãnh thổ trên thế giới</p>\n</div>\n\n<div class=\"list-about-item col-md-6 text-left\">\n<div class=\"icon-large wow zoomIn animated\" data-animation-delay=\"300\" data-animation-duration=\"1s\"><img alt=\"Đa dạng dịch vụ vận chuyển\" src=\"assets/client/Uploads/DigitalSystem/40/40f0bdda-0b29-47bd-b521-bde57dad17aa.jpg\" /></div>\n\n<h3>Đa dạng dịch vụ vận chuyển</h3>\n\n<p>TheHeMoi Express mang tới nhiều lựa chọn chuyển phát phù hợp với từng yêu cầu đặc thù của mỗi cá nhân/tổ chức</p>\n</div>\n\n<div class=\"list-about-item col-md-6 text-left\">\n<div class=\"icon-large wow zoomIn animated\" data-animation-delay=\"300\" data-animation-duration=\"1s\"><img alt=\"Nhanh chóng và tiết kiệm\" src=\"assets/client/Uploads/DigitalSystem/84/845dfebc-0a7f-41d4-9f9b-5475230447c2.jpg\" /></div>\n\n<h3>Nhanh chóng và tiết kiệm</h3>\n\n<p>Các dịch vụ vận chuyển được tối ưu hóa để đảm bảo thời gian phát hàng tới tay người nhận nhanh nhất với cước phí phù hợp nhất</p>\n</div>\n\n<div class=\"list-about-item col-md-6 text-left\">\n<div class=\"icon-large wow zoomIn animated\" data-animation-delay=\"300\" data-animation-duration=\"1s\"><img alt=\"Chủ động kiểm soát\" src=\"assets/client/Uploads/DigitalSystem/e6/e654eaa8-4817-4e59-ac0c-242b472c62ff.jpg\" /></div>\n\n<h3>Chủ động kiểm soát</h3>\n\n<p>Hệ thống Tracking và quản lý Online cho phép khách hàng có thể chủ động kiểm soát hành trình của đơn hàng mọi lúc mọi nơi</p>\n</div>\n</div>\n'),
(73, 'Tìm kiếm', 'noArea', 'vi', 1, NULL, 6, NULL, 0, 'wd_search', 0, '<form method=\'get\' action=\'search\'> <div class=\'input-group\'><input class=\'form-control\' placeholder=\'...\' type=\'text\' name=\'keyword\' /> <span class=\'input-group-btn\'><button class=\'btn btn-secondary\' type=\'submit\'><i class=\'fa fa-search\'></i></button> </span></div> </form>'),
(76, 'Bài viết theo danh mục', 'noArea', 'vi', 1, 'Tuyển dụng', 8, 1, 5, 'wd_articleByCate', 1, ''),
(77, 'Liên hệ', 'footer', 'vi', 1, '', 1, 0, 0, 'wd_text', 0, '<ul class=\"list-unstyled\">\n	<li>Hotline:&nbsp;<strong><a href=\"tel:19008907\" style=\"font-size: 16px;\">1900 8907</a></strong></li>\n	<li>Email:&nbsp;<a href=\"mailto:info@thehemoiexpress.vn\">info@thehemoiexpress.vn</a></li>\n	<li>ĐC: 110/26 - 110/28 Ông Ích Khiêm, Phường 5, Quận 11, HCM.</li>\n</ul>\n'),
(78, 'Bài viết mới nhất', 'noArea', 'vi', 1, NULL, 4, NULL, 5, 'wd_newArticle', 0, NULL),
(80, 'Khách hàng - Đối tác', 'body', 'vi', 1, '', 2, 13, 16, 'wd_slideShow', 0, ''),
(81, 'Văn bản', 'noArea', 'vi', 1, NULL, 1, NULL, 0, 'wd_text', 0, NULL),
(82, 'Danh mục bài viết', 'noArea', 'vi', 1, NULL, 2, NULL, 0, 'wd_articleCategories', 0, NULL),
(83, 'Danh mục sản phẩm', 'noArea', 'vi', 1, NULL, 5, NULL, 0, 'wd_productCategories', 0, NULL),
(84, 'Sán phẩm mới nhất', 'noArea', 'vi', 1, NULL, 7, NULL, 5, 'wd_newProduct', 0, NULL),
(85, 'Hình ảnh trình chiếu', 'noArea', 'vi', 1, NULL, 3, 0, 1, 'wd_slideShow', NULL, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
