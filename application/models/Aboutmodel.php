<?php
defined('BASEPATH') OR exit('');

class Aboutmodel extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }  
    
    public function getAll(){

        $result = $this->db->query('select * from about');
        if($result->num_rows() > 0){
            return $result->result_array();            
        }
        else{
            return array();
        }
    }
    
    public function update($data){

        $this->db->where('Id', $data['Id']);        
        $result = $this->db->update('about', $data); 
        if($result !== null){
            return true;
        }
        else{
            return null;
        } 
    }
    
}

?>
