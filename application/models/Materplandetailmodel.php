<?php
defined('BASEPATH') OR exit('');

class Materplandetailmodel extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }  
    
    public function getAll(){

        $result = $this->db->query('select * from master_plan_detail WHERE Status = 1 ORDER BY sort ASC');
        if($result->num_rows() > 0){
            return $result->result_array();            
        }
        else{
            return array();
        }
    }
    
    public function update($data){

        $this->db->where('id', $data['id']);        
        $result = $this->db->update('master_plan_detail', $data); 
        if($result->num_rows() !== 0){
            return $result->result_array();
        }
        else{
            return null;
        } 
    }   
            
    public function insert($data){
        $result = $this->db->insert('master_plan_detail', $data);
        if($result !== null){
            return true;
        }
        else{
            return false;
        }
    }
    
    public function delete($id){
        $param = array(
            'id' => $id  
        );
        $result = $this->db->delete('master_plan_detail', $param);
        if($result !== null){
            return true;
        }
        else{
            return false;
        }
    }

    public function edit($artId){
        $result = $this->db->query('select * from master_plan_detail
        where id = '.$artId.';');
        return ($result->num_rows() > 0)? $result->result_array() : array();
    }
    
}

?>
