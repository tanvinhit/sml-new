<?php
defined('BASEPATH') OR exit('');

class Connectmodel extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }  
    
    public function getAll(){

        $result = $this->db->query('select * from connect');
        if($result->num_rows() > 0){
            return $result->result_array();            
        }
        else{
            return array();
        }
    }

    public function getClient(){

        $result = $this->db->query('SELECT * from master_plan a
        left join master_plan_detail b on a.id = b.master_plan_id
        where a.Status = 1
        order by a.sort, b.sort ASC');
        if($result->num_rows() > 0){
            return $result->result_array();            
        }
        else{
            return array();
        }
    }
    
    public function update($data){

        $this->db->where('id', $data['id']);        
        $result = $this->db->update('connect', $data); 
        if($result !== null){
            return true;
        }
        else{
            return null;
        } 
    }
    
}

?>
