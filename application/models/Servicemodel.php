<?php
defined('BASEPATH') OR exit('');

class Servicemodel extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('phpsession');
    }    
    
    public function getAllServices($type, $lang){
        $currentLang = $this->phpsession->getCookie('monpham_language');
        if($type === 'admin'){ 
            if($lang === null){
                $result = $this->db->query('select ID,Name,Describes,Image,Language, Url, ServiceId
                                            from services;');
            }
            else{
                $result = $this->db->query('select ID,Name,Describes,Image,Language, Url, ServiceId
                                            from services    
                                            where Language = "'.$lang.'";');
            }
        }
        else{
            $result = $this->db->query('select ID,Name,Describes,Image,Language, Url, ServiceId
                                        from services
                                        where Language = "'.$currentLang.'";');
        }
        if($result->num_rows() > 0)
            return $result->result_array();                            
        return array();
        
    }

    public function getById($id){
        $query = $this->db->query("select *
                                from services
                                where ServiceId = '".$id."'");
        if($query->num_rows() > 0)
            return $query->result()[0];
        else return null;
    }
    
    public function updateService($Id, $name, $des, $img, $lang, $url, $serviceId){
        $param = array(            
            'Name' => $name,
            'Describes' => $des,            
            'Image' => $img,                        
            'Language' => $lang,
            'Url' => $url,
            'ServiceId' => $serviceId
        );        
        $this->db->where('ID', $Id);        
        $result = $this->db->update('services', $param);    
        if($result !== null)
            return true;       
        return false;
    }
    
    public function addService($name, $des, $img, $lang, $url, $serviceId){
        $param = array(            
            'Name' => $name,
            'Describes' => $des,            
            'Image' => $img,                        
            'Language' => $lang,
            'Url' => $url,
            'ServiceId' => $serviceId
        );  
        $result = $this->db->insert('services', $param);
        if($result !== null)
            return true;       
        return false;        
    }
    
    public function deleteService($Id){
        $param = array(
            'ID' => $Id  
        );
        $result = $this->db->delete('services', $param);
        if($result !== null){
            return true;
        }
        else{
            return false;
        }
    }
        
    public function checkExistsId($customId){
        $count = $this->db
                        ->where('ServiceId', $customId)
                        ->count_all_results('services');
        if($count > 0)
            return false;
        return true;
    }
}

?>