<?php
defined('BASEPATH') OR exit('');

class Plusservicemodel extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('phpsession');
    }    
    
    public function getAll(){        
        $result = $this->db->query('select *
                                    from plusservices
                                    where isDeleted = 0;');
        if($result->num_rows() > 0)
            return $result->result_array();                            
        return array();
        
    }
    
    public function update($data){
        $data['typePrice'] = (bool)$data['typePrice'];
        $this->db->where('plusServId', $data['plusServId']);        
        $result = $this->db->update('plusservices', $data);    
        if($result !== null)
            return true;       
        return false;
    }
    
    public function insert($data){
        $result = $this->db->insert('plusservices', $data);
        if($result !== null)
            return true;       
        return false;        
    }
    
    public function delete($Id){
        $param = array(
            'plusServId' => $Id  
        );
        $result = $this->db->delete('plusservices', $param);
        if($result !== null){
            return true;
        }
        else{
            return false;
        }
    }
        
}

?>