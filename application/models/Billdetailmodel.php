<?php

class Billdetailmodel extends CI_Model {
    public function __construct(){
        parent::__construct();            
        $this->load->database();
    }
    
    public function insert($data, $purchaseId){
        $purchaseDetail = array(
            'billId' => $purchaseId,
            'productName' => $data->name,
            'productUnit' => $data->unit,
            'productQuantity' => $data->quantity
        );
        
        $query = $this->db->insert('billdetail', $purchaseDetail);
        if($query)
            return true;
        return false;
    }

    public function getById($id){
        $query = $this->db->query('select *
                                from billdetails
                                where billId = '.$id);
        if($query->num_rows() > 0)
            return $query->result_array();
        else return null;
    }
}

?>