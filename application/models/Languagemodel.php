<?php
defined('BASEPATH') OR exit('');

class Languagemodel extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->database();         
    }    
    
    public function checkExistsLang($lang){        
        $result = $this->db->query('select 1 from languages where Code = "'.$lang.'";');
        if($result->num_rows() > 0)
            return true;
        return false;
    }
    
    public function getLanguages(){
        $result = $this->db->query('select ID,Name,Code,Icon,Status
                          from languages');
        if($result->num_rows() > 0)
            return $result->result_array();
        return array();
    }
    
    public function updateLanguage($data){               
        $this->db->where('ID', $data['ID']);        
        $result = $this->db->update('languages', $data);    
        if($result !== null){
            return true;            
        }
        else{
            return false;
        }
    }
    
    public function addLanguage($data){        
        $result = $this->db->insert('languages', $data);
        if($result !== null){
            return true;
        }
        else{
            return false;
        }
    }
    
    public function deleteLanguage($id){
        $param = array(
            'ID' => $id  
        );
        $result = $this->db->delete('languages', $param);
        if($result !== null){
            return true;
        }
        else{
            return false;
        }
    }
    
}

?>