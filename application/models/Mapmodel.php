<?php
defined('BASEPATH') OR exit('');

class Mapmodel extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->database();
//        $this->load->library('phpsession');
    }  
    
    public function getMapList(){
        $result = $this->db->query('select MapID,Address,Lattitude,Longitude,Phone
                                    from map');
        return ($result->num_rows() > 0)? $result->result_array() : array();
    }
    
    public function updateMarker($data){ 
        $map = array(            
            'Address' => $data['Address'],
            'Lattitude' => $data['Lattitude'],
            'Longitude' => $data['Longitude'],
            'Phone' => $data['Phone']            
        );       
        $this->db->where('MapID', $data['MapID']);
        $result = $this->db->update('map', $map);
        if($result !== null)
            return true;
        return false;
    }
    
    public function addNewMarker($data){   
        $map = array(            
            'Address' => $data['Address'],
            'Lattitude' => $data['Lattitude'],
            'Longitude' => $data['Longitude'],
            'Phone' => $data['Phone']            
        );              
        $result = $this->db->insert('map', $map);
        if($result !== null)
            return true;
        return false;
    }
    
    public function deleteMarker($id){        
        $this->db->where('MapID', $id);
        $result = $this->db->delete('map');
        if($result !== null)
            return true;
        return false;
    }

    public function getProvinces(){
        $result = $this->db->query('select MapID,Address,Lattitude,Longitude,Phone
                                    from map');
        return ($result->num_rows() > 0)? $result->result_array() : array();
    }

    public function getDistrictsByProvinceId(){
        $result = $this->db->query('select MapID,Address,Lattitude,Longitude,Phone
                                    from map');
        return ($result->num_rows() > 0)? $result->result_array() : array();
    }

    public function getWardsByDistrictId(){
        $result = $this->db->query('select MapID,Address,Lattitude,Longitude,Phone
                                    from map');
        return ($result->num_rows() > 0)? $result->result_array() : array();
    }
}
?>