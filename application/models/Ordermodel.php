<?php
class Ordermodel extends CI_Model{
    
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }
    
    public function getAll(){
        $query = $this->db->query('select * from orders order by orderId desc');
        if($query->num_rows() > 0)
            return $query->result_array();
        else return false;
    }
    
    public function insert($data){
        $query = $this->db->insert('orders',$data);
        if($query){
            $purchaseId = $this->db->insert_id();      
            return $purchaseId;
        }
        else return false;
    }
    
    public function update($data){
        if(is_numeric($data['orderId'])){
            $this->db->where('orderId',$data['orderId']);
            $query = $this->db->update('orders',$data);
            if($query)
                return true;
            else return false;
        }
    }
    public function updateStatus($data){
        if(is_numeric($data['orderId'])){
            $this->db->where('orderId',$data['orderId']);
            $query = $this->db->update('orders',array('status' => $data['status']));
            if($query)
                return true;
            else return false;
        }
    }
    
    public function delete($id){
        if(is_numeric($id)){
            $this->db->where('orderId',$id);
            $query = $this->db->delete('orderdetail');
            $this->db->where('orderId',$id);
            $query = $this->db->delete('orders');
            
            if($query)
                return true;
            else return false;
        }
        else return null;
    }
    
    public function getById($id){
        $query = $this->db->query('select pd.proId, pd.orderId,
                                pd.quantity, pd.proPrice, p.ProName as productName
                                from orderdetail pd inner join products p
                                on pd.proId = p.ProID
                                where pd.orderId = '.$id);
        if($query->num_rows() > 0)
            return $query->result_array();
        else return null;
    }
}
?>