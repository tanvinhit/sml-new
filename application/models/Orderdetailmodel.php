<?php

class Orderdetailmodel extends CI_Model {
    public function __construct(){
        parent::__construct();            
        $this->load->database();
    }
    
    public function insert($data, $purchaseId){
        $purchaseDetail = array(
            'orderId' => $purchaseId,
            'proId' => $data->ID,
            'quantity' => $data->quantity,
            'proPrice' => $data->price,
            'proDiscount' => $data->sale
        );
        
        $query = $this->db->insert('orderdetail', $purchaseDetail);
        if($query)
            return true;
        return false;
    }
}

?>