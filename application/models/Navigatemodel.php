<?php
defined('BASEPATH') OR exit('');

class Navigatemodel extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->database(); 
        $this->load->library('phpsession');
    }    
            
    private function showMenuOnClient($menus, &$htmlResult, $parent = 0, $class = false){
        # BƯỚC 1: LỌC DANH SÁCH MENU VÀ CHỌN RA NHỮNG MENU CÓ PARENT = $parent
        // Biến lưu menu lặp ở bước đệ quy này
        $menu_tmp = array();

        foreach ($menus as $key => $item) {
            // Nếu có parent bằng với $parent hiện tại
            if ((int) $item['ParentId'] == (int) $parent) {
                $menu_tmp[] = $item;
                // Sau khi thêm vào biên lưu trữ menu ở bước lặp
                // thì unset nó ra khỏi danh sách menu ở các bước tiếp theo
                unset($menus[$key]);
            }
        }

        # BƯỚC 2: lẶP MENU THEO DANH SÁCH MENU Ở BƯỚC 1
        // Điều kiện dừng của đệ quy là cho tới khi menu không còn nữa
        if ($menu_tmp) 
        {      
            if(!$class)
                $htmlResult .= '<ul class="nav navbar-nav" id="menu">';                
            else
                $htmlResult .= '<ul class="dropdown-menu">';
            foreach ($menu_tmp as $item) 
            {
                if(!$class)
                    $htmlResult .= '<li id="drop-'.$item['NavID'].'" class="dropdown">';
                else
                    $htmlResult .= '<li id="drop-'.$item['NavID'].'">';
                $url = explode("/",$item['NavMeta']);
                if($url[0] === 'http:' || $url[0] === 'https:')
                    $htmlResult .= '<a href="'.$item['NavMeta'].'" class="" target="_blank">'.$item['NavName'].'</a>';
                else{                    
                    if($item['Quantity'] > 0)
                        $htmlResult .= '<a class="" href="'.$item['NavMeta'].'">'.$item['NavName'].'</a>';                    
                    else
                        $htmlResult .= '<a class="" href="'.$item['NavMeta'].'">'.$item['NavName'].'</a>';                                        
                }                
                // Gọi lại đệ quy
                // Truyền vào danh sách menu chưa lặp và parent của menu hiện tại
                $this->showMenuOnClient($menus, $htmlResult, $item['NavID'], true);               
                $htmlResult .= '</li>';                
            }          
            $htmlResult .= '</ul>';            
        }
    }

    public function getNavigates($lang, $type, $isLandingPage = false){
        $result = array();        
        if($type === 'admin'){
            if(!isset($lang)){
                $result = $this->db->query('select NavID,NavName,NavMeta,ParentId,Status,NavLang,Position,Type
                                            from navigates
                                            order by Position;');
            }
            else{
                $result = $this->db->query('select NavID,NavName,NavMeta,ParentId,Status,NavLang,Position,Type
                                            from navigates
                                            where NavLang = "'.$lang.'"
                                            order by Position;');
            }
            return $result->result_array();
        }
        else{
            if(!isset($lang))
                $lang = $this->phpsession->getCookie('monpham_language');
            $result = $this->db->query('select NavID,NavName,NavMeta,ParentId,Status,NavLang,Position,Type,(select count(NavID) from navigates b where a.NavID = b.ParentId) as Quantity
                                            from navigates a
                                            where NavLang = "'.$lang.'" and Status = 1
                                            order by Position;');
            $html = '';
            $this->showMenuOnClient($result->result_array(), $html);
            return $html;
        }        
    }
    
    public function addNavigate($name, $meta, $lang, $temp = 0, $type = 0){
        $query = $this->db->query('select count(NavID) as result from navigates where ParentId = 0;');
        $row = $query->result();
        $pos = $row[0];
        $param = array(            
            'NavName' => $name,
            'NavMeta' => $meta,
            'NavLang' => $lang,
            'Position' => (int)($pos->result) + 1,
            'Type' => $type            
        );
        $result = $this->db->insert('navigates', $param);        
        if($result !== null){
            return true;
        }        
        return false;        
    }
    
    public function updateNavigate($data){
        $this->db->where('NavID', $data['NavID']);
        $result = $this->db->update('navigates', $data);
        if($result !== null){
            return true;
        }
        return false;
    }
    
    public function deleteNavigate($id, $parent, $pos){
        //update lại parentid con khi xóa menu cha
        $param = array(
            'ParentId' => $parent,
            'Position' => $pos
        );
        $this->db->where('ParentId', $id);
        $this->db->update('navigates', $param);
        //xóa menu
        $this->db->where('NavID', $id);
        $result = $this->db->delete('navigates');
        if($result !== null)
            return true;
        return false;
    }
    
    public function updatePosition($id, $newPos, $parent){
        $param = array(
            'ParentId' => $parent,
            'Position' => $newPos
        );
        $this->db->where('NavID', $id);
        $this->db->update('navigates', $param);
    }
            
}

?>