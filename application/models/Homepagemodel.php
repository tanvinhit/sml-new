<?php
defined('BASEPATH') OR exit('');

class Homepagemodel extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }  
    
    public function getAll(){

        $result = $this->db->query('select * from homepage');
        if($result->num_rows() > 0){
            return $result->result_array();            
        }
        else{
            return array();
        }
    }
    
    public function update($data){

        $this->db->where('id', $data['id']);        
        $result = $this->db->update('homepage', $data); 
        if($result !== null){
            return true;
        }
        else{
            return null;
        } 
    }
    
}

?>
