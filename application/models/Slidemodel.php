<?php
defined('BASEPATH') OR exit('');

class Slidemodel extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('phpsession');
    }    

    private function showSlider($arraySlide){
        $count = count($arraySlide);
        $html = '<ul>';            
        for ($i = 0; $i < $count; $i++){
            $html .= '<li data-transition="fade">
                        <img src="'.$arraySlide[$i]['Image'].'"
                            alt="'.$arraySlide[$i]['Title'].'" data-bgposition="center"
                            data-bgfit="cover" data-bgrepeat="no-repeat" />
                        <div class="container" style="width: 50em">
                            <div class="tp-caption-bb tp-caption main-title sfl" data-x="left"
                                data-y="center" data-hoffset="0" data-voffset="-80" data-speed="600"
                                data-start="500" data-endspeed="100" data-easing="Sine.easeOut">
                                <h2>'.$arraySlide[$i]['Title'].'</h2>
                                <p>'.$arraySlide[$i]['Describes'].'</p>';
            if(isset($arraySlide[$i]['ArtMeta']) && $arraySlide[$i]['ArtMeta'] !== '')
                $html .= '<a href="'.$arraySlide[$i]['ArtMeta'].'" class="btn btn-circle btn-default">Xem chi tiết</a>';
            else if(isset($arraySlide[$i]['CatMeta']) && $arraySlide[$i]['CatMeta'] !== '')
                $html .= '<a href="'.$arraySlide[$i]['CatMeta'].'" class="btn btn-circle btn-default">Xem chi tiết</a>';
            $html .= '</div>
                    </div>
                </li>';
        }
        $html .= '</ul>';
        return $html;
    }
    
    public function getAllSlides($type, $lang)
    {
        if ($type === 'admin') {
            if (!isset($lang)) {
                $result = $this->db->query('select ID,a.Image,Describes,ProLink,ArtMeta,ArtName,CateLink,CatMeta,CatName,Language,Title, a.sort
                                            from slides a left join categories b on a.CateId = b.CatID left join articles c on a.ProLink = c.ArtID
                                            order by a.sort asc;');
            } else {
                $result = $this->db->query('select ID,a.Image,Describes,ProLink,ArtMeta,ArtName,CateLink,CatMeta,CatName,Language,Title
                                            from slides a left join categories b on a.CateId = b.CatID left join articles c on a.ProLink = c.ArtID
                                            where Language = "' . $lang . '"
                                            order by a.sort asc;');
            }
            return $result->result_array();
        } else {
            if (!isset($lang)) {
                $lang = $this->phpsession->getCookie('monpham_language');
            }

            $result = $this->db->query('select ID,a.Image,Describes,ProLink,CateLink,Language,Title,CatName,ArtName,CatMeta,ArtMeta, a.sort
                                        from slides a left join categories b on a.CateId = b.CatID left join articles c on a.ProLink = c.ArtID
                                        where Language = "' . $lang . '"
                                        order by a.sort asc;');
            $html = $this->showSlider($result->result_array());
            return $html;
        }
    }

    public function updateSlide($data)
    {
        $this->db->where('ID', $data['ID']);
        $result = $this->db->update('slides', $data);
        if ($result !== null) {
            return true;
        }

        return false;
    }

    public function addSlide($data)
    {
        $result = $this->db->insert('slides', $data);
        if ($result !== null) {
            return true;
        }

        return false;
    }

    public function deleteSlide($Id)
    {
        $param = array(
            'ID' => $Id,
        );
        $result = $this->db->delete('slides', $param);
        if ($result !== null) {
            return true;
        } else {
            return false;
        }
    }

    public function changeSlidePosition($id, $pos)
    {
        $param = array(
            'Position' => $pos,
        );
        $this->db->where('ID', $id);
        $this->db->update('slides', $param);
    }

    public function getSlideByCateId($cateId)
    {
        $lang = $this->phpsession->getCookie('monpham_language');
        $lang = isset($lang) ? $lang : 'vi';
        $result = $this->db->query('select ID,a.Image,Describes,ProLink,ArtMeta,ArtName,CateLink,CatMeta,CatName,Language,Title, a.sort 
                                            from slides a left join categories b on a.CateId = b.CatID left join articles c on a.ProLink = c.ArtID
                                            where a.CateId = ' . $cateId . ' and Language="' . $lang . '" order by a.sort asc');
        return ($result->num_rows() > 0) ? $result->result_array() : array();
    }

    public function getSlideByCateName($cateName)
    {
        $lang = $this->phpsession->getCookie('monpham_language');
        $lang = isset($lang) ? $lang : 'vi';
        $result = $this->db->query("select ID,a.Image,Describes,ProLink,ArtMeta,ArtName,CateLink,CatMeta,CatName,Language,Title, a.sort
                                            from slides a left join categories b on a.CateId = b.CatID left join articles c on a.ProLink = c.ArtID
                                            where b.CatName = '" . $cateName . "' and Language= '" . $lang . "' order by a.sort asc");
        $html = $this->showSlider(($result->num_rows() > 0) ? $result->result_array() : array());
        return $html;
    }
}

?>