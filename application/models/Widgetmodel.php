<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Widgetmodel extends CI_Model {
        
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('phpsession');
        $this->load->model('Articlemodel');
        $this->load->model('Productmodel');
        $this->load->model('Categorymodel');
        $this->load->model('Visitormodel');
        $this->load->model('Mapmodel');
    }
    
    public function getHeader(){
        $obj = null;
        $result = $this->db->query('select HeaderId,Logo,Banner
                    from header;');
        if($result->num_rows() > 0){
            $row = $result->result();
            $obj = $row[0];
        }
            
        return $obj;
    }
            
    function showNestableCate($query, &$refVar, $parent = 0, $child = false){
        $menu_tmp = array();
        
        foreach ($query as $key => $item) {
            // Nếu có parent bằng với $parent hiện tại
            if ((int) $item['CatParent'] == (int) $parent) {
                $menu_tmp[] = $item;
                // Sau khi thêm vào biên lưu trữ menu ở bước lặp
                // thì unset nó ra khỏi danh sách menu ở các bước tiếp theo
                unset($query[$key]);
            }
        }

        # BƯỚC 2: lẶP MENU THEO DANH SÁCH MENU Ở BƯỚC 1
        // Điều kiện dừng của đệ quy là cho tới khi menu không còn nữa
        if ($menu_tmp) 
        {      
            if(!$child)
                $refVar .= '<ul class="list-unstyled m-0 p-0">';
            else
                $refVar .= '<ul class="list-unstyled m-0 p-0 children">';
            foreach ($menu_tmp as $item) 
            {                
                $refVar .= '<li>
                        <a href="'.$item['CatMeta'].'">
                            '.$item['CatName'].'
                        </a>';
                // Gọi lại đệ quy
                // Truyền vào danh sách menu chưa lặp và parent của menu hiện tại
                $this->showNestableCate($query, $refVar, $item['CatID'], true);
                $refVar .= '</li>';
            }
            $refVar .= '</ul>';
        }        
    }
    
    function getNestableCate($place, $type, $lang){        
        $result = $this->Categorymodel->getAllCategories($place, $lang, $type);        
        $sidebarCategory = '';
        $this->showNestableCate($result, $sidebarCategory);
        return $sidebarCategory;
    }

    function wd_searchBox(){
        $html = "<form method='get' action='".site_url('search')."'> <div class='input-group'><input class='form-control' placeholder='...' type='text' name='keyword' /> <span class='input-group-btn'><button class='btn btn-secondary' type='submit'><i class='fa fa-search'></i></button> </span></div> </form>";
        return $html;
    }
    
    function wd_visitorStatistic($data){
        $query = $this->Visitormodel->visitorStatistic();
        $arr = explode(',', $data);
        $view = '';
        for($i = 0; $i < count($arr); $i++){
            if($arr[$i] === 'total')
                $view .= '<p class="sidebar-visit-counter">
                            <img src="assets/includes/upload/images/icons/mvctotal.png" alt="total" class="img-statistic"> Tổng lượt: '.$query[4]['Number'].'
                          </p>';
            else if($arr[$i] === 'today')
                $view .= '<p class="sidebar-visit-counter">
                            <img src="assets/includes/upload/images/icons/mvctoday.png" alt="today" class="img-statistic"> Hôm nay: '.$query[0]['Number'].'</span>
                         </p>';
            else if($arr[$i] === 'yesterday')
                $view .= '<p class="sidebar-visit-counter">
                            <img src="assets/includes/upload/images/icons/mvcyesterday.png" alt="yesterday" class="img-statistic"> Hôm qua: '.$query[1]['Number'].'</span>
                         </p>';
            else if($arr[$i] === 'month')
                $view .= '<p class="sidebar-visit-counter">
                            <img src="assets/includes/upload/images/icons/mvconline.png" alt="month" class="img-statistic"> Tháng: '.$query[2]['Number'].'</span>
                         </p>';
            else if($arr[$i] === 'year')
                $view .= '<p class="sidebar-visit-counter">
                            <img src="assets/includes/upload/images/icons/mvcvisit.png" alt="year" class="img-statistic"> Năm: '.$query[3]['Number'].'</span>
                         </p>';
        }
        return $view;
    }
        
    function wd_fanPage($data){                
        $result = '<div class="fb-page" data-href="'.$data.'" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-height="169">
                            <div class="fb-xfbml-parse-ignore">
                                <blockquote cite="'.$data.'">
                                    <a href="'.$data.'"></a>
                                </blockquote>
                            </div>
                        </div>';
        return $result;
    }
                        
    public function getAllWidgets($lang, $data){                 
        $result = $this->db->query('select ID,Title,Describes,Area,Status,Position,CateID,Limits,Content,Method,Type,Language
                                from widgets
                                where Area = "'.$data['area'].'" and Language = "'.$lang.'"
                                order by Position;');    
        return ($result->num_rows() > 0)? $result->result_array() : array();                
    }
    
    function wd_slideShow($cateId){
        $result = $this->Slidemodel->getSlideByCateId($cateId);
        return $result;
    }

    public function getWidget($data, $lang = null){
        if(!isset($lang))
            $lang = $this->phpsession->getCookie('monpham_language');
        $result = $this->db->query('select ID,Title,Describes,Area,a.Status,Position,CateID,CatMeta,Limits,Content,Method,a.Type,Language
                                    from widgets a left join categories b on a.CateID = b.CatID
                                    where a.Status = 1 and Area = "'.$data['area'].'" and Language = "'.$lang.'"
                                    order by Position;');
        $result = $result->result_array();
        $count = count($result);
        if($count > 0){
            for($i = 0; $i < $count; $i++){
                if($result[$i]['Type'] === 1)
                    $result[$i]['data'] = $this->Categorymodel->getArtOrProListByCate($result[$i]['CateID'], $result[$i]['Limits'], $lang);
                else{
                    if($result[$i]['Method'] === 'wd_productCategories'){                            
                        $result[$i]['data'] = $this->getNestableCate($data['type'], 2, $lang);
                    }
                    if($result[$i]['Method'] === 'wd_articleCategories'){
                        $result[$i]['data'] = $this->getNestableCate($data['type'], 1, $lang);
                    }
                    if($result[$i]['Method'] === 'wd_allProduct'){
                        $result[$i]['data'] = $this->Productmodel->getAllPro($lang, $result[$i]['Limits']);
                    }
                    if($result[$i]['Method'] === 'wd_allArticle'){
                        $result[$i]['data'] = $this->Articlemodel->getAllArt($lang, $result[$i]['Limits']);
                    }
                    if($result[$i]['Method'] === 'wd_articleByCate'){
                        $result[$i]['data'] = $this->Articlemodel->getArtListForCate($result[$i]['CateID'],$lang);
                    }
                    if($result[$i]['Method'] === 'wd_productByCate'){
                        $result[$i]['data'] = $this->Productmodel->getProListForCate($result[$i]['CateID'],$lang);
                    }
                    if($result[$i]['Method'] === 'wd_featureArt'){
                        $result[$i]['data'] = $this->Articlemodel->getFeatureArt($result[$i]['Limits'], $lang);
                    }
                    if($result[$i]['Method'] === 'wd_featurePro'){
                        $result[$i]['data'] = $this->Productmodel->getFeaturePro($result[$i]['Limits'], $lang);
                    }
                    if($result[$i]['Method'] === 'wd_newProduct'){
                        $result[$i]['data'] = $this->Productmodel->getNewPro($result[$i]['Limits'], $lang);
                    }
                    if($result[$i]['Method'] === 'wd_newArticle'){
                        $result[$i]['data'] = $this->Articlemodel->getNewArt($result[$i]['Limits'], $lang);
                    }
                    if($result[$i]['Method'] === 'wd_markerList'){
                        $result[$i]['data'] = $this->Mapmodel->getMapList();
                    }
                    if($result[$i]['Method'] === 'wd_visitorStatistic'){
                        $result[$i]['data'] = $this->wd_visitorStatistic($result[$i]['Content']);
                    }
                    if($result[$i]['Method'] === 'wd_fanPage'){
                        $result[$i]['data'] = $this->wd_fanPage($result[$i]['Content']);
                    }
                    if($result[$i]['Method'] === 'wd_search'){
                        $result[$i]['data'] = $this->wd_searchBox();
                    }
                    if($result[$i]['Method'] === 'wd_slideShow'){
                        $result[$i]['data'] = $this->wd_slideShow($result[$i]['CateID']);
                    }
                }
            }
        }            
        return $result;
    }
    
    public function updateHeader($data){
        $result = false;
        $check_row = $this->db->query('select HeaderId from header;');
        if($check_row->num_rows() >= 1){
            $this->db->where('HeaderId', $data['HeaderId']);
            $result = $this->db->update('header', $data);
        }
        else{  
            $id = isset($data['HeaderId'])? $data['HeaderId'] : 1;
            $temp = array(
                'HeaderId' => $id,
                'Logo' => $data['Logo'],
                'Banner' => $data['Banner']
            );
            $result = $this->db->insert('header', $temp);
        }
        return $result;
    }
            
    public function updateWidget($param){
        $this->db->where('ID', $param['ID']);
        $result = $this->db->update('widgets', $param);
        return ($result !== null)? true : false;
    }
    
    public function addWidget($param){        
        $result = $this->db->insert('widgets', $param);
        return ($result !== null)? true : false;
    }
    
    public function delWidget($id){        
        $this->db->where('ID', $id);
        $result = $this->db->delete('widgets');
        return ($result !== null)? true : false;
    }
    
    public function updatePosition($param, $pos){
        $arg = array(
            'Area' => $param['area'],
            'Position' => $pos
        );        
        
        $this->db->where('ID', $param['id']);
        $this->db->update('widgets', $arg);
        
        $widget = $this->db->query('select Title,Status,Describes,CateID,Limits,Content,Method,Type,Language
                                        from widgets
                                        where ID = '.$param['id'].';');
        $widget = $widget->result();
        $type = $widget[0]->Type;
        $method = $widget[0]->Method;
        if($type === 0 || $method !== null){                                    
            $widgetExisted = $this->db->query('select ID from widgets 
                                                where Method = "'.$widget[0]->Method.'" and Area = "noArea";');
            if($widgetExisted->num_rows() === 0){                
                $data = array(
                    'Title' => $widget[0]->Title,
                    'Area' => 'noArea',
                    'Language' => $widget[0]->Language,
                    'Status' => $widget[0]->Status,
                    'Describes' => $widget[0]->Describes,
                    'Position' => $pos,
                    'CateID' => $widget[0]->CateID,
                    'Limits' => $widget[0]->Limits,
                    'Content' => $widget[0]->Content,
                    'Method' => $widget[0]->Method,
                    'Type' => $widget[0]->Type
                );
                $this->db->insert('widgets', $data);
            }
            else if($widgetExisted->num_rows() > 1){
                $this->db->where('ID', $param['id']);
                $this->db->delete('widgets');
            }
        }
    }
}
?>