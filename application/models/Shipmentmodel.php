<?php
defined('BASEPATH') OR exit('');

class Shipmentmodel extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('phpsession');
    }    
    
    public function getAll(){        
        $result = $this->db->query('select *
                                    from shipment
                                    where isDeleted = 0
                                    order by shipmentId desc;');
        if($result->num_rows() > 0)
            return $result->result_array();                            
        return array();
        
    }

    public function getTappingTo(){        
        $result = $this->db->query('select *, (select count(shipDetailId) from shipmentdetail b where b.shipmentId = a.shipmentId) as numberPackage, (select count(shipDetailId) from shipmentdetail b where b.shipmentId = a.shipmentId and isReceived = 1) as numberPackageReceived
                                    from shipment a
                                    where isDeleted = 0
                                    order by shipmentId desc;');
        if($result->num_rows() > 0)
            return $result->result_array();                            
        return array();
        
    }

    public function getById($id){        
        $result = $this->db->query('select *
                                    from shipment
                                    where shipmentId = '.$id.' and isDeleted = 0;');

        if($result->num_rows() > 0){
            $data = $result->result()[0]; 
            return $data;
        }
        return null;        
    }

    public function getDetailById($id){
        $result = $this->db->query('select a.*, packageCustomId, packageDatetime, fromPlaceDetail, toPlaceDetail, b.weight
                                    from shipmentdetail a join packages b on a.packageId = b.packageId
                                    where shipmentId = '.$id.'
                                    order by shipDetailId desc;');
        
        if($result->num_rows() > 0){
            $data = $result->result_array(); 
            return $data;
        }
        return array();   
    }

    public function checkExistsId($customId){
        $count = $this->db
                        ->where('isDeleted', false)
                        ->where('shipmentCustomId', $customId)
                        ->count_all_results('shipment');           
        if($count > 0)
            return false;
        return true;
    }
    
    public function update($data){
        $this->db->where('shipmentId', $data['shipmentId']);        
        $result = $this->db->update('shipment', $data);
        if($result !== null)
            return true;       
        return false;
    }

    public function tappingToPackage($shipDetailId, $isReceived){
        $this->db->where('shipDetailId', $shipDetailId);        
        $result = $this->db->update('shipmentdetail', array('isReceived' => $isReceived));
        if($result !== null)
            return true;       
        return false;
    }
    
    public function insert($data){
        $result = $this->db->insert('shipment', $data);
        if($result !== null){
            $shipmentId = $this->db->insert_id();      
            return $shipmentId;    
        }  
        return false;        
    }

    public function insertDetail($data){
        $param = array(
            'shipmentId' => $data['shipmentId'],
            'packageId' => $data['packageId']
        );
        $result = $this->db->insert('shipmentdetail', $param);
        if($result !== null)
            return true;       
        return false;        
    }
    
    public function delete($Id){
        //$result = $this->db->delete('packages', $param);
        $this->db->where('shipmentId',$Id);
        $result = $this->db->update('shipment',array('isDeleted' => true));
        if($result !== null){
            return true;
        }
        else{
            return false;
        }
    }

    public function deleteDetail($Id){
        $param = array(
            'shipDetailId' => $Id  
        );
        $result = $this->db->delete('shipmentdetail', $param);
        if($result !== null){
            return true;
        }
        else{
            return false;
        }
    }
        
}

?>