<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Resourcecatemodel extends CI_Model {
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('phpsession');
    }
    
    public function getResourceCate($lang, $type, $resType){        
        $currentLang = $this->phpsession->getCookie('monpham_language');
        if($type === 'admin'){
            if($lang !== null){
                $result = $this->db->query('select ID,Name,Language,Type
                                            from resourcecate
                                            where Type = '.$resType.' and Language = "'.$lang.'";');
            }
            else{
                $result = $this->db->query('select ID,Name,Language,Type
                                            from resourcecate
                                            Type = '.$resType.';');
            }
        }
        else{
            $result = $this->db->query('select ID,Name,Language,Type
                                        from resourcecate
                                        where Type = '.$resType.' and Language = "'.$currentLang.'";');
        }
        return ($result->num_rows() > 0)? $result->result_array() : array();
    }
    
    public function addResourceCat($data){        
        $result = $this->db->insert('resourcecate', $data);
        return ($result !== null)? true : false;
    }
    
    public function updateResourceCat($data){        
        $this->db->where('ID', $data['ID']);
        $result = $this->db->update('resourcecate', $data);
        return ($result !== null)? true : false;
    }
    
    public function delResourceCat($id){
        $this->db->where('ID', $id);
        $result = $this->db->delete('resourcecate');
        return ($result !== null)? true : false;
    }
}
?>