<?php
defined('BASEPATH') OR exit('');

class Categorymodel extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('phpsession');
    }    
        
    public function getAllCategories($type, $lang, $dataType){
        $result = array();
        $currentLang = $this->phpsession->getCookie('monpham_language');
        switch ($type){
            case 'admin':
                if($lang === null){
                    $result = $this->db->query('select CatID,CatName,CatMeta,CatDescribes,TempId,CatLang,Type,CatParent,(select CatName from categories where CatID = a.CatParent) as ParentName, Image, SeoTitle, SeoKeyword, SeoDescribes, SeoCanonica, MetaRobot
                                                from categories a
                                                order by CatID desc;');
                }
                else{
                    $result = $this->db->query('select CatID,CatName,CatMeta,CatDescribes,TempId,CatLang,Type,CatParent,(select CatName from categories where CatID = a.CatParent) as ParentName, Image, SeoTitle, SeoKeyword, SeoDescribes, SeoCanonica, MetaRobot
                                                from categories a
                                                where CatLang = "'.$lang.'"
                                                order by CatID desc;');
                }
                break;
            default:
                $result = $this->db->query('select CatID,CatName,CatMeta,CatDescribes,TempId,CatLang,Type,CatParent,(select CatName from categories where CatID = a.CatParent) as ParentName, Image, SeoTitle, SeoKeyword, SeoDescribes, SeoCanonica, MetaRobot
                                            from categories a
                                            where CatLang = "'.$currentLang.'" and Type = '.$dataType.' and Status = 1
                                            order by CatID desc;');
                break;
        }
        if($result->num_rows() > 0){
            return $result->result_array();            
        }
        else{
            return array();
        }
    }
    
    public function getCategories($type, $lang, $dataType){
        $result = array();
        $currentLang = $this->phpsession->getCookie('monpham_language');
        switch ($type){
            case 'admin':
                if($lang === null){
                    $result = $this->db->query('select CatID,CatName,CatMeta,CatDescribes,TempId,CatLang,Type,CatParent,(select CatName from categories where CatID = a.CatParent) as ParentName, Image, SeoTitle, SeoKeyword, SeoDescribes, SeoCanonica, MetaRobot
                                                from categories a
                                                where Type = '.$dataType.'
                                                order by CatID  ASC;');
                }
                else{
                    $result = $this->db->query('select CatID,CatName,CatMeta,CatDescribes,TempId,CatLang,Type,CatParent,(select CatName from categories where CatID = a.CatParent) as ParentName, Image, SeoTitle, SeoKeyword, SeoDescribes, SeoCanonica, MetaRobot
                                                from categories a
                                                where CatLang = "'.$lang.'" and Type = '.$dataType.'
                                                order by CatID  ASC;');
                }
                break;
            default:
                $result = $this->db->query('select CatID,CatName,CatMeta,CatDescribes,TempId,CatLang,Type,CatParent,(select CatName from categories where CatID = a.CatParent) as ParentName, Image, SeoTitle, SeoKeyword, SeoDescribes, SeoCanonica, MetaRobot
                                            from categories a
                                            where CatLang = "'.$currentLang.'" and Status = 1
                                            order by CatID  ASC;');
                break;
        }
        if($result->num_rows() > 0){
            return $result->result_array();
        }
        else{
            return array();
        }
    }
    
    public function updateCategory($data){
        $query = $this->db->query('select CatMeta from categories where CatID = "'.$data['CatID'].'";');
        $row = $query->result();
        $oldMeta = $row[0]->CatMeta;
        $this->db->where('CatID', $data['CatID']);
        $result = $this->db->update('categories', $data);
        $this->db->query('update navigates
                          set NavMeta = "'.$data['CatMeta'].'"
                          where NavMeta = "'.$oldMeta.'";');
        if($result !== null){
            return true;            
        }
        else{
            return false;
        }
    }
    
    public function addCategory($data){
        $result = $this->db->insert('categories', $data);
        if($result !== null){
            return true;
        }
        else{
            return false;
        }
    }
    
    public function deleteCategory($cateId){
        $param = array(
            'CatID' => $cateId  
        );
        $result = $this->db->delete('categories', $param);
        if($result !== null){
            return true;
        }
        else{
            return false;
        }
    }
    
    function getArtOrProListByCate($cateId, $limit, $lang){
        if($cateID !== ''){
            $arr = explode(',',$cateID);            
            $query = "select ProID as ID,ProName as Name,ProDescribes as Describes,ProMeta as Meta,Image,DateCreated,a.CatId,Author,Username,ViewCount,Video
                                from products a left join users c on a.Author = c.UserID
                                where a.CatID";
            $count = count($arr);
            for($i = 0; $i < $count - 1; $i++){                
                if($i === 0)
                    $query .= " like '%".$arr[$i]."%'";
                else $query .= " or a.CatID like '%".$arr[$i]."%'";
            }            
            $query .= " and ProID <> ".$ProID." and a.Status = 1 and ProLang = '".$lang."' order by DateCreated desc limit ".$limit;            
            $query .= "union all";
            $query .= "select ArtID as ID,ArtName as Name,ArtDescribes as Describes,ArtMeta as Meta,Image,DateCreated,a.CatId,Author,Username,ViewCount,Video
                    from articles a left join users c on a.Author = c.UserID
                    where a.CatID";            
            for($i = 0; $i < $count - 1; $i++){                
                if($i === 0)
                    $query .= " like '%".$arr[$i]."%'";
                else $query .= " or a.CatID like '%".$arr[$i]."%'";
            }            
            $query .= " and ArtID <> ".$artId." and a.Status = 1 and ArtLang = '".$lang."'  and Status = 1 order by DateCreated desc limit ".$limit;            
            $result = $this->db->query($query);

            if($result->num_rows() != 0)
                return $result->result_array();
            else return array();
        }
        else return array();
    }
      
    public function getCategory($meta, $lang = null){  
        if(!isset($lang))      
            $lang = $this->phpsession->getCookie('monpham_language');
        $result = $this->db->query('select CatID,CatName,CatDescribes,CatMeta,(select CatName 
                                            from categories 
                                            where CatID = (select CatParent from categories where CatMeta = "'.$meta.'")) as ParentName,(select CatMeta 
                                            from categories 
                                            where CatID = (select CatParent from categories where CatMeta = "'.$meta.'")) as ParentMeta,
                                            SeoTitle, SeoKeyword, SeoDescribes, Image, SeoCanonica, MetaRobot
                                    from categories 
                                    where CatMeta = "'.$meta.'" and CatLang = "'.$lang.'" and Status = 1  order by ID CatID');
        
        $category = null;
        if($result->num_rows() > 0){
            $row = $result->result();
            $category = $row[0];
        }        
        return $category;
    }

    public function editCategory($catId){
        $result = $this->db->query('select CatID,CatName,CatMeta,CatDescribes,TempId,CatLang,Type,CatParent,(select CatName from categories where CatID = a.CatParent) as ParentName, Image, SeoTitle, SeoKeyword, SeoDescribes, SeoCanonica, MetaRobot, Status
        from categories a where CatID = '.$catId);
        return ($result->num_rows() > 0)? $result->result_array() : array();
    }
    
    public function getListCateOfArtOrPro($STR_cateId){
        $cateList = array();
        
        if(isset($STR_cateId) && $STR_cateId !== ''){
            $queryString = 'select CatName, CatMeta from categories a
                    where Status = 1 and a.CatID in (';
                        
            $Arr_cateId = explode(',',$STR_cateId);
            $countList = count($Arr_cateId);
            for($i = 1; $i < $countList-1; $i++){
                if($i == $countList - 2)
                    $queryString .= $Arr_cateId[$i];
                else $queryString .= $Arr_cateId[$i].',';
            }
            
            $queryString .= ')';
            $cateList = $this->db->query($queryString);
            return $cateList->result();
        }
        return $cateList;
    }
                
}

?>