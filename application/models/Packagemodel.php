<?php
defined('BASEPATH') OR exit('');

class Packagemodel extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('phpsession');
    }    
    
    public function getAll(){        
        $result = $this->db->query('select a.*, b.Name as serviceName
                                    from packages a join services b on a.serviceId = b.ServiceId
                                    where isDeleted = 0
                                    order by packageId desc;');
        if($result->num_rows() > 0)
            return $result->result_array();                            
        return array();
        
    }

    public function getTappingTo(){        
        $result = $this->db->query('select *, b.Name as serviceName, (select count(packDetailId) from packagedetail b where b.packageId = a.packageId) as numberBill, (select count(packDetailId) from packagedetail b where b.packageId = a.packageId and isReceived = 1) as numberBillReceived
                                    from packages a join services b on a.serviceId = b.ServiceId join shipmentdetail c on a.packageId = c.packageId
                                    where a.isDeleted = 0 and c.isReceived = 1
                                    order by a.packageId desc;');
        if($result->num_rows() > 0)
            return $result->result_array();
        return array();
        
    }

    public function getById($id){        
        $result = $this->db->query('select a.*, b.Name as serviceName
                                    from packages a join services b on a.serviceId = b.ServiceId
                                    where packageId = '.$id.' and isDeleted = 0;');

        if($result->num_rows() > 0){
            $data = $result->result()[0]; 
            return $data;
        }
        return null;        
    }

    public function getByCode($id){        
        $result = $this->db->query("select *
                                    from packages
                                    where packageCustomId = '".$id."' and isDeleted = 0;");

        if($result->num_rows() > 0){
            $data = $result->result()[0]; 
            return $data;
        }
        return null;        
    }

    public function getDetailById($id){
        $result = $this->db->query('select a.*, b.*, c.Name as CustomerName, c.Phone as CustomerPhone, c.Address as CustomerAddress, c.Email as CustomerEmail, d.UserName
                                    from packagedetail a join bills b on a.billId = b.billId left join customers c on b.customerSend = c.ID join users d on b.userId = d.UserID
                                    where packageId = '.$id.' and b.isDeleted = 0
                                    order by packDetailId desc;');
        
        if($result->num_rows() > 0){
            $data = $result->result_array(); 
            return $data;
        }
        return array();   
    }

    public function checkExistsId($customId){
        $count = $this->db
                        ->where('isDeleted', false)
                        ->where('packageCustomId', $customId)
                        ->count_all_results('packages');           
        if($count > 0)
            return false;
        return true;
    }
    
    public function update($data){
        $this->db->where('packageId', $data['packageId']);        
        $result = $this->db->update('packages', $data);
        if($result !== null)
            return true;       
        return false;
    }
    
    public function insert($data){
        $result = $this->db->insert('packages', $data);
        if($result !== null){
            $packageId = $this->db->insert_id();      
            return $packageId;    
        }  
        return false;        
    }

    public function insertDetail($data){
        $param = array(
            'packageId' => $data['packageId'],
            'billId' => $data['billId']
        );
        $result = $this->db->insert('packagedetail', $param);
        if($result !== null)
            return true;       
        return false;        
    }
    
    public function delete($Id){
        //$result = $this->db->delete('packages', $param);
        $this->db->where('packageId',$Id);
        $result = $this->db->update('packages',array('isDeleted' => true));
        if($result !== null){
            return true;
        }
        else{
            return false;
        }
    }

    public function deleteDetail($Id){
        $param = array(
            'packDetailId' => $Id  
        );
        $result = $this->db->delete('packagedetail', $param);
        if($result !== null){
            return true;
        }
        else{
            return false;
        }
    }
        
    public function tappingToBill($packDetailId, $isReceived){
        $this->db->where('packDetailId', $packDetailId);        
        $result = $this->db->update('packagedetail', array('isReceived' => $isReceived));
        if($result !== null)
            return true;       
        return false;
    }
}

?>