<?php
defined('BASEPATH') OR exit('');

class Materplantypemodel extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }  
    
    public function getAll(){

        $result = $this->db->query('select * from master_plan_type WHERE Status = 1');
        if($result->num_rows() > 0){
            return $result->result_array();            
        }
        else{
            return array();
        }
    }
    
    public function updateDB($data){

        $this->db->where('id', $data['id']);        
        $result = $this->db->update('master_plan_type', $data); 
        if($result !== null){
            return TRUE;
        }
        else{
            return null;
        } 
    }   
            
    public function insertDB($data){
        $result = $this->db->insert('master_plan_type', $data);
        if($result !== null){
            return true;
        }
        else{
            return false;
        }
    }

    
    
    public function deleteDB($id){
        $param = array(
            'Status' => 0  
        );
        $this->db->where('id', $id);   
        $result = $this->db->update('master_plan_type', $param);
        if($result !== null){
            return true;
        }
        else{
            return false;
        }
    }
    
}

?>