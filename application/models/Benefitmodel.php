<?php
defined('BASEPATH') OR exit('');

class Benefitmodel extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }  
    
    public function getAll(){

        $result = $this->db->query('select * from benefit_list order by id asc;');
        if($result->num_rows() > 0){
            return $result->result_array();            
        }
        else{
            return array();
        }
    }
    
    public function update($data){

        $this->db->where('id', $data['id']);        
        $result = $this->db->update('benefit_list', $data); 
        if($result !== null){
            return true;
        }
        else{
            return null;
        } 
    }
    
    public function insert($data){
        $result = $this->db->insert('benefit_list', $data);
        if($result !== null){
            return true;
        }
        else{
            return false;
        }
    }

    public function delete($id){
        if(is_array($id)){
            $this->db->where_in('id', $id);
        }else{
            $this->db->where('id', $id);
        }
        $delete = $this->db->delete('benefit_list');
        return $delete ? true : false;
    }
}

?>
