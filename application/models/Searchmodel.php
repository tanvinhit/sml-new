<?php
    class Searchmodel extends CI_Model{
        
        public function __construct(){
            parent::__construct();            
            $this->load->database();
        }
        
        public function recordCount($langId, $keyword){
            $query = $this->db->query('select ArtName as name, Image as image, ArtMeta as meta, ArtDescribes as intro
                                        from articles 
                                        where ArtName like "%'.$keyword.'%"
                                        or ArtDescribes like "%'.$keyword.'%"
                                        or Content like "%'.$keyword.'%"
                                        and ArtLang = "'.$langId.'"'
                                        .'union all
                                        select CatName as name, Image as image, CatMeta as meta, CatDescribes as intro
                                        from categories
                                        where CatName like "%'.$keyword.'%"
                                        or CatDescribes like "%'.$keyword.'%"                                        
                                        and CatLang = "'.$langId.'"'
                                        .'union all
                                        select ProName as name, Image as image, ProMeta as meta, ProDescribes as intro
                                        from products 
                                        where ProName like "%'.$keyword.'%"
                                        or ProDescribes like "%'.$keyword.'%"
                                        or Content like "%'.$keyword.'%"
                                        and ProLang = "'.$langId.'"'
                                     );
            return $query->num_rows();
        }
        
        public function getResult($langId, $keyword, $limit, $page){            
            $query = array();            
            $query = $this->db->query('select ArtName as name, Image as image, ArtMeta as meta, ArtDescribes as intro, DateCreated, 1 as type
                                        from articles 
                                        where ArtName like "%'.$keyword.'%"
                                        or ArtDescribes like "%'.$keyword.'%"
                                        or Content like "%'.$keyword.'%"
                                        and ArtLang = "'.$langId.'"' 
                                        .'union all
                                        select CatName as name, Image as image, CatMeta as meta, CatDescribes as intro, "" as DateCreated, 2 as type
                                        from categories
                                        where CatName like "%'.$keyword.'%"
                                        or CatDescribes like "%'.$keyword.'%"                                        
                                        and CatLang = "'.$langId.'"'
                                        .'union all
                                        select ProName as name, Image as image, ProMeta as meta, ProDescribes as intro, DateCreated, 3 as type
                                        from products 
                                        where ProName like "%'.$keyword.'%"
                                        or ProDescribes like "%'.$keyword.'%"
                                        or Content like "%'.$keyword.'%"
                                        and ProLang = "'.$langId.'"'
                                        .' limit '.$limit.', '.$page);
            
            if($query->num_rows() > 0)
                return $query->result_array();
            else return array();
        }
    }
?>