<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Resourcemodel extends CI_Model {
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('phpsession');
    }
    
    public function getResources($lang, $type, $resCate){
        $currentLang = $this->phpsession->getCookie('monpham_language');
        if($type === 'admin'){
            if($lang !== null){
                $result = $this->db->query('select ResID,ResName,ResCate,b.Name,ResLang,CateLink,ProLink,Image,Frame,File,Describes
                                            from resources a join resourcecate b on a.ResCate=b.ID
                                            where ResCate = '.$resCate.' and ResLang = "'.$lang.'";');
            }
            else{
                $result = $this->db->query('select ResID,ResName,ResCate,b.Name,ResLang,CateLink,ProLink,Image,Frame,File,Describes
                                            from resources a join resourcecate b on a.ResCate=b.ID
                                            where ResCate = '.$resCate.';');
            }
        }
        else{ 
            $result = $this->db->query('select ResID,ResName,ResCate,b.Name,ResLang,CateLink,CatName,CatMeta,ProLink,ArtName,ArtMeta,a.Image,Frame,File,ResCate,a.Describes
                                            from resources a join resourcecate b on a.ResCate=b.ID left join categories c on a.CateLink = c.CatID left join articles d on a.ProLink = d.ArtID
                                            where b.Type = '.$resCate.' and ResLang = "'.$currentLang.'";');
        }
        return ($result->num_rows() > 0)? $result->result_array() : array();
    }
            
    public function addResource($data){        
        $result = $this->db->insert('resources', $data);
        return ($result !== null)? true : false;
    }
    
    public function updateResource($data){        
        $this->db->where('ResID', $data['ResID']);
        $result = $this->db->update('resources', $data);
        return ($result !== null)? true : false;
    }
    
    public function delResource($resId){
        $this->db->where('ResID', $resId);
        $result = $this->db->delete('resources');
        return ($result !== null)? true : false;
    }
    
}
?>