<?php echo '<?xml version="1.0" encoding="UTF-8" ?>' ?>
 
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc><?php echo base_url();?></loc>
        <priority>1.0</priority>
    </url>
 
    <!-- Your Sitemap -->
    <?php foreach($urlsList as $url) { ?>
    <url>       
        <loc>
            <?= base_url().$url['Url']; ?>
        </loc>
        <?php
            if($url['Type'] == 1)
                echo '<changefreq>monthly</changefreq><priority>0.8</priority>';
            else if($url['Type'] == 2)
                echo '<changefreq>daily</changefreq><priority>0.9</priority>';
            else if($url['Type'] == 3)
                echo '<changefreq>daily</changefreq><priority>0.9</priority>';
            else
                echo '<changefreq>daily</changefreq><priority>1</priority>';
            ?>
    </url>
    <?php } ?>
 
</urlset>