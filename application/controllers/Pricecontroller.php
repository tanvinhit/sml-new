<?php 

class Pricecontroller extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library(array('phpsession','emailhandler'));
        $this->load->model(array('Pricemodel'));
        date_default_timezone_set('Asia/Ho_Chi_Minh');
    }
    
    public function getAll(){
        $query = $this->Pricemodel->getAll();
        if($query)
            echo json_encode($query);
        else echo null;
    }

    public function getByCondition($fromPlace, $toPlace, $dcpWeight, $serviceId){
        $query = $this->Pricemodel->getByCondition($fromPlace, $toPlace, $dcpWeight, $serviceId);
        if($query)
            echo json_encode($query, JSON_NUMERIC_CHECK);
        else echo null;
    }
    
    public function insert(){
        $data = $this->input->post('data');
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $exists = $this->Pricemodel->getByCondition($data['fromPlace'], $data['toPlace'], $data['dcpWeight'], $data['service']);
            if(!isset($exists))
                $query = $this->Pricemodel->insert($data);
            else{
                $data['priceId'] = $exists->priceId;
                $query = $this->Pricemodel->update($data);
            }
            $result = array(
                'success' => true,
                'data' => $query
            );
            $json = json_encode($result);
            echo $json;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }  
    }

    public function insertMany(){
        $data = $this->input->post('data');
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $countList = count($data);
            $delete = $this->Pricemodel->deleteByArray($data);
            for($i = 0; $i < $countList; $i++){
                $query = $this->Pricemodel->insert($data[$i]);
            }
            $result = array(
                'success' => true,
                'data' => $query
            );
            $json = json_encode($result);
            echo $json;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }  
    }

    public function update(){
        $data = $this->input->post('data');
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $query = $this->Pricemodel->update($data);
            
            if($query)
                echo true;
            else echo false;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        } 
    }
    
    public function delete($id){
        $id = intval($id);
        $query = $this->Pricemodel->delete($id);
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            if($query)
                echo true;
            else echo false;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        } 
    }

    // public function getById($id){
    //     $id = intval($id);
    //     $query = $this->Pricemodel->getById($id);
        
    //     if($query)
    //         echo json_encode($query);
    //     else echo false;
    // }
    
    public function deleteMulti(){
        $request = $this->input->post('data');
        $countList = count($request);
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            for($i = 0; $i < $countList; $i++){
                $this->Pricemodel->delete($request[$i]);
            }
            echo json_encode(true);
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }
    }
}

?>