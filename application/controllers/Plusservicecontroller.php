<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Plusservicecontroller extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        //$this->load->library('session');
        $this->load->library('phpsession');
        $this->load->model('Plusservicemodel');
    }
                    
    public function getAll(){
        $result = $this->Plusservicemodel->getAll();                
        $json = json_encode($result);                            
        echo $json;
    }
    
    public function update(){
        $request = $this->input->post('data');
        
        //$session = $this->session->has_userdata('remember_me');        
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $request['isDeleted'] = false;
            $result = $this->Plusservicemodel->update($request);            
            $json = json_encode($result);
            echo $json;    
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }                
    }
    
    public function insert(){
        $request = $this->input->post('data');                                
        
        //$session = $this->session->has_userdata('remember_me');   
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = $this->Plusservicemodel->insert($request);            
            $json = json_encode($result);
            echo $json;    
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }
    }
    
    public function delete(){
        $request = $this->input->post('data');
        $Id = $request['plusServId'];
        
        //$session = $this->session->has_userdata('remember_me');
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = $this->Plusservicemodel->delete($Id);
            $json = json_encode($result);
            echo $json;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }            
    }
    
}

?>