<?php 

class Billcontroller extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library(array('phpsession','emailhandler'));
        $this->load->model(array('Billmodel', 'Billdetailmodel', 'Templatemodel'));
        date_default_timezone_set('Asia/Ho_Chi_Minh');
    }
    
    private function getCurrentLanguage(){
        $checkLang = $this->phpsession->getCookie('monpham_language');
        if($checkLang === null){
            $language = 'vi';
            $this->phpsession->saveCookie($language, 'monpham_language');            
        }
        $checkLang = $this->phpsession->getCookie('monpham_language');        
        return $checkLang;
    }

    public function index(){
        $lookup = $this->input->get('lookup');
        $filename = "lookup/lookup-bill";
        $currentLanguage = $this->getCurrentLanguage();
        $meta = $this->Templatemodel->getMetaByFileName($filename, $currentLanguage);  
        if(isset($meta)){
            $meta = substr($meta, 0, strlen($meta) - strlen('.html'));        
            return redirect($meta . "?lookup=" . $lookup);
        }
        return redirect('404');
    }

    public function getAll(){
        $request = $this->input->post('data');
        $query = $this->Billmodel->getAll($request['type'], $request['user_id'],$request['fromDate'],$request['toDate']);
        if($query)
            echo json_encode($query);
        else echo null;
    }
    
    public function insert(){
        $data = $this->input->post('data');        
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){            
            $isExists = $this->Billmodel->checkExistsBillId($data['billCustomId']);
            if($isExists){
                if (isset($data)) {
                    $query = $this->Billmodel->insert($data);
                $result = array(
                    'success' => true,
                    'data' => $query
                );
                $json = json_encode($result);
                echo $json;
                 }
                
            }
            else{
                $result = array(
                    'success' => false,
                    'message' => 'Mã phiếu đã tồn tại. Vui lòng đổi mã khác'
                ); 
                $json = json_encode($result);
                echo $json;
            }
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }  
    }

    public function insertMany(){
        $data = $this->input->post('data');
        $session = $this->phpsession->get(null, 'monpham_user');
        $resultArr = array();
        $resultErr = array();

        if($session !== null){
            $countList = count($data);            
            $query = $this->Billmodel->getByArray($data);
            
            for($i = 0; $i < $countList; $i++){     
                if(count($query) > 0) {
                    $isFind = (array_search($data[$i]['billCustomId'], $query) !== NULL);
                    if(!$isFind)
                    {
                        $id = $this->Billmodel->insert($data[$i]);
                        if(isset($id))
                            array_push($resultArr, $id);
                        else
                            array_push($resultErr, $data[$i]['billCustomId']);
                    }
                    else
                        array_push($resultErr, $data[$i]['billCustomId']);
                }
                else{
                    $id = $this->Billmodel->insert($data[$i]);
                    if(isset($id))
                        array_push($resultArr, $id);
                    else
                        array_push($resultErr, $data[$i]['billCustomId']);
                }
            }
            
            $result = array(
                'success' => true,
                'data' => $resultArr,
                'error' => $resultErr
            );
            $json = json_encode($result);
            echo $json;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }  
    }

    public function update(){
        $data = $this->input->post('data');
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $data['isDeleted'] = false;
            $query = $this->Billmodel->update($data);
            
            if($query)
                echo true;
            else echo false;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        } 
    }
    
    public function updateStatus(){
        $data = $this->input->post('data');
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $query = $this->Billmodel->updateStatus($data);
            
            if($query)
                $this->getAll();
            else echo false;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        } 
    }
    
    public function delete($id){
        $id = intval($id);
        $query = $this->Billmodel->delete($id);
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            if($query)
                echo true;
            else echo false;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        } 
    }

    public function getById($id){
        $id = intval($id);
        $query = $this->Billmodel->getById($id);
        
        if($query)
            echo json_encode($query, JSON_NUMERIC_CHECK);
        else echo false;
    }

    public function getByCode($code){
        $query = $this->Billmodel->getByCode($code);
        
        if($query)
            echo json_encode($query);
        else echo false;
    }

    // public function getDetailById($id){
    //     $id = intval($id);
    //     $query = $this->Billdetailmodel->getById($id);
        
    //     if($query)
    //         echo json_encode($query);
    //     else echo false;
    // }

    public function deleteMulti(){
        $request = $this->input->post('data');
        $countList = count($request);
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            for($i = 0; $i < $countList; $i++){
                $this->Billmodel->delete($request[$i]);
            }
            echo json_encode(true);
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }
    }

    public function getByProvince(){
        $request = $this->input->post('data');
        $prov_id = $request['prov_id'];
        $query = $this->Billmodel->getByProvince($prov_id);
        if($query)
            echo json_encode($query);
        else echo null;
    }
}

?>