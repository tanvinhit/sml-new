<?php 

class Cartcontroller extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library(array('phpsession','emailhandler'));
        $this->load->model(array('Ordermodel', 'Orderdetailmodel', 'Templatemodel'));
        date_default_timezone_set('Asia/Ho_Chi_Minh');
    }
    
    private function getCurrentLanguage(){
        $checkLang = $this->phpsession->getCookie('monpham_language');
        if($checkLang === null){
            $language = 'vi';
            $this->phpsession->saveCookie($language, 'monpham_language');            
        }
        $checkLang = $this->phpsession->getCookie('monpham_language');        
        return $checkLang;
    }

    public function index(){
        $filename = "cart/cart-detail";
        $currentLanguage = $this->getCurrentLanguage();
        $meta = $this->Templatemodel->getMetaByFileName($filename, $currentLanguage);  
        if(isset($meta)){
            $meta = substr($meta, 0, strlen($meta) - strlen('.html'));        
            return redirect($meta);
        }
        return redirect('404');
    }

    public function customerInformation(){
        $filename = "cart/cart-customer-information";
        $currentLanguage = $this->getCurrentLanguage();
        $meta = $this->Templatemodel->getMetaByFileName($filename, $currentLanguage);  
        if(isset($meta)){
            $meta = substr($meta, 0, strlen($meta) - strlen('.html'));        
            return redirect($meta);
        }
        return redirect('404');
    }

    public function confirmToOrder(){
        $filename = "cart/confirm-to-order";
        $currentLanguage = $this->getCurrentLanguage();
        $meta = $this->Templatemodel->getMetaByFileName($filename, $currentLanguage);  
        if(isset($meta)){
            $meta = substr($meta, 0, strlen($meta) - strlen('.html'));        
            return redirect($meta);
        }
        return redirect('404');
    }

    public function addItemIntoCart(){
        $product = json_decode(file_get_contents('php://input'));             
        $currentCart = $this->phpsession->get(null, 'cart');
        if(!isset($currentCart)){  
            $newCart = array();
            array_push($newCart, $product);
            $this->phpsession->save(null, $newCart, 'cart');            
        }
        else {
            $isExists = false;
            for($i = 0; $i < count($currentCart); $i++){
                if($currentCart[$i]->ID === $product->ID){
                    $currentCart[$i]->quantity += $product->quantity;
                    $isExists = true;
                    break;
                }
            }
            if(!$isExists){
                array_push($currentCart, $product);
            }
            $this->phpsession->save(null, $currentCart, 'cart');
        }
        $currentCart = $this->phpsession->get(null, 'cart');
        echo count($currentCart);
    }
    
    public function removeCart(){
        $this->phpsession->clear(null, 'cart');
    }
    
    public function removeItemInCart($productId){
        $currentCart = $this->phpsession->get(null, 'cart');        
        if(isset($currentCart)){  
            for($i = 0; $i < count($currentCart); $i++){
                if($currentCart[$i]->ID == $productId){
                    array_splice($currentCart, $i, 1);                       
                    break;
                }                
            }            
            $this->phpsession->save(null, $currentCart, 'cart');            
        }       
    }
    
    public function updateQuantityItemInCart($productId, $newQuantity){
        $currentCart = $this->phpsession->get(null, 'cart');
        $totalPrice = 0;
        if(isset($currentCart)){  
            for($i = 0; $i < count($currentCart); $i++){
                if($currentCart[$i]->ID === $productId){
                    $currentCart[$i]->quantity = $newQuantity;                    
                }
                $totalPrice += (intval($currentCart[$i]->price) - intval($currentCart[$i]->sale)) * intval($currentCart[$i]->quantity); 
            }
            $this->phpsession->save(null, $currentCart, 'cart');
            echo $totalPrice;
        }
    }
    
    public function setCustomerOrder() {
        $customer = json_decode(file_get_contents('php://input'));
        $this->phpsession->save(null, $customer, 'cart_customer');
    }
    
    public function orderConfirmation() {
        $session_customer = $this->phpsession->get(null, 'cart_customer');
        $session_cart = $this->phpsession->get(null, 'cart');
        
        if($session_customer !== null){
            $session_customer->createdDate = date('Y-m-d');
        }
        $purchaseId = $this->Ordermodel->insert($session_customer);
        
        $data['session_cart'] = $session_cart;
        $data['session_customer'] = (array) $session_customer;
        
        $data['orderCode'] = 'QMĐH' . date('d'). date('m'). date('y') . $purchaseId;
        $data['sumMoney'] = 0;
        foreach($session_cart as $item) {
            if($item->sale > 0)
                $data['sumMoney'] += $item->sale * $item->quantity;
            else $data['sumMoney'] += $item->price * $item->quantity;
            $this->Orderdetailmodel->insert($item, $purchaseId);
        }
        
        //Send mail
        $result = $this->emailhandler->sendEmail('ModunSoft Co.,Ltd', 'Đơn hàng', $session_customer->email, $this->load->view('email',$data,TRUE), 'info@modunsoft.com');

        if (!$result['success'])
        {
            echo $result['message'];
        }
        else{
            $this->phpsession->clear(null, 'cart');
            $this->phpsession->clear(null, 'cart_customer');
            echo 'OK';
        }        
    }

    public function getAll(){
        $query = $this->Ordermodel->getAll();
        if($query)
            echo json_encode($query);
        else echo null;
    }
    
    public function insert(){
        $data = $this->input->post('data');
        $data['createdDate'] = date('Y-m-d');

        $query = $this->Ordermodel->insert($data);
        echo $query;
    }

    public function update(){
        $data = $this->input->post('data');
        $query = $this->Ordermodel->update($data);
        
        if($query)
            $this->getAll();
        else echo false;
    }
    
    public function updateStatus(){
        $data = $this->input->post('data');
        $query = $this->Ordermodel->updateStatus($data);
        
        if($query)
            $this->getAll();
        else echo false;
    }
    
    public function delete($id){
        $id = intval($id);
        $query = $this->Ordermodel->delete($id);
        
        if($query)
            $this->getAll();
        else echo false;
    }
    public function getById($id){
        $id = intval($id);
        $query = $this->Ordermodel->getById($id);
        
        if($query)
            echo json_encode($query);
        else echo false;
    }

    public function deleteMultiOrder(){
        $request = $this->input->post('data');
        $countList = count($request);
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            for($i = 0; $i < $countList; $i++){
                $this->Ordermodel->delete($request[$i]);
            }
            echo json_encode(true);
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }
    }
}

?>