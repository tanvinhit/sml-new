<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servicecontroller extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        //$this->load->library('session');
        $this->load->library('phpsession');
        $this->load->model('Servicemodel');
    }
                    
    public function getServices($lang = null){
        $request = $this->input->post('data');
        $type = $request['type'];
        $result = $this->Servicemodel->getAllServices($type, $lang);                
        $json = json_encode($result);                            
        echo $json;
    }

    public function getById($serviceId){
        $result = $this->Servicemodel->getById($serviceId);               
        $json = json_encode($result);
        echo $json;
    }
    
    public function updateService(){
        $request = $this->input->post('data');
        $Id = $request['ID'];
        $serviceId = $request['ServiceId'];
        $name = $request['Name'];
        $des = ($request['Describes'])? $request['Describes'] : '';
        $img = isset($request['Image'])? $request['Image'] : '';                
        $lang = $request['Language'];
        $url = $request['Url'];
                               
        //$session = $this->session->has_userdata('remember_me');        
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = $this->Servicemodel->updateService($Id, $name, $des, $img, $lang, $url, $serviceId);            
            $json = json_encode($result);
            echo $json;    
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }                
    }
    
    public function addService(){
        $request = $this->input->post('data');      
        $serviceId = $request['ServiceId'];                          
        $name = $request['Name'];
        $des = ($request['Describes'])? $request['Describes'] : '';
        $img = isset($request['Image'])? $request['Image'] : '';                
        $lang = $request['Language'];
        $url = $request['Url'];
        
        //$session = $this->session->has_userdata('remember_me');   
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            if(!$this->Servicemodel->checkExistsId($data['ServiceId'])){
                $result = array(
                    'success' => false,
                    'message' => 'Mã đã tồn tại. Vui lòng đổi mã khác'
                ); 
                $json = json_encode($result);
                return $json;
            }

            $result = $this->Servicemodel->addService($name, $des, $img, $lang, $url, $serviceId); 
            $response = array(
                'success' => true,
                'data' => $result
            );            
            $json = json_encode($response);
            echo $json;    
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }
    }
    
    public function deleteService(){
        $request = $this->input->post('data');
        $Id = $request['ID'];
        
        //$session = $this->session->has_userdata('remember_me');
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = $this->Servicemodel->deleteService($Id);
            $json = json_encode($result);
            echo $json;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }            
    }
    
}

?>