<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slidecontroller extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        //$this->load->library('session');
        $this->load->library('phpsession');
        $this->load->model('Slidemodel');
    }              
                
    public function getSlides($lang = null){
        $request = $this->input->post('data');
        $type = $request['type'];
        $result = $this->Slidemodel->getAllSlides($type, $lang);
        if($type === 'admin') {
            echo json_encode($result);
        }
        else {
            echo $result;            
        }
    }
    
    public function updateSlide(){
        $request = $this->input->post('data');
        $data = array(
            'ID' => $request['ID'],
            'Image' => isset($request['Image'])? $request['Image'] : '',
            'Describes' => isset($request['Describes'])? $request['Describes'] : '',
            'ProLink' => isset($request['ProLink'])? $request['ProLink'] : 0,
            'CateLink' => isset($request['CateLink'])? $request['CateLink'] : 0,        
            'Language' => $request['Language'],
            'Title' => isset($request['Title'])? $request['Title'] : '',
            'CateId' => isset($request['Title'])? $request['CateId'] : '',
            'sort' => isset($request['Title'])? $request['sort'] : ''
        );
                               
        //$session = $this->session->has_userdata('remember_me');        
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = $this->Slidemodel->updateSlide($data);            
            $json = json_encode($result);
            echo $json;    
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }                
    }
    
    public function addSlide(){
        $request = $this->input->post('data');                        
        $data = array(            
            'Image' => isset($request['Image'])? $request['Image'] : '',
            'Describes' => isset($request['Describes'])? $request['Describes'] : '',
            'ProLink' => isset($request['ProLink'])? $request['ProLink'] : 0,
            'CateLink' => isset($request['CateLink'])? $request['CateLink'] : 0,        
            'Language' => $request['Language'],
            'Title' => isset($request['Title'])? $request['Title'] : '',
            'CateId' => isset($request['CateId'])? $request['CateId'] : ''
        );    
        
        //$session = $this->session->has_userdata('remember_me');   
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = $this->Slidemodel->addSlide($data);            
            $json = json_encode($result);
            echo $json;    
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }
    }
    
    public function deleteSlide(){
        $request = $this->input->post('data');
        $Id = $request['ID'];
        
        //$session = $this->session->has_userdata('remember_me');
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = $this->Slidemodel->deleteSlide($Id);
            $json = json_encode($result);
            echo $json;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }            
    }
    
    public function changeSlidePosition(){
        $request = $this->input->post('data');        
        for($i = 0; $i < count($request); $i++){
            $this->Slidemodel->changeSlidePosition($request[$i]['id'], $request[$i]['pos']);
        }        
    }
    
    public function getHtmlSlideshow(){
        $lang = $this->phpsession->getCookie('monpham_language');
        $result = $this->Slidemodel->getAllSlides('client', $lang);
        echo $result;
    }
    
    public function getByCateId($cateId){
        $result = $this->Slidemodel->getSlideByCateId($cateId);
        if(count($result)> 0)
            echo json_encode($result);
        else echo null;
    }

    public function deleteMultiSlide(){
        $request = $this->input->post('data');
        $countList = count($request);
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            for($i = 0; $i < $countList; $i++){
                $this->Slidemodel->deleteSlide($request[$i]);
            }
            echo json_encode(true);
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }
    }
}

?>