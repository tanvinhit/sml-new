<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Articlecontroller extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('phpsession');        
        $this->load->model('Articlemodel');
    }
         
    public function loadAllArticles($lang = null){
        $request = $this->input->post('data');
        $type = $request['type'];        
        $result = $this->Articlemodel->getAllArticles($type, $lang);        
        $json = json_encode($result);            
               
        echo $json;
    }
     
    public function loadArticles($lang = null){
        $request = $this->input->post('data');
        $type = $request['type'];        
        $result = $this->Articlemodel->getArticles($type, $lang);        
        $json = json_encode($result);             
               
        echo $json;
    }
    
    public function updateArticle(){
        $request = $this->input->post('data');
      
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){  
            $request['ArtMeta'] = $request['ArtMeta'].'.html';
            $request['SeoCanonica'] = $request['SeoCanonica'].'.html';          
            $result = $this->Articlemodel->updateArticle($request);
            $json = json_encode($result);
            echo $json;    
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }                
    }
    
    public function addArticle(){
        $request = $this->input->post('data');                        
      
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $request['ArtMeta'] = $request['ArtMeta'].'.html';
            $request['SeoCanonica'] = $request['SeoCanonica'].'.html';
            $result = $this->Articlemodel->addArticle($request);
            $json = json_encode($result);
            echo $json;    
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }
    }
    
    public function deleteArticle(){
        $request = $this->input->post('data');
        $Id = $request['id'];
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = $this->Articlemodel->deleteArticle($Id);
            $json = json_encode($result);
            echo $json;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }            
    }        
       
    public function editArticle($artId){
        $result = $this->Articlemodel->editArticle($artId);
        $json = json_encode($result);
        echo $json;
    }
    
    public function viewArt(){
        $request = $this->input->post('data');
        $meta = $request['meta'];
        $result = $this->Articlemodel->getArtOrPro($meta);
        $json = json_encode($result);
        echo $json;
    }
    
    public function delMultiArticles(){
        $request = $this->input->post('data');
        $countList = count($request);
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            for($i = 0; $i < $countList; $i++){
                $this->Articlemodel->deleteArticle($request[$i]);
            }
            echo json_encode(true);
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }
    }

    public function getArtMeta(){
        $request = $this->input->post('data');
        $meta = $request['meta'];
        $lang = "vi";
        $result = $this->Articlemodel->getArt($meta, $lang);
        $json = json_encode($result);
        echo $json;
    }
}

?>