<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Languagecontroller extends CI_Controller {
	    
    public function __construct(){   
        parent::__construct();
        $this->load->helper('url');  
        $this->load->library('phpsession');
        $this->load->model('Languagemodel');        
    }
        
    public function getAllLanguages(){
        $result = $this->Languagemodel->getLanguages();
        $currentLang = $this->phpsession->getCookie('monpham_language');
        $lang = array(
            'ID' => 0,
            'Name' => 'Current',
            'Code' => $currentLang,
            'Icon' => '',
            'Status' => 1
        );
        
        array_unshift($result, $lang);
        $json = json_encode($result);
        echo $json;
    }
    
    public function updateLanguage(){
        $request = $this->input->post('data');  
                             
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = $this->Languagemodel->updateLanguage($request);            
            $json = json_encode($result);
            echo $json;    
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }                
    }
    
    public function addLanguage(){
        $request = $this->input->post('data');                
                        
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = $this->Languagemodel->addLanguage($request);            
            $json = json_encode($result);
            echo $json;    
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }
    }
    
    public function deleteLanguage(){
        $request = $this->input->post('data');
        $id = $request['ID'];
        
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = $this->Languagemodel->deleteLanguage($id);
            $json = json_encode($result);
            echo $json;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }            
    }
    
    public function checkLanguage(){
        $request = $this->input->post('data');
        $lang = $request['lang'];
        $result = $this->Languagemodel->checkExistsLang($lang);
        $json = json_encode($result);
        echo $json;
    }
        
    public function deleteMultiLang(){
        $request = $this->input->post('data');
        $countList = count($request);
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            for($i = 0; $i < $countList; $i++){
                $this->Languagemodel->deleteLanguage($request[$i]);
            }
            echo json_encode(true);
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }
    }

    public function switchLanguage($lang){
        $checkLang = $this->phpsession->getCookie('monpham_language');       
        if($checkLang === null){            
            $this->phpsession->saveCookie($lang, 'monpham_language');             
        }
        else{   
            if($lang !== $checkLang){                        
                $this->phpsession->clearCookie('monpham_language');
                $this->phpsession->saveCookie($lang, 'monpham_language');
            }
        }
    }
}

?>