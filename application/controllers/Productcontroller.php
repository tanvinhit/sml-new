<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productcontroller extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        //$this->load->library('session');
        $this->load->library('phpsession');        
        $this->load->model('Productmodel');
    }
         
    public function loadAllProducts($lang = null){
        $request = $this->input->post('data');
        $type = $request['type'];        
        $result = $this->Productmodel->getAllProducts($type, $lang);        
        $json = json_encode($result);            
               
        echo $json;
    }
     
    public function loadProducts($lang = null){
        $request = $this->input->post('data');
        $type = $request['type'];        
        $result = $this->Productmodel->getProducts($type, $lang);        
        $json = json_encode($result);             
               
        echo $json;
    }
    
    public function updateProduct(){
        $request = $this->input->post('data');

        //$session = $this->session->has_userdata('remember_me');        
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){    
            $request['ProMeta'] = $request['ProMeta'].'.html';
            $request['SeoCanonica'] = $request['SeoCanonica'].'.html';        
            $result = $this->Productmodel->updateProduct($request);
            $json = json_encode($result);
            echo $json;    
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }                
    }
            
    public function addProduct(){
        $request = $this->input->post('data');                        

        //$session = $this->session->has_userdata('remember_me');        
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $request['ProMeta'] = $request['ProMeta'].'.html';
            $request['SeoCanonica'] = $request['SeoCanonica'].'.html';
            $result = $this->Productmodel->addProduct($request);
            $json = json_encode($result);
            echo $json;    
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }
    }
    
    public function deleteProduct(){
        $request = $this->input->post('data');
        $Id = $request['id'];
        
        //$session = $this->session->has_userdata('remember_me');
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = $this->Productmodel->deleteProduct($Id);
            $json = json_encode($result);
            echo $json;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }            
    }        
       
    public function editProduct($artId){
        $result = $this->Productmodel->editProduct($artId);
        $json = json_encode($result);
        echo $json;
    }
    
    public function viewPro(){
        $request = $this->input->post('data');
        $meta = $request['meta'];
        $result = $this->Productmodel->getPro($meta);
        $json = json_encode($result);
        echo $json;
    }        
       
    public function delMultiProducts(){
        $request = $this->input->post('data');
        $countList = count($request);
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            for($i = 0; $i < $countList; $i++){
                $this->Productmodel->deleteProduct($request[$i]);
            }
            echo json_encode(true);
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }
    }
}

?>