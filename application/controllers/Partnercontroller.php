<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partnercontroller extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');        
        $this->load->library(array('phpsession', 'vigenere'));
        $this->load->model('Partnermodel');
    }
    
    public function getAll(){
        $result = $this->Partnermodel->getAll();
        $json = '';        
        $json = json_encode($result);   
        echo $json;
    }
    
    public function updatePartner(){
        $request = $this->input->post('data');
            
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = array();
            $result = $this->Partnermodel->update($request); 
            if($result)
                echo true;
            else echo false;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }                
    }

    public function insertPartner(){
        $data = $this->input->post('data');
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $query = $this->Partnermodel->insert($data);
            if($query)
                echo true;
            else echo false;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }  
    }

    public function deletePartner(){
        $request = $this->input->post('data');
        $id = intval($request['id']);

        $query = $this->Partnermodel->delete($id);
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            if($query)
                echo true;
            else echo false;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        } 
    }
}

?>