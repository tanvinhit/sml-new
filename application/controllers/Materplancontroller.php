<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Materplancontroller extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');        
        $this->load->library(array('phpsession', 'vigenere'));
        $this->load->model('Materplanmodel');
    }
    
    public function getAll(){
        $result = $this->Materplanmodel->getAll();
        $json = '';        
        $json = json_encode($result);   
        echo $json;
    }

    public function getClient(){
        $result = $this->Materplanmodel->getClient();
        $json = '';        
        $json = json_encode($result);   
        echo $json;
    }
    
    public function updateMasterPlan(){
        $request = $this->input->post('data');
            
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = array();
            $result = $this->Materplanmodel->updateMasterPlan($request); 
            $json = json_encode($result);
            echo $json; 
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }                
    }
    
    public function insertMasterPlan(){
        $request = $this->input->post('data'); 
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = $this->Materplanmodel->insertMasterPlan($request);          
            $json = json_encode($result);
            echo $json;    
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }
    }
    
    public function deleteMasterPlan(){
        $request = $this->input->post('data');
        $id = $request['id'];     
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = $this->Materplanmodel->deleteMasterPlan($id);
            $json = json_encode($result);
            echo $json;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }            
    }
}

?>