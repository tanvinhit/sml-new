<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Benefitcontroller extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');        
        $this->load->library(array('phpsession', 'vigenere'));
        $this->load->model('Benefitmodel');
    }
    
    public function getAll(){
        $result = $this->Benefitmodel->getAll();
        $json = '';        
        $json = json_encode($result);   
        echo $json;
    }
    
    public function updateBenefit(){
        $request = $this->input->post('data');
            
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = array();
            $result = $this->Benefitmodel->update($request); 
            if($result)
                echo true;
            else echo false;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }                
    }

    public function insertBenefit(){
        $data = $this->input->post('data');
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $query = $this->Benefitmodel->insert($data);
            if($query)
                echo true;
            else echo false;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }  
    }

    public function deleteBenefit(){
        $request = $this->input->post('data');
        $id = intval($request['id']);

        $query = $this->Benefitmodel->delete($id);
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            if($query)
                echo true;
            else echo false;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        } 
    }
}

?>