<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Printcontroller extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library(array('phpsession', 'vigenere'));
        $this->load->model(array('Usermodel', 'Billmodel', 'Packagemodel', 'Shipmentmodel','Assignmentmodel'));
    }
    
    public function printBill($id){
        $check_session = $this->phpsession->get(null, 'monpham_user');
        if($check_session !== null){
            $email = $check_session['Email'];
            $pwd = $this->vigenere->decryptString($check_session['Password']);
            $isLogin = $this->Usermodel->checkSessionUser($email, $pwd);
            if($isLogin){
                $bill = $this->Billmodel->getPrintById($id);
                $this->load->view('../../assets/admin/email_templates/billing.html', $bill);
            }
            else{
                $this->phpsession->clear(null, 'monpham_user');
                $this->load->view('admin/login.html');     
            }
        }
        else{
            $this->load->view('admin/login.html'); 
        }
    }
    
    public function printPackage($id){
        $check_session = $this->phpsession->get(null, 'monpham_user');
        if($check_session !== null){
            $email = $check_session['Email'];
            $pwd = $this->vigenere->decryptString($check_session['Password']);
            $isLogin = $this->Usermodel->checkSessionUser($email, $pwd);
            if($isLogin){
                $package = $this->Packagemodel->getById($id);
                $packageDetails = $this->Packagemodel->getDetailById($id);
                $package->packageDetails = $packageDetails;
                $this->load->view('../../assets/admin/email_templates/package.html', $package);                
            }
            else{
                $this->phpsession->clear(null, 'monpham_user');
                $this->load->view('admin/login.html');     
            }
        }
        else{
            $this->load->view('admin/login.html'); 
        }
    }

    public function printShipment($id){
        $check_session = $this->phpsession->get(null, 'monpham_user');
        if($check_session !== null){
            $email = $check_session['Email'];
            $pwd = $this->vigenere->decryptString($check_session['Password']);
            $isLogin = $this->Usermodel->checkSessionUser($email, $pwd);
            if($isLogin){
                $shipment = $this->Shipmentmodel->getById($id);
                $shipmentDetails = $this->Shipmentmodel->getDetailById($id);
                $shipment->shipmentDetails = $shipmentDetails;
                $this->load->view('../../assets/admin/email_templates/shipment.html', $shipment);                 
            }
            else{
                $this->phpsession->clear(null, 'monpham_user');
                $this->load->view('admin/login.html');     
            }
        }
        else{
            $this->load->view('admin/login.html'); 
        }
    }

    public function printPostman($id){
        $check_session = $this->phpsession->get(null, 'monpham_user');
        if($check_session !== null){
            $email = $check_session['Email'];
            $pwd = $this->vigenere->decryptString($check_session['Password']);
            $isLogin = $this->Usermodel->checkSessionUser($email, $pwd);
            if($isLogin){
                $bill = $this->Assignmentmodel->getPrintById($id);
                print_r(count($bill));
                $this->load->view('../../assets/admin/email_templates/postman.html', $bill);         
            }
            else{
                $this->phpsession->clear(null, 'monpham_user');
                $this->load->view('admin/login.html');     
            }
        }
        else{
            $this->load->view('admin/login.html'); 
        }
    }
    
}

?>