<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Assignmentcontroller extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        //$this->load->library('session');
        $this->load->library('phpsession');
        $this->load->model('Assignmentmodel');
    }
    
    public function addAssignment(){
        $request = $this->input->post('data');                       
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session != null){
            $result = $this->Assignmentmodel->addAssignment($request);
            $json = json_encode($result);
            echo $json;    
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }
    }

    public function updateAssignment(){
        $request = $this->input->post('data');                       
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session != null){ 
            for($i = 0; $i < $request['COUNT_LENGTH']; $i++){
                $result = $this->Assignmentmodel->updateAssign(intval($request['LIST_ID'][$i]),$request['UserID'],$request['prov_name']);
            }
            echo json_encode(true);
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }
    }

    public function approveAssignment(){
        $request = $this->input->post('data');                       
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session != null){ 
            $result = $this->Assignmentmodel->approveAssign($request['data']['billCustomId'],$request['data']['user_id'],$request['data']['assignment_Id']);
            echo json_encode(true);
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }
    }

    public function deliveryAssignment(){
        $request = $this->input->post('data');                       
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session != null){ 
            $result = $this->Assignmentmodel->deliveryAssign($request['data']['billCustomId'],intval($request['data']['user_id']),$request['data']['assignment_Id'],intval($request['data']['Status']),$request['data']['image'], $request['data']['receiveIsName'],$request['data']['Comment']);
            echo json_encode(true);
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }
    }

    public function getById(){
        $request = $this->input->post('data');  
        $query = $this->Assignmentmodel->getById($request['billId']);
        if($query)
            echo json_encode($query);
        else echo false;
    }

    public function getByIdofAssign(){
        $request = $this->input->post('data');  
        $query = $this->Assignmentmodel->getByIdofAssign($request['billCustomId']);
        if($query)
            echo json_encode($query);
        else echo false;
    }

    public function getByUserId(){
        $request = $this->input->post('data');  
        $query = $this->Assignmentmodel->getByUserId($request['user_id'],$request['assignDate']);
        if($query)
            echo json_encode($query);
        else echo false;
    }   

    public function sendApproveByUserId(){
        $request = $this->input->post('data');  
        $query = $this->Assignmentmodel->sendApproveByUserId($request['user_id'],$request['assignDate'],$request['assignment_Id'],$request['billCustomId']);
        if($query)
            echo json_encode($query);
        else echo false;
    }

    public function deleteAssign($id){
        $query = $this->Assignmentmodel->deleteAssign($id);
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            if($query)
                echo true;
            else echo false;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        } 
    }
}

?>