<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Mapcontroller extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Mapmodel');
        $this->load->library('phpsession');
    }
    
    public function getMapList(){
        $result = $this->Mapmodel->getMapList();
        $json = json_encode($result);
        echo $json;
    }
    
    public function updateMarker(){
        $request = $this->input->post('data');
                
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = $this->Mapmodel->updateMarker($request);
            $json = json_encode($result);
            echo $json;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }        
    }
    
    public function addNewMarker(){
        $request = $this->input->post('data');        
        
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = $this->Mapmodel->addNewMarker($request);
            $json = json_encode($result);
            echo $json;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }        
    }
    
    public function deleteMarker(){
        $request = $this->input->post('data');
        $id = $request['MapID'];        
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = $this->Mapmodel->deleteMarker($id);
            $json = json_encode($result);
            echo $json;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }        
    }

    public function getProvinces(){
        $result = file_get_contents('assets/app_data/provinces.json');
        echo $result;
    }

    public function getDistrictsByProvinceId($provinceId){
        $result = json_decode(file_get_contents('assets/app_data/districts.json'));
        $array = array();
        foreach($result as $item){
            if($item->dist_prov_id === $provinceId)
                array_push($array, $item);
        }
        echo json_encode($array);
    }

    public function getHubById($provinceId){
        $result = json_decode(file_get_contents('assets/app_data/hub.json'));
        $array = array();
        foreach($result as $item){
            if($item->M_HUB === $provinceId)
                array_push($array, $item);
        }
        echo json_encode($array);
    }

    public function getDistrictsById($districtId){
        //$districtId = strtoupper($districtId);
        //$array = array();
        //$file_path = 'assets/includes/upload/files/diadanh/DIADANH.xlsx';
        //ini_set('memory_limit', '-1');
        //include (__DIR__.'/../libraries/Classes/PHPExcel/IOFactory.php');
        //$inputFileName = $file_path; 
        //$objReader = PHPExcel_IOFactory::createReader('Excel2007');
        //$objReader->setReadDataOnly(true);
        //$objPHPExcel = $objReader->load($inputFileName);
        //$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        //$arrayCount = count($allDataInSheet);  // Here get total count of row in that Excel sheet
        //for($i=2; $i <= $arrayCount; $i++)
        //{          
        //    if($allDataInSheet[$i]["F"] === $districtId)
        //        array_push($array, $allDataInSheet[$i]);
        //}
        //echo json_encode($array);
		
		$result = json_decode(file_get_contents('assets/app_data/diadanh.json'));
        $array = array();
        foreach($result as $item){
            if($item->MAQUANCHAR === $districtId)
                array_push($array, $item);
        }
        echo json_encode($array);
    }

    public function getWardsByDistrictId($districtId){
        $result = json_decode(file_get_contents('assets/app_data/wards.json'));
        $array = array();
        foreach($result as $item){
            if($item->ward_dist_id === $districtId)
                array_push($array, $item);
        }
        echo json_encode($array);
    }
}
?>