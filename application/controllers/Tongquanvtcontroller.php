<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tongquanvtcontroller extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');        
        $this->load->library(array('phpsession', 'vigenere'));
        $this->load->model('Tongquanvtmodel');
    }
    
    public function getAll(){
        $result = $this->Tongquanvtmodel->getAll();
        $json = '';        
        $json = json_encode($result);   
        echo $json;
    }
    
    public function updateTQVT(){
        $request = $this->input->post('data');
            
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = array();
            $result = $this->Tongquanvtmodel->update($request); 
            if($result)
                echo true;
            else echo false;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }                
    }

    public function insertTQVT(){
        $data = $this->input->post('data');
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $query = $this->Tongquanvtmodel->insert($data);
            if($query)
                echo true;
            else echo false;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }  
    }

    public function deleteTQVT(){
        $request = $this->input->post('data');
        $id = intval($request['id']);

        $query = $this->Tongquanvtmodel->delete($id);
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            if($query)
                echo true;
            else echo false;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        } 
    }
}

?>