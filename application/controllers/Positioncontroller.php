<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Positioncontroller extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');        
        $this->load->library(array('phpsession', 'vigenere'));
        $this->load->model('Positionmodel');
    }
    
    public function getAll(){
        $result = $this->Positionmodel->getAll();
        $json = '';        
        $json = json_encode($result);   
        echo $json;
    }

    public function getClient(){
        $result = $this->Materplanmodel->getClient();
        $json = '';        
        $json = json_encode($result);   
        echo $json;
    }
    
    public function updatePosition(){
        $request = $this->input->post('data');
            
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = array();
            $result = $this->Positionmodel->update($request); 
            $json = json_encode($result);
            echo $json; 
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }                
    }
}

?>