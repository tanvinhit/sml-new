<?php
class Searchcontroller extends CI_Controller{
    
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('phpsession');
        $this->load->model(array('Searchmodel', 'Templatemodel'));
    }
    
    private function getCurrentLanguage(){
        $checkLang = $this->phpsession->getCookie('monpham_language');
        if($checkLang === null){
            $language = 'vi';
            $this->phpsession->saveCookie($language, 'monpham_language');            
        }
        $checkLang = $this->phpsession->getCookie('monpham_language');        
        return $checkLang;
    }

    public function index(){        
        $keyword = $this->input->get('keyword');
        $filename = "search";
        $currentLanguage = $this->getCurrentLanguage();
        $meta = $this->Templatemodel->getMetaByFileName($filename, $currentLanguage);  
        if(isset($meta)){
            $meta = substr($meta, 0, strlen($meta) - strlen('.html'));        
            return redirect($meta . "?keyword=" . $keyword);
        }
        return redirect('404');
    }

    public function getResult(){
        $data = json_decode(file_get_contents('php://input'),true);
        $result = $this->Searchmodel->getResult($data);
        if($result !== false){
            echo json_encode($result);
        }
        else echo null;
    }        
}
?>