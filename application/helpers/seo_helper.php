<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * SEO Helper
 *
 * Generates Meta tags for search engines optimizations, open graph, twitter, robots
 *
 * @author		Elson Tan (elsodev.com, Twitter: @elsodev)
 * @version     1.0
 */

/**
 * SEO General Meta Tags
 *
 * Generates general meta tags for description, open graph, twitter, robots
 * Using title, description and image link from config file as default
 *
 * @access  public
 * @param   array   enable/disable different meta by setting true/false
 * @param   string  Title
 * @param   string  Description (155 characters)
 * @param   string  Image URL
 * @param   string  Page URL
 */

if(! function_exists('meta_tags')){
    function meta_tags($enable = array('general' => true, 'og'=> true, 'twitter'=> true, 'robot'=> true), $title = '', $desc = '', $imgurl ='', $url = ''){
        $CI =& get_instance();
        $CI->load->model('Templatemodel');

        
        $CI->load->helper('url');
        $page_url=current_url();
        $result = '';
        
        $output = '';
        if($page_url == base_url()){
            
            

            if($url == ''){
                $url = base_url();
            }
            if($page_url == $url)
            {
                $result = $CI->Templatemodel->getTemplateByMeta('trang-chu');
            }
    
            
       
        
        

            //uses default set in seo_config.php
            if($title == ''){
                $title = $CI->config->item('seo_title');
            }
            if($desc == ''){
                $desc = $CI->config->item('seo_desc');
            }
            if($imgurl == ''){
                $imgurl = "https://muinesummerland.vn/assets/includes/upload/files/Panorama_Pool.png";
            }
            
            $output .= '<title>' .$result->SeoTitle. '</title>';

            if($enable['general']){
                $output .= '<meta name="description" content="'.$result->SeoDescribes.'" />';
            }
            if($enable['robot']){
                $output .= '<meta name="robots" content="index,follow"/>';

            } else {
                $output .= '<meta name="robots" content="noindex,nofollow"/>';
            }

            //open graph
            if($enable['og']){
                $output .= '<meta property="og:title" content="'.$result->SeoTitle.'"/>'
                    .'<meta property="og:type" content="'.$result->SeoDescribes.'"/>'
                    .'<meta property="og:image" content="'.$imgurl.'"/>'
                    .'<meta property="og:url" content="'.$url.'"/>';
            }

            //twitter card
            if($enable['twitter']){
                $output .= '<meta name="twitter:card" content="summary"/>'
                    .'<meta name="twitter:title" content="'.$result->SeoTitle.'"/>'
                    .'<meta name="twitter:url" content="'.$url.'"/>'
                    .'<meta name="twitter:description" content="'.$result->SeoDescribes.'"/>'
                    .'<meta name="twitter:image" content="'.$imgurl.'"/>';
            }
            echo $output;
        }else{
            
            

            // echo $output;
            
            if($page_url == base_url() .'biet-thu.html'){
                $result = $CI->Templatemodel->getTemplateByMeta('biet-thu');

                if($title == ''){
                    $title = $CI->config->item('seo_title');
                }
                if($desc == ''){
                    $desc = $CI->config->item('seo_desc');
                }
                if($imgurl == ''){
                    $imgurl = "https://muinesummerland.vn/assets/includes/upload/files/Panorama_Pool.png";
                }
                
                $output .= '<title>' .$result->SeoTitle. '</title>';
    
                if($enable['general']){
                    $output .= '<meta name="description" content="'.$result->SeoDescribes.'" />';
                }
                if($enable['robot']){
                    $output .= '<meta name="robots" content="index,follow"/>';
    
                } else {
                    $output .= '<meta name="robots" content="noindex,nofollow"/>';
                }
    
                //open graph
                if($enable['og']){
                    $output .= '<meta property="og:title" content="'.$result->SeoTitle.'"/>'
                        .'<meta property="og:type" content="'.$result->SeoDescribes.'"/>'
                        .'<meta property="og:image" content="'.$imgurl.'"/>'
                        .'<meta property="og:url" content="'.$url.'"/>';
                }
    
                //twitter card
                if($enable['twitter']){
                    $output .= '<meta name="twitter:card" content="summary"/>'
                        .'<meta name="twitter:title" content="'.$result->SeoTitle.'"/>'
                        .'<meta name="twitter:url" content="'.$url.'"/>'
                        .'<meta name="twitter:description" content="'.$result->SeoDescribes.'"/>'
                        .'<meta name="twitter:image" content="'.$imgurl.'"/>';
                }
                echo $output;
            }

            if($page_url == base_url() .'can-ho.html'){
                $result = $CI->Templatemodel->getTemplateByMeta('can-ho'); 

                if($title == ''){
                    $title = $CI->config->item('seo_title');
                }
                if($desc == ''){
                    $desc = $CI->config->item('seo_desc');
                }
                if($imgurl == ''){
                    $imgurl = "https://muinesummerland.vn/assets/includes/upload/files/Panorama_Pool.png";
                }
                
                $output .= '<title>' .$result->SeoTitle. '</title>';
    
                if($enable['general']){
                    $output .= '<meta name="description" content="'.$result->SeoDescribes.'" />';
                }
                if($enable['robot']){
                    $output .= '<meta name="robots" content="index,follow"/>';
    
                } else {
                    $output .= '<meta name="robots" content="noindex,nofollow"/>';
                }
    
                //open graph
                if($enable['og']){
                    $output .= '<meta property="og:title" content="'.$result->SeoTitle.'"/>'
                        .'<meta property="og:type" content="'.$result->SeoDescribes.'"/>'
                        .'<meta property="og:image" content="'.$imgurl.'"/>'
                        .'<meta property="og:url" content="'.$url.'"/>';
                }
    
                //twitter card
                if($enable['twitter']){
                    $output .= '<meta name="twitter:card" content="summary"/>'
                        .'<meta name="twitter:title" content="'.$result->SeoTitle.'"/>'
                        .'<meta name="twitter:url" content="'.$url.'"/>'
                        .'<meta name="twitter:description" content="'.$result->SeoDescribes.'"/>'
                        .'<meta name="twitter:image" content="'.$imgurl.'"/>';
                }
                echo $output;
            }

            if($page_url == base_url() .'tin-tuc.html'){
                $result = $CI->Templatemodel->getTemplateByMeta('tin-tuc'); 

                if($title == ''){
                    $title = $CI->config->item('seo_title');
                }
                if($desc == ''){
                    $desc = $CI->config->item('seo_desc');
                }
                if($imgurl == ''){
                    $imgurl = "https://muinesummerland.vn/assets/includes/upload/files/Panorama_Pool.png";
                }
                
                $output .= '<title>' .$result->SeoTitle. '</title>';
    
                if($enable['general']){
                    $output .= '<meta name="description" content="'.$result->SeoDescribes.'" />';
                }
                if($enable['robot']){
                    $output .= '<meta name="robots" content="index,follow"/>';
    
                } else {
                    $output .= '<meta name="robots" content="noindex,nofollow"/>';
                }
    
                //open graph
                if($enable['og']){
                    $output .= '<meta property="og:title" content="'.$result->SeoTitle.'"/>'
                        .'<meta property="og:type" content="'.$result->SeoDescribes.'"/>'
                        .'<meta property="og:image" content="'.$imgurl.'"/>'
                        .'<meta property="og:url" content="'.$url.'"/>';
                }
    
                //twitter card
                if($enable['twitter']){
                    $output .= '<meta name="twitter:card" content="summary"/>'
                        .'<meta name="twitter:title" content="'.$result->SeoTitle.'"/>'
                        .'<meta name="twitter:url" content="'.$url.'"/>'
                        .'<meta name="twitter:description" content="'.$result->SeoDescribes.'"/>'
                        .'<meta name="twitter:image" content="'.$imgurl.'"/>';
                }
                echo $output;
            }

            if (strpos($page_url, base_url().'tin-tuc/') !== false ) { 
                $length_page_url = strlen($page_url);
                $length_base_url = strlen(base_url().'tin-tuc/');

                $result = $CI->Templatemodel->getArt(substr($page_url, $length_base_url, $length_page_url),'vi'); 

                if($title == ''){
                    $title = $CI->config->item('seo_title');
                }
                if($desc == ''){
                    $desc = $CI->config->item('seo_desc');
                }
                if($imgurl == ''){
                    $imgurl = base_url().$result->Image;
                }
                
                $output .= '<title>' .$result->SeoTitle. '</title>';
    
                if($enable['general']){
                    $output .= '<meta name="description" content="'.$result->SeoDescribes.'" />';
                }
                if($enable['robot']){
                    $output .= '<meta name="robots" content="index,follow"/>';
    
                } else {
                    $output .= '<meta name="robots" content="noindex,nofollow"/>';
                }
    
                //open graph
                if($enable['og']){
                    $output .= '<meta property="og:title" content="'.$result->SeoTitle.'"/>'
                        .'<meta property="og:type" content="'.$result->SeoDescribes.'"/>'
                        .'<meta property="og:image" content="'.$imgurl.'"/>'
                        .'<meta property="og:url" content="'.$url.'"/>';
                }
    
                //twitter card
                if($enable['twitter']){
                    $output .= '<meta name="twitter:card" content="summary"/>'
                        .'<meta name="twitter:title" content="'.$result->SeoTitle.'"/>'
                        .'<meta name="twitter:url" content="'.$url.'"/>'
                        .'<meta name="twitter:description" content="'.$result->SeoDescribes.'"/>'
                        .'<meta name="twitter:image" content="'.$imgurl.'"/>';
                }
                echo $output;
            }else{
                $output .= '<title>Admin</title>';
                $output .= '<meta name="description" content="Admin" />';
                $output .= '<meta name="robots" content="index,follow"/>';
                $output .= '<meta name="robots" content="noindex,nofollow"/>';
            }
            

        }
    }
}